# Falco

## Design

A simple Win32 reverse engineering challenge, compiled for ARM64. The program plays MIDI notes based on keyboard input. The player has to play the correct sequence of notes to get the flag.

## Build

Build with Visual Studio 2022.

## Solution

The flag is XORed with a sequence of MIDI notes. There is a separate lookup table that maps keyboard keys to notes. Pressing the keys "AAAGAAFAADASAAAGAAFAADASAAAGAAFAADASA" will display the flag. This will play the synth riff from Rock Me Amadeus by Falco.
