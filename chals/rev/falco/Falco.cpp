// Falco.cpp : Defines the entry point for the application.
//

#include "Falco.h"
#include "framework.h"

// Global Variables:
HINSTANCE hInst; // current instance

HMIDIOUT g_hMidiOut;

const struct KeyToNote
{
    UCHAR vk;
    UCHAR note;
} g_KeyToNote[] = {
    {'A', 57}, // A
    {'W', 58}, // A#
    {'S', 59}, // B
    {'D', 60}, // C
    {'R', 61}, // C#
    {'F', 62}, // D
    {'T', 63}, // D#
    {'G', 64}, // E
    {'H', 65}, // F
    {'U', 66}, // F#
    {'J', 67}, // G
    {'I', 68}, // G#
    {'K', 69}, // A
    {'O', 70}, // A#
    {'L', 71}, // B
    {';', 72}, // C
};

#define SYNTH_LEAD_SQUARE 81

// MIDI commands
#define CHANGE_INSTRUMENT(channel, instrument) ((0x000000C0 | channel) | (instrument << 8))
#define NOTE_ON(note) (0x7f0090 | (note << 8))
#define NOTE_OFF(note) (0x90 | (note << 8))

// The correct sequence of notes: the synth riff from Falco's Rock Me Armadeus
const UCHAR kSequence[] = {57, 57, 57, 64, 57, 57, 62, 57, 57, 60, 57, 59, 57, 57, 57, 64, 57, 57, 62,
                           57, 57, 60, 57, 59, 57, 57, 57, 64, 57, 57, 62, 57, 57, 60, 57, 59, 57};

// Key position
UCHAR g_Pos = 0;

// Stores the password typed by the user
UCHAR g_Password[sizeof(kSequence) + 1] = {0};

// cybears{rock_me_arm_a_deus} XOR "AAAGAAFAADASAAAGAAFAADASAAAGAAFAADASA"
const UCHAR g_EncodedFlag[] = {0x22, 0x38, 0x23, 0x22, 0x20, 0x33, 0x35, 0x3a, 0x33, 0x2b, 0x22, 0x38, 0x1e, 0x2c,
                               0x24, 0x18, 0x20, 0x33, 0x2b, 0x1e, 0x20, 0x1b, 0x25, 0x36, 0x34, 0x32, 0x3c};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Entrypoint
int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine,
                      _In_ int nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // Open MIDI device
    int rc = midiOutOpen(&g_hMidiOut, 0, 0, 0, CALLBACK_NULL);
    if (rc != MMSYSERR_NOERROR)
    {
        MessageBox(NULL, TEXT("Falco"), TEXT("cannot play midi sounds"), MB_ICONERROR);
        return -1;
    }

    // Set instrument
    char channel = 0;
    midiOutShortMsg(g_hMidiOut, CHANGE_INSTRUMENT(0, SYNTH_LEAD_SQUARE));

    // Register window class
    WNDCLASSEXW wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FALCO));
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = 0;
    wcex.lpszClassName = TEXT("FalcoClass");
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    RegisterClassExW(&wcex);

    hInst = hInstance;

    // Create window
    HWND hWnd = CreateWindowW(TEXT("FalcoClass"), TEXT("Falco"), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 400,
                              300, nullptr, nullptr, hInstance, nullptr);

    if (!hWnd)
    {
        return FALSE;
    }

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, NULL, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    midiOutReset(g_hMidiOut);
    midiOutClose(g_hMidiOut);

    return (int)msg.wParam;
}

// Map a key to a note
unsigned char FindNote(USHORT keycode)
{
    for (int i = 0; i < ARRAYSIZE(g_KeyToNote); i++)
    {
        if (g_KeyToNote[i].vk == keycode)
        {
            return g_KeyToNote[i].note;
        }
    }

    return 0;
}

// Window procedure
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    DWORD command = 0;
    unsigned char note = 0;

    switch (message)
    {
    case WM_PAINT: {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);
        RECT client_rect = {0};
        SIZE text_size = {0};
        TCHAR help_text[] = TEXT("Press a key to continue");
        TCHAR decoded_flag[sizeof(g_EncodedFlag) + 1] = {0};

        // Get window size
        GetClientRect(hWnd, &client_rect);

        // Set font
        HFONT font =
            CreateFontA(0, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, DEFAULT_CHARSET, 0, 0, 0, DEFAULT_PITCH, "Arial");
        HFONT orig = (HFONT)SelectObject(hdc, font);

        // Draw help text
        GetTextExtentPoint(hdc, help_text, ARRAYSIZE(help_text), &text_size);
        TextOut(hdc, ((client_rect.right - client_rect.left) / 2 - (text_size.cx / 2)),
                ((client_rect.bottom - client_rect.top) / 2 - (text_size.cy / 2)), help_text, ARRAYSIZE(help_text));

        // Draw flag if the correct sequence is played
        if (g_Pos >= ARRAYSIZE(kSequence))
        {
            for (int i = 0; i < sizeof(g_EncodedFlag); i++)
            {
                decoded_flag[i] = g_EncodedFlag[i] ^ g_Password[i % (sizeof(g_Password) - 1)];
            }
            TextOut(hdc, 8, 8, decoded_flag, 27);
        }

        SelectObject(hdc, orig);
        DeleteObject(font);

        EndPaint(hWnd, &ps);
    }
    break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    case WM_KEYDOWN:

    {
        // Only start playing the note on the first WM_KEYDOWN message
        if ((HIWORD(lParam) & KF_REPEAT) == 0)
        {
            note = FindNote(wParam);
            if (note != 0)
            {
                if (note == kSequence[g_Pos])
                {
                    g_Password[g_Pos] = (UCHAR)wParam;
                    g_Pos++;
                    InvalidateRect(hWnd, NULL, TRUE);
                }
                else
                {
                    g_Pos = 0;
                }

                // Play the note
                command = NOTE_ON(note);
                midiOutShortMsg(g_hMidiOut, command);
            }
        }
    }
    break;
    case WM_KEYUP:
        // Stop playing the note
        note = FindNote(wParam);
        if (note != 0)
        {
            command = NOTE_OFF(note);
            midiOutShortMsg(g_hMidiOut, command);
        }
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}
