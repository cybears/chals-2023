#include <Windows.h>
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>

enum Command : uint8_t
{
    NoOp = 0,
    SetPos = 1,
    PrintChar = 2,
};

void MoveTo(short x, short y)
{
    COORD pos = {x, y};
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleCursorPosition(hStdout, pos);
}

bool __declspec(noinline) HandleCommand(Command cmd, FILE *flag_file)
{
    bool result = true;
    switch (cmd)
    {
    case Command::NoOp:
        break;
    case Command::SetPos: {
        short x = 0, y = 0;

        fread(&x, sizeof(x), 1, flag_file);
        fread(&y, sizeof(y), 1, flag_file);

        MoveTo(x, y);
        break;
    }
    case Command::PrintChar: {
        char buffer[64];
        char len = 0;

        fread(&len, sizeof(len), 1, flag_file);

        // BUGBUG: Stack overflow
        fread(buffer, sizeof(char), len, flag_file);

        buffer[len] = 0;
        puts(buffer);
        break;
    }

    default:
        assert(cmd);
        result = false;
        break;
    }

    return result;
}

int main(int argc, char *argv[])
{
    // Open flag file
    FILE *flag_file = fopen("flag.txt", "rb");
    if (!flag_file)
    {
        puts("Cannot find flag file");
        return 1;
    }

    // Read commands from flag file
    Command cmd;
    while (fread(&cmd, sizeof(cmd), 1, flag_file) == sizeof(Command))
    {
        if (!HandleCommand(cmd, flag_file))
        {
            break;
        }

        Sleep(50);
    }

    fclose(flag_file);
}
