# Time Travel

## Design

This challenge is centered around WinDbg's Time Travel Debugging feature. The player has to recover a flag from a time travel trace of a program that decodes a flag.

The challenge is designed so that running strings does not reveal the flag. The challenge is also designed so that you need to set multiple breakpoints to get the data required to recover the flag.

## Build

The executable is built using CMake. The `generate.py` script generates the flag data file. This file contains the sequence of instructions that prints the flag.

The challenge is built by running the executable with `ttd.exe` from WinDbg or `tttracer.exe` from recent versions of Windows.

## Solution

* Dump out all of the calls to "puts" by setting a breakpoint eg: `bp ucrtbase!puts ".printf \"print %ma\\n\", @rcx; g"`
* Dump out all of the calls to "SetConsoleCursorPosition" by setting another breakpoint eg: `bp kernel32!SetConsoleCursorPosition ".printf \"move %d\\n\", @rdx; g"`
* Run the trace to collect all of the log messages
* Write a script to piece them together


Sample solution script:
```

# log from WinDbg
lines = """
move 0
print The flag isn't: cybears{no_really_this_isnt_the_flag}
move 11
print :    
move 24
move 59
print t
move 55
print e
move 49
print _
move 25
print e
move 48
print k)
""" # ... etc.

buffer = bytearray(b' ' * 120)
for line in lines:
  if line.startswith("move"):
    pos = int(line[5:])
  elif line.startswith("print"):
    buffer[pos:pos+len(line) - 6] = line[6:].encode()

print(buffer.decode().rstrip())
```