import pathlib
import random
import struct
import abc
import enum
import sys
import itertools
from typing import Iterable

SEED = 31337


class CommandId(enum.IntEnum):
    NoOp = 0
    SetPos = 1
    PrintChar = 2


class Command(abc.ABC):
    def serialize(self) -> bytes:
        raise NotImplementedError


class Noop(Command):
    def serialize(self) -> bytes:
        return struct.pack("<B", CommandId.NoOp)


class Move(Command):
    x: int
    y: int

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def serialize(self) -> bytes:
        return struct.pack("<BHH", CommandId.SetPos, self.x, self.y)


class PrintChars(Command):
    chars: str

    def __init__(self, chars: str):
        self.chars = chars

    def serialize(self) -> bytes:
        encoded = self.chars.encode("utf-8")
        assert len(encoded) < 256
        return struct.pack("<BB", CommandId.PrintChar, len(encoded)) + encoded


def split_string_into_prints(message: str, chunk_size: int = 2) -> Iterable[Command]:
    for pos in range(0, len(message), 2):
        yield PrintChars(message[pos : pos + 2])


def main():
    if len(sys.argv) < 2:
        print(f"usage: {sys.argv[0]} flag-file")
        return

    flag_file_path = pathlib.Path(sys.argv[1]).absolute()

    random.seed(SEED)

    flag = "were_you_expecting_a_back_to_the_future_quote?"

    commands = [
        Noop(),
        Move(0, 0),
    ]

    # Print the fake flag
    commands.append(PrintChars("The flag isn't: cybears{no_really_this_isnt_the_flag}"))

    # Rewind and remove the "n't"
    commands.append(Move(11, 0))
    commands.append(PrintChars(":   "))

    commands.append(Move(24, 0))

    # Build random set of commands to print the key
    print_key_commands = []
    for i in range(0, len(flag)):
        print_key_commands.append((Move(24 + i, 0), PrintChars(flag[i])))

    random.shuffle(print_key_commands)

    commands.extend(itertools.chain(*print_key_commands))

    # Add the final brace
    commands.extend((Move(24 + len(flag), 0), PrintChars("}")))

    # Add the last command which crashes the process
    commands.append(PrintChars(" " * 64))

    print(f"{len(commands)} commands")
    with open(flag_file_path, "wb") as flag_file:
        for command in commands:
            flag_file.write(command.serialize())

    print(f"Generated flag file: {flag_file_path}")


if __name__ == "__main__":
    main()
