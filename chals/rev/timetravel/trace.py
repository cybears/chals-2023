import subprocess
import sys
import pathlib
import tempfile
import shutil
import os


def main():
    if len(sys.argv) < 5:
        print(
            f"usage: {sys.argv[0]} path-to-timetravel.exe path-to-flag.bin path-to-trace-dir challenge-name"
        )

    exe_path = pathlib.Path(sys.argv[1]).absolute()
    flag_path = pathlib.Path(sys.argv[2]).absolute()
    trace_path = pathlib.Path(sys.argv[3]).absolute()
    challenge_name = sys.argv[4]

    print(f"Challenge binary: {exe_path}")
    print(f"Flag file: {exe_path}")
    print(f"Trace path: {trace_path}")
    print(f"Challenge name: {challenge_name}")

    # Create a temp directory
    with tempfile.TemporaryDirectory() as temp_dir:
        temp_path = pathlib.Path(temp_dir).absolute()
        print(f"Temp directory: {temp_path}")

        # Copy files to temp dir
        temp_exe_path = temp_path / f"{challenge_name}.exe"
        shutil.copy(exe_path, temp_exe_path)
        shutil.copy(flag_path, temp_path / "flag.txt")

        print("Environment: ")
        env = dict()

        # Strip out any CI variables
        redacted_keys = (
            "CI_",
            "CBCI_",
            "RUNNER_",
            "GITLAB",
            "FF_",
            "IMAGE_",
            "SECRET",
        )
        for k in os.environ.keys():
            redact = False
            for redact_key in redacted_keys:
                if k.upper().startswith(redact_key):
                    print(f"Stripping {k}")
                    redact = True
                    break

            if not redact:
                env[k] = os.environ[k]
                print(f"{k}={env[k]}")

        # Clear out existing traces
        existing_traces = trace_path.glob(f"{challenge_name}*.run")
        for existing_trace in existing_traces:
            print(f"Removing existing trace {existing_trace}")
            os.unlink(existing_trace)

        # Find TTD recorder
        ttd_path = subprocess.check_output(args=["where", "ttd.exe"]).decode().rstrip()
        print(f"TTD: {ttd_path}")

        # run challenge with TTD
        args = [ttd_path, "-out", str(trace_path), str(temp_exe_path)]

        print(f"executing: {' '.join(args)}")
        subprocess.check_output(args, cwd=temp_dir, env=env)
        traces = list(trace_path.glob("*.run"))
        if len(traces) != 1:
            print(traces)
            raise Exception("didn't find the trace?")

        # Compress the trace file
        trace_file = pathlib.Path(traces[0]).absolute()
        compressed_trace_file = (trace_path / f"{challenge_name}.7z").absolute()
        args = [
            "7z",
            "a",
            str(compressed_trace_file),
            str(trace_file),
        ]
        print(f"Compressing: {' '.join(args)}")
        subprocess.check_call(args)

        # Delete the original trace file
        os.unlink(trace_file)


if __name__ == "__main__":
    main()
