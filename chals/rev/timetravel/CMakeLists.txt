cmake_minimum_required(VERSION 3.20)

project(TimeTravel)

option(BUILD_TTD_TRACE OFF "Generate the time travel trace after compiling")
set(CHALLENGE_NAME "TimeAfterTime" CACHE INTERNAL "Challenge name")

add_executable(
    TimeTravel
    timetravel.cpp
)

target_compile_definitions(
    TimeTravel
    PRIVATE
    _CRT_SECURE_NO_WARNINGS
)

# Disable incremental linking and override the default PDB path
target_link_options(
    TimeTravel
    PRIVATE
    $<$<NOT:$<CONFIG:Debug>>:/INCREMENTAL:NO>
    $<$<NOT:$<CONFIG:Debug>>:/PDBALTPATH:${CHALLENGE_NAME}.pdb>
)

# Set debugging options
set_property(
    TARGET
    TimeTravel
    PROPERTY VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
)

set_property(
    DIRECTORY
    "${CMAKE_CURRENT_SOURCE_DIR}"
    PROPERTY VS_STARTUP_PROJECT TimeTravel
)

set(FLAG_FILE ${CMAKE_CURRENT_BINARY_DIR}/flag.txt)

# Run script to generate flag file
find_package(Python3 COMPONENTS Interpreter)
add_custom_command(
    TARGET TimeTravel
    POST_BUILD
    COMMAND ${Python3_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/generate.py ${FLAG_FILE}
)

if (BUILD_TTD_TRACE)
    add_custom_command(
        TARGET TimeTravel
        POST_BUILD
        COMMAND ${Python3_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/trace.py $<TARGET_FILE:TimeTravel> ${FLAG_FILE} ${CMAKE_CURRENT_BINARY_DIR} ${CHALLENGE_NAME}
    )
endif()