// XplatCTFView.h : interface of the CXplatCTFView class
//
/////////////////////////////////////////////////////////////////////////////

class CXplatCTFView : public CView
{
protected: // create from serialization only
	CXplatCTFView();
	DECLARE_DYNCREATE(CXplatCTFView)

// Attributes
public:
	CXplatCTFDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXplatCTFView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CXplatCTFView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CXplatCTFView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in XplatCTFView.cpp
inline CXplatCTFDoc* CXplatCTFView::GetDocument()
   { return (CXplatCTFDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////
