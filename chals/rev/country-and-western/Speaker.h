#ifndef SPEAKER_H
#define SPEAKER_H

class Speaker
{

public:
	virtual void Speak(const char * message);

};


#endif // SPEAKER_H