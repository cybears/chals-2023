@echo off

rmdir /s /q Debug
rmdir /s /q Release
rmdir /s /q MacDbg
rmdir /s /q MacRel

set OLDPATH=%PATH%
set OLDLIB=%LIB%
set OLDINCLUDE=%INCLUDE%

call c:\msdev\bin\VCVARS32.BAT x86
echo Windows Debug
nmake /f XplatCTF.mak CFG="XplatCTF - Win32 Debug"
echo Windows Release
nmake /f XplatCTF.mak CFG="XplatCTF - Win32 Release"

set PATH=%OLDPATH%
set LIB=%OLDLIB%
set INCLUDE=%OLDINCLUDE%

call c:\msdev\bin\VCVARS32.BAT m68k
echo Macintosh Debug
nmake /f XplatCTF.mak CFG="XplatCTF - Macintosh Debug"
echo Macintosh Release
nmake /f XplatCTF.mak CFG="XplatCTF - Macintosh Release"



