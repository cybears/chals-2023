# Microsoft Developer Studio Generated NMAKE File, Format Version 40001
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101
# TARGTYPE "Macintosh Application" 0x0301

!IF "$(CFG)" == ""
CFG=XplatCTF - Win32 Debug
!MESSAGE No configuration specified.  Defaulting to XplatCTF - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "XplatCTF - Macintosh Release" && "$(CFG)" !=\
 "XplatCTF - Macintosh Debug" && "$(CFG)" != "XplatCTF - Win32 Release" &&\
 "$(CFG)" != "XplatCTF - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "XplatCTF.mak" CFG="XplatCTF - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "XplatCTF - Macintosh Release" (based on "Macintosh Application")
!MESSAGE "XplatCTF - Macintosh Debug" (based on "Macintosh Application")
!MESSAGE "XplatCTF - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "XplatCTF - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "XplatCTF - Win32 Debug"

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "MacRel"
# PROP BASE Intermediate_Dir "MacRel"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "MacRel"
# PROP Intermediate_Dir "MacRel"
# PROP Target_Dir ""
OUTDIR=.\MacRel
INTDIR=.\MacRel

ALL : "$(OUTDIR)\XplatCTF.exe"

CLEAN : 
	-@erase ".\MacRel\XplatCTF.exe"
	-@erase ".\MacRel\MainFrm.obj"
	-@erase ".\MacRel\XplatCTF.pch"
	-@erase ".\MacRel\StdAfx.obj"
	-@erase ".\MacRel\XplatCTFDoc.obj"
	-@erase ".\MacRel\util.obj"
	-@erase ".\MacRel\CorrectDialog.obj"
	-@erase ".\MacRel\XplatCTF.obj"
	-@erase ".\MacRel\Speaker.obj"
	-@erase ".\MacRel\XplatCTFView.obj"
	-@erase ".\MacRel\XplatCTFMac.rsc"
	-@erase ".\MacRel\XplatCTF.rsc"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

MTL=mktyplib.exe
# ADD BASE MTL /nologo /D "NDEBUG" /mac
# ADD MTL /nologo /D "NDEBUG" /mac
MTL_PROJ=/nologo /D "NDEBUG" /mac 
CPP=cl.exe
# ADD BASE CPP /nologo /AL /Q68s /W3 /GX /O2 /D "_WINDOWS" /D "_MAC" /D "_68K_" /D "WIN32" /D "NDEBUG" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /AL /Q68s /W3 /GX /O2 /D "_WINDOWS" /D "_MAC" /D "_68K_" /D "WIN32" /D "NDEBUG" /D "_MBCS" /Yu"stdafx.h" /c
CPP_PROJ=/nologo /AL /Q68s /W3 /GX /O2 /D "_WINDOWS" /D "_MAC" /D "_68K_" /D\
 "WIN32" /D "NDEBUG" /D "_MBCS" /Fp"$(INTDIR)/XplatCTF.pch" /Yu"stdafx.h"\
 /Fo"$(INTDIR)/" /c 
CPP_OBJS=.\MacRel/
CPP_SBRS=

.c{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.c{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

RSC=rc.exe
# ADD BASE RSC /l 0xc09 /r /d "_MAC" /d "_68K_" /d "NDEBUG"
# ADD RSC /l 0xc09 /r /d "_MAC" /d "_68K_" /d "NDEBUG"
RSC_PROJ=/l 0xc09 /r /m /fo"$(INTDIR)/XplatCTF.rsc" /d "_MAC" /d "_68K_" /d\
 "NDEBUG" 
MRC=mrc.exe
# ADD BASE MRC /D "_68K_" /D "_MAC" /D "NDEBUG" /NOLOGO
# ADD MRC /D "_68K_" /D "_MAC" /D "NDEBUG" /NOLOGO
MRC_PROJ=/D "_68K_" /D "_MAC" /D "NDEBUG" /l 0xc09 /NOLOGO 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/XplatCTF.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 /nologo /MAC:bundle /machine:M68K
# ADD LINK32 /nologo /MAC:bundle /MAC:creator="XPLA" /machine:M68K
LINK32_FLAGS=/nologo /MAC:bundle /MAC:type="APPL" /MAC:creator="XPLA"\
 /pdb:"$(OUTDIR)/XplatCTF.pdb" /machine:M68K /out:"$(OUTDIR)/XplatCTF.exe" 
LINK32_OBJS= \
	"$(INTDIR)/MainFrm.obj" \
	"$(INTDIR)/StdAfx.obj" \
	"$(INTDIR)/XplatCTFDoc.obj" \
	"$(INTDIR)/util.obj" \
	"$(INTDIR)/CorrectDialog.obj" \
	"$(INTDIR)/XplatCTF.obj" \
	"$(INTDIR)/Speaker.obj" \
	"$(INTDIR)/XplatCTFView.obj" \
	"$(INTDIR)/XplatCTFMac.rsc" \
	"$(INTDIR)/XplatCTF.rsc"

"$(OUTDIR)\XplatCTF.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

MFILE32=mfile.exe
# ADD BASE MFILE32 COPY /NOLOGO
# ADD MFILE32 COPY /NOLOGO
MFILE32_FLAGS=COPY /NOLOGO 
MFILE32_FILES= \
	"$(OUTDIR)/XplatCTF.exe"

"$(OUTDIR)\XplatCTF.trg" : "$(OUTDIR)" $(MFILE32_FILES)
    $(MFILE32) $(MFILE32_FLAGS) .\MacRel\XplatCTF.exe\
 "$(MFILE32_DEST):XplatCTF.exe"

DOWNLOAD : "$(OUTDIR)" $(MFILE32_FILES)
    $(MFILE32) $(MFILE32_FLAGS) .\MacRel\XplatCTF.exe\
 "$(MFILE32_DEST):XplatCTF.exe"

!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "MacDbg"
# PROP BASE Intermediate_Dir "MacDbg"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "MacDbg"
# PROP Intermediate_Dir "MacDbg"
# PROP Target_Dir ""
OUTDIR=.\MacDbg
INTDIR=.\MacDbg

ALL : "$(OUTDIR)\XplatCTF.exe"

CLEAN : 
	-@erase ".\MacDbg\vc40.pdb"
	-@erase ".\MacDbg\XplatCTF.pch"
	-@erase ".\MacDbg\XplatCTF.pdb"
	-@erase ".\MacDbg\MainFrm.obj"
	-@erase ".\MacDbg\XplatCTF.obj"
	-@erase ".\MacDbg\Speaker.obj"
	-@erase ".\MacDbg\StdAfx.obj"
	-@erase ".\MacDbg\XplatCTFDoc.obj"
	-@erase ".\MacDbg\XplatCTFView.obj"
	-@erase ".\MacDbg\CorrectDialog.obj"
	-@erase ".\MacDbg\util.obj"
	-@erase ".\MacDbg\XplatCTF.rsc"
	-@erase ".\MacDbg\XplatCTFMac.rsc"
	-@erase ".\MacDbg\XplatCTF.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

MTL=mktyplib.exe
# ADD BASE MTL /nologo /D "_DEBUG" /mac
# ADD MTL /nologo /D "_DEBUG" /mac
MTL_PROJ=/nologo /D "_DEBUG" /mac 
CPP=cl.exe
# ADD BASE CPP /nologo /AL /Q68s /Q68m /W3 /GX /Zi /Od /D "_WINDOWS" /D "_MAC" /D "_68K_" /D "WIN32" /D "_DEBUG" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /AL /Q68s /Q68m /W3 /GX /Zi /Od /D "_WINDOWS" /D "_MAC" /D "_68K_" /D "WIN32" /D "_DEBUG" /D "_MBCS" /Yu"stdafx.h" /c
CPP_PROJ=/nologo /AL /Q68s /Q68m /W3 /GX /Zi /Od /D "_WINDOWS" /D "_MAC" /D\
 "_68K_" /D "WIN32" /D "_DEBUG" /D "_MBCS" /Fp"$(INTDIR)/XplatCTF.pch"\
 /Yu"stdafx.h" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\MacDbg/
CPP_SBRS=

.c{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.c{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

RSC=rc.exe
# ADD BASE RSC /l 0xc09 /r /d "_MAC" /d "_68K_" /d "_DEBUG"
# ADD RSC /l 0xc09 /r /d "_MAC" /d "_68K_" /d "_DEBUG"
RSC_PROJ=/l 0xc09 /r /m /fo"$(INTDIR)/XplatCTF.rsc" /d "_MAC" /d "_68K_" /d\
 "_DEBUG" 
MRC=mrc.exe
# ADD BASE MRC /D "_68K_" /D "_MAC" /D "_DEBUG" /NOLOGO
# ADD MRC /D "_68K_" /D "_MAC" /D "_DEBUG" /NOLOGO
MRC_PROJ=/D "_68K_" /D "_MAC" /D "_DEBUG" /l 0xc09 /NOLOGO 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/XplatCTF.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 /nologo /MAC:bundle /debug /machine:M68K
# ADD LINK32 /nologo /MAC:bundle /MAC:creator="XPLA" /debug /machine:M68K
LINK32_FLAGS=/nologo /MAC:bundle /MAC:type="APPL" /MAC:creator="XPLA"\
 /pdb:"$(OUTDIR)/XplatCTF.pdb" /debug /machine:M68K\
 /out:"$(OUTDIR)/XplatCTF.exe" 
LINK32_OBJS= \
	"$(INTDIR)/MainFrm.obj" \
	"$(INTDIR)/XplatCTF.obj" \
	"$(INTDIR)/Speaker.obj" \
	"$(INTDIR)/StdAfx.obj" \
	"$(INTDIR)/XplatCTFDoc.obj" \
	"$(INTDIR)/XplatCTFView.obj" \
	"$(INTDIR)/CorrectDialog.obj" \
	"$(INTDIR)/util.obj" \
	"$(INTDIR)/XplatCTF.rsc" \
	"$(INTDIR)/XplatCTFMac.rsc"

"$(OUTDIR)\XplatCTF.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

MFILE32=mfile.exe
# ADD BASE MFILE32 COPY /NOLOGO
# ADD MFILE32 COPY /NOLOGO
MFILE32_FLAGS=COPY /NOLOGO 
MFILE32_FILES= \
	"$(OUTDIR)/XplatCTF.exe"

"$(OUTDIR)\XplatCTF.trg" : "$(OUTDIR)" $(MFILE32_FILES)
    $(MFILE32) $(MFILE32_FLAGS) .\MacDbg\XplatCTF.exe\
 "$(MFILE32_DEST):XplatCTF.exe"

DOWNLOAD : "$(OUTDIR)" $(MFILE32_FILES)
    $(MFILE32) $(MFILE32_FLAGS) .\MacDbg\XplatCTF.exe\
 "$(MFILE32_DEST):XplatCTF.exe"

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
OUTDIR=.\Release
INTDIR=.\Release

ALL : "$(OUTDIR)\XplatCTF.exe" "$(OUTDIR)\XplatCTF.bsc"

CLEAN : 
	-@erase ".\Release\XplatCTF.bsc"
	-@erase ".\Release\util.sbr"
	-@erase ".\Release\XplatCTF.pch"
	-@erase ".\Release\Speaker.sbr"
	-@erase ".\Release\XplatCTF.sbr"
	-@erase ".\Release\XplatCTFView.sbr"
	-@erase ".\Release\CorrectDialog.sbr"
	-@erase ".\Release\StdAfx.sbr"
	-@erase ".\Release\MainFrm.sbr"
	-@erase ".\Release\XplatCTFDoc.sbr"
	-@erase ".\Release\XplatCTF.exe"
	-@erase ".\Release\util.obj"
	-@erase ".\Release\Speaker.obj"
	-@erase ".\Release\XplatCTF.obj"
	-@erase ".\Release\XplatCTFView.obj"
	-@erase ".\Release\CorrectDialog.obj"
	-@erase ".\Release\StdAfx.obj"
	-@erase ".\Release\MainFrm.obj"
	-@erase ".\Release\XplatCTFDoc.obj"
	-@erase ".\Release\XplatCTF.res"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /c
CPP_PROJ=/nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D\
 "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)/" /Fp"$(INTDIR)/XplatCTF.pch" /Yu"stdafx.h"\
 /Fo"$(INTDIR)/" /c 
CPP_OBJS=.\Release/
CPP_SBRS=.\Release/

.c{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.c{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

MTL=mktyplib.exe
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /win32
MTL_PROJ=/nologo /D "NDEBUG" /win32 
RSC=rc.exe
# ADD BASE RSC /l 0xc09 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0xc09 /d "NDEBUG" /d "_AFXDLL"
RSC_PROJ=/l 0xc09 /fo"$(INTDIR)/XplatCTF.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/XplatCTF.bsc" 
BSC32_SBRS= \
	"$(INTDIR)/util.sbr" \
	"$(INTDIR)/Speaker.sbr" \
	"$(INTDIR)/XplatCTF.sbr" \
	"$(INTDIR)/XplatCTFView.sbr" \
	"$(INTDIR)/CorrectDialog.sbr" \
	"$(INTDIR)/StdAfx.sbr" \
	"$(INTDIR)/MainFrm.sbr" \
	"$(INTDIR)/XplatCTFDoc.sbr"

"$(OUTDIR)\XplatCTF.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386
LINK32_FLAGS=/nologo /subsystem:windows /incremental:no\
 /pdb:"$(OUTDIR)/XplatCTF.pdb" /machine:I386 /out:"$(OUTDIR)/XplatCTF.exe" 
LINK32_OBJS= \
	"$(INTDIR)/util.obj" \
	"$(INTDIR)/Speaker.obj" \
	"$(INTDIR)/XplatCTF.obj" \
	"$(INTDIR)/XplatCTFView.obj" \
	"$(INTDIR)/CorrectDialog.obj" \
	"$(INTDIR)/StdAfx.obj" \
	"$(INTDIR)/MainFrm.obj" \
	"$(INTDIR)/XplatCTFDoc.obj" \
	"$(INTDIR)/XplatCTF.res"

"$(OUTDIR)\XplatCTF.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "$(OUTDIR)\XplatCTF.exe" "$(OUTDIR)\XplatCTF.bsc"

CLEAN : 
	-@erase ".\Debug\vc40.pdb"
	-@erase ".\Debug\XplatCTF.pch"
	-@erase ".\Debug\vc40.idb"
	-@erase ".\Debug\XplatCTF.bsc"
	-@erase ".\Debug\MainFrm.sbr"
	-@erase ".\Debug\XplatCTF.sbr"
	-@erase ".\Debug\StdAfx.sbr"
	-@erase ".\Debug\util.sbr"
	-@erase ".\Debug\Speaker.sbr"
	-@erase ".\Debug\CorrectDialog.sbr"
	-@erase ".\Debug\XplatCTFDoc.sbr"
	-@erase ".\Debug\XplatCTFView.sbr"
	-@erase ".\Debug\XplatCTF.exe"
	-@erase ".\Debug\CorrectDialog.obj"
	-@erase ".\Debug\XplatCTFDoc.obj"
	-@erase ".\Debug\XplatCTFView.obj"
	-@erase ".\Debug\MainFrm.obj"
	-@erase ".\Debug\XplatCTF.obj"
	-@erase ".\Debug\StdAfx.obj"
	-@erase ".\Debug\util.obj"
	-@erase ".\Debug\Speaker.obj"
	-@erase ".\Debug\XplatCTF.res"
	-@erase ".\Debug\XplatCTF.ilk"
	-@erase ".\Debug\XplatCTF.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /c
CPP_PROJ=/nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)/" /Fp"$(INTDIR)/XplatCTF.pch"\
 /Yu"stdafx.h" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.\Debug/

.c{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.c{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

MTL=mktyplib.exe
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /win32
MTL_PROJ=/nologo /D "_DEBUG" /win32 
RSC=rc.exe
# ADD BASE RSC /l 0xc09 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0xc09 /d "_DEBUG" /d "_AFXDLL"
RSC_PROJ=/l 0xc09 /fo"$(INTDIR)/XplatCTF.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/XplatCTF.bsc" 
BSC32_SBRS= \
	"$(INTDIR)/MainFrm.sbr" \
	"$(INTDIR)/XplatCTF.sbr" \
	"$(INTDIR)/StdAfx.sbr" \
	"$(INTDIR)/util.sbr" \
	"$(INTDIR)/Speaker.sbr" \
	"$(INTDIR)/CorrectDialog.sbr" \
	"$(INTDIR)/XplatCTFDoc.sbr" \
	"$(INTDIR)/XplatCTFView.sbr"

"$(OUTDIR)\XplatCTF.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386
LINK32_FLAGS=/nologo /subsystem:windows /incremental:yes\
 /pdb:"$(OUTDIR)/XplatCTF.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)/XplatCTF.exe" 
LINK32_OBJS= \
	"$(INTDIR)/CorrectDialog.obj" \
	"$(INTDIR)/XplatCTFDoc.obj" \
	"$(INTDIR)/XplatCTFView.obj" \
	"$(INTDIR)/MainFrm.obj" \
	"$(INTDIR)/XplatCTF.obj" \
	"$(INTDIR)/StdAfx.obj" \
	"$(INTDIR)/util.obj" \
	"$(INTDIR)/Speaker.obj" \
	"$(INTDIR)/XplatCTF.res"

"$(OUTDIR)\XplatCTF.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

################################################################################
# Begin Target

# Name "XplatCTF - Macintosh Release"
# Name "XplatCTF - Macintosh Debug"
# Name "XplatCTF - Win32 Release"
# Name "XplatCTF - Win32 Debug"

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\ReadMe.txt

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\XplatCTF.cpp

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

DEP_CPP_XPLAT=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\MainFrm.h"\
	".\XplatCTFDoc.h"\
	".\XplatCTFView.h"\
	

"$(INTDIR)\XplatCTF.obj" : $(SOURCE) $(DEP_CPP_XPLAT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

DEP_CPP_XPLAT=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\MainFrm.h"\
	".\XplatCTFDoc.h"\
	".\XplatCTFView.h"\
	

"$(INTDIR)\XplatCTF.obj" : $(SOURCE) $(DEP_CPP_XPLAT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

DEP_CPP_XPLAT=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\MainFrm.h"\
	".\XplatCTFDoc.h"\
	".\XplatCTFView.h"\
	

"$(INTDIR)\XplatCTF.obj" : $(SOURCE) $(DEP_CPP_XPLAT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\XplatCTF.sbr" : $(SOURCE) $(DEP_CPP_XPLAT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

DEP_CPP_XPLAT=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\MainFrm.h"\
	".\XplatCTFDoc.h"\
	".\XplatCTFView.h"\
	

"$(INTDIR)\XplatCTF.obj" : $(SOURCE) $(DEP_CPP_XPLAT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\XplatCTF.sbr" : $(SOURCE) $(DEP_CPP_XPLAT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\StdAfx.cpp

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

DEP_CPP_STDAF=\
	".\StdAfx.h"\
	
# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /AL /Q68s /W3 /GX /O2 /D "_WINDOWS" /D "_MAC" /D "_68K_" /D\
 "WIN32" /D "NDEBUG" /D "_MBCS" /Fp"$(INTDIR)/XplatCTF.pch" /Yc"stdafx.h"\
 /Fo"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\XplatCTF.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

DEP_CPP_STDAF=\
	".\StdAfx.h"\
	
# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /AL /Q68s /Q68m /W3 /GX /Zi /Od /D "_WINDOWS" /D "_MAC" /D\
 "_68K_" /D "WIN32" /D "_DEBUG" /D "_MBCS" /Fp"$(INTDIR)/XplatCTF.pch"\
 /Yc"stdafx.h" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\XplatCTF.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

DEP_CPP_STDAF=\
	".\StdAfx.h"\
	
# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D\
 "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)/" /Fp"$(INTDIR)/XplatCTF.pch" /Yc"stdafx.h"\
 /Fo"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\StdAfx.sbr" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\XplatCTF.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

DEP_CPP_STDAF=\
	".\StdAfx.h"\
	
# ADD CPP /Yc"stdafx.h"

BuildCmds= \
	$(CPP) /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)/" /Fp"$(INTDIR)/XplatCTF.pch"\
 /Yc"stdafx.h" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\StdAfx.obj" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\StdAfx.sbr" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\XplatCTF.pch" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\MainFrm.cpp

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

DEP_CPP_MAINF=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\MainFrm.h"\
	".\XplatCTFDoc.h"\
	".\Speaker.h"\
	".\CorrectDialog.h"\
	

"$(INTDIR)\MainFrm.obj" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

DEP_CPP_MAINF=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\MainFrm.h"\
	".\XplatCTFDoc.h"\
	".\Speaker.h"\
	".\CorrectDialog.h"\
	

"$(INTDIR)\MainFrm.obj" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

DEP_CPP_MAINF=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\MainFrm.h"\
	".\XplatCTFDoc.h"\
	".\Speaker.h"\
	".\CorrectDialog.h"\
	

"$(INTDIR)\MainFrm.obj" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\MainFrm.sbr" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

DEP_CPP_MAINF=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\MainFrm.h"\
	".\XplatCTFDoc.h"\
	".\Speaker.h"\
	".\CorrectDialog.h"\
	

"$(INTDIR)\MainFrm.obj" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\MainFrm.sbr" : $(SOURCE) $(DEP_CPP_MAINF) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\XplatCTFDoc.cpp

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

DEP_CPP_XPLATC=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\XplatCTFDoc.h"\
	

"$(INTDIR)\XplatCTFDoc.obj" : $(SOURCE) $(DEP_CPP_XPLATC) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

DEP_CPP_XPLATC=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\XplatCTFDoc.h"\
	
NODEP_CPP_XPLATC=\
	".\0"\
	

"$(INTDIR)\XplatCTFDoc.obj" : $(SOURCE) $(DEP_CPP_XPLATC) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

DEP_CPP_XPLATC=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\XplatCTFDoc.h"\
	

"$(INTDIR)\XplatCTFDoc.obj" : $(SOURCE) $(DEP_CPP_XPLATC) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\XplatCTFDoc.sbr" : $(SOURCE) $(DEP_CPP_XPLATC) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

DEP_CPP_XPLATC=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\XplatCTFDoc.h"\
	

"$(INTDIR)\XplatCTFDoc.obj" : $(SOURCE) $(DEP_CPP_XPLATC) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\XplatCTFDoc.sbr" : $(SOURCE) $(DEP_CPP_XPLATC) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\XplatCTFView.cpp

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

DEP_CPP_XPLATCT=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\XplatCTFDoc.h"\
	".\XplatCTFView.h"\
	".\util.h"\
	

"$(INTDIR)\XplatCTFView.obj" : $(SOURCE) $(DEP_CPP_XPLATCT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

DEP_CPP_XPLATCT=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\XplatCTFDoc.h"\
	".\XplatCTFView.h"\
	".\util.h"\
	

"$(INTDIR)\XplatCTFView.obj" : $(SOURCE) $(DEP_CPP_XPLATCT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

DEP_CPP_XPLATCT=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\XplatCTFDoc.h"\
	".\XplatCTFView.h"\
	".\util.h"\
	

"$(INTDIR)\XplatCTFView.obj" : $(SOURCE) $(DEP_CPP_XPLATCT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\XplatCTFView.sbr" : $(SOURCE) $(DEP_CPP_XPLATCT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

DEP_CPP_XPLATCT=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\XplatCTFDoc.h"\
	".\XplatCTFView.h"\
	".\util.h"\
	

"$(INTDIR)\XplatCTFView.obj" : $(SOURCE) $(DEP_CPP_XPLATCT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\XplatCTFView.sbr" : $(SOURCE) $(DEP_CPP_XPLATCT) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\XplatCTFMac.r

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"


"$(INTDIR)\XplatCTFMac.rsc" : $(SOURCE) "$(INTDIR)"
   $(MRC) /o"$(INTDIR)/XplatCTFMac.rsc" /D "_68K_" /D "_MAC" /D "NDEBUG" /l\
 0xc09 /NOLOGO $(SOURCE)


!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"


"$(INTDIR)\XplatCTFMac.rsc" : $(SOURCE) "$(INTDIR)"
   $(MRC) /o"$(INTDIR)/XplatCTFMac.rsc" /D "_68K_" /D "_MAC" /D "_DEBUG" /l\
 0xc09 /NOLOGO $(SOURCE)


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\XplatCTF.rc

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

DEP_RSC_XPLATCTF=\
	".\res\XplatCTF.ico"\
	".\res\XplatCTFDoc.ico"\
	".\res\XplatCTF.rc2"\
	

"$(INTDIR)\XplatCTF.rsc" : $(SOURCE) $(DEP_RSC_XPLATCTF) "$(INTDIR)"
   $(RSC) $(RSC_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

DEP_RSC_XPLATCTF=\
	".\res\XplatCTF.ico"\
	".\res\XplatCTFDoc.ico"\
	".\res\XplatCTF.rc2"\
	

"$(INTDIR)\XplatCTF.rsc" : $(SOURCE) $(DEP_RSC_XPLATCTF) "$(INTDIR)"
   $(RSC) $(RSC_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

DEP_RSC_XPLATCTF=\
	".\res\XplatCTF.ico"\
	".\res\XplatCTFDoc.ico"\
	".\res\XplatCTF.rc2"\
	

"$(INTDIR)\XplatCTF.res" : $(SOURCE) $(DEP_RSC_XPLATCTF) "$(INTDIR)"
   $(RSC) /l 0xc09 /fo"$(INTDIR)/XplatCTF.res" /d "NDEBUG" /d "_AFXDLL"\
 $(SOURCE)


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

DEP_RSC_XPLATCTF=\
	".\res\XplatCTF.ico"\
	".\res\XplatCTFDoc.ico"\
	".\res\XplatCTF.rc2"\
	

"$(INTDIR)\XplatCTF.res" : $(SOURCE) $(DEP_RSC_XPLATCTF) "$(INTDIR)"
   $(RSC) /l 0xc09 /fo"$(INTDIR)/XplatCTF.res" /d "_DEBUG" /d "_AFXDLL"\
 $(SOURCE)


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Speaker.h

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Speaker.cpp

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

DEP_CPP_SPEAK=\
	".\Speaker.h"\
	
# SUBTRACT CPP /YX /Yc /Yu

"$(INTDIR)\Speaker.obj" : $(SOURCE) $(DEP_CPP_SPEAK) "$(INTDIR)"
   $(CPP) /nologo /AL /Q68s /W3 /GX /O2 /D "_WINDOWS" /D "_MAC" /D "_68K_" /D\
 "WIN32" /D "NDEBUG" /D "_MBCS" /Fo"$(INTDIR)/" /c $(SOURCE)


!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

DEP_CPP_SPEAK=\
	".\Speaker.h"\
	
# SUBTRACT CPP /YX /Yc /Yu

"$(INTDIR)\Speaker.obj" : $(SOURCE) $(DEP_CPP_SPEAK) "$(INTDIR)"
   $(CPP) /nologo /AL /Q68s /Q68m /W3 /GX /Zi /Od /D "_WINDOWS" /D "_MAC" /D\
 "_68K_" /D "WIN32" /D "_DEBUG" /D "_MBCS" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c\
 $(SOURCE)


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

DEP_CPP_SPEAK=\
	".\Speaker.h"\
	
# SUBTRACT CPP /YX /Yc /Yu

BuildCmds= \
	$(CPP) /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D\
 "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)/" /Fo"$(INTDIR)/" /c $(SOURCE) \
	

"$(INTDIR)\Speaker.obj" : $(SOURCE) $(DEP_CPP_SPEAK) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\Speaker.sbr" : $(SOURCE) $(DEP_CPP_SPEAK) "$(INTDIR)"
   $(BuildCmds)

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

DEP_CPP_SPEAK=\
	".\Speaker.h"\
	
# SUBTRACT CPP /YX /Yc /Yu

BuildCmds= \
	$(CPP) /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS"\
 /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)/" /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c\
 $(SOURCE) \
	

"$(INTDIR)\Speaker.obj" : $(SOURCE) $(DEP_CPP_SPEAK) "$(INTDIR)"
   $(BuildCmds)

"$(INTDIR)\Speaker.sbr" : $(SOURCE) $(DEP_CPP_SPEAK) "$(INTDIR)"
   $(BuildCmds)

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\util.h

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\util.cpp

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

DEP_CPP_UTIL_=\
	".\util.h"\
	

"$(INTDIR)\util.obj" : $(SOURCE) $(DEP_CPP_UTIL_) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

DEP_CPP_UTIL_=\
	".\util.h"\
	

"$(INTDIR)\util.obj" : $(SOURCE) $(DEP_CPP_UTIL_) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

DEP_CPP_UTIL_=\
	".\util.h"\
	

"$(INTDIR)\util.obj" : $(SOURCE) $(DEP_CPP_UTIL_) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\util.sbr" : $(SOURCE) $(DEP_CPP_UTIL_) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

DEP_CPP_UTIL_=\
	".\util.h"\
	

"$(INTDIR)\util.obj" : $(SOURCE) $(DEP_CPP_UTIL_) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\util.sbr" : $(SOURCE) $(DEP_CPP_UTIL_) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\CorrectDialog.cpp

!IF  "$(CFG)" == "XplatCTF - Macintosh Release"

DEP_CPP_CORRE=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\CorrectDialog.h"\
	

"$(INTDIR)\CorrectDialog.obj" : $(SOURCE) $(DEP_CPP_CORRE) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Macintosh Debug"

DEP_CPP_CORRE=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\CorrectDialog.h"\
	

"$(INTDIR)\CorrectDialog.obj" : $(SOURCE) $(DEP_CPP_CORRE) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Release"

DEP_CPP_CORRE=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\CorrectDialog.h"\
	

"$(INTDIR)\CorrectDialog.obj" : $(SOURCE) $(DEP_CPP_CORRE) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\CorrectDialog.sbr" : $(SOURCE) $(DEP_CPP_CORRE) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ELSEIF  "$(CFG)" == "XplatCTF - Win32 Debug"

DEP_CPP_CORRE=\
	".\StdAfx.h"\
	".\XplatCTF.h"\
	".\CorrectDialog.h"\
	

"$(INTDIR)\CorrectDialog.obj" : $(SOURCE) $(DEP_CPP_CORRE) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"

"$(INTDIR)\CorrectDialog.sbr" : $(SOURCE) $(DEP_CPP_CORRE) "$(INTDIR)"\
 "$(INTDIR)\XplatCTF.pch"


!ENDIF 

# End Source File
# End Target
# End Project
################################################################################
