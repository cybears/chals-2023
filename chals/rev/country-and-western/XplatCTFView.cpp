// XplatCTFView.cpp : implementation of the CXplatCTFView class
//

#include "stdafx.h"
#include "XplatCTF.h"

#include "XplatCTFDoc.h"
#include "XplatCTFView.h"
#include "util.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXplatCTFView

IMPLEMENT_DYNCREATE(CXplatCTFView, CView)

BEGIN_MESSAGE_MAP(CXplatCTFView, CView)
	//{{AFX_MSG_MAP(CXplatCTFView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXplatCTFView construction/destruction

CXplatCTFView::CXplatCTFView()
{
	// TODO: add construction code here

}

CXplatCTFView::~CXplatCTFView()
{
}

BOOL CXplatCTFView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}


/////////////////////////////////////////////////////////////////////////////
// CXplatCTFView drawing

void CXplatCTFView::OnDraw(CDC* pDC)
{
	CXplatCTFDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	int i = 0;

	// Generate final crypt key from the four integers
	BYTE key[16];

	pDoc->GetDecryptionKey(key, sizeof(key));


#ifdef _DEBUG
	CString keyStr = "[debug] Decryption Key: ";
	for (i = 0; i < sizeof(key); i++)
	{
		CString hexDigit;
		hexDigit.Format("%02x", key[i]);
		keyStr = keyStr + hexDigit;
	}

	pDC->TextOut(8, 32, keyStr);

#endif


	// Decrypt the flag
#ifdef _MAC
	// "Correct! The flag is cybears{remember_when_macs_were_big_endian?}"
	// encrypted with little endian key
	const BYTE encrypted_flag[] = {0xd2,0x16,0x0b,0xe3,0x0e,0xd5,0xaa,0x5e,0x0a,0x33,0x37,0xda,0x23,0x5c,0x9f,0x5c,0xf6,0x59,0x10,0xe2,0x51,0x96,0xbd,0x06,0x48,0x02,0x3e,0xcd,0x70,0x41,0x81,0x58,0xfc,0x1c,0x14,0xf3,0x0e,0xc4,0x81,0x08,0x42,0x02,0x31,0xe0,0x6e,0x5b,0x90,0x4e,0xce,0x0e,0x1c,0xe3,0x0e,0xe9,0xbc,0x16,0x4d,0x38,0x3a,0xd1,0x67,0x53,0x92,0x53,0xae,0x04};
	const unsigned int expected_crc32 = 0x8a8f2bfa;
#else
	// "Correct! The flag is: cybears{remember_when_mfc_was_the_future?}"
	// encrypted with little endian key
	const BYTE encrypted_flag[] = {
	0xd2,0x16,0x0b,0xe3,0x0e,0xd5,0xaa,0x5e,0x0a,0x33,0x37,0xda,0x23,0x5c,0x9f,0x5c,0xf6,0x59,0x10,0xe2,0x51,0x96,0xbd,0x06,0x48,0x02,0x3e,0xcd,0x70,0x41,0x81,0x58,0xfc,0x1c,0x14,0xf3,0x0e,0xc4,0x81,0x08,0x42,0x02,0x31,0xe0,0x6e,0x5c,0x90,0x62,0xe6,0x18,0x0a,0xce,0x1f,0xde,0xbb,0x20,0x4c,0x12,0x2b,0xca,0x71,0x5f,0xcc,0x40
	};
	const unsigned int expected_crc32 = 0x46899305;
#endif

	BYTE decrypted_flag[sizeof(encrypted_flag) + 1] = {0};

	pDoc->DecryptMessage(encrypted_flag, sizeof(encrypted_flag), (BYTE*) decrypted_flag);

	// Set text color depending on if the CRC32 of the decrypted flag is correct
	unsigned int decrypted_crc32 = crc32b(decrypted_flag);
	if (decrypted_crc32 == expected_crc32)
	{
		pDC->SetTextColor(RGB(0, 224, 0));
	}
	
	pDC->TextOut(8, 8, decrypted_flag);
	
}

/////////////////////////////////////////////////////////////////////////////
// CXplatCTFView diagnostics

#ifdef _DEBUG
void CXplatCTFView::AssertValid() const
{
	CView::AssertValid();
}

void CXplatCTFView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CXplatCTFDoc* CXplatCTFView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CXplatCTFDoc)));
	return (CXplatCTFDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CXplatCTFView message handlers
