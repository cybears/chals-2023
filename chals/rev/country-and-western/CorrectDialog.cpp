// CorrectDialog.cpp : implementation file
//

#include "stdafx.h"
#include "XplatCTF.h"
#include "CorrectDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCorrectDialog dialog


CCorrectDialog::CCorrectDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CCorrectDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCorrectDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCorrectDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCorrectDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCorrectDialog, CDialog)
	//{{AFX_MSG_MAP(CCorrectDialog)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCorrectDialog message handlers
