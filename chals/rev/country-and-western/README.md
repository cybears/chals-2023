# Country and Western

## Design

* The binary is compiled for two systems/architectures: Windows (x86) and Macintosh (68k).
* C++ and MFC are used to obscure control flow to code that checks the key is correct.
* Some strings are lightly obfuscated
* There is an intentional bug in the handling of the key that results in the endianness of the key being flipped on the Macintosh.

## Handout
* The handout is a single ISO file that is a hybrid ISO9660/HFS image
* Modern Mac, Windows and Linux will attempt to mount the ISO9660 file system and there is a single windows PE binary in there (XplatCTF.exe)
* Old Macintosh computers will mount the HFS filesystem and the Macintosh mach file will be there (XplatCTF.exe.bin)
* To get the second file (without an old Macintosh) you can
  * use the free `IsoBuster` software on Windows (doesn't really work under wine but YMMV)
  * Use `hfsutils` on linux: 
     * `sudo apt install hfsutils`
     * `hmount country-and-western.iso`
     * `hls -a`
     * `hcopy XplatCTF.exe .` <- this will extract the mach binary to your local dir
     * `humount`  
* If players have trouble extracting the files from the ISO, a copy of the binaries are in the `bin` directory in the repo. The Macintosh version is packaged as a MacBinary file in order to retain the file's resource fork. The resource fork contains the Macintosh version. The data fork is the PE file, which is unused in the Macintosh version.

### "Crypto"

The keys are:

1. 2440657297 (LE) - 0x91797991 (same on both architectures)
2. 2145302123 (LE) - 0x7fdeb66b
3. 3210700586 (LE) - 0xbf5f672a
4. 1039350275 (LE) - 0x3df33a03

Key (little endian): 917979916bb6de7f2a675fbf033af33d
Key (big endian): 917979917fdeb66bbf5f672a3df33a03

Encoded flag for Windows version:
https://gchq.github.io/CyberChef/#recipe=From_Hex('Auto'/disabled)XOR(%7B'option':'Hex','string':'917979916bb6de7f2a675fbf033af33d'%7D,'Standard',false)To_Hex('0x%20with%20comma',0)&input=Q29ycmVjdCEgVGhlIGZsYWcgaXM6IGN5YmVhcnN7cmVtZW1iZXJfd2hlbl9tZmNfd2FzX3RoZV9mdXR1cmU/fQ

Encoded flag for Macintosh version:
https://gchq.github.io/CyberChef/#recipe=XOR(%7B'option':'Hex','string':'917979916bb6de7f2a675fbf033af33d'%7D,'Standard',false)To_Hex('0x%20with%20comma',0)&input=VGhlIGZsYWcgaXM6IGN5YmVhcnN7cmVtZW1iZXJfd2hlbl9tYWNzX3dlcmVfYmlnX2VuZGlhbj99


## Build

* Install Microsoft Visual C++ 4.0
* Install Microsoft Visual C++ Cross Development Edition for Macintosh
* Run rebuildall.bat. This builds debug/release binaries for Windows and Macintosh.
* Collect both release binaries and package them as an ISO

## Solution

Use the following key file for the Windows version:

```
2440657297
2145302123
3210700586
1039350275
```

Use the following key file for the Macintosh version:

```
2440657297
1807146623
711417791
54195005
```

It should be possible to complete the challenge without performing any reverse engineering of the Macintosh binary, as long as you can run it. The keys have been chosen such that putting the key for the Windows version into the Macintosh version reveals a partial flag. On both platforms the message starts with "Correct! The flag is cybears{". Given the first XOR key is the same on both big and little endian, the characters "Corr" and "g is" will be the same. This should hopefully be enough of a hint to be able to generate the right key file and run the Macintosh version to produce the flag.

Note that IDA and Ghidra cannot load Macintosh programs in their default configuration. At the time of writing there is no simple loader plugin for these binaries. There are some [Macintosh 68k scripts for Ghidra](https://github.com/ubuntor/m68k_mac_reversing_tools) that can parse the resource fork format and extract code segments from it.

To run the Macintosh binary, you will need a Macintosh emulator (or an old Mac). A popular option is [BasiliskII](https://www.emaculation.com/doku.php/basilisk_ii), which is available for Windows, macOS and Linux. BasiliskII requires a ROM and a hard disk image to run. An easier option is [Infinite Mac](https://infinitemac.org/1994/System%207.5), which emulates a Mac in your browser. It supports drag and drop, so you can drag the ISO onto it and it mounts as a CD-ROM.
