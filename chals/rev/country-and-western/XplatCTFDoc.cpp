// XplatCTFDoc.cpp : implementation of the CXplatCTFDoc class
//

#include "stdafx.h"
#include "XplatCTF.h"

#include "XplatCTFDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXplatCTFDoc

IMPLEMENT_DYNCREATE(CXplatCTFDoc, CDocument)

BEGIN_MESSAGE_MAP(CXplatCTFDoc, CDocument)
	//{{AFX_MSG_MAP(CXplatCTFDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXplatCTFDoc construction/destruction

CXplatCTFDoc::CXplatCTFDoc()
{
	for (int i = 0; i < NUMBER_OF_KEYS; i++)
	{
		m_Key[i] = 0;
	}
}

CXplatCTFDoc::~CXplatCTFDoc()
{
}

BOOL CXplatCTFDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	for (int i = 0; i < NUMBER_OF_KEYS; i++)
	{
		m_Key[i] = 0;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CXplatCTFDoc serialization

void CXplatCTFDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CXplatCTFDoc diagnostics

#ifdef _DEBUG
void CXplatCTFDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CXplatCTFDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CXplatCTFDoc commands

BOOL CXplatCTFDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	CStdioFile docFile;

	if (!docFile.Open(lpszPathName, CFile::modeWrite | CFile::modeCreate))
	{
		return FALSE;
	}

	for (int i = 0; i < NUMBER_OF_KEYS; i++)
	{
		CString numberStr;
		numberStr.Format("%d\n", m_Key[i]);
		docFile.WriteString(numberStr);
	}

	return TRUE;
}

BOOL CXplatCTFDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	CStdioFile docFile;
	CString line;
	
	if (!docFile.Open(lpszPathName, CFile::modeRead))
	{
		
		return FALSE;
	}

	for (int i = 0; i < NUMBER_OF_KEYS; i++)
	{
		docFile.ReadString(line);
		m_Key[i] = strtoul(line, NULL, 10);
	}

	
	return TRUE;
}
