#include "Speaker.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#ifdef _MAC
#include <macos/speech.h>
#include <macos/gestalt.h>
#endif

#include <Windows.h>

void Speaker::Speak(const char * message)
{
#ifdef _MAC

	// Check we can actually speak the message
	long hasSpeech = 0;
	OSErr ret = Gestalt(gestaltSpeechAttr, &hasSpeech);

	if (ret == noErr && (((hasSpeech >> gestaltSpeechMgrPresent) & 1) != 0))
	{
		Str255 str;
		if (strlen(message) < (sizeof(str) - 1))
		{
			sprintf((char*) str, "%s", message);
			c2pstr((char*)str);
	
			SpeakString(str);
		}
	}
	else
	{
		char msgbox[512];
		sprintf(msgbox, "%s", message, " (this would be way cooler if you had the Speech Manager installed)");
		MessageBox(0, msgbox, "Cross Platform CTF", MB_ICONINFORMATION);
	}

#else
	MessageBox(0, message, "Cross Platform CTF", MB_ICONINFORMATION);
#endif
}

