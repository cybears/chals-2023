// XplatCTFDoc.h : interface of the CXplatCTFDoc class
//
/////////////////////////////////////////////////////////////////////////////

#define NUMBER_OF_KEYS	4

class CXplatCTFDoc : public CDocument
{
protected: // create from serialization only
	CXplatCTFDoc();
	DECLARE_DYNCREATE(CXplatCTFDoc)

	unsigned int m_Key[NUMBER_OF_KEYS];

// Attributes
public:

	unsigned int Key(int i) { return m_Key[i]; }

	size_t GetDecryptionKeySize()
	{
		return sizeof(unsigned int) * NUMBER_OF_KEYS;
	}

	void GetDecryptionKey(BYTE* out_key, size_t size)
	{
		int i = 0;

		if (size >= GetDecryptionKeySize())
		{
			for (i = 0; i < 4; i++)
			{
				// INTENTIONAL BUG: This key will be different on Mac and Windows due to endianness.
				*(reinterpret_cast<unsigned int*>(out_key+i*4)) = Key(i);
			}
		}
	}

	void DecryptMessage(const BYTE * message, const int message_len, BYTE * output)
	{	
		BYTE key[16];

		GetDecryptionKey(key, sizeof(key));

		// TODO: replace with better crypt?
		int i = 0;
		for (i = 0; i < message_len; i++)
		{
			output[i] = message[i] ^ (key[i % 16]);
		}
	}

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXplatCTFDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CXplatCTFDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CXplatCTFDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
