// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "XplatCTF.h"

#include "MainFrm.h"
#include "XplatCTFDoc.h"
#include "Speaker.h"
#include "CorrectDialog.h"

#include <assert.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// TODO: move to common defines
// Keys
#define KEY1 0x91797991
#define KEY2 0x7fdeb66b
#define KEY3 0xbf5f672a
#define KEY4 0x3df33a03

// Key (little endian): 917979916bb6de7f2a675fbf033af33d
// Key (big endian): 917979917fdeb66bbf5f672a3df33a03


typedef int (WINAPI * MessageBoxAFunc)(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType);

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_COMMAND(ID_KEY_CHECK1, OnKeyCheck1)
	ON_COMMAND(ID_KEY_CHECK2, OnKeyCheck2)
	ON_COMMAND(ID_KEY_CHECK3, OnKeyCheck3)
	ON_COMMAND(ID_KEY_CHECK4, OnKeyCheck4)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFrameWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnKeyCheck1() 
{
	// Level 1: Simple comparison of value

	Speaker speaker;
	BOOL correct = FALSE;

	CXplatCTFDoc * doc = (CXplatCTFDoc*) GetActiveDocument();
	if (doc != NULL)
	{
		if (doc->Key(0) == KEY1)
		{
			correct = TRUE;
		}
	}

	if (correct)
	{
		speaker.Speak("Correct!");
	}
	else
	{
		speaker.Speak("No, that isn't the right key");
	}
}

// Functions from here use obfuscated strings
static void XorString(char * str, char key) {
	for (unsigned int i = 0; i < strlen(str); i++) {
		str[i] = str[i] ^ key;
	}
}

static void SubXorString(char* str, char key, char offset, char* out) {
	unsigned int len = strlen(str);
	for (unsigned int i = 0; i < len; i++) {
		out[i] = (str[i] - offset) ^ key;
	}
	out[len] = 0;
}

void CMainFrame::OnKeyCheck2() 
{
	// Level 2: Simple string obfuscation. Still findable by xref to Speak function.

	char correct_message[] = "Plep#w$gkvvagp%"; // "That's correct!" ^ 0x04
	char incorrect_message[] = "Jkta($plep#w$jkp$gkvvagp*"; // "Nope, that's not correct" ^ 0x04

	
	Speaker speaker;
	BOOL correct = FALSE;

	CXplatCTFDoc * doc = (CXplatCTFDoc*) GetActiveDocument();
	if (doc != NULL)
	{
		if (doc->Key(1) == KEY2)
		{
			correct = TRUE;
		}
	}

	if (correct)
	{
		XorString(correct_message, 0x04);
		speaker.Speak(correct_message);
	}
	else
	{
		XorString(incorrect_message, 0x04);
		speaker.Speak(incorrect_message);
	}

}

void CMainFrame::OnKeyCheck3() 
{
	// Level 3: Different string obfuscation. Removed xref via Speak() function on Win32.

#ifdef _MAC
	// "Key number three is fitter, happier and more correct."
	// default voice on the Macintosh is the same as in that Radiohead song ;)
	unsigned char correct_message[] = {0x9e,0xb4,0xb0,0x79,0xbb,0xa4,0xbc,0xb7,0xb4,0xa7,0x79,0xa5,0xc1,0xa7,0xb4,0xb4,0x79,0xc0,0xa6,0x79,0xb3,0xc0,0xa5,0xa5,0xb4,0xa7,0x7d,0x79,0xc1,0xb8,0xa9,0xa9,0xc0,0xb4,0xa7,0x7d,0x79,0xb8,0xbb,0xb5,0x79,0xbc,0xba,0xa7,0xb4,0x79,0xb6,0xba,0xa7,0xa7,0xb4,0xb6,0xa5,0x7b, 0};
#else
	unsigned char correct_message[] = {0x9d,0xba,0xba,0xbe,0xa6,0x79,0xb2,0xba,0xba,0xb5,0x79,0xa5,0xba,0x79,0xbc,0xb4,0x78, 0};
#endif
	unsigned char incorrect_message[] = {0x9b,0xba,0xa9,0xb4,0x78, 0}; // "Nope!"
	unsigned char user32_string[] = {0xa4,0xa6,0xb4,0xa7,0x66,0x67, 0}; // "user32"
	unsigned char messageboxa_string[] = {0x9c,0xb4,0xa6,0xa6,0xb8,0xb2,0xb4,0x97,0xba,0xb1,0x98, 0}; // "MessageBoxA"
	char message[64];
	char temp[64];

	memset(message, 0, sizeof(message));
	memset(temp, 0, sizeof(temp));

	Speaker speaker;
	BOOL correct = FALSE;

	CXplatCTFDoc * doc = (CXplatCTFDoc*) GetActiveDocument();
	if (doc != NULL)
	{
		if (doc->Key(2) != 0 && (doc->Key(2) ^ doc->Key(1)) == (KEY2 ^ KEY3))
		{
			correct = TRUE;
		}
	}

	if (correct)
	{
		SubXorString((char*) correct_message, 0x17, 0x42, message);
	}
	else
	{
		SubXorString((char*) incorrect_message, 0x17, 0x42, message);
	}

	// Avoid all of the key check functions being easily findable through xrefs to Speaker::Speak().
	// Win32 only
#ifdef _MAC
	speaker.Speak(message);
#else // !_MAC

	// Get handle to User32
	SubXorString((char*) user32_string, 0x17, 0x42, temp);
	HMODULE hUser32 = GetModuleHandleA(temp);
	assert(hUser32 != NULL);

	// Get address of MessageBoxA
	SubXorString((char*) messageboxa_string, 0x17, 0x42, temp);
	MessageBoxAFunc pfnMessageBoxA = (MessageBoxAFunc) GetProcAddress(hUser32, temp);
	assert(pfnMessageBoxA != NULL);

	// Call MessageBoxA()
	pfnMessageBoxA(0, message, "", MB_ICONINFORMATION);

#endif // MAC
}

void CMainFrame::OnKeyCheck4() 
{
	BOOL correct = FALSE;

	CXplatCTFDoc * doc = (CXplatCTFDoc*) GetActiveDocument();
	if (doc != NULL)
	{
		if (doc->Key(3) == (doc->Key(2) - KEY3 + KEY4))
		{
			correct = TRUE;
		}
	}

	if (correct)
	{
		CCorrectDialog dlg;

		dlg.DoModal();
	}
}
