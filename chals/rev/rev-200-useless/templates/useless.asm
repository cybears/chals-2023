section .data
    blob db "{{shuffled_str}}",0

section .text
    global _start

_start:
{% for shuffled_pos, original_pos, chr in byte_list %}
{% if [1, 2]|random == 1 %}
    mov     rbx, {{shuffled_pos - loop.index0}}
    mov     rcx, {{loop.index0}}
    mov     rdi, continue_{{loop.index0}}
    jmp     indirect
continue_{{loop.index0}}:
{% else %}
    movzx   rax, byte [blob+{{shuffled_pos}}]
{% endif %}
{% endfor %}
    call    exit

indirect:
    add     rbx, rcx
    movzx   rax, byte [rbx+blob]
    jmp     rdi

exit:
    mov rax, 60
    xor rdi, rdi
    syscall