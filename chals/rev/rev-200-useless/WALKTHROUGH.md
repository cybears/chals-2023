# rev-200-useless

This challenge compiles hand crafted assembly to unshuffle a shuffled message

## Steps - Official Walkthrough
<details>
<summary>Spoiler warning</summary>

## Solution

1. Open the binary in a disassembler like Ghidra.
2. Notice that there is a global variable called blob that appears to contain base64 encoded data. When you attempt to base64 decode this blob, it's clear that it's not readable text.
3. Reading the disassembly for a bit, you get the feeling that the binary knows how to unshuffle the base64 encoded blob, but it does not save it anywhere.
4. You realise the next correct byte is moved into the RAX register but is never used. Hence the name of the challenge.
5. You now use either GDB with some scripted automation to dump the contents of RAX every time it changes or, like me, you decide to use Unicorn to emulate the binary and use `UC_HOOK_MEM_READ` to build the string out for your self. See [my solution](./solve.py).

### Running solve.py

```
(venv) exploit@sirpwnsalot:~/chals-2023/chals/rev/rev-200-useless$ pip install -r requirements.txt
Requirement already satisfied: bcrypt==4.0.1 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 1)) (4.0.1)
Requirement already satisfied: capstone==5.0.1 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 2)) (5.0.1)
Requirement already satisfied: certifi==2023.7.22 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 3)) (2023.7.22)
Requirement already satisfied: cffi==1.15.1 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 4)) (1.15.1)
Requirement already satisfied: charset-normalizer==3.2.0 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 5)) (3.2.0)
Requirement already satisfied: colored-traceback==0.3.0 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 6)) (0.3.0)
Requirement already satisfied: cryptography==41.0.4 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 7)) (41.0.4)
Requirement already satisfied: idna==3.4 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 8)) (3.4)
Requirement already satisfied: intervaltree==3.1.0 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 9)) (3.1.0)
Requirement already satisfied: Jinja2==3.1.2 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 10)) (3.1.2)
Requirement already satisfied: Mako==1.2.4 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 11)) (1.2.4)
Requirement already satisfied: MarkupSafe==2.1.3 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 12)) (2.1.3)
Requirement already satisfied: packaging==23.1 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 13)) (23.1)
Requirement already satisfied: paramiko==3.3.1 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 14)) (3.3.1)
Requirement already satisfied: plumbum==1.8.2 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 15)) (1.8.2)
Requirement already satisfied: psutil==5.9.5 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 16)) (5.9.5)
Requirement already satisfied: pwn==1.0 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 17)) (1.0)
Requirement already satisfied: pwntools==4.11.0 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 18)) (4.11.0)
Requirement already satisfied: pycparser==2.21 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 19)) (2.21)
Requirement already satisfied: pyelftools==0.30 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 20)) (0.30)
Requirement already satisfied: Pygments==2.16.1 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 21)) (2.16.1)
Requirement already satisfied: PyNaCl==1.5.0 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 22)) (1.5.0)
Requirement already satisfied: pyserial==3.5 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 23)) (3.5)
Requirement already satisfied: PySocks==1.7.1 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 24)) (1.7.1)
Requirement already satisfied: python-dateutil==2.8.2 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 25)) (2.8.2)
Requirement already satisfied: requests==2.31.0 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 26)) (2.31.0)
Requirement already satisfied: ROPGadget==7.4 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 27)) (7.4)
Requirement already satisfied: rpyc==5.3.1 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 28)) (5.3.1)
Requirement already satisfied: six==1.16.0 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 29)) (1.16.0)
Requirement already satisfied: sortedcontainers==2.4.0 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 30)) (2.4.0)
Requirement already satisfied: unicorn==2.0.1.post1 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 31)) (2.0.1.post1)
Requirement already satisfied: urllib3==2.0.5 in ./venv/lib/python3.10/site-packages (from -r requirements.txt (line 32)) (2.0.5)
Requirement already satisfied: pip>=6.0.8 in ./venv/lib/python3.10/site-packages (from pwntools==4.11.0->-r requirements.txt (line 18)) (22.0.2)
(venv) exploit@sirpwnsalot:~/chals-2023/chals/rev/rev-200-useless$ python3 solve.py useless
[*] '/home/exploit/chals-2023/chals/rev/rev-200-useless/useless'
    Arch:     amd64-64-little
    RELRO:    No RELRO
    Stack:    No canary found
    NX:       NX unknown - GNU_STACK missing
    PIE:      No PIE (0x400000)
    Stack:    Executable
    RWX:      Has RWX segments
In the realm of code and keys, a challenge flag unfurls,
A cryptic invitation to test the sharpest minds of the world.
A CTF's emblem, in the digital domain it flies,
A puzzle for the curious, a prize for the wise.

Its colors aren't crimson, nor stripes of red and white,
But a pixelated tapestry, a matrix of bytes in sight.
In binary brilliance, it beckons with a glow,
A hacker's quest, where hidden knowledge may flow.

In the binary breeze, it flutters like a digital dream,
A treasure map of bits and bytes, in the cyber stream.
Each challenge flag, a ciphered tale to be untold,
For those who dare to venture, for the brave and the bold.

The flag is cybears{U53l355_but_5t1ll_g0t_th3_fl@g}.

Within its enigma, there lies a hidden key,
A riddle to be solved, a door to set minds free.
From steganographic secrets to cryptic clues concealed,
The CTF challenge flag, a puzzle's fate revealed.

A maze of algorithms, a labyrinth of code,
In this digital wilderness, brave hunters erode.
Their minds' terrain, with logic's torch in hand,
They journey through the bytes, they map the digital land.

With each byte deciphered, with each byte unveiled,
The challenge flag draws nearer, its mystery unsealed.
And when the final code falls into its place,
A virtual victory, a triumphant embrace.

So, salute the challenge flag in the realm of bytes and keys,
Where hackers and codebreakers seek the answers it frees.
In the world of CTF, where minds are set to sail,
The challenge flag stands tall, a digital holy grail.
FLAG FOUND!
```
</details>
