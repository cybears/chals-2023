import base64
import os
import random
import sys

from jinja2 import Environment, FileSystemLoader


def byte_list_to_str(byte_list):
    return "".join([ c for _, c in byte_list])


if __name__ == "__main__":
    env = Environment(loader=FileSystemLoader("./templates"), trim_blocks=True, lstrip_blocks=True)
    poem_template = env.get_template("poem.txt")
    asm_template = env.get_template("useless.asm")

    flag = "cybears{U53l355_but_5t1ll_g0t_th3_fl@g}"
    if "CHALLENGE_FLAG" in os.environ:
        flag = os.environ["CHALLENGE_FLAG"]

    poem = poem_template.render(flag=flag)
    encoded_poem = base64.b64encode(poem.encode())
    print(f"{encoded_poem=}", file=sys.stderr)
    byte_list = [ (i, chr(byte)) for i, byte in enumerate(encoded_poem) ]
    random.shuffle(byte_list)
    shuffled_str = byte_list_to_str(byte_list)
    byte_list = [ (i, y, char) for i, (y, char) in enumerate(byte_list) ]
    byte_list.sort(key=lambda x: x[1])

    print(f"{shuffled_str=}", file=sys.stderr)

    asm = asm_template.render(
        shuffled_str=shuffled_str,
        byte_list=byte_list
    )
    
    print(asm)
