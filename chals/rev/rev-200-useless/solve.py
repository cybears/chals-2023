import pwn
import sys
import base64

from unicorn import *
from unicorn.x86_const import *


PAGE_SIZE = 0x1000


# This will contain the unshuffled base64 blob
blob = b""

path_to_bin = sys.argv[1]

def hook_code(uc: Uc, type, address, size, value, user_data):
    global blob
    blob += uc.mem_read(address, size)


if __name__ == "__main__":
    # Parse the ELF executable.
    elf = pwn.ELF(path_to_bin)
    # Contains the code in the ELF
    text_section = elf.get_section_by_name(".text")
    # Contains the blob in the ELF
    data_section = elf.get_section_by_name(".data")

    # start_ptr     - Emulation begins here
    # text_map_addr - Code section begins here
    # text_size     - Size of code section
    # text_map_size - Size of code section aligned to PAGE_SIZE
    # text_offset   - Byte offset of code section in the ELF file
    # data_map_addr - Data section begins here
    # data_offset   - Byte offset to data section in the ELF file
    # data_size     - Size of data section
    # data_map_size - Size of data section aligned to PAGE_SIZE
    # end_ptr       - Emulation stops here

    start_ptr = elf.symbols["_start"]
    text_map_addr = text_section.header["sh_addr"]
    text_size = text_section.data_size
    text_map_size = (text_size + PAGE_SIZE - 1) & ~(PAGE_SIZE - 1)
    text_offset = text_section.header["sh_offset"]
    data_map_addr = data_section.header["sh_addr"]
    data_offset = data_section.header["sh_offset"]
    data_size = data_section.data_size
    data_map_size = (data_size + PAGE_SIZE - 1) & ~(PAGE_SIZE - 1)
    end_ptr = elf.symbols["indirect"] - 5

    # Initialize the emulator
    uc = Uc(UC_ARCH_X86, UC_MODE_64)
    # Allocate memory for code and data
    uc.mem_map(text_map_addr, text_map_size)
    uc.mem_map(data_map_addr, data_map_size)

    # Read in code and data into the emulator
    with open(path_to_bin, "rb") as binary:
        binary.seek(text_offset)
        uc.mem_write(text_map_addr, binary.read(text_size))
        binary.seek(data_offset)
        uc.mem_write(data_map_addr, binary.read(data_size))

    # Add emulator hook to analyze each memory read
    uc.hook_add(UC_HOOK_MEM_READ, hook_code)

    # Begin emulation
    uc.emu_start(start_ptr, end_ptr)

    # The blob can be decoded now.
    decoded_blob = base64.b64decode(blob)
    print(decoded_blob.decode())

    if b"cybears" in decoded_blob:
        print("FLAG FOUND!")
        exit(0)
    else:
        print("ERROR, COULDN'T FIND FLAG")
        exit(-1)
