# misc-100-legaleagle

There's nothing especially tricky about this, you just need to pay attention closely to all the clauses.


## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

The initial phrase used inside the flag, "counsel is not required here" suggests that you don't need no stinkin' lawyer, you totally got this!

## Solution

The things you particularly need to watch out for:
* *The lowercase letter t* has been defined to be the character `w`
* When you add up all the negatives and modifications, clause 2.3 requires you to substitute all `e`s for `3` except for the second one in that word.
* The Alabama financial year ends on 30 September, which makes the month of the competition month number 11 (in 0-up counting), which means that you apply the modifications in section 2 in reverse order, which in turn means that you can ignore the crap about Collatz Stopping Time and Turkish keyboards, because at that point the flag contains only letters and braces.

The final version of the phrase used inside the flag has now shifted to "counsel is now required".
Even if you don't need a lawyer, you may need counselling!


## References
* https://en.wikipedia.org/wiki/Fiscal_year#State_governments
</details>


