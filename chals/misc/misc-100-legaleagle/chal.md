## 1 The Flag
1.1 The form of *The Flag* shall be the Competition Prefix followed by the Flag Body followed by the Competition Suffix.<br>
1.2 The Competition Prefix shall be the concatenation of *the lowercase letter c*, *the lowercase letter y*, *the lowercase letter b*, *the lowercase letter e*, *the lowercase letter a*, *the lowercase letter r*, *the lowercase letter s*, and *the open brace symbol*, in accordance with the other challenges in *The Competition*.<br>
1.3 The Competition Suffix shall be *the close brace symbol* in accordance with the other challenges in *The Competition*.<br>
1.4 The Flag Body shall be a phrase consisting of five words, subject to such modifications as described by section 2.<br>
1.4.1 The first word of the phrase shall be the concatenation of *the lowercase letter c*, *the lowercase letter o*, *the lowercase letter u*, *the lowercase letter n*, *the lowercase letter s*, *the lowercase letter e*, and *the lowercase letter l*.<br>
1.4.2 The second word of the phrase shall be the concatenation of *the lowercase letter i* with *the lowercase letter s*.<br>
1.4.3 The third word of the phrase shall be the concatenation of *the lowercase letter n*, *the lowercase letter o*, and *the lowercase letter t*.<br>
1.4.4 The fourth word of the phrase shall be the concatenation of *the lowercase letter r*, *the lowercase letter e*, *the lowercase letter q*, *the lowercase letter u*, *the lowercase letter i*, *the lowercase letter r*, *the lowercase letter e*, and *the lowercase letter d*.<br>
1.4.5 The fifth word of the phrase shall be the concatenation of *the lowercase letter h*, *the lowercase letter e*, *the lowercase letter r*, and *the lowercase letter e*.<br>
1.5 Each word shall be separated from adjacent words by spaces, except for the second word, which shall be separated from the first word by *the hyphen symbol* and from the third word by *the underscore symbol*, and the fourth word, which shall be separated from the third word by *the hyphen symbol*.

## 2 Modifications
2.1 These modifications should be applied ascending order. That is, apply the modification specified in clause 2.2 before applying the modification specified, inter alios, clause 2.3.<br>
2.2 Any instance of *the lowercase letter r* shall be replaced by *the uppercase letter R*.<br>
2.3 Any instance of *the lowercase letter e* shall be replaced by *the numeral 3* unless it is not the second instance in that word of the phrase.<br>
2.4 Any instance of *the lowercase letter o* shall be replaced by *the numeral 0* unless that instance is adjacent to another numeral.<br>
2.5 Any instance of *the lowercase letter s* shall be replaced by *the numeral 5* unless the preceding instance had been replaced by *the numeral 5* in which case that instance shall be replaced by *the dollar symbol*.<br>
2.6 Any instance of *the lowercase letter l* shall be replaced by *the pipe symbol*.<br>
2.7 For each number contained in the flag in turn, alternately apply the following transitions:<br>
2.7.1 Replace the number with the symbol that shares a key with that number in the Turkish F-keyboard layout.<br>
2.7.2 Replace the number with the Collatz Total Stopping Time of that number.

## 3 Special Terms
3.1 To the extent of any inconsistency between these Special Terms and the General Terms (being all the terms that are not Special Terms), these Special Terms override the General Terms.<br>
3.2 The General Terms are amended as follows:<br>
3.2.1 Clause 1.4 the word "five" is replaced by the word "four".<br>
3.2.2 Clause 1.4.5 is deleted.<br>
3.2.3 Clause 2.3 the word "unless" is replaced by the phrase "only if".<br>
3.3 The modifications in Clause 2 should be applied in descending order. That is, apply the modification specified in clause 2.2 after applying the modification specified, inter alios, clause 2.3.<br>
3.4 The provisions of Clause 3.3 only apply if *The Competition* takes place in an odd-numbered month.<br>
3.5 For the purposes of Clause 3.4, months are numbered from 0, starting from the beginning of the Financial Year in the US state of Alabama.

## 4 Definitions
4.1 For the purposes of the General Terms and the Special Terms, the following words shall have the following meanings: <br>
4.1.1 *You* means the person, team, bot or any other such entity involved wholly or partially in solving this challenge.<br>
4.1.2 *We*, *us* means the organisers, jointly and severally, of *The Competition*.<br>
4.1.3 *The Competition* means the CTF event at BSides Canberra 2023.<br>
4.1.4 *The Flag* means the sequence of characters that demonstrate that, and that *you* submit to *us* as proof that, a solution to a particular challenge has been obtained.<br>
4.1.5 *The uppercase letter R* means the character at unicode code point U+0052.<br>
4.1.6 *The lowercase letter a* means the character at unicode code point U+0061.<br>
4.1.7 *The lowercase letter b* means the character at unicode code point U+0062.<br>
4.1.8 *The lowercase letter c* means the character at unicode code point U+0063.<br>
4.1.9 *The lowercase letter d* means the character at unicode code point U+0064.<br>
4.1.10 *The lowercase letter e* means the character at unicode code point U+0065.<br>
4.1.11 *The lowercase letter i* means the character at unicode code point U+0069.<br>
4.1.12 *The lowercase letter l* means the character at unicode code point U+006C.<br>
4.1.13 *The lowercase letter n* means the character at unicode code point U+006E.<br>
4.1.14 *The lowercase letter o* means the character at unicode code point U+006F.<br>
4.1.15 *The lowercase letter q* means the character at unicode code point U+0071.<br>
4.1.16 *The lowercase letter r* means the character at unicode code point U+0072.<br>
4.1.17 *The lowercase letter s* means the character at unicode code point U+0073.<br>
4.1.18 *The lowercase letter t* means the character at unicode code point U+0077.<br>
4.1.19 *The lowercase letter u* means the character at unicode code point U+0075.<br>
4.1.20 *The lowercase letter y* means the character at unicode code point U+0079.<br>
4.1.21 *The dollar symbol* means the character at unicode code point U+0024.<br>
4.1.22 *The hyphen symbol* means the character at unicode code point U+002D.<br>
4.1.23 *The underscore symbol* means the character at unicode code point U+005F.<br>
4.1.24 *The open brace symbol* means the character at unicode code point U+007B.<br>
4.1.25 *The pipe symbol* means the character at unicode code point U+007C.<br>
4.1.26 *The close brace symbol* means the character at unicode code point U+007D.<br>
4.1.27 *The numeral 0* means the character at unicode code point U+0030.<br>
4.1.28 *The numeral 3* means the character at unicode code point U+0033.<br>
4.1.29 *The numeral 5* means the character at unicode code point U+0035.<br>
4.2 All other words shall have their standard meanings as per usual vocabulary, parlance, lexicon, or zeitgeist; except for those words that are defined in the Act.
