# Legal Eagle

* _author_: Cybears:BearArms
* _title_: Legal Eagle
* _points_: 100
* _tags_:  misc

## Attachments
* None. Players need to find the clues for the flag in the fine print for the CTF website.

## Notes
* The style and formatting of the clues can be changed to blend in to whereever they sit

