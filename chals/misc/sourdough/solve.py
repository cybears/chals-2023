#!/usr/bin/env python3
from __future__ import print_function

import argparse
import os
from pwn import context, process, remote, gdb, p32, log
from lib.encode import encode, pack_int8
from lib.instructions import Instructions
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CHALLENGE_BINARY = os.path.join(SCRIPT_DIR, 'server.py')

def do_sploit(proc):

    data = proc.readuntil('instructions:')
    log.debug('%r', data)

    # 0
    ins = encode(Instructions.LDA, 14)
    # 1
    ins += encode(Instructions.SUB, 15)
    # 2
    ins += encode(Instructions.JC, 6)
    # 3
    ins += encode(Instructions.LDA, 14)
    # 4
    ins += encode(Instructions.OUT)
    # 5
    ins += encode(Instructions.HLT)
    # 6
    ins += encode(Instructions.STA, 14)
    # 7
    ins += encode(Instructions.LDA, 13)
    # 8
    ins += encode(Instructions.ADD, 12)
    # 9
    ins += encode(Instructions.STA, 13)
    # 10
    ins += encode(Instructions.JMP, 0)
    # 11
    ins += pack_int8(0)
    # 12
    ins += pack_int8(1)
    # 13: divisor
    ins += pack_int8(0)

    # About to send the answer.
    log.progress(f'Sending  number of instructions: {len(ins)}')
    proc.sendline(str(len(ins)))

    data = proc.readuntil('instructions:')
    log.debug('%r', data)
    log.progress('Sending  answer')
    proc.sendline(ins)

    # The program prints the output,
    output = proc.readline()
    log.debug('%r', output)

    # Then the program prints the flag or error,
    output = proc.readuntil("flag: ").rstrip()
    log.debug('%r', output)
    flag = proc.readline().rstrip()
    log.debug('%r', flag)
    try:
        with open(os.path.join(SCRIPT_DIR, 'flag.txt'), 'rb') as flag_file:
            real_flag = flag_file.read()
            assert flag == real_flag
            log.success(str(flag))
    except IOError:
        if b'cybears{' in flag:
            log.success(str(flag))
        else:
            log.failure(str(flag))

def connect_to_binary(args):
    proc = None
    if args.local:
        # Run the challenge binary locally and get a helper object
        # to communicate with it.
        # see: http://docs.pwntools.com/en/stable/tubes/processes.html
        proc = process(CHALLENGE_BINARY)
    else:
        # Connect to the challenge server on a remote machine
        # see http://docs.pwntools.com/en/stable/tubes/sockets.html?highlight=remote#pwnlib.tubes.remote.remote
        proc = remote(args.hostname, args.port)
    return proc

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l',
                        '--local',
                        action='store_true',
                        default=False,
                        help='Run exploit against the challenge binary running on the local computer')
    parser.add_argument('--hostname',
                        type=str,
                        default="localhost",
                        help='Hostname of server hosting the challenge binary')
    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=2320,
                        help='Port of the server hosting the challenge binary')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        default=False,
                        help='Verbose logging')

    args = parser.parse_args()

    if args.verbose:
        context.log_level = 'debug'
    return args


if __name__ == "__main__":
    a = parse_args()
    p = connect_to_binary(a)
    do_sploit(p)
