from .instructions import Instructions
from struct import unpack

def decode(machine_code: bytes):
    instructions = []
    for b in machine_code:
         instructions.append(decode_byte(b))
    return instructions

def decode_byte(machine_code: int):
    return (Instructions(machine_code >> 4), machine_code & 0xF)