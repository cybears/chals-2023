from enum import IntEnum
Instructions = IntEnum('Instructions', [
    'NOP',
    'LDA',
    'ADD',
    'SUB',
    'STA',
    'LDI',
    'JMP',
    'JC',
    'JZ',
    'RESERVED_1',
    'RESERVED_2',
    'RESERVED_3',
    'RESERVED_4',
    'RESERVED_5',
    'OUT',
    'HLT'], start=0)