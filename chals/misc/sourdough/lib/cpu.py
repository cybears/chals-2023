import logging
from lib.decode import decode_byte, Instructions

class Cpu():
    REGA = 0
    REGB = 0
    CARRY_FLAG = False
    ZERO_FLAG = True
    INSTRUCTION_PTR = 0

    mem = [0]*16

    def _get_byte(self, index: int):
        return int(self.mem[index])

    def __init__(self, initial_memory: bytes):
        self.REGA = 0
        self.REGB = 0
        self.OUT = []
        self.tick_counts = 0
        for idx, val in enumerate(initial_memory):
            self.mem[idx] = val
    
    def _load_instruction(self):
        return decode_byte( self.mem[self.INSTRUCTION_PTR])
    

    def _do_next_instruction(self):
        (instruction, arg) = self._load_instruction()
        logging.debug(f"Next instruction: {(instruction, arg)}")
        halt = False
        self.INSTRUCTION_PTR += 1
        match instruction:
            case Instructions.LDA:
                self.loadA(arg)
            case Instructions.ADD:
                self.add(arg)
            case Instructions.SUB:
                self.sub(arg)
            case Instructions.STA:
                self.storeA(arg)
            case Instructions.LDI:
                self.load_immediate(arg)
            case Instructions.JMP:
                self.INSTRUCTION_PTR = arg
            case Instructions.JZ:
                self.INSTRUCTION_PTR = arg if self.ZERO_FLAG else self.INSTRUCTION_PTR
            case Instructions.JC:
                self.INSTRUCTION_PTR = arg if self.CARRY_FLAG else self.INSTRUCTION_PTR
            case Instructions.OUT:
                self.OUT.append(self.REGA)
            case Instructions.HLT:
                halt = True
            case _:
                pass
        self.tick_counts += 1
        return halt if self.tick_counts < 10000 else True

    def loadAndRun(self, instructions: bytes):
        for idx, val in enumerate(instructions):
            self.mem[idx] = val
        # run through instructions
        while True:
            if self._do_next_instruction():
                break

    def getRegA(self):
        return self.REGA

    def getCarry(self):
        return self.CARRY_FLAG

    def getZero(self):
        return self.ZERO_FLAG
    
    def get_out(self):
        return self.OUT

    def loadA(self, address: int):
        self.REGA = self._get_byte(address)
        self.ZERO_FLAG = (self.REGA == 0)

    def storeA(self, address: int):
        self.mem[address] = self.REGA

    def load_immediate(self, value: int):
        self.REGA = value
        self.ZERO_FLAG = (self.REGA == 0)

    def add(self, address: int):
        # obviously dont need b reg but to pay tribute to the real thing
        self.REGB = self._get_byte(address)
        logging.debug(f'self.REGB: {self.REGB}')
        self.CARRY_FLAG = bool((self.REGA + self.REGB) >> 8)
        logging.debug(f'test: {(self.REGA + self.REGB)}')
        logging.debug(f'Carry flag: {self.CARRY_FLAG}')
        self.REGA = (self.REGA + self.REGB) & 0xFF
        self.ZERO_FLAG = (self.REGA == 0)

    def sub(self, address: int):
        # obviously dont need b reg but to pay tribute to the real thing
        self.REGB = 256 - self._get_byte(address)
        self.CARRY_FLAG = bool((self.REGA + self.REGB) >> 8)
        logging.debug(f'sub: self.REGB: {self.REGB}')
        self.REGA = (self.REGA + self.REGB) & 0xFF
        logging.debug(f'sub: self.REGA: {self.REGA}')
        logging.debug(f'sub:Carry flag: {self.CARRY_FLAG}')
        self.ZERO_FLAG = (self.REGA == 0)
