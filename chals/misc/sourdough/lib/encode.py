from .instructions import Instructions
from struct import pack

def encode(inst: Instructions, arg: int = 0):
    return pack('B', ((inst & 0xF) << 4) | (arg & 0xF))

def pack_int8(arg: int):
    return pack('B', arg & 0xFF)
