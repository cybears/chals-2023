#!/usr/bin/env python3
import socketserver
from lib.cpu import Cpu
from lib.instructions import Instructions
from lib.encode import encode, pack_int8
from lib.decode import decode
import os
import secrets
import sys
import logging
import binascii

FLAG=b""

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
with open(os.path.join(SCRIPT_DIR, 'flag.txt'), 'rb') as flag_file:
    FLAG = flag_file.read()

def sendall(data):
    sys.stdout.buffer.write(data )
    sys.stdout.flush()

def sendline(data):
    sendall(data + b"\n")

def read_int():
    return int(sys.stdin.buffer.readline())

def read_bytes(num: int):
    buf = sys.stdin.buffer.read(num)
    try:
        if binascii.unhexlify(buf):
            buf += sys.stdin.buffer.read(num)
            return binascii.unhexlify(buf)
    except:
        return buf

def run():
    sendline(b"Welcome to my computer. To win and get the flag you will need to send me the instructions to calculate and output the modulus of two numbers. I only have 16 bytes of memory, item 14 (zero index) is set as the number and 15 and the divisor so don't override these please.")
    sendline(b"Provide instructions in ascii hex format i.e.: '0102F0', or as a bytestring (i.e. b'\ x 00 \ x 02 \ x f0' without ths spaces in python) after telling me how many instructions you will send (3 in this example!). I will take this an copy into my memory and execute.")
    sendall(b"Number of instructions: ")
    num_instructions = read_int()

    if num_instructions > 16:
        sendline(b"Did you not believe me when I said I only have 16 bytes of memory?")
        return
        
    sendall(b"Send me your instructions: ")
    ins = read_bytes(num_instructions)
    instructions = ins
    instructions += encode(Instructions.HLT) * (14 - len(instructions))
    
    try:
        # try to run a nubmer of time to confirm its a working program and not just lucky

        sendline(b"Running your program 10 times to confirm it works")
        valid = True
        for i in range(10):
            number = secrets.choice(range(1, 255))
            divisor = secrets.choice(range(1, number))
            # set the number and  divisor if they didn't try and overwrite them
            if len(instructions) == 14:
                run_instructions = instructions + pack_int8(number) + pack_int8(divisor)
            cpu = Cpu("")
            cpu.loadAndRun(run_instructions)
            output = cpu.get_out()
            sendline(f'Output run {i+1}: {output}'.encode())
            if len(output) != 1 or output[0] != number % divisor:
                valid = False
                break
        if valid:
            sendall(b'Congratulations, have a flag: ')
            sendline(FLAG)
        else:
            sendline(b"Not the answer I was looking for.")
        
    except:
        sendline(b'Invalid program')



if __name__ == "__main__":
    run()