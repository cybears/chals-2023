# Sourdough 

The challenge revolves around programming an emulated version of Ben Eater's breadboard computer. 

## Steps - Official Walkthrough

<details>
<summary>Spoiler warning</summary>

### Solution
* Once you know the instruction set the challenge is fairly straight forward. 

Some things that might trip people up

1. Ben changed the opcodes part way through the series. A hint is that a  solution will likely need conditional jumps. The last video in the series is called "Conditional jump instructions" and the instruction set can be seen here: https://youtu.be/Zg1NdPKoosU?list=PLowKtXNTBypGqImE405J2565dvjafglHU&t=1899
1. Only one value should be printed to the "display" (OUT) this needs to be the correct answer to get the flag.


</details>

