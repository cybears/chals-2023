from lib.instructions import Instructions
from lib.encode import encode, pack_int8
from lib.cpu import Cpu
def test_mult():
    # 0
    ins = encode(Instructions.LDA, 14)
    # 1
    ins += encode(Instructions.SUB, 12)
    # 2
    ins += encode(Instructions.JC, 6)
    # 3
    ins += encode(Instructions.LDA, 13)
    # 4
    ins += encode(Instructions.OUT)
    # 5
    ins += encode(Instructions.HLT)
    # 6
    ins += encode(Instructions.STA, 14)
    # 7
    ins += encode(Instructions.LDA, 13)
    # 8
    ins += encode(Instructions.ADD, 15)
    # 9
    ins += encode(Instructions.STA, 13)
    # 10
    ins += encode(Instructions.JMP, 0)
    # 11
    ins += pack_int8(0)
    # 12
    ins += pack_int8(1)
    # 13: product
    ins += pack_int8(0)
    # 14
    ins += pack_int8(7)
    # 15
    ins += pack_int8(4)
    cpu = Cpu("")
    print(ins)
    cpu.loadAndRun(ins)
    assert cpu.getRegA() == 28
    assert cpu.get_out() == [28]

def test_div():
    # 0
    ins = encode(Instructions.LDA, 14)
    # 1
    ins += encode(Instructions.SUB, 15)
    # 2
    ins += encode(Instructions.JC, 6)
    # 3
    ins += encode(Instructions.LDA, 13)
    # 4
    ins += encode(Instructions.OUT)
    # 5
    ins += encode(Instructions.HLT)
    # 6
    ins += encode(Instructions.STA, 14)
    # 7
    ins += encode(Instructions.LDA, 13)
    # 8
    ins += encode(Instructions.ADD, 12)
    # 9
    ins += encode(Instructions.STA, 13)
    # 10
    ins += encode(Instructions.JMP, 0)
    # 11
    ins += pack_int8(0)
    # 12
    ins += pack_int8(1)
    # 13: divisor
    ins += pack_int8(0)
    # 14
    ins += pack_int8(200)
    # 15
    ins += pack_int8(2)
    cpu = Cpu("")
    cpu.loadAndRun(ins)
    #assert cpu.getRegA() == 1
    assert cpu.get_out() == [100]



def test_mod():
    # 0
    ins = encode(Instructions.LDA, 14)
    # 1
    ins += encode(Instructions.SUB, 15)
    # 2
    ins += encode(Instructions.JC, 6)
    # 3
    ins += encode(Instructions.LDA, 14)
    # 4
    ins += encode(Instructions.OUT)
    # 5
    ins += encode(Instructions.HLT)
    # 6
    ins += encode(Instructions.STA, 14)
    # 7
    ins += encode(Instructions.LDA, 13)
    # 8
    ins += encode(Instructions.ADD, 12)
    # 9
    ins += encode(Instructions.STA, 13)
    # 10
    ins += encode(Instructions.JMP, 0)
    # 11
    ins += pack_int8(0)
    # 12
    ins += pack_int8(1)
    # 13: divisor
    ins += pack_int8(0)
    # 14
    ins += pack_int8(218)
    # 15
    ins += pack_int8(20)
    cpu = Cpu("")
    cpu.loadAndRun(ins)
    assert cpu.get_out() == [18]

    #b'\x1e?v\x1e\xe0\xf0N\x1d,M`\x00\x01\x00\x01\x14'