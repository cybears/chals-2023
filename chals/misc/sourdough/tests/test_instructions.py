from lib.decode import decode
from lib.instructions import Instructions
from lib.encode import encode, pack_int8
from lib.cpu import Cpu

def test_decode_lda():
    ins = encode(Instructions.LDA, 2)
    decoded = decode(ins)
    assert decoded[0][0] == Instructions.LDA
    assert decoded[0][1] == 2

def test_lda():
    ins = encode(Instructions.NOP, 0)
    ins += pack_int8(3)
    cpu = Cpu(ins)
    cpu.loadA(1)
    assert cpu.getRegA() == 3

def test_loadAndRun():
    ins = encode(Instructions.LDA, 1)
    ins += pack_int8(3)
    cpu = Cpu(ins)
    ins = encode(Instructions.LDA, 2)
    ins += encode(Instructions.HLT, 1)
    ins += pack_int8(2)
    cpu.loadAndRun(ins)
    assert cpu.getRegA() == 2

def test_add():
    ins = encode(Instructions.NOP, 0)
    ins += pack_int8(4)
    cpu = Cpu(ins)
    cpu.load_immediate(1)
    cpu.add(1)
    assert cpu.getRegA() == 5

def test_add_carry():

    ins = encode(Instructions.NOP, 0)
    ins += pack_int8(200)
    cpu = Cpu(ins)
    cpu.load_immediate(100)
    cpu.add(1)
    assert cpu.getRegA() == 44
    assert cpu.getCarry() == True

def test_add_carry2():
    ins = encode(Instructions.NOP, 0)
    ins += pack_int8(255)
    cpu = Cpu(ins)
    cpu.load_immediate(255)
    cpu.add(1)
    assert cpu.getCarry() == True
    assert cpu.getRegA() == 254

def test_add_carry3():
    ins = encode(Instructions.NOP, 0)
    ins += pack_int8(3)
    cpu = Cpu(ins)
    cpu.load_immediate(255)
    cpu.add(1)
    assert cpu.getCarry() == True
    assert cpu.getRegA() == 2
    cpu.add(1)
    assert cpu.getCarry() == False
    assert cpu.getRegA() == 5

def test_sub():
    ins = encode(Instructions.NOP, 0)
    ins += pack_int8(4)
    cpu = Cpu(ins)
    cpu.load_immediate(6)
    cpu.sub(1)
    assert cpu.getRegA() == 2
    assert cpu.getCarry() == True

def test_sub_zero():
    ins = encode(Instructions.NOP, 0)
    ins += pack_int8(4)
    cpu = Cpu(ins)
    cpu.load_immediate(4)
    cpu.sub(1)
    assert cpu.getRegA() == 0
    assert cpu.getZero() == True

def test_sub_zero_cary():
    ins = encode(Instructions.NOP, 0)
    ins += pack_int8(12)
    cpu = Cpu(ins)
    cpu.load_immediate(4)
    cpu.sub(1)
    assert cpu.getRegA() == 248
    assert cpu.getZero() == False
    assert cpu.getCarry() == False

def test_sub_zero2():
    ins = encode(Instructions.NOP, 0)
    ins += pack_int8(4)
    cpu = Cpu(ins)
    cpu.load_immediate(6)
    cpu.sub(1)
    assert cpu.getRegA() == 2

def test_sta():
    ins = encode(Instructions.NOP, 0)
    ins += pack_int8(4)
    cpu = Cpu(ins)
    cpu.loadA(1)
    cpu.storeA(3)
    assert cpu._get_byte(3) == 4

def test_ldi():
    cpu = Cpu(b'')
    cpu.load_immediate(0x10)
    assert cpu.getRegA() == 0x10

def test_jmp():
    # LDI 2
    # JMP 3
    # LDI 0
    # HLT
    # so A should equal 2 NOT 0
    ins = encode(Instructions.LDI, 2)
    ins += encode(Instructions.JMP, 3)
    ins += encode(Instructions.LDI, 0)
    ins += encode(Instructions.HLT, 0)
    cpu = Cpu(b'')
    cpu.loadAndRun(ins)
    assert cpu.getRegA() == 2
    assert cpu.getZero() == False
    assert cpu.getCarry() == False


def test_jz():
    # LDI 2
    # JZ 3
    # LDI 0
    # HLT
    # so A should equal 0 NOT 2
    ins = encode(Instructions.LDI, 2)
    ins += encode(Instructions.JZ, 3)
    ins += encode(Instructions.LDI, 0)
    ins += encode(Instructions.HLT, 0)
    cpu = Cpu(b"")
    cpu.loadAndRun(ins)
    assert cpu.getRegA() == 0
    assert cpu.getZero() == True
    assert cpu.getCarry() == False



def test_jz_pos():
    # LDI 0
    # JZ 3
    # LDI 2
    # HLT
    # so A should equal 0 NOT 2
    ins = b''
    ins += encode(Instructions.LDI, 0)
    ins += encode(Instructions.JZ, 3)
    ins += encode(Instructions.LDI, 2)
    ins += encode(Instructions.HLT)
    cpu = Cpu("")
    cpu.loadAndRun(ins)
    assert cpu.getRegA() == 0
    assert cpu.getZero() == True
    assert cpu.getCarry() == False

def test_jc():
    ins = b""
    ins += encode(Instructions.LDI, 10)
    ins += encode(Instructions.ADD, 5)
    ins += encode(Instructions.JC, 4)
    ins += encode(Instructions.LDI, 0)
    ins += encode(Instructions.HLT)
    ins += pack_int8(253)
    cpu = Cpu("")
    cpu.loadAndRun(ins)
    assert cpu.getRegA() == 7



def test_out():
    ins = b""
    ins += encode(Instructions.LDI, 10)
    ins += encode(Instructions.OUT)
    ins += encode(Instructions.ADD, 5)
    ins += encode(Instructions.OUT)
    ins += encode(Instructions.HLT)
    ins += pack_int8(50)
    cpu = Cpu("")
    cpu.loadAndRun(ins)
    assert cpu.getRegA() == 60
    assert cpu.get_out() == [10, 60]


def test_bad_jmp():
    ins = b""
    ins += encode(Instructions.JMP, 20)
    cpu = Cpu("")
    cpu.loadAndRun(ins)


def test_inv_loop():
    ins = b""
    ins += encode(Instructions.JMP, 0)
    cpu = Cpu("")
    cpu.loadAndRun(ins)