from lib.instructions import Instructions
from lib.encode import encode, pack_int8
def test_ldi():
    assert encode(Instructions.LDI, 255) == b'_'