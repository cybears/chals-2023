# Sourdough

* _author_: Cybears:1nv@l1d
* _title_: Sourdough
* _points_: 100
* _tags_:  misc

## build nsjail container to serve challenge
* `docker build -t sourdough -f Dockerfile.nsjail .`
* `docker run --privileged  -it --rm -p 2323:2323 sourdough`

## build solver container and test against server
* `docker build -t sourdough-solve -f Dockerfile.test .`
* `docker run --network=host -it --rm sourdough-solve python3 solve.py --hostname 127.0.0.1 --port 2323`
