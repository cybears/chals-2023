# Legal Eagle

* _author_: Cybears:Heef
* _title_: dark|QR|light
* _points_: 100
* _tags_:  misc, beginner

## Attachments
* None. Players will find the challenge on the conference hall banners.

## Notes
* We can drop photos of the banners into the challenge as files for remote players.

