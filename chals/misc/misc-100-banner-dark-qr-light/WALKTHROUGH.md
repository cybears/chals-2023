# dark|QR|light Walkthrough

This challenge is on the printed banners for the CTF.

While the QR codes on each banner look whole, they are in fact split in half on a vertical line, with each half placed on a separate banner (next to the corresponding opposite half from the other QR code).

If you take the two halves of the 'dark' QR code and put them together, you get this message:
`MYE NESE T 03 lg yer{koqcdxlgjmhzts`

And the two halves of the 'light' QR code yield this:
`AHMULAHDCF22!Fa:cbasinwroefasupayv}`

The characters of the flag message alternate between the two decodes messages, to print the full message: 

```py
q1 = 'MYE NESE T 03 lg yer{koqcdxlgjmhzts'
q2 = 'AHMULAHDCF22!Fa:cbasinwroefasupayv}'
flag = "".join(q1[i] + q2[i] for i in range(len(q1)))
print(f"[*] Recovered message: {flag}")
```

Solution image attached: <banner_qr_solutions.png>
