# Porcine Enclosure

* _author_: Cybears:Heef
* _title_: Porcine Enclosure
* _points_: 100
* _tags_:  misc, beginner

## Attachments
* None. The challenge text lives on the "Admin" desk CTF banner at the venue.

## Notes
* Hopefully the print resolution and colours are accurate enough for the text to show up
* We can upload an attachment of the banner as a png (see misc/dark-qr-light) for remote players

