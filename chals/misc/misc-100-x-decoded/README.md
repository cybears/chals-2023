# X-Decoded

* _author_: Cybears:GingerBear
* _title_: X-Decoded
* _points_: 50
* _tags_:  misc

## Challenge Text
During the Bsides Canberra break last year, the cybears found a TypeX machine at the ASD Decoded Exhibition at the National Museum of Australia
https://twitter.com/ASDGovAu/status/1529240255974498309

Looking closely at the configuration the cybears saw this message:
"UVQNC OXMSF XXGIK BOR"

Can you help them deciper what this said? (Flag is all lower-case)

There's a virtual TypeX machine here: https://typex.virtualcolossus.co.uk/Typex/ but maybe you can find another way with the help of ASD's friends at GCHQ.

The cybears saw the ring settings and initial values were as follows:
1. A A
2. S S
3. D D
4. G G
5. G G
(None of the rotors were reversed)

The 1st rotor ring
MCYLPQUVRXGSOAWNBJEZDTFKHI<BFHNKUW

The 2nd rotor ring:
KHWENRCBISXJQGOFMAPVYZDLTU<CFHNQUW

The 3rd rotor ring:
YBPDZMGIKQCUSATREHOJNLFWXV<HFBNQUW

The 4th rotor ring:
AZNJCGDLVIHXOBRPMSWQUKFYET<BFHNQUW

The 5th rotor ring:
BGQXUTOVFCZPJIHSWERYNDAMLK<WUQNBFH

Reflector:
AS DC FG IE KB LU MH OR TN VZ WQ XJ YP


## Handouts
* (none) 

## Notes
* TypeX Decryption Challenge
