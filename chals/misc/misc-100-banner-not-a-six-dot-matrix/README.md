# Not A Six Dot Matrix

* _author_: Cybears:Heef
* _title_: Not A Six Dot Matrix
* _points_: 100
* _tags_:  misc, beginner

## Attachments
* None. The challenge text lives on the "Main Hall" CTF banner at the venue.

## Notes
* Hopefully the print resolution and colours are accurate enough for the text to show up
* We can upload an attachment of the banner as a png (see misc/dark-qr-light) for remote players
* Lots of options for hints for this one: Talking about vision impairment, alternatives to braille, or other indirect references to a writing system.
