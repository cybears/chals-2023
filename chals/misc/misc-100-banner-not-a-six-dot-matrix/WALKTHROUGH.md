# Not A Six Dot Matrix Walkthrough

The message is encoded using the ELIA system -- an alternative to Braille.

The only hint is the name of the challenge, but ELIA is a very little known writing system that did not really take off. It was produced almost a decade ago but hasn't caught on (maybe because they use ™️ too much on their website!).

It is however one of the only braille alternatives, and immediately recognisable if you find it's website. It's in the top page of results if you google "alternative to braille" or similar: http://www.theeliaidea.com/elia-frames2

Once you discover how the message is written, it's a simple monoalphabetic substitution.
