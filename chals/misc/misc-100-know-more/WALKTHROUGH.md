# Do you want to know more? Walkthrough

The BSides CTF sponsor is ASD. The challenge text hints at a job ad. There will also be a QR Code on the CTF room banner that leads to the ASD Jobs website.

The player should locate the job that is most closely aligned with the skills of a CTF player and find the flag within the job ad.

This challenge should be fairly straightforward and was requested by the sponsor. 

Any hints required should refer to finding a job.

