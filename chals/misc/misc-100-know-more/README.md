# Would you like to know more?

* _author_: Cybears:Mr Bearenstain
* _title_: Would you like to know more?
* _points_: 100
* _tags_:  misc, beginner

## Attachments
* None. 

## Notes
* OSINT Challenge
* The sponsor of the CTF this year is ASD. ASD has put up a job ad to coincide with BSides. 
* There should also be a qrcode on the sponsorship banner that leads to the job ad with the flag.
