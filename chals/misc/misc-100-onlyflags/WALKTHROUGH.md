# OnlyFlags

This challenge displays one example of an encrypted MP4 steganographic technique

## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

## Solution

The MP4 format allows for binary inclusions into the file, whilst preserving the original video and audio tracks. While there are some expensive and complex implenmetations of this - one free online example is PepperWatch.

Binary analysis will indicate that the there are additional file atoms, or extra chunks in MDAT atom, although this is a challenging undertaking.

For this challenge - the supplied mp4 file has an AES-256 encrypted blob hidden with the mp4.
The resultant file is also a valid mp4, which for this challenge has the flag played back as a blurred image.

This is a novel implemetation, with additional features shown at https://www.youtube.com/watch?v=4Lw7CA-Fppc

There's also a few wild hints in the metadata, although all point to the url, where the explainer and code can be found:
(https://github.com/PepperWatch/mp4steg/)
Hints are Hex, Charcode, Decimal, Binary, Base32, Base45, Base58, Base62, Base64, Morse and Braille

## Steps to solve: 
1. Running `exiftool onlyfans.mp4` gives a number of hints to pepperwatch github. The challenge description also notes that the password is in the Album Art
2. Running `exiftool -s onlyfans.mp4` shows what TAGs are available. Notably, the "CoverArt" tag is important
3. Running `exiftool -CoverArt -b onlyfans.mp4 > art.jpg` will extract the binary image from the metadata
4. Opening this shows a (hard to read) password of `CYB3@R$`
5. Googling the steg technique (pepperwatch) leads to an example website: https://pepperwatch.com/decode
6. You can upload the onlyfans.mp4 and put in the password (IN LOWERCASE) to recover the embedded movie/image.
7. Note that this flag also needs to be entered in lowercase (even though it looks uppercase)



</details>

