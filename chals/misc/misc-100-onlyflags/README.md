# OnlyFlags

* _author_: Cybears:GingerBear
* _title_: OnlyFlags
* _points_: 100
* _tags_:  misc

## Flags
* 'cybears{onlyf!@g$foundh3r3}'

## Challenge Text
```markdown
        The cybears had a video that only their most exclusive fans were meant to see. It had some very spicy and peppery content!
        Adversaries shared this video to a public channel to try and embarass Cybears, but lucky for them the real MP4 video is
        encrypted inside the MP4 video - but wait.. they left their encryption password on the album cover - oh no!
        
        In case they forgot how to decrypt the file, the cybears also hid some other hints.

