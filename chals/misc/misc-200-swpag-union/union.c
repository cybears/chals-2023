// lua 5.3 has integer division

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#define NUM_WORKERS 99
#define NUM_HATS (NUM_WORKERS+1)
#define MAXFILESIZE 8192

#ifdef DEBUG
	const int trials = 10;
#else
	const int trials = 100;
#endif

const int timeout = 10;

FILE* urand;
lua_State *Worker;
char fp[0x100] = {0};

bool checkBuf(unsigned char *buf, unsigned int len) {
	unsigned int i = 0;
	for (i=0; i<len; i++)
	{
		if( ((buf[i] < 0x20) || (buf[i] > 0x7f)) //disallow non-printable characters, except
				&& (buf[i]!=0x0a) 	 //carriage return
 				&& (buf[i]!=0x0d) 	 //line feed
				&& (buf[i]!=0x09) 	 //tab
				&& (buf[i]!=0x0) 	 //null
				)
		{
			printf("Invalid char: [%c]\n", buf[i]);
			return false;
		}
	}
	return true;
}

bool getfile() {
	puts("send your file");
	//generate random filename to hold player submitted lua script
	unsigned char randchars[17];
	fread(randchars, sizeof(unsigned char), 16, urand);
	for (int i = 0; i < 16; i++) randchars[i] = randchars[i] % 26 + 'a';
	randchars[16] = 0;
	sprintf(fp, "/tmp/%s.lua", randchars);
	FILE* writefile = fopen(fp, "w");
	
	if (!writefile) return false;

	//read in player lua script a line at a time
	//validate that the buffer contains "good" characters 
	//and doesn't get too big (8K should be big enough to solve)
	char *buff = NULL;
	const char *sentinel = "-- ENDFILE";
 	ssize_t nread = 0;	
	size_t len = 0;
	size_t tally = 0;
	while (true) { 
		nread = getline(&buff, &len, stdin);	
		
		if (nread == -1)
		{
			printf("ERROR: EOF or ENOMEM\n");
			free(buff);
			return false;
		}

		if (!checkBuf((unsigned char *) buff, nread))
		{
			printf("ERROR: Invalid character in lua found\n");
			fclose(writefile);
			free(buff);
			return false;
		}

		tally += nread;
		if (tally > MAXFILESIZE)
		{
			printf("ERROR: Max file size is %d\n", MAXFILESIZE);
			fclose(writefile);
			free(buff);
			return false;
		}
		fwrite(buff, 1, nread, writefile);

		if (strstr(buff, sentinel) != NULL)
		{
#ifdef DEBUG
			printf("DEBUG: Sentinel found\n");
#endif	
			free(buff);
			break;
		}
		free(buff); buff = NULL;

	}
	fclose(writefile);

#ifdef DEBUG
	printf("finished getfile()\n");
#endif

	return true;
}



//Do one round of player input
int play_round() {
	/*
	Create a random ordering of hats
	Loop:
		Create Worker W
		Give W previous observations
		Give W future lineup
		Get answer from W
		Check answer is valid
		Close W
	Once everyone is done, count who is correct
	*/
	int score = 0;
	int hat_array[NUM_HATS];
	int free_hats[NUM_HATS];
	int previous_calls[NUM_WORKERS];

	for (int i=0; i<NUM_HATS; i++){
		free_hats[i] = i;
	}
	for (int i=0; i<NUM_WORKERS; i++){
		previous_calls[i] = 0;
	}

	unsigned int rand = 0;
	for (int i = NUM_HATS-1; i > -1; i--){
			fread(&rand, sizeof(unsigned int), 1, urand);
			int c = rand % (i+1);
			hat_array[i] = free_hats[c];
			free_hats[c] = free_hats[i];

	}

	for (int w=0; w < NUM_WORKERS; w++){

		//Make Worker
		Worker = luaL_newstate();
		// Give them table and math stdlibs
		luaL_requiref(Worker, LUA_TABLIBNAME, luaopen_table, 1); lua_pop(Worker, 1);
		luaL_requiref(Worker, LUA_MATHLIBNAME, luaopen_math, 1); lua_pop(Worker, 1);
			
		if (luaL_dofile(Worker, fp)) return 0;


		//Send information to the workers
		//Send past calls, and future lineup
		lua_getglobal(Worker, "Worker");
		lua_createtable(Worker,w,0); //Send previous call
		for (int i=0; i < w;i++)
		{
			lua_pushinteger(Worker, (lua_Integer)previous_calls[i]);
			lua_rawseti(Worker, -2, i+1);
		}
		
		lua_createtable(Worker,NUM_WORKERS-w-1,0); //Send future lineup
		for (int i=w+1;i<NUM_WORKERS;i++)
		{
			lua_pushinteger(Worker, (lua_Integer)hat_array[i+1]);
			lua_rawseti(Worker, -2, i-w);
		}



		lua_call(Worker, 2, 1); //Perform worker turn, 2 inputs 1 output

		//Receive call
		int response = 0;
		response = lua_tointeger(Worker, -1);
		lua_settop(Worker,0); //reset the stack

		
		lua_close(Worker);
		//Uncomment if you want to see the worker behaviour
		//printf("Worker %d has hat %d, called %d\n",w,hat_array[w+1],response);

		//Check validity of call
		if ((response < 0) || response >= NUM_HATS){
				printf("Oh no! Everyone was fired because of an invalid call!\n");
			return 0;
		}
		for (int p = 0; p < w; p++){
			if(previous_calls[p] == response){
				printf("Oh no! Everyone was fired because of a repeated call!\n");
				return 0;
			}
		}

		//Score call
		previous_calls[w] = response;
		if (response == hat_array[w+1]){
			score++;
		}

	}
	return score;
	
}

//run multiple trials 
int trial() {
	printf("Running trials...\n");
	int min_score = NUM_WORKERS;
	for (int i = 0; i < trials; i++) {
		int score = play_round();
		if (score < min_score){
			if (score == 0){
				return 0;
			}
			min_score = score;
		}
	}
	return min_score;
}

int run() {
	int min_score = trial();
	
	return min_score;
}

int main(void) {
	setvbuf(stdout, NULL, _IONBF, 0);
	urand = fopen("/dev/urandom", "r");
	if (!urand) return 0;
	//alarm(timeout);
	if (getfile()) {
		int ret = run();
		printf("Minimum score: %d\n",ret);
		if (ret >= (NUM_WORKERS>>1)){
			printf("Threshold %d: Earnt ",NUM_WORKERS>>1);
			system("cat /home/noob/flag1.txt");
		}
		else {
			puts("failed");
		}
		if (ret >= NUM_WORKERS-1){
			printf("Threshold %d: Earnt ",NUM_WORKERS-1);
			system("cat /home/noob/flag2.txt");
		} 
		
	}
	fclose(urand);
	return 0;
}

