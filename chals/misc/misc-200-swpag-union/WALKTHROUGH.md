# Unionised Logicians 

Puzzle challenge where the player needs to encode a strategy for a number of prisoners to escape. These strategies are written into a lua script file which is run on the server. 

## Steps - Official Walkthrough

<details>
<summary>Spoiler warning</summary>

### Solution
* See references 

## References
* https://www.tanyakhovanova.com/publications/ALineOfWizards.pdf
* The formulation of this puzzle originally appeared in 2013 Tournament of the Towns and was invented by Konstantin Knop and Alexander Shapovalov

</details>

