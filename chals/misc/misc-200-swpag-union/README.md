# Unionised Logicians

* _author_: DrJ
* _title_: Unionised Logicians
* _tags_:  misc

## Story

The king has been set upon by calamity, and he has been swayed to persecute the logicians.

Accustomed to the machinations of the logician's union, the king has prepared a dastardly ceremony to fire them.

The 99 logicians have been placed facing down a line, and then given a numbered hat so they can see all the hats in front of them, but none of the hats behind them, nor their own hat.

Each logician has been given a different hat from a set of 100 hats in total, numbered 0 to 99. One hat is removed, never seen by the logicians. 

Starting from the back of the line (who can see everyone else), the logician calls out the colour of hat they think they have. Then, play progresses to the logician in front of them until all logicians in the line have announced a valid number.

Once every logician has called out a number, all who are correct get to keep their jobs, and all who are incorrect are fired.

Additionally, each number can only be called out once. If any number is repeated, all logicians will be fired.

The logicians have grown to expect this from the king, so they formulate a Lua script to describe their behaviour, with the intention of saving as many of their jobs as possible.

Your task is to submit a Lua script (based on solution.lua) describing the worker's behaviour.

Send your solution to the challenge server on port $PORT$. You can use netcat for this purpose e.g.

`cat solution.lua | nc CHALLENGE.SERVER $PORT$ -q0`

Your solution will be tried 100 times, and your score will be the lowest number saved in any trial. If any logician calls out a value other than that of a valid number (0..99), all logicians will lose their jobs.

You will receive the flag if you can score at least 98 out of the 99.


## Notes - Build
requires: 
* liblua5.3-0, liblua5.3-dev

* Run `make && make test` 
* Run `make debug && make test` for debug

## References
* Framework and LUA scripting template borrowed from the awesome PlaidCTF 2020 Stegasaurus challenge
* https://ctftime.org/task/11304


