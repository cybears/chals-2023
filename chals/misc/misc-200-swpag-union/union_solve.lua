num_hats = 100

function missing_numbers(previous_calls,future_lineup)
  missing = {}
  for i=0,num_hats-1 do
    for p=1,#previous_calls do
      if previous_calls[p] == i then
        goto found
      end
    end
    for f=1,#future_lineup do
      if future_lineup[f] == i then
        goto found
      end
    end
    table.insert(missing,i)
    ::found::
  end
  return missing
end

function count_loops(perm)
  seen = {}
  for i=0,num_hats-1 do
    seen[i] = nil   
  end
  loop_count = 0
  ::find_loop::
  
  for i=0,num_hats-1 do
    j = i
    if seen[j] == nil then
      loop_count = loop_count + 1
      while seen[j] == nil do
        seen[j] = true
        j = perm[j]
      end
    end
  end
  
  
  return loop_count
end

function Worker(previous_calls,future_lineup)
  missing = missing_numbers(previous_calls,future_lineup)
  
  perm = {}
  
  perm[0] = missing[1]
  for i=1,#previous_calls do
    perm[i] = previous_calls[i]
  end
  perm[#previous_calls+1] = missing[2]
  for i=1,#future_lineup do
    perm[#previous_calls+1+i] = future_lineup[i]
  end
  
  count = count_loops(perm)
  if count%2 == 1 then
    return missing[2]
  else
    return missing[1]
  end
end


-- ENDFILE
