from pwn import * 
import argparse

if __name__ == "__main__":

        parser = argparse.ArgumentParser()
        parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge", required=False)
        parser.add_argument('-l', '--liveness', help="If remote - just check the server is responding", required=False)
        args = parser.parse_args()

        if args.liveness != None:
            if args.remote == None:
                log.failure("-r/--remote required for liveness check")
                exit(-1)
            host = args.remote.split(":")[0]
            port = int(args.remote.split(":")[1])
            p = remote(host, port)
            try:
                r = p.readuntil("file")
            except Exception as e:
                log.failure("ERROR: liveness check - could not connect to host {}".format(e))
                exit(-1)
            log.success("SUCCESS: Connected to server, exitting")
            p.close()
            exit(0)

        if args.remote != None:
            host = args.remote.split(":")[0]
            port = int(args.remote.split(":")[1])
            p = remote(host, port)
        else:
            p = process("./union")

        r = p.readuntil("file")

        with open("union_solve.lua", "r") as f: 
            d = f.read()
        p.send(d)
        p.readline()
        p.readline()
        p.readline()
        p.readline()
        output = p.readline()

        if (b'cybears' in output):
            log.success("FLAG FOUND!!")
            success = True
            p.close()
            exit(0)
        else:
            log.failure("ERROR: Run command failed [{}]".format(output))
            p.close()
            exit(-1)


