# Cryptic Hacker Crossword

This challenge is just a cryptic crossword, but the solutions are all 'hacking' themed.

To generate a solution filled grid, just supply any argument to `site/generate.py` when run:

```sh
`./generate.py       > hacker-cryptic.html`
`./generate.py solve > hacker-cryptic-solved.html`
```

Solution png in `grid_images` folder: <crossword_filled.png>

