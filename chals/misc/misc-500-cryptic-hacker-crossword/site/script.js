CLUE_STARTS = [
    [1,5],[1,7],[1,17],[1,19],
    [2,1],[2,12],
    [3,23],[3,25],
    [4,1],[4,10],[4,17],[4,21],
    [5,15],
    [6,2],[6,19],[6,23],
    [7,19],
    [8,4],[8,8],[8,14],[8,16],[8,17],
    [9,10],[9,12],[9,17],
    [10,5],
    [11,1],[11,14],[11,23],
    [12,8],
    [13,5],[13,25],
    [14,22],
    [15,2],[15,16],
    [16,12],
    [17,6],[17,16],[17,19],
    [18,5],
    [19,3],[19,18],[19,21],[19,24],
    [20,1],[20,10],[20,12],
    [21,1],
    [22,10],[22,15],[22,22],
    [23,1],[23,22],
    [24,5],[24,6],[24,17],
    [25,1],[25,11],
]

function generateFlag() {
    // Grab the letter in each numbered square (ie. clue starting box)
    var flag = 'cybears{';
    for (var i = 0; i < CLUE_STARTS.length; i++) {
        var e = document.getElementById(CLUE_STARTS[i][0] + '-' + CLUE_STARTS[i][1]);
        if (e) { 
            if (e.value != "") { flag += e.value.toUpperCase(); } else { flag += "_"; }
        }
    }
    document.getElementById('flag').innerHTML = flag + '}';
}
