#!/usr/bin/env python3
import base64
import sys

if len(sys.argv) > 1:
    SHOW_SOLUTIONS = True
else:
    SHOW_SOLUTIONS = False

CLUES_ACROSS = [
    [3,  "A crash recording found players outscored umpires&nbsp(4,4)"], # COREDUMP
    [5,  "Write beyond bounds when bigger muscles break banks&nbsp(6,8)"], # BUFFEROVERFLOW
    [9,  "See 49 Across"],  # REQUESTFORGERY
    [11, "FWD: Hi Ross! Remake that movie starring Travolta, Jackman and Berry&nbsp(9)"], # SWORDFISH
    [13, "A lovely lace language&nbsp(3)"], # ADA
    [14, "Gibson hacker confused archive orders&nbsp(5,8)"], # CRASHOVERRIDE
    [16, "The top three exemptions for running a windows program&nbsp(3)"], # EXE
    [17, "Unique interface identifier formed when pomace lost the raven's poet&nbsp(3)"], # MAC
    [18, "Return fire after victory starts your check&nbsp(6)"], # VERIFY
    [21, "Horse even runs on my box&nbsp(1,1)"], # OS
    [25, "Heads of terribly lazy sloths used to padlock traffic&nbsp(1,1,1)"], # TLS
    [26, "The fastest penguin to emerge with a portage&nbsp(6)"], # GENTOO
    [27, "Hoop without a ring on one leg&nbsp(3)"], # HOP
    [28, "Asking with a quiet query: Are you there, host?&nbsp(4)"], # PING
    [29, "This kind of sled does nothing&nbsp(3)"], # NOP
    [31, "I replace evil gates to form a raised entitlement&nbsp(8,9)"], # ESCALATEPRIVILEGE
    [34, "Gynt's friend&nbsp(4)"], # PEER
    [35, "Troll shoes used to grant unlimited access&nbsp(4,6)"], # ROOTSHELLS
    [36, "Every third bit of pram holds firmware keys&nbsp(1,1,1)"], # TPM
    [39, "Three's one of these when Roy, Moss and Jen hunt bugs&nbsp(5)"], # CROWD
    [40, "Order cute pumpers or a powerful calculator&nbsp(13)"], # SUPERCOMPUTER
    [43, "I just can't factor who leads the autobots&nbsp(5)"], # PRIME
    [46, "Take advantage of my former spouse, a messed up pilot&nbsp(7)"], # EXPLOIT
    [48, "Common bug was rotten an hour after noon&nbsp(3,2,3)"], # OFFBYONE
    ["49, 9", "Listen Sir, verse I'd re-crest for Jerry to trigger a remote query&nbsp(6,4,7,7)"], # SERVERSIDE
    [52, "Tie up around ten to quit&nbsp(4)"], # EXIT
    [53, "Port available for program launch&nbsp(4)"], # OPEN
    [54, "Visits mines to protect the stack&nbsp(6)"], # CANARY
    [56, "Flash AC kernel core breaks into computers&nbsp(6)"], # HACKER
    [57, "Traded fifty romans for loud feet to have awesome skillz&nbsp(4)"], # LEET
    [58, "Charge didn't sound like a tic or a toe&nbsp(6)"], # ATTACK
]

CLUES_DOWN = [
    [1,  "Flip over train driver to determine binary function&nbsp(7,8)"],  # REVERSEENGINEER
    [2,  "A shoe to turn on&nbsp(4)"],  # BOOT
    [3,  "His cipher makes a good salad&nbsp(6)"],  # CAESAR
    [4,  "Lure poet inside git storage&nbsp(4)"],  # REPO
    [6,  "Loiter oddly to spoof&nbsp(3)"],  # LIE
    [7,  "Snide Chanel shifted one to become a medium in parallel&nbsp(4-7)"],  # SIDECHANNEL
    [8,  "Acid level within creaky telecom manipulator&nbsp(6)"],  # PHREAK
    [10, "Library contains compressed malware&nbsp(3)"], # RAR
    [12, "A learner in the disco certain of a responsible announcement&nbsp(10)"], # DISCLOSURE
    [14, "The art of ciphers uses underground tomb oxygen to plot the vertical axis&nbsp(12)"], # CRYPTOGRAPHY
    [15, "Spooner will stash the smack at the new return address!&nbsp(5,3,5)"], # SMASHTHESTACK
    [19, "Unfit filesystem&nbsp(3)"], # FAT
    [20, "Carefully organise allocations to pile up a bridal partner&nbsp(4,5)"], # HEAPGROOM
    [22, "Alternating sets ergo hidden information <i>(abbr)</i>&nbsp(4)"], # STEG
    [23, "An oversupply of unknowns confounded Setec Astronomy&nbsp(3,4,7)"], # TOOMANYSECRETS
    [24, "Chess starter dons beret to be a moral hacker&nbsp(5,3)"], # WHITEHAT
    [30, "Your current goal is to arrest the golf target&nbsp(7,3,4)"], # CAPTURETHEFLAG
    [32, "This brown breakfast is better when salted&nbsp(4)"], # HASH
    [33, "I knew tor shuffled her connections&nbsp(7)"], # NETWORK
    [37, "Annoy a bunny that lost his tail&nbsp(3)"], # BUG
    [38, "Ascertain internal public key&nbsp(4)"], # CERT
    [41, "Unreachable office without frozen water queue&nbsp(7)"], # OFFLINE
    [42, "Smaug hid rage showing a dragon disassembler&nbsp(6)"], # GHIDRA
    [44, "Chaotic disorder might sew edge&nbsp(6)"], # MAYHEM
    [45, "The law was written up in one document&nbsp(4)"], # CODE
    [47, "Connect to any of these in a storm&nbsp(4)"], # PORT
    [50, "A less instructive architecture was initially really intriguing secret code&nbsp(1,1,1,1)"], # RISC
    [51, "Inexorable or exclusive?&nbsp(3)"], # XOR
    [55, "Heart pain from fake brains&nbsp(1,1)"], # AI
]

CLUE_STARTS = [
    (1,5),(1,7),(1,17),(1,19),
    (2,1),(2,12),
    (3,23),(3,25),
    (4,1),(4,10),(4,17),(4,21),
    (5,15),
    (6,2),(6,19),(6,23),
    (7,19),
    (8,4),(8,8),(8,14),(8,16),(8,17),
    (9,10),(9,12),(9,17),
    (10,5),
    (11,1),(11,14),(11,23),
    (12,8),
    (13,5),(13,25),
    (14,22),
    (15,2),(15,16),
    (16,12),
    (17,6),(17,16),(17,19),
    (18,5),
    (19,3),(19,18),(19,21),(19,24),
    (20,1),(20,10),(20,12),
    (21,1),
    (22,10),(22,15),(22,22),
    (23,1),(23,22),
    (24,5),(24,6),(24,17),
    (25,1),(25,11),
]

# Cell map because I did not write any crossword layout logic :D
GRID = [
   "----R-B---------COREDUMP-",
   "BUFFEROVERFLOW--A-E------",
   "----V-O----I----E-P---S-P",
   "REQUESTFORGERY--SWORDFISH",
   "----R----A----ADA---I-D-R",
   "-CRASHOVERRIDE--R-S-S-EXE",
   "-R--E-------------MAC-C-A",
   "-Y-VERIFY----H-OS-A-L-H-K",
   "-P--N--A-T-W-E--TLS-O-A--",
   "-T--GENTOO-H-A--E-H-S-N--",
   "HOP-I----O-I-PING-T-U-NOP",
   "-G--N--C-M-T-G----H-R-E--",
   "-R--ESCALATEPRIVILEGE-L-H",
   "-A--E--P-N-H-O----S--N--A",
   "-PEER--T-Y-A-O-ROOTSHELLS",
   "-H-----U-S-TPM----A--T--H",
   "-Y---B-R-E-----C--CROWD--",
   "----SUPERCOMPUTER-K--O---",
   "--O--G-T-R-----R-G--PRIME",
   "C-F----H-EXPLOIT-H---K-A-",
   "OFFBYONE-T-O-----I-----Y-",
   "D-L----F-SERVERSIDE--X-H-",
   "EXIT---L---T--I--R---OPEN",
   "--N-CANARY----S-HACKER-M-",
   "LEET-I-G--ATTACK---------",
]

with open('favicon.png', 'rb') as f:
    FAVICON = base64.b64encode(f.read()).decode()
with open('script.js') as f:
    CODE = f.read()
with open('style.css') as f:
    STYLE = f.read()

STYLE_CLUE_NUMBERS = "\n".join(f'.sq-label-{i+1} {{ grid-row: {pos[0]}/{pos[0]}; grid-column: {pos[1]}/{pos[1]}; }}' for i, pos in enumerate(CLUE_STARTS))

# Header
print(f'''<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <title>Cryptic Hacker Crossword</title>
    <link rel="icon" type="image/png" href="data:image/png;base64,{FAVICON}" />
    <style>
{STYLE}
{STYLE_CLUE_NUMBERS}
    </style>
</head>

<body>

<h1>Cryptic Hacker Crossword</h1>
<div class="flexmode">
''')

# Cells
print('    <div class="xw-box">')
print()
print('        <div class="xw">')
for i in range(25):
    print(f"\n            <!-- ROW {i+1} -->")
    for j in range(25):
        if GRID[i][j] == "-":
            print(f'            <span id="{i+1}-{j+1}" class="sq-void"></span>')
        else:
            if SHOW_SOLUTIONS:
                print(f'            <input id="{i+1}-{j+1}" class="sq" type="text" minlength="1" maxlength="1" pattern="^[A-Za-z]{{1}}$" value="{GRID[i][j]}" />')
            else:
                print(f'            <input id="{i+1}-{j+1}" class="sq" type="text" minlength="1" maxlength="1" pattern="^[A-Za-z]{{1}}$" value="" />')
    print(f"            <!-- END {i+1} -->")

# Labels
print('\n            <div class="xw xw-labels">')
for i in range(58):
    print(f'                <span id="label-{i+1}" class="sq-label sq-label-{i+1}"><span class="sq-label-text">{i+1}</span></span>')
print('            </div>')

print('        </div>')
print('    </div>')

# Clues
print()
print('    <div class="clues-box">')

print('        <dl class="clues clues--across">')
print('            <dt class="clues-title">ACROSS</dt>')
for c in CLUES_ACROSS:
    print(f'                <dd class="clue clue-a-{c[0]}" data-number="{c[0]}">{c[1]}</dd>')
print('        </dl>')

print('        <dl class="clues clues--down">')
print('            <dt class="clues-title">DOWN</dt>')
for c in CLUES_DOWN:
    print(f'                <dd class="clue clue-d-{c[0]}" data-number="{c[0]}">{c[1]}</dd>')
print('        </dl>')

print('    </div>')

print()
print('</div>')

# Footer
print(f'''
<div class="flag-generator">
Finished? <button onClick="generateFlag()">Click</button> to generate flag based on your answers: <br><span id="flag"></span>
</div>
<script type="text/javascript">
{CODE}
</script>
</body>
</html>''')
