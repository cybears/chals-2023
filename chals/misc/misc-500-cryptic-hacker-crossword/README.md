# Cryptic Hacker Crossword

* _author_: Cybears:Heef
* _title_: Cryptic Hacker Crossword
* _points_: 500
* _tags_:  misc, beginner

## Attachments
* cryptic-hacker-crossword.html
* cryptic-hacker-crossword.pdf

## Notes
* Might be printed copies available at the CTF Admin desk if I'm organised.
