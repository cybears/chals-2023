# Stegk on the Grille

You can infer a lot about the history of an SVG file by examining the XML elements closely. 

## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

The two files you have are an image full of text, and another image with a bunch of red rectangles and a transparent background.
The obvious thing to do is overlay one onto the other, and you should get very excited when the big rectangle perfectly fits over `cybears{` and the other rectangles line up nicely with other letters.

You can read the letters from L-R, T-B to get `cybears{ThiSfLAGisa4GerY}`.
But hold off hitting that submit button, champ, that was way too easy.

## Solution

On inspection of the XML of the SVG, notice that most of the rectangles have ids of `rect0` to `rect14`.
In Inkscape, if you copy-paste an element, the id of the new element is based on the id of the original.
So `rect3-3` and `rect3-6` were copied from `rect3` _after_ `rect3` was made.
And `rect3-6-3`, `rect3-6-5` and `rect3-6-7` are copies of `rect3-6`.

Notice too, that there is a _group_ of rectangles.
The way Inkscape handles movements of groups is that the coordinates of each element of the group are kept as they are as the group moves around, and instead the group object has the information about how it was moved.
What this means is that, given a group, you can reverse the movements of the group to find out where the elements were _at the time they were grouped together_.
If you reverse this translate operation, you should see that the rectangles still neatly identify letters.

You can also find one rectangle that has no fill and no border, so is invisible in the final image.

Once you delete the copied rectangles, and make visible the hidden rectangle, and reset the group to its original location, you can read off the flag: not by where the rectangles are on the image, but by the order in which they were created - numerical order of their ids.


## References
* https://en.wikipedia.org/wiki/Grille_(cryptography)
</details>


