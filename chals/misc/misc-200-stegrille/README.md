# Stegk on the Grille

* _author_: Cybears:BearArms
* _title_: Stegk on the Grille
* _points_: 200
* _tags_:  misc

## Handouts
* flag.png
* flag.svg

## Notes
* No special infrastructure required. Just two static files to be handed out that contain the flag. 
