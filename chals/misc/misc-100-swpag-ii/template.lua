-- if print does not exist just make it a no-op so our code works on the server too
if print == nil then
    print = function(...) end
end


function Prisoner1(id, round, responses)
    print("Hi, I'm a prisoner" )
    print("A_id",id)
    print("A_round",round)
    print("A_responses",responses)
    print(responses[1], responses[2], responses[3], responses[4], "...", responses[49], responses[50]) 

    --TODO: write an algorithm to decide the next box to open, given your prisoner ID, round number and responses so far

    return 1
end

-- ENDFILE

