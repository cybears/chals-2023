# Shall We Play A Game II?

* _author_: Cybears:cipher
* _title_: Shall We Play A Game II?
* _points_: 200
* _tags_:  misc

## Challenge Text
* See MANIFEST.yml

## Handouts
* `swpag2.c`: source file
* `template.lua`: example file to send

## Notes - Build
requires: 
* liblua5.3-0, liblua5.3-dev

* Run `make && make test` (with solve.lua and flag.txt in same directory)
* Run `make debug && make test` for debug

## Notes - Docker
* `docker build -t swpag_test -f Dockerfile.nsjail . `
* `docker run -v $(pwd):/home -it --rm -p 2323:2323 --privileged --entrypoint=bash  swpag_test` (run in container outside of nsjail) 
* `docker run -it --rm -p 2323:2323 --privileged  swpag_test` (run in container within nsjail)
 

## References
* Framework and LUA scripting template borrowed from the awesome PlaidCTF 2020 Stegasaurus challenge
* https://ctftime.org/task/11304


