# Shall We Play A Game II? 

Puzzle challenge where the player needs to encode a strategy for a number of prisoners to escape. These strategies are written into a lua script file which is run on the server. 

## Steps - Official Walkthrough

<details>
<summary>Spoiler warning</summary>

### Solution
* If players choose randomly, they have a `(1/2)^100` chance of all escaping as each cybear will have a 50% chance of guessing correctly
* Instead, and somewhat counter intuitively, there is a strategy that succeeds roughly 30% of the time, and is completely dependent on the initial "shuffle" (or permutation) of the boxes
* Each prisoner has the same strategy. On the first round, the prisoner guesses the box of their own ID. If they are correct, they are done, otherwise, for the next guess, they choose the box whose ID was just revealed. 
* The prisoner keeps going with this strategy, each round they choose the box of the previous ID that was revealed. 
* Now, the chance that the prisoner will find their own ID within 50 turns is exactly the chance that the permutation has a cycle of less than length 50. 
* Equally, for all players to succeed, we must consider the probability that for a random permutation, that _all_ of its cycles are less than length 50
* It turns out, this is around 30%! So while we can't guarantee an escape, this definitely increases the chances :P

## References
* https://puzzling.stackexchange.com/questions/16/100-prisoners-names-in-boxes

</details>

