// lua 5.3 has integer division

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#define MAXFILESIZE 8192
#define NUM_PRISONERS 100
#define NUM_ATTEMPTS 50

const int timeout = 10;

FILE* urand;
//lua_State (*Prisoner)[NUM_PRISONERS]; //we'll actually need 100 prisoner states, otherwise they will be able to share information between themsleves and recover the box configuration
lua_State *Prisoner;

char fp[0x100] = {0};

unsigned int boxes[NUM_PRISONERS] = {0};

/* Fisher-Yates shuffle from rosetta-code */

/* define a shuffle function. e.g. decl_shuffle(double).
 * advantage: compiler is free to optimize the swap operation without
 *            indirection with pointers, which could be much faster.
 * disadvantage: each datatype needs a separate instance of the function.
 *            for a small funciton like this, it's not very big a deal.
 */
#define decl_shuffle(type)				\
void shuffle_##type(type *list, size_t len) {		\
	int j;						\
	type tmp;					\
	while(len) {					\
		j = irand(len);				\
		if (j != len - 1) {			\
			tmp = list[j];			\
			list[j] = list[len - 1];	\
			list[len - 1] = tmp;		\
		}					\
		len--;					\
	}						\
}							\

/* random integer from 0 to n-1 */
int irand(int n)
{
	int r, rand_max = RAND_MAX - (RAND_MAX % n);
	/* reroll until r falls in a range that can be evenly
	 * distributed in n bins.  Unless n is comparable to
	 * to RAND_MAX, it's not *that* important really. */
	while ((r = rand()) >= rand_max);
	return r / (rand_max / n);
}

/* declare and define int type shuffle function from macro */
decl_shuffle(int);


bool checkBuf(unsigned char *buf, unsigned int len) {
	unsigned int i = 0;
	for (i=0; i<len; i++)
	{
		if( ((buf[i] < 0x20) || (buf[i] > 0x7f)) //disallow non-printable characters, except
				&& (buf[i]!=0x0a) 	 //carriage return
 				&& (buf[i]!=0x0d) 	 //line feed
				&& (buf[i]!=0x09) 	 //tab
				)
		{
			printf("Invalid char: [%c]\n", buf[i]);
			return false;
		}
	}
	return true;
}

bool getfile() {
	puts("send your file");
	//generate random filename to hold player submitted lua script
	unsigned char randchars[17];
	fread(randchars, sizeof(unsigned char), 16, urand);
	for (int i = 0; i < 16; i++) randchars[i] = randchars[i] % 26 + 'a';
	randchars[16] = 0;
	sprintf(fp, "/tmp/%s.lua", randchars);
	FILE* writefile = fopen(fp, "w");
	
	if (!writefile) return false;

	//read in player lua script a line at a time
	//validate that the buffer contains "good" characters 
	//and doesn't get too big (8K should be big enough to solve)
	char *buff = NULL;
	const char *sentinel = "-- ENDFILE";
 	ssize_t nread = 0;	
	size_t len = 0;
	size_t tally = 0;
	while (true) { 
		nread = getline(&buff, &len, stdin);	
		
		if (nread == -1)
		{
			printf("ERROR: EOF or ENOMEM\n");
			free(buff);
			return false;
		}

		if (!checkBuf((unsigned char *) buff, nread))
		{
			printf("ERROR: Invalid character in lua found\n");
			fclose(writefile);
			free(buff);
			return false;
		}

		tally += nread;
		if (tally > MAXFILESIZE)
		{
			printf("ERROR: Max file size is %d\n", MAXFILESIZE);
			fclose(writefile);
			free(buff);
			return false;
		}
		fwrite(buff, 1, nread, writefile);

		if (strstr(buff, sentinel) != NULL)
		{
#ifdef DEBUG
			printf("DEBUG: Sentinel found\n");
#endif	
			free(buff);
			break;
		}
		free(buff); buff = NULL;

	}
	fclose(writefile);

#ifdef DEBUG
	printf("finished getfile()\n");
#endif

	return true;
}


//run multiple trials 
bool trial(int prisoner_id, unsigned int boxes[NUM_PRISONERS]) {
	unsigned int responses[NUM_ATTEMPTS] = {0};
	unsigned int prisoner_guess = 0;
	printf("running checks... \n");
	
	for (int i = 0; i < NUM_ATTEMPTS; i++) {
		lua_getglobal(Prisoner, "Prisoner1");
#ifdef DEBUG
		printf("[-] DEBUG: check number %d\n", i);
#endif
		//send id, round number and responses to prisoner to decide
		lua_pushinteger(Prisoner, (lua_Integer) prisoner_id);
		lua_pushinteger(Prisoner, (lua_Integer) i);
		lua_createtable(Prisoner, NUM_ATTEMPTS, 0);
		for (int j = 0; j < NUM_ATTEMPTS; j++) {
			lua_pushinteger(Prisoner, (lua_Integer)responses[j]);
			lua_rawseti(Prisoner, -2, j+1);
		}
		lua_call(Prisoner, 3, 1); //call Prisoner1() with 3 args and 1 return

		//get prisoners response
		prisoner_guess = lua_tointeger(Prisoner, -1);
		lua_settop(Prisoner, 0); //reset the stack

#ifdef DEBUG
		printf("DEBUG: id: %d, round %d, guess %d, entry %d\n", prisoner_id, i, prisoner_guess, boxes[prisoner_guess]);
#endif
		// update responses with prisoners_guess
		responses[i] = boxes[prisoner_guess-1];

		// check if prisoner has found themselves, if so, count as a win and go straight onto the next prisoner
		if (prisoner_id == boxes[prisoner_guess-1]) {
			printf("prisoner [%d] found own ID!\n", prisoner_id);
			return true;
		}
	}
	printf("prisoner [%d] completed 50 trials and didn't find own ID \n", prisoner_id);
	return false;
}

bool run(unsigned int boxes[NUM_PRISONERS]) {

	bool everyone_won = true;

	for (int i = 1; i <= NUM_PRISONERS; i++) {
		// Reinstatiate lua state each time so there is no shared state between prisoners
		Prisoner = luaL_newstate(); 

		// Give them table and math stdlibs
		luaL_requiref(Prisoner, LUA_TABLIBNAME, luaopen_table, 1); lua_pop(Prisoner, 1);
		luaL_requiref(Prisoner, LUA_MATHLIBNAME, luaopen_math, 1); lua_pop(Prisoner, 1);
#ifdef DEBUG
		//add this to get print() and io.write()
		luaL_requiref(Prisoner, "base", luaopen_base, 1); lua_pop(Prisoner, 1);
		luaL_requiref(Prisoner, "io", luaopen_io, 1); lua_pop(Prisoner, 1);
#endif
		if (luaL_dofile(Prisoner, fp)) return false;

		bool won = trial(i, boxes);
		lua_close(Prisoner);

		everyone_won &= won; //if everyone is true, then this will pass.
	}

	return everyone_won;
}

int main(void) {
	int i;
	setvbuf(stdout, NULL, _IONBF, 0);
	urand = fopen("/dev/urandom", "r");
	if (!urand) return 0;
	//alarm(timeout);
	unsigned int seed=0; 
	fread(&seed, sizeof(unsigned int), 1, urand);
	srand(seed);

	for (i = 0; i < NUM_PRISONERS; i++) boxes[i] = i+1;
#ifdef DEBUG
	for (printf("before:"), i = 0; i < NUM_PRISONERS || !printf("\n"); i++)
		printf(" %d", boxes[i]);
#endif
	// perform initial shuffle of boxes
	shuffle_int(boxes, NUM_PRISONERS);
#ifdef DEBUG
	for (printf("after: "), i = 0; i < NUM_PRISONERS || !printf("\n"); i++)
		printf(" %d", boxes[i]);
#endif

	if (getfile()) {
		if (run(boxes)) {
			system("cat /home/noob/flag.txt");
		} else {
			puts("failed");
		}
	}
	fclose(urand);
	return 0;
}

