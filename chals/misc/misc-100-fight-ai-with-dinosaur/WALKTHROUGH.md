## Fight the AI with a Dinosaur
=================

## Notes - Walkthrough
* Neural Linguistic Steganography - a combination of arithmetic coding, a powerful entropy coding algorithm, with large modern language models (GPT-2) allows for a practical steganography system that fools both statistical steganalysis and human detection.
Natural language is a desirable cover signal given its everyday occurrence, but traditionally it has been challenging to encode large amounts of information in text without sacrificing quality.

## Steg Method
* This challenge uses Unicode encoding, Huffman steg (https://ieeexplore.ieee.org/document/8470163) and a 2^4 truncation length.
 
## Solving
* Players can either build the projects as described here: 
https://github.com/harvardnlp/NeuralSteganography/blob/master/run_single.py
* Or an online example is hosted https://steganography.live/info
* Enter the LM context `The Cybears found themselves face-to-face with a formidable opponent - an AI called Mayhem. We need your help.`

* Enter the cover text (note the leading space and line breaks) ` If only the world, and all of humanity was willing and able to help.

Cybear Games is the brain trust of the Cybears. A multi-franchise studio, they're in our backyard building a new franchise that can compete`

* Message encode algorithm: Unicode (Hint in Challenge Text)
* Steganography algorithm: Huffman
* Truncation Length: 2^4

## References
*  "Neural Linguistic Steganography," Zachary M. Ziegler*, Yuntian Deng*, Alexander M. Rush. https://github.com/harvardnlp/NeuralSteganography
