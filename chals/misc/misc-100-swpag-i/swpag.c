// lua 5.3 has integer division

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#define SIDELENGTH 16
#define BOARDSIZE (SIDELENGTH*SIDELENGTH)
#define MAXFILESIZE 8192

#ifdef DEBUG
	const int trials = 100;
#else
	const int trials = 10000;
#endif

const int timeout = 10;

FILE* urand;
lua_State *Bumblebear, *Pandamonium;
char fp[0x100] = {0};

bool checkBuf(unsigned char *buf, unsigned int len) {
	unsigned int i = 0;
	for (i=0; i<len; i++)
	{
		if( ((buf[i] < 0x20) || (buf[i] > 0x7f)) //disallow non-printable characters, except
				&& (buf[i]!=0x0a) 	 //carriage return
 				&& (buf[i]!=0x0d) 	 //line feed
				&& (buf[i]!=0x09) 	 //tab
				&& (buf[i]!=0x0) 	 //null
				)
		{
			printf("Invalid char: [%c]\n", buf[i]);
			return false;
		}
	}
	return true;
}

bool getfile() {
	puts("send your file");
	//generate random filename to hold player submitted lua script
	unsigned char randchars[17];
	fread(randchars, sizeof(unsigned char), 16, urand);
	for (int i = 0; i < 16; i++) randchars[i] = randchars[i] % 26 + 'a';
	randchars[16] = 0;
	sprintf(fp, "/tmp/%s.lua", randchars);
	FILE* writefile = fopen(fp, "w");
	
	if (!writefile) return false;

	//read in player lua script a line at a time
	//validate that the buffer contains "good" characters 
	//and doesn't get too big (8K should be big enough to solve)
	char *buff = NULL;
	const char *sentinel = "-- ENDFILE";
 	ssize_t nread = 0;	
	size_t len = 0;
	size_t tally = 0;
	while (true) { 
		nread = getline(&buff, &len, stdin);	
		
		if (nread == -1)
		{
			printf("ERROR: EOF or ENOMEM\n");
			free(buff);
			return false;
		}

		if (!checkBuf((unsigned char *) buff, nread))
		{
			printf("ERROR: Invalid character in lua found\n");
			fclose(writefile);
			free(buff);
			return false;
		}

		tally += nread;
		if (tally > MAXFILESIZE)
		{
			printf("ERROR: Max file size is %d\n", MAXFILESIZE);
			fclose(writefile);
			free(buff);
			return false;
		}
		fwrite(buff, 1, nread, writefile);

		if (strstr(buff, sentinel) != NULL)
		{
#ifdef DEBUG
			printf("DEBUG: Sentinel found\n");
#endif	
			free(buff);
			break;
		}
		free(buff); buff = NULL;

	}
	fclose(writefile);

#ifdef DEBUG
	printf("finished getfile()\n");
#endif

	return true;
}

//Do one round of player input
bool part1_checkonce() {
	// Create random board
	int board[BOARDSIZE];
	unsigned int treasure = 0;
	unsigned int rand = 0;

	//Generate a random board
	for (int i = 0; i < BOARDSIZE; i++) 
		{
			fread(&rand, sizeof(unsigned int), 1, urand);
			board[i] = rand & 1;
		}
	//Generate a random cell to hide the treasure
	fread(&treasure, sizeof(unsigned int), 1, urand);
	treasure %= BOARDSIZE;

#ifdef DEBUG
	printf("treasure: %d\nboard: ", treasure);
	for (int i = 0; i < BOARDSIZE; i++) 
		{
			printf("%d,", board[i]);
		}printf("\n");
#endif

	// Send the board to Bumblebear
	lua_getglobal(Bumblebear, "Bumblebear1");
	
	//send board
	lua_createtable(Bumblebear, BOARDSIZE, 0);
	for (int i = 0; i < BOARDSIZE; i++) {
		lua_pushinteger(Bumblebear, (lua_Integer)board[i]);
		lua_rawseti(Bumblebear, -2, i+1);
	}
	//send treasure location
	lua_pushinteger(Bumblebear, (lua_Integer)treasure);
	lua_call(Bumblebear, 2, 1); //call Bumblebear1 with 2 args and 1 return
	
	// Get Bumblebear's response
	int flip = 0;
	flip = lua_tointeger(Bumblebear, -1);
	lua_settop(Bumblebear, 0); //reset the stack

	// Check Bumblebear's response
	//ensure 0 < flip < BOARDSIZE
	if ((flip < 0) || (flip > BOARDSIZE))
	{
		printf("ERROR: Invalid flip value\n");
		return false;
	}
	//Flip Bumblebears choice
	board[flip] ^= 1;

#ifdef DEBUG
	printf("flip: %d\ntreasure: %d\nbumblebear ", flip, treasure);
	for (int i = 0; i < BOARDSIZE; i++) 
		{
			if(i == flip) printf("*");

			printf("%d,", board[i]);
		}printf("\n");
#endif

	// Send the board to Pandamonium
	lua_getglobal(Pandamonium, "Pandamonium1");
	lua_createtable(Pandamonium, BOARDSIZE, 0);
	for (int i = 0; i < BOARDSIZE; i++) {
		lua_pushinteger(Pandamonium, (lua_Integer)board[i]); //including flipped
		lua_rawseti(Pandamonium, -2, i+1);
	}
	lua_call(Pandamonium, 1, 1);

	// Check Pandamonium's response
	unsigned int guess = lua_tointeger(Pandamonium, -1);
	lua_settop(Pandamonium, 0); //reset the stack

#ifdef DEBUG
	printf("Pandamonium's guess: %d\n", guess);
	if (guess == treasure) printf("**** SUCCESS! ****\n");
#endif

	return guess == treasure;
}

//run multiple trials 
bool trial() {
	printf("running trials... ");
	for (int i = 0; i < trials; i++) {
#ifdef DEBUG
		printf("[-] DEBUG: Trail number %d\n", i);
#endif
		if (!part1_checkonce()) return false;
	}
	printf("passed\n");
	return true;
}

bool run() {
	// Create two instances of the code, one for all of Bumblebear's handling
	// and the other for all of Pandamonium's
	Bumblebear = luaL_newstate(); Pandamonium = luaL_newstate();

	// Give them table and math stdlibs
	luaL_requiref(Bumblebear, LUA_TABLIBNAME, luaopen_table, 1); lua_pop(Bumblebear, 1);
	luaL_requiref(Bumblebear, LUA_MATHLIBNAME, luaopen_math, 1); lua_pop(Bumblebear, 1);
#ifdef DEBUG
	//add this to get print() and io.write()
	luaL_requiref(Bumblebear, "base", luaopen_base, 1); lua_pop(Bumblebear, 1);
	luaL_requiref(Pandamonium, "base", luaopen_base, 1); lua_pop(Pandamonium, 1);
	luaL_requiref(Bumblebear, "io", luaopen_io, 1); lua_pop(Bumblebear, 1);
	luaL_requiref(Pandamonium, "io", luaopen_io, 1); lua_pop(Pandamonium, 1);
#endif
	luaL_requiref(Pandamonium, LUA_TABLIBNAME, luaopen_table, 1); lua_pop(Pandamonium, 1);
	luaL_requiref(Pandamonium, LUA_MATHLIBNAME, luaopen_math, 1); lua_pop(Pandamonium, 1);

	if (luaL_dofile(Bumblebear, fp)) return false;
	if (luaL_dofile(Pandamonium, fp)) return false;

	bool won = trial();
	lua_close(Bumblebear); lua_close(Pandamonium);
	return won;
}

int main(void) {
	setvbuf(stdout, NULL, _IONBF, 0);
	urand = fopen("/dev/urandom", "r");
	if (!urand) return 0;
	//alarm(timeout);
	if (getfile()) {
		if (run()) {
			system("cat /home/noob/flag.txt");
		} else {
			puts("failed");
		}
	}
	fclose(urand);
	return 0;
}

