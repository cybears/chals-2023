# Shall We Play A Game? 

Multiparty challenge where the player needs to encode a strategy between two actors. These strategies are written into a lua script file which is run on the server. 

## Steps - Official Walkthrough

<details>
<summary>Spoiler warning</summary>

### Solution
* The player needs to encode 8-bits of information into the modified grid.
* They can do this by encoding the target square into the "parity" of the whole board. 
* 

## References
* https://datagenetics.com/blog/december12014/index.html

</details>

