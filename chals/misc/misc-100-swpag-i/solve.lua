-- if print does not exist just make it a no-op so our code works on the server too
if print == nil then
    print = function(...) end
end

function checkParity(board)
	local parity = 0
	for i = 1,#board do
		if board[i] == 0 then
			parity = parity~(i-1)
		end
	end
	return parity
end

function Bumblebear1(board, treasure)
    print("Hi, I'm Bumblebear" )
    print("A_board",board)
    -- for i = 1,#board do
    --  	print(board[i])
    --end
    print("A_treasure",treasure)

    local flip = 127
    flip = checkParity(board)
 
    print("A_parity:", flip)
    print("A_flip:", flip~treasure)

    return flip~treasure
end

function Pandamonium1(board)
    print("Hi, I'm Pandamonium")
    -- get the board from Bumblebear
    print("board",board)
    print(board[1], board[2], board[3], board[4], board[5], "...", board[62], board[63], board[64])
   -- io.write("---", board[1], "---")


    local guess = checkParity(board)

    print("B_guess",guess)
    return guess
end

-- ENDFILE
