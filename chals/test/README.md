# Test Challenges

These challenges are taken from the `cybears/chals-test` repo as examples of
challenge structure and to confirm that CI is functioning. We can remove these
whenever they become annoying/not useful to retain here since they exist in the
other repository.
