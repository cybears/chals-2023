# A Place Called Vertigo

* _author_: Cybears:GingerBear
* _title_: A Place Called Vertigo!
* _points_: 100
* _tags_:  misc

## Flags
* '.cybears{mu$!c}.'

## Challenge Text
```markdown
Cybears lost their iPod, with their favourite 'pre-bundled no-opt-out artist' U2 on it!
One of their favourite tracks had a hidden watermark applied - but dont worry, it's encrypted with a key. 
Unfortunately the AI found the key on pastebin:
"key: db928fb0b0081a3e9c225d51aa1b688e"
Hope no one finds that file!

Flag format is ".cybears{XXXXX}."
