A-Place-Called-Vertigo
=================

## Notes - Walkthrough
* u2.mp3 on the badge, has a watermark on the file, which is encrypted with the following key:
* Key: db928fb0b0081a3e9c225d51aa1b688e
* The watermark has a payload:
* Payload: 7b7d637962656172737b6d752421637d
* When converted from hex:
* Flag: .cybears{mu$!c}.

## Steg Method
* This challenge uses the patchwork algorithm to hide the data in the spectrum of the audio file. The signal is split into 1024 sample frames. For each frame, some pseoudo-randomly selected amplitudes of the frequency bands of a 1024-value FFTs are increased or decreased slightly, which can be detected later. The algorithm used here is inspired by Martin Steinebach: Digitale Wasserzeichen für * Audiodaten. Darmstadt University of Technology 2004, ISBN 3-8322-2507-2
 
## Steg Method
* A sound file is loaded as an input, and a 128-bit message is stored in a watermark in the output sound file. For human listeners, the files typically sound the same.
* A common implementation of this is the open source audiowmark 

## References
* https://uplex.de/audiowmark/README.html
