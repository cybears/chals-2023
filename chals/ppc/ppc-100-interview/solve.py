import json
import yaml  # PyYAML is a dependency of pybears, and will have to be loaded separately if not using pybears


# Load the flag and handout name from the MANIFEST. This way there is only _one_ place the flag needs to be specified
with open('MANIFEST.yml', 'r') as f:
    manifest = yaml.safe_load(f)

correctflag = manifest["challenge"]["flags"][0]["content"]
handout_fname = manifest["challenge"]["handouts"][0]


def ll_enum(lldict):
    """
    Given a dictionary representing a set of memory addresses, enumerate any Linked Lists in that memory
    :param lldict:
    :return: A dictionory containing the address of the head of the list as keys, and the items of that LL as values
    """
    completions = {}  # key: head of list; value: list
    seen_nodes = set()  # Used to detect loops, and skip past nodes we've already visited
    for node in lldict.keys():
        if node in seen_nodes:
            continue
        completions[node] = ll_trav(lldict, node, seen_nodes, completions)
    return completions


def ll_trav(lldict, head, seen=None, completions=None):
    """
    Given a dictionary representing a set of memory addresses, traverse a linked list starting at the given head,
    being careful of loops
    :param lldict: The "memory"
    :param head: Where we start
    :param seen: Nodes we've seen before - used to detect loops, also to skip past stuff we've already done
    :param completions: Used so that we don't have to repeat work
    :return: The items in the linked list starting at `head`
    """
    if completions is None:
        completions = {}
    if seen is None:
        seen = set()
    if head not in lldict:
        return None

    nxt = head
    items = []
    while nxt is not None:  # While we're not at the end of the List
        seen.add(nxt)
        nxt, item = lldict[nxt]
        items.append(item)
        if nxt in completions:  # We know what the rest of the list is, because we've already done it
            items.extend(completions.pop(nxt))  # This completion is no longer needed
            break
        if nxt in seen:  # We have a loop!
            break
    return items


with open(handout_fname, 'r') as f:
    lld = json.load(f)
proc_ll = ll_enum(lld)  # Enumerate the first linked list
lldict2 = json.loads("".join(list(proc_ll.values())[0]))  # Deserialise into the second dictionary
proc_ll = ll_enum(lldict2)
flag_list = None
for k, v in proc_ll.items():
    # Find the list of flag components. It's the one with addresses as items, rather than characters
    if len(v[0]) > 1:
        flag_list = k

flag_components = []
for snippet_key in proc_ll[flag_list]:
    flag_components.extend(ll_trav(lldict2, snippet_key))
flag = "".join(flag_components)
assert flag == correctflag
print("Solved! Flag =", flag)
