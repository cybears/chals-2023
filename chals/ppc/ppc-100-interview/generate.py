import json
import random
import yaml  # PyYAML is a dependency of pybears, and will have to be loaded separately if not using pybears
from string import ascii_letters, digits, punctuation
from uuid import uuid4

# Load the flag from the MANIFEST. This way there is only _one_ place the flag needs to be specified
with open('MANIFEST.yml', 'r') as f:
    manifest = yaml.safe_load(f)

flag = manifest["challenge"]["flags"][0]["content"]
handout_fname = manifest["challenge"]["handouts"][0]
garbage = ascii_letters + digits + punctuation


def add_list(lldict, contents, loop=False):
    """
    Given a dictionary that represents a region of memory with keys being considered memory addresses,
      add a linked list to that dictionary.
    :param lldict: Dictionary with keys of UUID, values being the items stored at UUID's "address"
    :param contents: Either a string or list, or a number. If a number, create a string that long of random characters
    :param loop: Do I be a jerk and link the tail of the list back to its head?
    :return: The UUID/address of the head of the Linked List
    """
    if isinstance(contents, (str, list)):
        len_list = len(contents)
    elif isinstance(contents, int):
        len_list = contents
        contents = "".join(random.choices(garbage, k=len_list))
    else:
        return None

    start = str(uuid4())
    this = start
    for i, c in enumerate(contents):
        if i == len_list - 1:
            # We are at the end
            if loop:
                nxt = start
            else:
                nxt = None
        else:
            nxt = str(uuid4())
        lldict[this] = (nxt, c)
        this = nxt

    return start


def sort_dict(lldict):
    """ Given a dictionary, return a new dictioniary with keys in sorted order """
    keys = sorted(lldict.keys())
    return {k: lldict[k] for k in keys}


def split_flag(flg):
    """ Take a string and split it into short chunks """
    while flg:
        snippet_length = min(len(flg), random.randint(2, 4))
        snippet = flg[:snippet_length]
        yield snippet
        flg = flg[snippet_length:]


lld = {}  # This will be the dictionary containing the flag
snippet_list = []
for s in split_flag(flag):
    snippet_list.append(add_list(lld, s, random.random() < 0.5))  # Keep the heads of all the components, in order
add_list(lld, snippet_list)  # Add that list of heads into the dictionary
for _ in range(50):  # Add some chaff
    add_list(lld, random.randint(2, 4), random.random() < 0.5)

lld = sort_dict(lld)
lld_str = json.dumps(lld)  # Serialise the sorted dictionary into a big string. Pass this string back into a _new_ dict
out_dict = {}
add_list(out_dict, lld_str)
out_dict = sort_dict(out_dict)

with open(handout_fname, "w") as f:
    json.dump(out_dict, f)  # Serialise this new dictionary, to give to players
