# ppc-100-interview

This challenge puts you at the hands of an unimaginitive tech interviewer who still thinks it's a good idea to ask candidates to traverse a linked list.
However, you haven't been provided with the head of the list!

## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

The challenge presents a json file that can be deserialised into a single dictionary.
The intent is that the UUIDs that are the keys for the dictionary can be considered memory addresses, where the values are the memory contents at that address.
For almost all values, the first element is another address that is in the dictionary, so this provides an ordering on the elements.

## Solution

There are a number of ways one could approach this.
One way is to iterate through the keys, skipping keys you've already seen, and following each key as far as you can until you get to the end (with `None` as the first element).
If, as you traverse the list, you find an element you've already seen, you can shortcut the rest of the process, because you already know what the completion from that point in the list is.

If you do this carefully, you'll find that you get a single sequence of characters that forms a string that itself can be deserialised into a dictionary.

So you try the same trick again.
This time, however, you have to be careful of loops.
Instead of getting one long sequence of characters, this "region of memory" contains a bunch of indepentent linked lists, some of which are circularly linked.
One list stands out from the rest, though: instead of a sequence of characters, it's a sequence of UUIDs.
Those UUIDs are all "addresses".
Traversing the linked lists from each of those addresses (stopping once you encounter a loop) gives a sequence of characters that can be concatenated to form the flag.

A full solution is in `solve.py`.


## References
* https://xkcd.com/2483/
</details>


