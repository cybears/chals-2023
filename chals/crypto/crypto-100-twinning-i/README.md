# Twinning I

* _author_: Cybears:cipher
* _title_: Twinning I
* _points_: 100
* _tags_:  crypto, beginner

## Attachments
* `output.json`: Generated modulii and ciphertext
* `generate_modulii.py`: script to generate primes and cipher

