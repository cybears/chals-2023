# crypto-100-twinning-i

This challenge is a beginner challenge that takes "twin primes" (primes that differ by 2) and utilises them as RSA modulii 

## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

## Solution

Given the generation script creates twin primes, we can write the RSA modulus like this: 
 * `N = p*(p+2)`
and then
 * `0 = p^2 +2*p - N` giving us a quadratic equation in `p`
 * This can be solved with the General Formula for solving quadratics so that `p = -1 +/- sqrt(1+N)` noting that we will take the positive square root

## Solution (Alternates)
 * Given how close `p` is, we could simply take the integer square root on `N` and guess it is `-1`, `0`, or `1` away from `p` and this would also work
 * A bit heavier duty, but this modulus is also susceptible to a Fermat Factoring Attack as `p-q` is "small" (actually, very small). You could either code this up yourself or use a tool like `https://github.com/RsaCtfTool/RsaCtfTool`


</details>


