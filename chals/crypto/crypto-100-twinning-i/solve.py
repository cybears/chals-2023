from Crypto.Util.number import getPrime, isPrime, long_to_bytes, bytes_to_long, inverse
import json
import math

with open("output.json", "r") as f: 
        d = f.read()

j = json.loads(d)

n = j['n']
e = j['e']
c = j['c']

# note that n = p*(p+2) 
# so p^2 + 2*p - n = 0
# hence p = -1 + isqrt(1+n)

# math.isqrt only in python > 3.8
p = -1 + math.isqrt(1+n)

q = n//p

d = inverse(e, (p-1)*(q-1))
m = long_to_bytes(pow(c, d, n))

if b'cybears' in m:
    print("SUCCESS: FLAG FOUND!!")
    exit(0)
else: 
    print("ERROR: Flag not found")
    exit(-1)


