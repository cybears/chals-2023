from Crypto.Util.number import getPrime, isPrime, long_to_bytes, bytes_to_long, inverse
from cybears import flag
import json

bitlen = 1024
count = 0 

while(True): 
    count = count+1
    if(count %100 == 0): 
        print("count: {}".format(count))
    p1 = getPrime(bitlen)
    p2 = p1+2
    if isPrime(p2):
        print("Took {} rounds".format(count))
        break
    
n = p1*p2
e = 65537

f = bytes_to_long(flag)

c = pow(f, e, n)


j = {"n": n, "e":e, "c":c}

print(json.dumps(j))

d = inverse(e, (p1-1)*(p2-1))

m = long_to_bytes(pow(c, d, n))

test_flag = m
print("Flag matches? {}".format(test_flag == flag))
 


