from pwn import * 
from google.protobuf.internal.encoder import _VarintEncoder
from google.protobuf.internal.decoder import _DecodeVarint
import server_pb2 as spb
from google.protobuf.internal.decoder import _DecodeError
import argparse

def send_message(s, msg):
    """ Send a message, prefixed with its size, to a TPC/IP socket """
    data = msg.SerializeToString()
    mh = spb.MessageHeader()
    mh.msglen = len(data)
    mh.type = msg.type
    s.send(mh.SerializeToString() + data)
    return

def msg_type(msgtype):
    if msgtype == spb.MSG_LOGIN_REQUEST:
        return spb.LoginRequest()
    elif msgtype == spb.MSG_LOGIN_RESPONSE:
        return spb.LoginResponse()
    elif msgtype == spb.MSG_REGISTER_REQUEST:
        return spb.RegisterRequest()
    elif msgtype == spb.MSG_REGISTER_RESPONSE:
        return spb.RegisterResponse()
    elif msgtype == spb.MSG_MESSAGE_REQUEST:
        return spb.MessageRequest()
    elif msgtype == spb.MSG_MESSAGE_RESPONSE:
        return spb.MessageResponse()
    elif msgtype == spb.MSG_FLAG_REQUEST:
        return spb.FlagRequest()
    elif msgtype == spb.MSG_FLAG_RESPONSE:
        return spb.FlagResponse()
    else:
        return None   

def check_fields(msg):
    result = True
    for field in msg.DESCRIPTOR.fields_by_name.keys():
        if msg.DESCRIPTOR.fields_by_name[field].label == msg.DESCRIPTOR.fields_by_name[field].LABEL_REQUIRED:
            result &= msg.HasField(field)
    return result

def recv_message(s):
    """ Receive a message, prefixed with its size and type, from stdin """
    # Receive the size of the message data
    # expect [MessageHeader][Message of type]
    data = b''
    header = spb.MessageHeader()
    while True:
        data+= s.recv(1)
        try:
            header.ParseFromString(data)
            if check_fields(header):
                break
        except _DecodeError as e:
            pass

    # Receive the message data
    data = s.recv(header.msglen)

    # Decode the message and validate all required fields are present
    msg = msg_type(header.type)
    if msg != None:
        try:
            msg.ParseFromString(data)
            if not check_fields(msg):
                return None
            return msg
        except _DecodeError:
            return None
    else:
        return None

# IDEA
# The token validation is performed on the user provided token and a valid signature
# Further, the token is serialised and hashed with MD5 prior to the HMAC
# If we can generate an MD5 collision in the token, with a controlled prefix, we can bypass this validation
# in particular, given two strings of the same length, A and B, we can find C and D such that 
# MD5(A||C) == MD5(B||D)

# We let A = serialisation(uid:2, role:0, salt: ??)
# and B = serialisation(uid:2, role:1, salt: ??)

# The only minor issue is that the serialisation of the salt field includes a length... we will need to guess how many blocks the collision will need to satisfy - this may be up to 9

# In [82]: build_token(2,spb.USER, b'aaaaaa').SerializeToString()
# Out[82]: b'\x08\x02\x10\x00\x1a\x06aaaaaa'
#             ^uid    ^role  ^salt   ^value
#                 ^2     ^0      ^len=6
# In [87]: build_token(2,spb.ADMIN, b'aaaaaa').SerializeToString()
# Out[87]: b'\x08\x02\x10\x01\x1a\x06aaaaaa'

# We pre-arrange this collisions and register a user with defined salt = C
# We login and get a valid token (this user will have uid = 2 and role = USER (0) )

# Then, we modify the unsigned token to be uid = 2 and role = ADMIN (1), then change the salt to be D. We keep the valid signature of the original token

# Then submit this to get the flag!

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge", required=True)
    args = parser.parse_args()

    if args.remote != None:
        host = args.remote.split(":")[0]
        port = int(args.remote.split(":")[1])
    else:
        exit(0)

    #context.log_level = 'debug'
    context.log_level = 'info'

    # Extract collision salts
    with open("prefix1.bin.coll", "rb") as f:
        p1 = f.read()
    with open("prefix2.bin.coll", "rb") as f:
        p2 = f.read()

    t1 = spb.UnsignedToken()
    t1.ParseFromString(p1)
    t2 = spb.UnsignedToken()
    t2.ParseFromString(p2)

    s = remote(host, port)

    ## REGISTER
    rr = spb.RegisterRequest()
    rr.type = spb.MSG_REGISTER_REQUEST
    rr.name = b"TestName"
    rr.password = b"TestPassword"
    #rr.salt = b'CALCULATEDHASHCOLLIONPART1'
    # Make sure we set the salt (C) to the first collision with role=user
    rr.salt = t1.salt

    send_message(s, rr)

    ## REGISTER RESPONSE
    reg_resp = recv_message(s)
    if reg_resp != None:
        print("DEBUG: received {}".format(reg_resp))

    ## LOGIN
    l = spb.LoginRequest()
    l.type = spb.MSG_LOGIN_REQUEST
    l.uid = reg_resp.uid
    l.password = b"TestPassword"
    send_message(s, l)

    ## LOGIN RESPONSE
    # This login response will have a valid signed token for role=user
    login_resp = recv_message(s)
    if login_resp != None:
        print("DEBUG: received {}".format(login_resp))

    ## REQUEST FLAG
    flag_req = spb.FlagRequest()
    flag_req.type = spb.MSG_FLAG_REQUEST
    flag_req.uid = login_resp.uid
    flag_req.role = spb.ADMIN 
    flag_req.token.CopyFrom(login_resp.token)

    flag_req.token.token.role = spb.ADMIN # Make sure we update the role of the _token_ to be admin. The server will trust this as there is a signature check applied! 
    #flag_req.token.token.salt = b'CALCULATEDHASHCOLLISIONPART2' 
    flag_req.token.token.salt = t2.salt # Make sure we update the salt to be the updated 2nd collision with role=admin

    send_message(s, flag_req)

    flag_resp = recv_message(s)
    if flag_resp != None:
        #print("DEBUG: received {}".format(flag_resp))
        if b'cybears' in flag_resp.msg:
            print("SUCCESS! Flag found")
            s.close()
            exit(0)

    s.close()
    exit(-1)

