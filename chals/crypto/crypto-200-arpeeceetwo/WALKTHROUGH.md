# ArpeeCeeTwo

This challenge is to try and identify an authentication bypass in a protobuf base RPC server.

## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

## Solution

###IDEA
* The token validation is performed on the user provided token and a valid signature (the server assumes as it is signed, it will be valid, rather than checking the server side database)
* Further, the token is serialised and hashed with MD5 _prior_ to the HMAC
* If we can generate an MD5 collision in the token, with a controlled prefix, we can bypass this validation. In particular, given two strings of the same length, A and B, we can find C and D such that 
`MD5(A||C) == MD5(B||D)`

* We let `A = serialisation(uid:2, role:0, salt: ??)`
* and `B = serialisation(uid:2, role:1, salt: ??)`

The only minor issue is that the serialisation of the salt field includes a length... we will need to guess how many blocks the collision will need to satisfy - this may be up to 9 blocks.

```
In [82]: build_token(2,spb.USER, b'aaaaaa').SerializeToString()
Out[82]: b'\x08\x02\x10\x00\x1a\x06aaaaaa'
            ^uid    ^role  ^salt   ^value
                ^2     ^0      ^len=6
In [87]: build_token(2,spb.ADMIN, b'aaaaaa').SerializeToString()
Out[87]: b'\x08\x02\x10\x01\x1a\x06aaaaaa'
```

* We pre-arrange this collisions and register a user with defined salt = C
* We login and get a valid token (this user will have uid = 2 and role = USER (0) )

* Then, we modify the unsigned token to be uid = 2 and role = ADMIN (1), then change the salt to be D. We keep the valid signature of the original token

* Then submit this to get the flag!


### Generating collisions
We generate collisions by manually creating the prefix that we need. I ended up doing it twice so I could be sure about that the collision length would be, the first time with a placeholder length value, then the second updated with the correct length field. The hash collisions were generated with `hashclash` below, based on the following blog

* https://natmchugh.blogspot.com/2015/02/create-your-own-md5-collisions.html
* https://github.com/cr-marcstevens/hashclash

I was concerned about how long a collision generation would take in such a short CTF, but each run took about 5 hours on my hex-core i7 without GPU assistance. 


</details>


