# ArpeeCeeTwo

* _author_: Cybears:cipher
* _title_: ArpeeCeeTwo
* _points_: 200
* _tags_:  crypto

## Attachments
* `client.py`: Example application to interact with the server RPC calls
* `server.proto`: Protobuf specification for server and client
* `server.py`: Python server file 

## Build/run notes
If using pip3 protobuf > 3.20 you either need to: 
1. downgrade to 3.20 or
2. `export PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python`

(we end up pinning pip protobuf to 3.20.3 in the docker image )

## Build protobuf compiler docker image
* `docker build --network=host -t rpc_builder -f Dockerfile.build .`
* `docker run -it --rm -v $(pwd):/home/ -w /home rpc_builder protoc -I=. --python_out=. server.proto`

## build nsjail container to serve challenge
* `docker build --network=host -t rpc_nsjail -f Dockerfile.nsjail .`
* `docker run --privileged  -it --rm -p 2323:2323  rpc_nsjail`

## build solver container and test against server
* `docker build -t arpeeceetwo-solve -f Dockerfile.solve .`
* `docker run --network=host -it --rm arpeeceetwo-solve python3 solve.py -r 127.0.0.1:2323`

## run locally with socat
* `socat -d TCP-LISTEN:2323,reuseaddr,fork EXEC:"python3 server.py"`


