# crypto-300-twinning-ii

This challenge is a challenge that takes "twin primes" (primes that differ by 2) and utilises them to create _two_ RSA modulii 

## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

## Solution

Given the generation script creates twin primes (`p1` and `p2`), we can write the two RSA modulii like this: 
 * `N1 = p1*q1`
 * `N2 = p2*q2`
 * note that `p1 == p2 + 2`

and then
 * `p1 - p2 = 2` so in particular
 * `2*q2 + N2 = 0 mod p1`. This equation also holds mod `p1*q2`

Consider the polynomial
 * `f(x) = x + N2/2 mod N1*N1`then `f(x)` has a small root `q2` and can be found using Coppersmith's attack

## References
* Based on `https://crypto.stackexchange.com/questions/25010/factoring-two-rsa-moduli-n-i-p-i-cdot-q-i-knowing-that-p-2-p-12` and "Factoring with implicit hints"

</details>


