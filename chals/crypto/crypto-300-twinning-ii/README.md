# Twinning II 

* _author_: Cybears:cipher
* _title_: Twinning II
* _points_: 300
* _tags_:  crypto

## Attachments
* `output.json`: Generated modulii and ciphertext
* `generate_modulii.sage`: script to generate primes and cipher

