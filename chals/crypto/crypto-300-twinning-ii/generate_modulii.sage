from Crypto.Util.number import getPrime, isPrime, long_to_bytes, bytes_to_long, inverse
from cybears import flag
import json

set_verbose(2)
bitlen = 1024
t = 32
upper_bound = bitlen+t
lower_bound = bitlen-t
count = 0
while True:
        count = count+1
        if(count % 100 == 0): 
                print("count: {}".format(count))
        p2 = random_prime(2^(upper_bound), lbound=2^(upper_bound-1)+2^(upper_bound-2), proof=False)
        if is_pseudoprime(p2 + 2):
                p1 = p2 + 2
                print("Took {} goes".format(count))
                break

print("found in {} tries".format(count))

q1 = random_prime(2^(lower_bound), lbound=2^(lower_bound-1)+2^(lower_bound-2), proof=False)
q2 = random_prime(2^(lower_bound), lbound=2^(lower_bound-1)+2^(lower_bound-2), proof=False)

n1 = p1*q1
n2 = p2*q2
e = 65537

f1 = bytes_to_long(flag[0:len(flag)//2])
f2 = bytes_to_long(flag[len(flag)//2:])

c1 = pow(f1, e, n1)
c2 = pow(f2, e, n2)


j = {"n1": int(n1), "n2":int(n2), "e":int(e), "c1":int(c1), "c2":int(c2)}

with open("output.json", "w") as f:
    f.write(json.dumps(j))

d1 = inverse(e, (p1-1)*(q1-1))
d2 = inverse(e, (p2-1)*(q2-1))

m1 = long_to_bytes(int(pow(c1, d1, n1)))
m2 = long_to_bytes(int(pow(c2, d2, n2)))

test_flag = m1+m2
print("Flag matches? {}".format(test_flag == flag))
 


