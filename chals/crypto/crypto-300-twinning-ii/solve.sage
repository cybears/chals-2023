import json
from Crypto.Util.number import getPrime, isPrime, long_to_bytes, bytes_to_long, inverse
set_verbose(2)

with open("output.json", "r") as f:
	d = f.read()

j = json.loads(d)

n1 = j['n1']
n2 = j['n2']
e = j['e']
c1 = j['c1']
c2 = j['c2']

bitlen = 1024
slack = 32

P.<x> = Zmod(n1*n2)[]
f = x + inverse_mod(2, n1*n2)*n2 % (n1*n2)
s=f.small_roots(X=2^(bitlen-slack), beta=0.499, epsilon=1/80)

if len(s) > 1:
	q2 = Integer(s[1])
	print("FOUND {}-bit solution: {}".format(Integer(s[1]).nbits(), s[1]))
	p2 = n2//q2
	p1 = p2 + 2
	q1 = n1//p1

	d1 = inverse(e, (p1-1)*(q1-1))
	d2 = inverse(e, (p2-1)*(q2-1))

	m1 = long_to_bytes(int(pow(Integer(c1), d1, n1)))
	m2 = long_to_bytes(int(pow(Integer(c2), d2, n2)))

	if b'cybears' in (m1+m2):
		print(f"SUCCESS: FOUND FLAG! {m1+m2}")
		exit(int(0))
	else:
		print("ERROR: FAILED TO FIND FLAG")
		exit(int(-1))

else: 
	print("small_roots failed to find a root") 
	exit(int(-1))



