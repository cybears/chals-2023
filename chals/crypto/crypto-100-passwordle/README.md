# Passwordle

* _author_: Cybears:cipher and (H)
* _title_: Passwordle
* _points_: 100
* _tags_:  crypto

## Attachments
* (none)

## Notes
* Based on the react-wordle game https://github.com/cwackerfuss/react-wordle.git

## Build/run notes
* Will only build in production now. For local build/test
  * build: `docker build -t reactle:prod -f website/docker/Dockerfile .`
  * run: `docker run -d -p 80:8080  --name reactle-prod reactle:prod`
  * browse to: `http://localhost:80`

## Adding levels
* Add the new solution to the array at `src/constants/wordlist.ts`
* Add the level description to `src/constants/levels.ts`
* If there are now no longer 9 levels, the encrypted flag (`encflag`) needs to be updated at `src/components/modals/LevelSelectModal.tsx` as the correct key for this is formed from the completed game state.
 * The new encrypted flag can be generated using `python3 encrypt_flag.py <num_levels>`
