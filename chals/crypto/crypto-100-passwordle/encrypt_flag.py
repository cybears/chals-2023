import sys
import Crypto.Cipher.Blowfish as bf
import hashlib
import base64

# The flag is encrypted in the source of passwordle so it is not trivially recoverable
# The key to decrypt is the encoded game state. If the number of levels changes, the flag needs to be re-encrypted
# run as `python3 encrypt_flag.py 9` where 9 is the number of levels of passwordle

#kk = '{"1":"win","2":"win","3":"win","4":"win","5":"win","6":"win","7":"win","8":"win","9":"win"}'
#win_key = hashlib.md5(kk.encode()).hexdigest()
#enc_nine = 'FA0/Z8leqO/Z98pC0tNH1fGdyV2T5HyDTe9Bmi+G/5d8AnohsYZQ3D2eWAxwgC1R'
#b = bf.new(win_key.encode(), bf.MODE_ECB)
#print(b.decrypt(base64.b64decode(enc_nine.encode())))

flag = b'cybears{my_v01c3_15_my_p@55p0rt.V3r1fy_M3}'

def generate_game_state(num_levels=9):
    return "{"+",".join([f'"{str(x)}":"win"' for x in range(1,num_levels+1)])+"}" 

def encrypt_flag(data=flag, num_levels=9):
    kk = generate_game_state(num_levels)
    bfkey = hashlib.md5(kk.encode()).hexdigest()
    b = bf.new(bfkey.encode(), bf.MODE_ECB)
    datapad = data + b'\x00'*(8-len(flag)%8)
    return base64.b64encode(b.encrypt(datapad))
    
print(encrypt_flag(num_levels=int(sys.argv[1])))



