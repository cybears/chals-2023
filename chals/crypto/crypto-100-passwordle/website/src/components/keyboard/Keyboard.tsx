import { useEffect } from 'react'

import { DELETE_TEXT, ENTER_TEXT } from '../../constants/strings'
import { getStatuses } from '../../lib/statuses'
import { Key } from './Key'

type Props = {
  onChar: (value: string) => void
  onDelete: () => void
  onEnter: () => void
  onShift: () => void
  solution: string
  guesses: string[]
  isRevealing?: boolean
  isShifted: boolean
  isKeyboardActive: boolean
}

export const Keyboard = ({
  onChar,
  onDelete,
  onEnter,
  onShift,
  solution,
  guesses,
  isRevealing,
  isShifted,
  isKeyboardActive,
}: Props) => {
  const charStatuses = getStatuses(solution, guesses)

  const onClick = (value: string) => {
    if (value === 'ENTER') {
      onEnter()
    } else if (value === 'SHIFT') {
      onShift()
    } else if (value === 'DELETE') {
      onDelete()
    } else {
      onChar(value)
    }
  }

  useEffect(() => {
    const listener = (e: KeyboardEvent) => {
      if (e.code === "ShiftLeft" || e.code === "ShiftRight") {
        if (!isShifted && !e.getModifierState('CapsLock')) { onShift() }
      } else if (e.code === 'CapsLock') {
        onShift()
      }
    }
    window.addEventListener('keydown', listener)
    return () => {
      window.removeEventListener('keydown', listener)
    }
  }, [isShifted, onShift])

  useEffect(() => {
    const listener = (e: KeyboardEvent) => {
      if (e.getModifierState('CapsLock') && !isShifted) { onShift() }
      if (e.code === 'Enter' || e.code === 'NumpadEnter' ) {
        onEnter()
      } else if (e.code === "ShiftLeft" || e.code === "ShiftRight") {
        if (!e.getModifierState('CapsLock')) { onShift() }
      } else if (e.code === 'Backspace') {
        onDelete()
      } else {
        const key = e.key
        if (key.length === 1) {  
          onChar(key)
        }
      }
    }
    
    window.addEventListener('keyup', listener)
    return () => {
      window.removeEventListener('keyup', listener)
    }
  }, [isShifted, onEnter, onDelete, onShift, onChar])

  if (!isKeyboardActive) { return (
    <div></div>
  ) }
  else if (isShifted) { return (
    <div className="mt-6 md:mt-6">
      <div className="mb-1 flex justify-center">
        {['~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', `_`, `+`].map((key) => (
          <Key
            value={key}
            key={key}
            onClick={onClick}
            status={charStatuses[key]}
            isRevealing={isRevealing}
          />
        ))}
      </div>
      <div className="mb-1 flex justify-center">
        {[' ', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}'].map((key) => (
          <Key
            value={key}
            key={key}
            onClick={onClick}
            status={charStatuses[key]}
            isRevealing={isRevealing}
          />
        ))}
      </div>
      <div className="mb-1 flex justify-center">
        <Key width={36} value="SHIFT" onClick={onClick} isShifted={isShifted}>
          {'↑'}
        </Key>
        {['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"'].map((key) => (
          <Key
            value={key}
            key={key}
            onClick={onClick}
            status={charStatuses[key]}
            isRevealing={isRevealing}
          />
        ))}
      </div>
      <div className="flex justify-center">
        <Key width={65.4} value="ENTER" onClick={onClick}>
          {ENTER_TEXT}
        </Key>
        {['Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?'].map((key) => (
          <Key
            value={key}
            key={key}
            onClick={onClick}
            status={charStatuses[key]}
            isRevealing={isRevealing}
          />
        ))}
        <Key width={65.4} value="DELETE" onClick={onClick}>
          {DELETE_TEXT}
        </Key>
      </div>
    </div> 
  ) }

  else { return (
    <div className="mt-6 md:mt-6">
      <div className="mb-1 flex justify-center">
        {['`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', `-`, `=`].map((key) => (
          <Key
            value={key}
            key={key}
            onClick={onClick}
            status={charStatuses[key]}
            isRevealing={isRevealing}
          />
        ))}
      </div>
      <div className="mb-1 flex justify-center">
        {[' ', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']'].map((key) => (
          <Key
            value={key}
            key={key}
            onClick={onClick}
            status={charStatuses[key]}
            isRevealing={isRevealing}
          />
        ))}
      </div>
      <div className="mb-1 flex justify-center">
        <Key width={36} value="SHIFT" onClick={onClick}>
          {'↑'}
        </Key>
        {['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\''].map((key) => (
          <Key
            value={key}
            key={key}
            onClick={onClick}
            status={charStatuses[key]}
            isRevealing={isRevealing}
          />
        ))}
      </div>
      <div className="flex justify-center">
        <Key width={65.4} value="ENTER" onClick={onClick}>
          {ENTER_TEXT}
        </Key>
        {['z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/'].map((key) => (
          <Key
            value={key}
            key={key}
            onClick={onClick}
            status={charStatuses[key]}
            isRevealing={isRevealing}
          />
        ))}
        <Key width={65.4} value="DELETE" onClick={onClick}>
          {DELETE_TEXT}
        </Key>
      </div>
    </div>
  ) }
}
