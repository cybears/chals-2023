import './App.css'

import { default as GraphemeSplitter } from 'grapheme-splitter'
import { useEffect, useState } from 'react'
import Div100vh from 'react-div-100vh'

import { AlertContainer } from './components/alerts/AlertContainer'
import { Grid } from './components/grid/Grid'
import { Keyboard } from './components/keyboard/Keyboard'
import { InfoModal } from './components/modals/InfoModal'
import { LevelSelectModal } from './components/modals/LevelSelectModal'
import { SettingsModal } from './components/modals/SettingsModal'
import { Navbar } from './components/navbar/Navbar'
import {
  DISCOURAGE_INAPP_BROWSERS,
  MAX_CHALLENGES,
  REVEAL_TIME_MS,
  WELCOME_INFO_MODAL_MS,
} from './constants/settings'
import {
  CORRECT_WORD_MESSAGE,
  DISCOURAGE_INAPP_BROWSER_TEXT,
  HARD_MODE_ALERT_MESSAGE,
  NOT_ENOUGH_LETTERS_MESSAGE,
  WIN_MESSAGES,
  HASH_MESSAGES,
} from './constants/strings'
import {
  LEVELS
} from './constants/levels'
import { useAlert } from './context/AlertContext'
import { isInAppBrowser } from './lib/browser'
import {
  getStoredIsHighContrastMode,
  loadGameStateFromLocalStorage,
  saveGameStateToLocalStorage,
  setStoredIsHighContrastMode,
  saveLevelResult,
} from './lib/localStorage'
import {
  findFirstUnusedReveal,
  getGameLevel,
  isWinningWord,
  solution,
  unicodeLength,
} from './lib/words'
import { useReward } from 'react-rewards';
import { Md5 } from 'ts-md5';

function App() {
  const gameLevel = getGameLevel()
  const prefersDarkMode = window.matchMedia(
    '(prefers-color-scheme: dark)'
  ).matches

  const { showError: showErrorAlert, showSuccess: showSuccessAlert } =
    useAlert()
  
  const { reward: passwordReward, isAnimating: isPasswordAnimating } = 
    useReward(
      'rewardId', 
      'confetti',
      {
        elementCount: 250
      }
    );

  const [isShifted, setIsShifted] = useState(false)
  const [isKeyboardActive, toggleKeyboardActive] = useState(
    localStorage.getItem('keyboard') === "on"
  )
  const [currentGuess, setCurrentGuess] = useState('')
  const [isGameWon, setIsGameWon] = useState(false)
  const [isInfoModalOpen, setIsInfoModalOpen] = useState(false)
  const [isLevelSelectOpen, setIsLevelSelectOpen] = useState(false)
  const [isSettingsModalOpen, setIsSettingsModalOpen] = useState(false)
    const [currentRowClass, setCurrentRowClass] = useState('')
  const [isGameLost, setIsGameLost] = useState(false)
  const [isDarkMode, setIsDarkMode] = useState(
    localStorage.getItem('theme')
      ? localStorage.getItem('theme') === 'dark'
      : prefersDarkMode
      ? true
      : false
  )
  const [isHighContrastMode, setIsHighContrastMode] = useState(
    getStoredIsHighContrastMode()
  )
  const [isRevealing, setIsRevealing] = useState(false)
  const [guesses, setGuesses] = useState<string[]>(() => {
    const loaded = loadGameStateFromLocalStorage(false)
    if (loaded?.solution !== solution) {
      return []
    }
    const gameWasWon = loaded.guesses.includes(solution)
    if (gameWasWon) {
      setIsGameWon(true)
    }
    if (loaded.guesses.length === MAX_CHALLENGES && !gameWasWon) {
      setIsGameLost(true)
      showErrorAlert(CORRECT_WORD_MESSAGE(solution), {
        persist: true,
      })
    }
    return loaded.guesses
  })

  const [isHardMode, setIsHardMode] = useState(
    localStorage.getItem('gameMode')
      ? localStorage.getItem('gameMode') === 'hard'
      : false
  )

  useEffect(() => {
    // if no game state on load,
    // show the user the how-to info modal
    if (!loadGameStateFromLocalStorage(true)) {
      localStorage.setItem('gameState', JSON.stringify({}))
      setTimeout(() => {
        setIsInfoModalOpen(true)
      }, WELCOME_INFO_MODAL_MS)
    }
  }, [])

  useEffect(() => {
    DISCOURAGE_INAPP_BROWSERS &&
      isInAppBrowser() &&
      showErrorAlert(DISCOURAGE_INAPP_BROWSER_TEXT, {
        persist: false,
        durationMs: 7000,
      })
  }, [showErrorAlert])

  useEffect(() => {
    if (isDarkMode) {
      document.documentElement.classList.add('dark')
    } else {
      document.documentElement.classList.remove('dark')
    }

    if (isHighContrastMode) {
      document.documentElement.classList.add('high-contrast')
    } else {
      document.documentElement.classList.remove('high-contrast')
    }
  }, [isDarkMode, isHighContrastMode])

  const handleDarkMode = (isDark: boolean) => {
    setIsDarkMode(isDark)
    localStorage.setItem('theme', isDark ? 'dark' : 'light')
  }

  const handleHardMode = (isHard: boolean) => {
    if (guesses.length === 0 || localStorage.getItem('gameMode') === 'hard') {
      setIsHardMode(isHard)
      localStorage.setItem('gameMode', isHard ? 'hard' : 'normal')
    } else {
      showErrorAlert(HARD_MODE_ALERT_MESSAGE)
    }
  }

  const handleHighContrastMode = (isHighContrast: boolean) => {
    setIsHighContrastMode(isHighContrast)
    setStoredIsHighContrastMode(isHighContrast)
  }

  const clearCurrentRowClass = () => {
    setCurrentRowClass('')
  }

  useEffect(() => { 
    saveGameStateToLocalStorage(true, { guesses, solution })
  }, [guesses])

  useEffect(() => {
    if (isGameWon) {
      saveLevelResult(Number(gameLevel), 'win');
      const winMessage =
        WIN_MESSAGES[Math.floor(Math.random() * WIN_MESSAGES.length)]
      const delayMs = REVEAL_TIME_MS * (solution.length)
      showSuccessAlert(winMessage, {
        delayMs,
        onClose: () => setTimeout(() => {
          setIsLevelSelectOpen(true)
        }, 2 * REVEAL_TIME_MS),
      })
    }

    if (isGameLost) {
      saveLevelResult(Number(gameLevel), 'loss');
      setTimeout(() => {
        setIsLevelSelectOpen(true)
      }, (solution.length + 1) * REVEAL_TIME_MS * 2)
    }
  }, [gameLevel, isGameWon, isGameLost, showSuccessAlert])

  const onChar = (value: string) => {
    if (
      unicodeLength(`${currentGuess}${value}`) <= solution.length &&
      guesses.length < MAX_CHALLENGES &&
      !isGameWon
    ) {
      setCurrentGuess(`${currentGuess}${value}`)
    }
  }

  const onShift = () => {
    setIsShifted(!isShifted)
  }

  const toggleKeyboard = () => {
    // change not yet taken effect, so save the opposite!
    localStorage.setItem('keyboard', !isKeyboardActive ? "on" : "off")
    toggleKeyboardActive(!isKeyboardActive)
  }

  const onDelete = () => {
    setCurrentGuess(
      new GraphemeSplitter().splitGraphemes(currentGuess).slice(0, -1).join('')
    )
  }

  const showCurrentHash = () => {
    if (isGameWon) {
      return Md5.hashStr(solution)
    }
    if (isRevealing) {
      return HASH_MESSAGES[Math.floor(Math.random() * HASH_MESSAGES.length)]
    }
    if (currentGuess.length === solution.length) {
      return ":) ?"
    } else if (currentGuess.length === 0) {
      return "..."
    } else {
      return Md5.hashStr(currentGuess)
    }
  }

  const onEnter = () => {
    if (isGameWon || isGameLost) {
      return
    }

    if (!(unicodeLength(currentGuess) === solution.length)) {
      setCurrentRowClass('jiggle')
      return showErrorAlert(NOT_ENOUGH_LETTERS_MESSAGE, {
        onClose: clearCurrentRowClass,
      })
    }

    // enforce hard mode - all guesses must contain all previously revealed letters
    if (isHardMode) {
      const firstMissingReveal = findFirstUnusedReveal(currentGuess, guesses)
      if (firstMissingReveal) {
        setCurrentRowClass('jiggle')
        return showErrorAlert(firstMissingReveal, {
          onClose: clearCurrentRowClass,
        })
      }
    }

    setIsRevealing(true)
    // turn this back off after all
    // chars have been revealed
    setTimeout(() => {
      setIsRevealing(false)
    }, REVEAL_TIME_MS * solution.length)

    const winningWord = isWinningWord(currentGuess)

    if (
      unicodeLength(currentGuess) === solution.length &&
      guesses.length < MAX_CHALLENGES &&
      !isGameWon
    ) {
      setGuesses([...guesses, currentGuess])
      setCurrentGuess('')

      if (winningWord) {
        passwordReward();
        return setIsGameWon(true)
      }

      if (guesses.length === MAX_CHALLENGES - 1) {
        setIsGameLost(true)
        showErrorAlert(CORRECT_WORD_MESSAGE(solution), {
          persist: true,
          delayMs: 2 * REVEAL_TIME_MS * (solution.length + 1),
        })
      }
    }
  }

  return (
    <Div100vh>
      <div className="flex h-full flex-col">
        <Navbar
          setIsInfoModalOpen={setIsInfoModalOpen}
          setIsLevelSelectOpen={setIsLevelSelectOpen}
          setIsSettingsModalOpen={setIsSettingsModalOpen}
          toggleKeyboard={toggleKeyboard}
          isKeyboardActive={isKeyboardActive}
        />
        <div className="flex items-center justify-center">
          <div className="mb-4 text-xl font-bold text-gray-800 dark:text-gray-300 underline">Level {gameLevel}</div>
        </div>
        <div className="ml-12 flex items-center justify-center">
            <div className="mb-4 text-l text-gray-800 dark:text-gray-300" dangerouslySetInnerHTML={{__html: LEVELS[Number(gameLevel)-1]}} />
        </div>
        <div className="mx-auto flex w-full grow flex-col px-1 pt-2 pb-8 sm:px-6 md:max-w-7xl lg:px-8 short:pb-2 short:pt-2">
          <div className="flex grow flex-col justify-center pb-6 short:pb-2">
            <Grid
              solution={solution}
              guesses={guesses}
              currentGuess={currentGuess}
              isRevealing={isRevealing}
              currentRowClassName={currentRowClass}
            />
            <button disabled={isPasswordAnimating} >
              <span id="rewardId" />
            </button>
          </div>
          <div className="flex items-center justify-center">
            <div className="text-s font-mono text-yellow-600 dark:text-blue-300">
              {showCurrentHash()}
            </div>
          </div>
          <div className="flex items-center justify-center">
            <div className="text-s font-mono text-red-500 dark:text-purple-300">
              {Md5.hashStr(solution)}
            </div>
          </div>
          <Keyboard
            onChar={onChar}
            onDelete={onDelete}
            onEnter={onEnter}
            onShift={onShift}
            solution={solution}
            guesses={guesses}
            isRevealing={isRevealing}
            isShifted={isShifted}
            isKeyboardActive={isKeyboardActive}
          />
          <InfoModal
            isOpen={isInfoModalOpen}
            handleClose={() => setIsInfoModalOpen(false)}
          />
          <LevelSelectModal
            isOpen={isLevelSelectOpen}
            handleClose={() => setIsLevelSelectOpen(false)}
          />
          <SettingsModal
            isOpen={isSettingsModalOpen}
            handleClose={() => setIsSettingsModalOpen(false)}
            isHardMode={isHardMode}
            handleHardMode={handleHardMode}
            isDarkMode={isDarkMode}
            handleDarkMode={handleDarkMode}
            isHighContrastMode={isHighContrastMode}
            handleHighContrastMode={handleHighContrastMode}
          />
          <AlertContainer />
        </div>
      </div>
    </Div100vh>
  )
}

export default App
