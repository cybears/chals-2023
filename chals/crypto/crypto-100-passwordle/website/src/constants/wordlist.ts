export const WORDS = [
  'password',                       // level 1
  '25742',                          // level 2 ALPHA = 25742
  'god',                            // level 3
  'Password01',                     // level 4
  'ILoveEnerg0n!',                  // level 5
  '30150225',                       // level 6
  'd!nobotLover09',                 // level 7
  'deMystifier',                    // level 8
  'Correct Horse Battery Staple.',  // level 9
]