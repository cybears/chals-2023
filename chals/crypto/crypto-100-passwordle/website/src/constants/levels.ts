export const LEVELS: string[] = [

    
    // Level 1
    `<!-- Level 1 -->
    • The user <i>Bumblebear</i> set the password for their hypersonic cruiser </br>
    • All you know is that they hate computers and are very lazy </br>
    • Don't overthink it, this is the first level! </br>
    <!-- End Level 1 -->`, 

    // Level 2
    `<!-- Level 2 -->
    • The password is a 5 digit PIN </br>
    • It is used to lock the target's <b>SecureVault</b> communicator app </br>
    • This app does not let consecutive digits repeat in a passcode </br>
    &nbsp&nbsp&nbsp<i>eg. <span style="color:green">12134</span> is valid </br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <span style="color:red">73346</span> is not. <br></i>
    • You get six tries before the application locks down!
    <!-- End Level 2 -->`, 
    
    // Level 3
    `<!-- Level 3 -->
    • An ancient Earth system you are targetting has a <b>root</b> user </br>
    • The database is an emulated 1981 SPARC server, that does not support special characters </br>
    • The bot who owns the account a big fan of the 1995 movie <b>Hackers</b> </br>
    • You can see in their inbox a message titled: <br>
    <br>&nbsp&nbsp&nbsp<i>"Carefully prepared memo on commonly-used passwords"</i>
    <!-- End Level 3 -->`, 
    
    // Level 4
    `<!-- Level 4 -->
    • <i>Bumblebear</i> from Level 1 has come to your attention again. </br>
    • You try their old password but it fails to authenticate </br>
    • Reading company emails, you see their new IT policy mandates: <br>
    &nbsp&nbsp&nbsp - at least one <b>lowercase</b> letter; and </br>
    &nbsp&nbsp&nbsp - at least one <b>uppercase</b> letter; and </br>
    &nbsp&nbsp&nbsp - at least two <b>numbers</b>. </br>
    • This user is still lazy.. So what is their new password? </br>
    <!-- End Level 4 -->`, 
    
    // Level 5
    `<!-- Level 5 -->
    • You have forgotten your own password! </br>
    • Usually you write them down on a sticky note under your keyboard ...</i></br>
    • <b>Success</b>! You find the following: </br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i><s>I<3Energon</s> </br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i><s>EnergonIsMyLifeForce!</s> </br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i><s>PowerForCybeartron!</s> </br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i><s>#ILoveBricks</s> </br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i>Matrix0fLeadership</br>
    <!-- End Level 5 -->`, 
    
    // Level 6
    `<!-- Level 6 -->
    • A violent revolutionary is planning an attack on a peaceful planet </br>
    • Reading through the intelligence available, you discover the following: </br>
    &nbsp&nbsp&nbsp - <b>Name: &nbsp</b> Ursine Magnus </br>
    &nbsp&nbsp&nbsp - <b>Date of Manufacture (Earth date): &nbsp&nbsp&nbsp&nbsp</b> February 25, 3015. </br>
    &nbsp&nbsp&nbsp - <b>Other: &nbsp</b> Expert in hand-to-hand combat and heavy weapons. </br>
    • What is the 8 digit PIN to the revolutionary's communicator? </br>
    <!-- End Level 6 -->`, 
    
    // Level 7
    `<!-- Level 7 -->
    • Target <b>Ravager</b> has a login credential username: <b>Grimlock</b> </br>
    • Their accounts on other systems are named: <b>Slag</b>, <b>Swoop</b>, and <b>Snarl</b> </br>
    • Ravagers's organisation forces password reset each month </br>
    • What was their password on 25 September, 3022? </br>
    <!-- End Level 7 -->`, 
    
    // Level 8
    `<!-- Level 8 -->
    • You have received intelligence that included a photograph of your target's desk</br>
    • Underneath their stained energon mug is a copy of an ancient earth newspaper</br>
    • The puzzle pages are on top, with the following clue circled in the Times Cryptic: </br>
    <span style="color:orange">
    <br><i>After half tide, <u>M</u>rs. Fire's husband heard an individual 
    clearing up perplexing circumstances (11)</i></span>
    <!-- End Level 8 -->`, 
    
    // Level 9 
    `<!-- Level 9 -->
    • An avid reader of the xkcd webcomic has encrypted a critical file </br>
    • The file is called <b>936.zip</b> </br>
    • They needed a simple way to share the password with their covert agent on another planet </br>
    • ... but this user is a stickler for proper grammar. </br>
    <!-- End Level 9 -->`, 
]
