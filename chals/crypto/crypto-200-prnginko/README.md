# pRNGinko

* _author_: Cybears:squarebear
* _title_: pRNGinko
* _points_: 200
* _tags_:  crypto,rev

## Attachments
* `handout/prnginko`: Server binary

## Notes - Build
* build the server binary
* `docker build -t prnginko-build -f Dockerfile.build .`
* `docker run -it --rm -v $(pwd):/build --workdir /build prnginko-build make`
* 
* run and solve locally:
* `socat -d TCP-LISTEN:3141,reuseaddr,fork EXEC:"prnginko"`

* run and test with docker
* `docker build -t prnginko -f Dockerfile.nsjail .`
* `docker run -it --rm -p 2323:2323 --privileged prnginko`

* Then 
* `nc localhost 2323` to connect

* Build the solver and test
* `docker build -t prnginko-test -f Dockerfile.test .`
* `docker run --network=host -it --rm prnginko-test --workdir / python3 solve.py HOST=127.0.0.1 PORT=2323`

