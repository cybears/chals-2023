// C99 + __uint128_t MWC, 128 bits of state, period approx. 2^127

/* The state must be neither all zero, nor x = 2^64 - 1, c = MWC_A1 -
   1. The condition 0 < c < MWC_A1 - 1 is thus sufficient. */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <stdbool.h>
#include <setjmp.h>

#define E_OK 0
#define E_NO_FLAG 1
#define E_CHAL_FAIL 2
#define E_SEED_FAIL 3

#define MAX_TURNS 10
#define MWC_A1 0xff3a275c007b8ee6

int bit_pos = -1;
uint32_t rng_val = 0;
uint32_t prng_state = 0;
uint32_t prng_shift = 1;
int buckets[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};

jmp_buf ptr_setup1;
jmp_buf ptr_setup2;

uint32_t (*ptr_next)(void) = NULL;

int (*ptr_layer_0_0)(void) = NULL;

int (*ptr_layer_1_0)(void) = NULL;
int (*ptr_layer_1_1)(void) = NULL;

int (*ptr_layer_2_0)(void) = NULL;
int (*ptr_layer_2_1)(void) = NULL;
int (*ptr_layer_2_2)(void) = NULL;

int (*ptr_layer_3_0)(void) = NULL;
int (*ptr_layer_3_1)(void) = NULL;
int (*ptr_layer_3_2)(void) = NULL;
int (*ptr_layer_3_3)(void) = NULL;

int (*ptr_layer_4_0)(void) = NULL;
int (*ptr_layer_4_1)(void) = NULL;
int (*ptr_layer_4_2)(void) = NULL;
int (*ptr_layer_4_3)(void) = NULL;
int (*ptr_layer_4_4)(void) = NULL;

int (*ptr_layer_5_0)(void) = NULL;
int (*ptr_layer_5_1)(void) = NULL;
int (*ptr_layer_5_2)(void) = NULL;
int (*ptr_layer_5_3)(void) = NULL;
int (*ptr_layer_5_4)(void) = NULL;
int (*ptr_layer_5_5)(void) = NULL;

int (*ptr_layer_6_0)(void) = NULL;
int (*ptr_layer_6_1)(void) = NULL;
int (*ptr_layer_6_2)(void) = NULL;
int (*ptr_layer_6_3)(void) = NULL;
int (*ptr_layer_6_4)(void) = NULL;
int (*ptr_layer_6_5)(void) = NULL;
int (*ptr_layer_6_6)(void) = NULL;

int (*ptr_layer_7_0)(void) = NULL;
int (*ptr_layer_7_1)(void) = NULL;
int (*ptr_layer_7_2)(void) = NULL;
int (*ptr_layer_7_3)(void) = NULL;
int (*ptr_layer_7_4)(void) = NULL;
int (*ptr_layer_7_5)(void) = NULL;
int (*ptr_layer_7_6)(void) = NULL;
int (*ptr_layer_7_7)(void) = NULL;

int (*ptr_layer_8_0)(void) = NULL;
int (*ptr_layer_8_1)(void) = NULL;
int (*ptr_layer_8_2)(void) = NULL;
int (*ptr_layer_8_3)(void) = NULL;
int (*ptr_layer_8_4)(void) = NULL;
int (*ptr_layer_8_5)(void) = NULL;
int (*ptr_layer_8_6)(void) = NULL;
int (*ptr_layer_8_7)(void) = NULL;
int (*ptr_layer_8_8)(void) = NULL;

void (*ptr_set_pointers_0)(void) = NULL;
void (*ptr_set_pointers_1)(void) = NULL;

void handle_alarm(__attribute__ ((unused)) int sig){
    printf("Times up!\n");
    exit(0);
}


uint32_t rng_value() {
    const __uint64_t t = MWC_A1 * (__uint64_t) prng_state + prng_shift;
    prng_shift = t >> 32;
    return prng_state = t;
}

uint32_t next() {

    uint32_t i;

    if (bit_pos < 0){
        bit_pos = 31;
        rng_val = rng_value();
    }

    i = rng_val >> bit_pos & 1;

    bit_pos--;
    return i;
}

// Layer 8
int layer_8_0() {
    printf(" 16    8     4     2     1     2     4     8    16\n");
    printf("  ^                                               \n");
    buckets[0] += 1;
    return 16;
}

int layer_8_1() {
    printf(" 16    8     4     2     1     2     4     8    16\n");
    printf("       ^                                          \n");
    buckets[1] += 1;
    return 8;
}

int layer_8_2() {
    printf(" 16    8     4     2     1     2     4     8    16\n");
    printf("             ^                                    \n");
    buckets[2] += 1;
    return 4;
}

int layer_8_3() {
    printf(" 16    8     4     2     1     2     4     8    16\n");
    printf("                   ^                              \n");
    buckets[3] += 1;
    return 2;
}

int layer_8_4() {
    printf(" 16    8     4     2     1     2     4     8    16\n");
    printf("                         ^                        \n");
    buckets[4] += 1;
    return 1;
}

int layer_8_5() {
    printf(" 16    8     4     2     1     2     4     8    16\n");
    printf("                               ^                  \n");
    buckets[5] += 1;
    return 2;
}

int layer_8_6() {
    printf(" 16    8     4     2     1     2     4     8    16\n");
    printf("                                     ^            \n");
    buckets[6] += 1;
    return 4;
}
int layer_8_7() {
    printf(" 16    8     4     2     1     2     4     8    16\n");
    printf("                                           ^      \n");
    buckets[7] += 1;
    return 8;
}
int layer_8_8() {
    printf(" 16    8     4     2     1     2     4     8    16\n");
    printf("                                                ^ \n");
    buckets[8] += 1;
    return 16;
}




// Layer 7
int layer_7_0() {
    if (ptr_next() == 0) {
        printf("L   .     .     .     .     .     .     .     .   \n");
        printf("   /                                              \n");
        printf("  /                                               \n");
        return ptr_layer_8_0();
    } else {
        printf("R   .     .     .     .     .     .     .     .   \n");
        printf("     \\                                            \n");
        printf("      \\                                           \n");
        return ptr_layer_8_1();
    }
}

int layer_7_1() {
    if (ptr_next() == 0) {
        printf("L   .     .     .     .     .     .     .     .   \n");
        printf("         /                                        \n");
        printf("        /                                         \n");
        return ptr_layer_8_1();
    } else {
        printf("R   .     .     .     .     .     .     .     .   \n");
        printf("           \\                                      \n");
        printf("            \\                                     \n");
        return ptr_layer_8_2();
    }
}

int layer_7_2() {
    if (ptr_next() == 0) {
        printf("L   .     .     .     .     .     .     .     .   \n");
        printf("               /                                  \n");
        printf("              /                                   \n");
        return ptr_layer_8_2();
    } else {
        printf("R   .     .     .     .     .     .     .     .   \n");
        printf("                 \\                                \n");
        printf("                  \\                               \n");
        return ptr_layer_8_3();
    }
}

int layer_7_3() {
    if (ptr_next() == 0) {
        printf("L   .     .     .     .     .     .     .     .   \n");
        printf("                     /                            \n");
        printf("                    /                             \n");
        return ptr_layer_8_3();
    } else {
        printf("R   .     .     .     .     .     .     .     .   \n");
        printf("                       \\                          \n");
        printf("                        \\                         \n");
        return ptr_layer_8_4();
    }
}

int layer_7_4() {
    if (ptr_next() == 0) {
        printf("L   .     .     .     .     .     .     .     .   \n");
        printf("                           /                      \n");
        printf("                          /                       \n");
        return ptr_layer_8_4();
    } else {
        printf("R   .     .     .     .     .     .     .     .   \n");
        printf("                             \\                    \n");
        printf("                              \\                   \n");
        return ptr_layer_8_5();
    }
}

int layer_7_5() {
    if (ptr_next() == 0) {
        printf("L   .     .     .     .     .     .     .     .   \n");
        printf("                                 /                \n");
        printf("                                /                 \n");
        return ptr_layer_8_5();
    } else {
        printf("R   .     .     .     .     .     .     .     .   \n");
        printf("                                   \\              \n");
        printf("                                    \\             \n");
        return ptr_layer_8_6();
    }
}

int layer_7_6() {
    if (ptr_next() == 0) {
        printf("L   .     .     .     .     .     .     .     .   \n");
        printf("                                       /          \n");
        printf("                                      /           \n");
        return ptr_layer_8_6();
    } else {
        printf("R   .     .     .     .     .     .     .     .   \n");
        printf("                                         \\        \n");
        printf("                                          \\       \n");
        return ptr_layer_8_7();
    }
}

int layer_7_7() {
    if (ptr_next() == 0) {
        printf("L   .     .     .     .     .     .     .     .   \n");
        printf("                                             /    \n");
        printf("                                            /     \n");
        return ptr_layer_8_7();
    } else {
        printf("R   .     .     .     .     .     .     .     .   \n");
        printf("                                               \\  \n");
        printf("                                                \\ \n");
        return ptr_layer_8_8();
    }
}

// Layer 6
int layer_6_0() {
    if (ptr_next() == 0) {
        printf("L      .     .     .     .     .     .     .   \n");
        printf("      /                                        \n");
        printf("     /                                         \n");
        return ptr_layer_7_0();
    } else {
        printf("R      .     .     .     .     .     .     .   \n");
        printf("        \\                                      \n");
        printf("         \\                                     \n");
        return ptr_layer_7_1();
    }
}

int layer_6_1() {
    if (ptr_next() == 0) {
        printf("L      .     .     .     .     .     .     .   \n");
        printf("            /                                   \n");
        printf("           /                                    \n");
        return ptr_layer_7_1();
    } else {
        printf("R      .     .     .     .     .     .     .   \n");
        printf("              \\                                \n");
        printf("               \\                               \n");
        return ptr_layer_7_2();
    }
}

int layer_6_2() {
    if (ptr_next() == 0) {
        printf("L      .     .     .     .     .     .     .   \n");
        printf("                  /                            \n");
        printf("                 /                             \n");
        return ptr_layer_7_2();
    } else {
        printf("R      .     .     .     .     .     .     .   \n");
        printf("                    \\                          \n");
        printf("                     \\                         \n");
        return ptr_layer_7_3();
    }
}

int layer_6_3() {
    if (ptr_next() == 0) {
        printf("L      .     .     .     .     .     .     .   \n");
        printf("                        /                      \n");
        printf("                       /                       \n");
        return ptr_layer_7_3();
    } else {
        printf("R      .     .     .     .     .     .     .   \n");
        printf("                          \\                    \n");
        printf("                           \\                   \n");
        return ptr_layer_7_4();
    }
}

int layer_6_4() {
    if (ptr_next() == 0) {
        printf("L      .     .     .     .     .     .     .   \n");
        printf("                              /                \n");
        printf("                             /                 \n");
        return ptr_layer_7_4();
    } else {
        printf("R      .     .     .     .     .     .     .   \n");
        printf("                                \\              \n");
        printf("                                 \\             \n");
        return ptr_layer_7_5();
    }
}

int layer_6_5() {
    if (ptr_next() == 0) {
        printf("L      .     .     .     .     .     .     .   \n");
        printf("                                    /          \n");
        printf("                                   /           \n");
        return ptr_layer_7_5();
    } else {
        printf("R      .     .     .     .     .     .     .   \n");
        printf("                                      \\        \n");
        printf("                                       \\       \n");
        return ptr_layer_7_6();
    }
}

int layer_6_6() {
    if (ptr_next() == 0) {
        printf("L      .     .     .     .     .     .     .   \n");
        printf("                                          /    \n");
        printf("                                         /     \n");
        return ptr_layer_7_6();
    } else {
        printf("R      .     .     .     .     .     .     .   \n");
        printf("                                            \\  \n");
        printf("                                             \\ \n");
        return ptr_layer_7_7();
    }
}

// Layer 5
int layer_5_0() {
    if (ptr_next() == 0) {
        printf("L         .     .     .     .     .     .   \n");
        printf("         /                                  \n");
        printf("        /                                   \n");
        return ptr_layer_6_0();
    } else {
        printf("R         .     .     .     .     .     .   \n");
        printf("           \\                                \n");
        printf("            \\                               \n");
        return ptr_layer_6_1();
    }
}

int layer_5_1() {
    if (ptr_next() == 0) {
        printf("L         .     .     .     .     .     .   \n");
        printf("               /                            \n");
        printf("              /                             \n");
        return ptr_layer_6_1();
    } else {
        printf("R         .     .     .     .     .     .   \n");
        printf("                 \\                          \n");
        printf("                  \\                         \n");
        return ptr_layer_6_2();
    }
}

int layer_5_2() {
    if (ptr_next() == 0) {
        printf("L         .     .     .     .     .     .   \n");
        printf("                     /                      \n");
        printf("                    /                       \n");
        return ptr_layer_6_2();
    } else {
        printf("R         .     .     .     .     .     .   \n");
        printf("                       \\                    \n");
        printf("                        \\                   \n");
        return ptr_layer_6_3();
    }
}

int layer_5_3() {
    if (ptr_next() == 0) {
        printf("L         .     .     .     .     .     .   \n");
        printf("                           /                \n");
        printf("                          /                 \n");
        return ptr_layer_6_3();
    } else {
        printf("R         .     .     .     .     .     .   \n");
        printf("                             \\              \n");
        printf("                              \\             \n");
        return ptr_layer_6_4();
    }
}

int layer_5_4() {
    if (ptr_next() == 0) {
        printf("L         .     .     .     .     .     .   \n");
        printf("                                 /          \n");
        printf("                                /           \n");
        return ptr_layer_6_4();
    } else {
        printf("R         .     .     .     .     .     .   \n");
        printf("                                   \\        \n");
        printf("                                    \\       \n");
        return ptr_layer_6_5();
    }
}

int layer_5_5() {
    if (ptr_next() == 0) {
        printf("L         .     .     .     .     .     .   \n");
        printf("                                       /    \n");
        printf("                                      /     \n");
        return ptr_layer_6_5();
    } else {
        printf("R         .     .     .     .     .     .   \n");
        printf("                                         \\  \n");
        printf("                                          \\ \n");
        return ptr_layer_6_6();
    }
}

// Layer 4
int layer_4_0() {
    if (ptr_next() == 0) {
        printf("L            .     .     .     .     .      \n");
        printf("            /                               \n");
        printf("           /                                \n");
        return ptr_layer_5_0();
    } else {
        printf("R            .     .     .     .     .      \n");
        printf("              \\                             \n");
        printf("               \\                            \n");
        return ptr_layer_5_1();
    }
}

int layer_4_1() {
    if (ptr_next() == 0) {
        printf("L            .     .     .     .     .      \n");
        printf("                  /                         \n");
        printf("                 /                          \n");
        return ptr_layer_5_1();
    } else {
        printf("R            .     .     .     .     .      \n");
        printf("                    \\                       \n");
        printf("                     \\                      \n");
        return ptr_layer_5_2();
    }
}

int layer_4_2() {
    if (ptr_next() == 0) {
        printf("L            .     .     .     .     .      \n");
        printf("                        /                   \n");
        printf("                       /                    \n");
        return ptr_layer_5_2();
    } else {
        printf("R            .     .     .     .     .      \n");
        printf("                          \\                 \n");
        printf("                           \\                \n");
        return ptr_layer_5_3();
    }
}

int layer_4_3() {
    if (ptr_next() == 0) {
        printf("L            .     .     .     .     .      \n");
        printf("                              /             \n");
        printf("                             /              \n");
        return ptr_layer_5_3();
    } else {
        printf("R            .     .     .     .     .      \n");
        printf("                                \\           \n");
        printf("                                 \\          \n");
        return ptr_layer_5_4();
    }
}

int layer_4_4() {
    if (ptr_next() == 0) {
        printf("L            .     .     .     .     .      \n");
        printf("                                    /       \n");
        printf("                                   /        \n");
        return ptr_layer_5_4();
    } else {
        printf("R            .     .     .     .     .      \n");
        printf("                                      \\     \n");
        printf("                                       \\    \n");
        return ptr_layer_5_5();
    }
}

// Layer 3
int layer_3_0() {
    if (ptr_next() == 0) {
        printf("L               .     .     .     .         \n");
        printf("               /                            \n");
        printf("              /                             \n");
        return ptr_layer_4_0();
    } else {
        printf("R               .     .     .     .         \n");
        printf("                 \\                          \n");
        printf("                  \\                         \n");
        return ptr_layer_4_1();
    }
}

int layer_3_1() {
    if (ptr_next() == 0) {
        printf("L               .     .     .     .         \n");
        printf("                     /                      \n");
        printf("                    /                       \n");
        return ptr_layer_4_1();
    } else {
        printf("R               .     .     .     .         \n");
        printf("                       \\                    \n");
        printf("                        \\                   \n");
        return ptr_layer_4_2();
    }
}

int layer_3_2() {
    if (ptr_next() == 0) {
        printf("L               .     .     .     .         \n");
        printf("                           /                \n");
        printf("                          /                 \n");
        return ptr_layer_4_2();
    } else {
        printf("R               .     .     .     .         \n");
        printf("                             \\              \n");
        printf("                              \\             \n");
        return ptr_layer_4_3();
    }
}

int layer_3_3() {
    if (ptr_next() == 0) {
        printf("L               .     .     .     .         \n");
        printf("                                 /          \n");
        printf("                                /           \n");
        return ptr_layer_4_3();
    } else {
        printf("R               .     .     .     .         \n");
        printf("                                   \\        \n");
        printf("                                    \\       \n");
        return ptr_layer_4_4();
    }
}

// Layer 2
int layer_2_0() {
    if (ptr_next() == 0) {
        printf("L                  .     .     .            \n");
        printf("                  /                         \n");
        printf("                 /                          \n");
        return ptr_layer_3_0();
    } else {
        printf("R                  .     .     .            \n");
        printf("                    \\                       \n");
        printf("                     \\                      \n");
        return ptr_layer_3_1();
    }
}

int layer_2_1() {
    if (ptr_next() == 0) {
        printf("L                  .     .     .            \n");
        printf("                        /                   \n");
        printf("                       /                    \n");
        return ptr_layer_3_1();
    } else {
        printf("R                  .     .     .            \n");
        printf("                          \\                 \n");
        printf("                           \\                \n");
        return ptr_layer_3_2();
    }
}

int layer_2_2() {
    if (ptr_next() == 0) {
        printf("L                  .     .     .            \n");
        printf("                              /             \n");
        printf("                             /              \n");
        return ptr_layer_3_2();
    } else {
        printf("R                  .     .     .            \n");
        printf("                                \\           \n");
        printf("                                 \\          \n");
        return ptr_layer_3_3();
    }
}

// Layer 1
int layer_1_0() {
    if (ptr_next() == 0) {
        printf("L                     .     .               \n");
        printf("                     /                      \n");
        printf("                    /                       \n");
        return ptr_layer_2_0();
    } else {
        printf("R                     .     .               \n");
        printf("                       \\                    \n");
        printf("                        \\                   \n");
        return ptr_layer_2_1();
    }
}

int layer_1_1() {
    if (ptr_next() == 0) {
        printf("L                     .     .               \n");
        printf("                           /                \n");
        printf("                          /                 \n");
        return ptr_layer_2_1();
    } else {
        printf("R                     .     .               \n");
        printf("                             \\              \n");
        printf("                              \\             \n");
        return ptr_layer_2_2();
    }
}

// Layer 0
int layer_0_0() {
    if (ptr_next() == 0) {
        printf("L                        .                  \n");
        printf("                        /                   \n");
        printf("                       /                    \n");
        return ptr_layer_1_0();
    } else {
        printf("R                        .                  \n");
        printf("                          \\                 \n");
        printf("                           \\                \n");
        return ptr_layer_1_1();
    }
}

int load_flag(char* in_param, size_t size){
    FILE* fp = fopen("./flag.txt", "r");
    if (fp == NULL){
        return -E_NO_FLAG;
    }
    fread(in_param, size, 1, fp);
    fclose(fp);
}

int set_seed(){
    struct timespec up_time;
    int res;
    res = clock_gettime(CLOCK_BOOTTIME, &up_time);
    if (res != 0) {
        return -E_SEED_FAIL;
    }
    prng_state = up_time.tv_nsec * 1000000000 + up_time.tv_sec;
    return -E_OK;
}

void print_welcome(){
    printf("~~  Welcome to pRNGinko   ~~\n");
    printf("The amazing RNG Plinko game!\n");
    printf("!!Sponsored by SportsBear!!\n !!Gamble Reversingly!!\n\n");
    printf("I bet you can't get a perfect 10 game score...\n");
    printf("Help:\n");
    printf("g - play a game!\n");
    printf("p - let me practice!\n");
}

void print_prompt(int total_score, int turns_left){
    printf("Your score is %d\n", total_score);
    printf("You have %d/10 games left!\n", turns_left);
    printf("> ");
}

int input_loop(){
    int in_c;
    int turns_left = MAX_TURNS;
    int total_score = 0;
    char flag[255] = {0};
    bool handled = false;

    print_welcome();
    print_prompt(total_score, turns_left);

    while (turns_left > 0){
        in_c = getchar();
        switch (in_c){
            case 'g':
                total_score += ptr_layer_0_0();
                turns_left--;
                handled = true;
                break;
            case 'p':
                ptr_layer_0_0();
                handled = true;
                break;
            default:
                break;
        }

        if (handled){
            print_prompt(total_score, turns_left);
            handled = false;
        }
    }

    if (total_score == MAX_TURNS * 16){
        load_flag(flag, sizeof(flag) - 1);
        printf("Woah! You beat SportsBear!\n"
               "Better take your flag and get out of here before he breaks your legs!\n%s\n", flag);
        return -E_OK;
    } else {
        printf("Better luck rng_value time!\n");
        return -E_CHAL_FAIL;
    }

}

void set_pointers_0(){
    ptr_layer_8_0 = &layer_8_0;
    ptr_layer_7_6 = &layer_7_6;
    ptr_layer_4_4 = &layer_4_4;
    ptr_layer_3_0 = &layer_3_0;
    ptr_layer_0_0 = &layer_0_0;
    ptr_layer_7_5 = &layer_7_5;
    ptr_layer_5_0 = &layer_5_0;
    ptr_layer_8_1 = &layer_8_1;
    ptr_layer_6_6 = &layer_6_6;
    ptr_layer_4_0 = &layer_4_0;
    ptr_layer_8_4 = &layer_8_4;
    ptr_layer_4_3 = &layer_4_3;
    ptr_layer_6_2 = &layer_6_2;
    ptr_next = &next;
    ptr_layer_2_0 = &layer_2_0;
    ptr_layer_6_1 = &layer_6_1;
    ptr_layer_8_2 = &layer_8_2;
    ptr_layer_3_1 = &layer_3_1;
    ptr_layer_2_2 = &layer_2_2;
    ptr_layer_3_3 = &layer_3_3;
    ptr_layer_8_3 = &layer_8_3;
    ptr_layer_5_3 = &layer_5_3;
    ptr_layer_6_0 = &layer_6_0;
    ptr_layer_3_2 = &layer_3_2;
}

void set_pointers_1(){
    ptr_layer_6_4 = &layer_6_4;
    ptr_layer_7_0 = &layer_7_0;
    ptr_layer_5_4 = &layer_5_4;
    ptr_layer_8_5 = &layer_8_5;
    ptr_layer_4_1 = &layer_4_1;
    ptr_layer_5_1 = &layer_5_1;
    ptr_layer_7_2 = &layer_7_2;
    ptr_layer_8_7 = &layer_8_7;
    ptr_layer_6_3 = &layer_6_3;
    ptr_layer_7_4 = &layer_7_4;
    ptr_layer_8_6 = &layer_8_6;
    ptr_layer_1_0 = &layer_1_0;
    ptr_layer_2_1 = &layer_2_1;
    ptr_layer_7_1 = &layer_7_1;
    ptr_layer_5_2 = &layer_5_2;
    ptr_layer_6_5 = &layer_6_5;
    ptr_layer_7_3 = &layer_7_3;
    ptr_layer_4_2 = &layer_4_2;
    ptr_layer_7_7 = &layer_7_7;
    ptr_layer_1_1 = &layer_1_1;
    ptr_layer_5_5 = &layer_5_5;
    ptr_layer_8_8 = &layer_8_8;
}


void ptr_init_1(){
    ptr_set_pointers_1 = set_pointers_1;
    longjmp(ptr_setup2, 1);
}


void ptr_init_0(){
    ptr_set_pointers_0 = set_pointers_0;

    if (setjmp(ptr_setup2)){
        ptr_set_pointers_1();
    } else {
        ptr_init_1();
    }

    longjmp(ptr_setup1, 1);
}

int main(__attribute__ ((unused)) int argc, __attribute__ ((unused)) char *argv[]) {
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);

    signal(SIGALRM, handle_alarm);
    alarm(60 * 5);

    if (set_seed() < 0){
        return -EXIT_FAILURE;
    }

    if (setjmp(ptr_setup1)){
        ptr_set_pointers_0();
    } else {
        ptr_init_0();
    }

    return input_loop();
}
