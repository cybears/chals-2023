import logging

from pwn import *

# log.level = logging.DEBUG

# Set up pwntools for the correct architecture
exe = None
if args.LOCAL:
    exe = context.binary = ELF(args.EXE or 'prnginko')

host = args.HOST or '127.0.0.1'
port = int(args.PORT or 2323)

gdbscript = '''
tbreak main
continue
'''.format(**locals())

i32 = 0xffffffff
i64 = 0xffffffffffffffff
c = 18391055304419413734
x = 0
y = 0
current_rng_val = 0
byte_offset = -1


def generate_new_number():
    global x, y
    intval = (x * c + y) & i64

    x = intval & i32
    y = intval >> 32
    return x, y


def next():
    global byte_offset, current_rng_val

    if byte_offset < 0:
        byte_offset = 3
        current_rng_val = generate_new_number()[0]

    mask = 0xff << (byte_offset * 8)
    masked_val = (current_rng_val & mask) >> (byte_offset * 8)
    byte_offset -= 1
    return masked_val


def find_good_state():
    global x, y

    i = 0
    val = next()
    while True:
        if val == 0x00 or val == 0xff:
            break
        val = next()
        i += 1

    return i


def solve_rng(obs1, obs2):
    global x, y
    intval = (i32 + 1 + obs2 - (obs1 * c & i32)) & i32
    x = obs1
    y = intval
    return generate_new_number()


def getnum(p):
    # Play 4 practice games and extract the game data
    # some pwntools thing here
    binary = ''
    for i in range(4):
        p.sendline(b'p')
        res = p.recvuntil(b'>').decode('utf-8')

        for line in res.splitlines():
            line = line.strip()
            if line.startswith('L'):
                binary += '0'
            elif line.startswith('R'):
                binary += '1'
            else:
                pass

    return int(binary, 2)


def practice_game(p, attempts=1):
    # Chunk the number of attempts up into blocks of
    # 50 plays
    block_size = 100
    blocks = attempts // block_size
    remainder = attempts % block_size

    for _ in range(blocks):
        p.sendline(b'p' * block_size)
        for i in range(block_size):
            res = p.recvuntil(b'>')
            debug(res.decode())

    # Remainder of games
    p.sendline(b'p' * remainder)
    for i in range(remainder):
        res = p.recvuntil(b'>')
        debug(res.decode())

def play_game(p):
    # some pwntools thing here
    p.sendline(b'g')
    res = p.recvuntil(b'>')
    info(res.decode())


def start_local(argv=[], *a, **kw):
    '''Execute the target binary locally'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([exe.path] + argv, *a, **kw)


def start_remote(argv=[], *a, **kw):
    '''Connect to the process on the remote host'''
    global host, port

    io = connect(host, port)
    if args.GDB:
        gdb.attach(io, gdbscript=gdbscript)
    return io


def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.LOCAL:
        return start_local(argv, *a, **kw)
    else:
        return start_remote(argv, *a, **kw)


instance = start()
preamble = instance.readuntil(b">")
info(preamble.decode())

# Get the first two numbers so that we can see where our RNG is at
obs1 = getnum(instance)
obs2 = getnum(instance)
x, y = solve_rng(obs1, obs2)

# Validate the RNG
check = generate_new_number()[0]
test_num = getnum(instance)
assert check == test_num

# Advance through winning games
for _ in range(10):
    num_practice_runs = find_good_state()
    practice_game(instance, num_practice_runs)
    play_game(instance)
    sleep(0.5)


flag = instance.recv(2048, timeout=1).decode('utf-8')

# Hack
if flag.strip() == "":
    flag = instance.recv(2048, timeout=1).decode('utf-8')

if 'cybears{' in flag:
    success(flag)
else:
    error("Could not find flag!")

instance.close()
