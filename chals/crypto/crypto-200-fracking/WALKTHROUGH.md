# crypto-200-Fracking

This challenge is a challenge that takes the fractional part of the square-root of a number, and asks you to recover that original number

## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

## Solution
* Let `N` be an integer between `0` and `2^n`, that is not a perfect square, given `r = sqrt(N) - [sqrt(N)]`, to some precision greater than `n`, how do you recover `N`?
* Let's write `k` for `[sqrt(N)]`, and observe that `(k+r)^2=k^2+2kr+r^2=N`.

* Setting `m = N-k^2` this means we are looking for integers `k`, `l` such that `2kr - m = -r^2`.

* To find these two integers, we first define `p = 2^(n+4)`, where `n` is the bit size of `N`, and then generate a lattice using the vectors
`(1, 0, 2pr)`, `(0, 1, -p)`
* This lattice contains the point `(k, m, 2kpr-pm) = (k, m, -pr^2)`

* Since we have chosen `p` to be large compared to `k` and `m`, this point is very close to the point `(0, 0, -pr^2)`, so finding the closest lattice point to this point will give us the desired two integers `k` and `m`, and allow us to reconstruct `N`.


## References
* Sophie Schmeig discussed this problem on twitter in response to Crown Sterling discussing a crypto-system (possible factoring algorithm??) based on this property. Needless to say, it is not secure as a crypto-system.
* https://twitter.com/SchmiegSophie/status/1477016397179879430



</details>


