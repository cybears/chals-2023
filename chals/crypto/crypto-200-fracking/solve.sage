import Crypto.Cipher.AES as AES
from Crypto.Util.number import long_to_bytes, bytes_to_long
from binascii import *
import json

n=256

# extract the challenge values (cipher and redacted fractional part)
with open("output.json", "r") as f: 
	d = f.read()
j = json.loads(d)
cipher = unhexlify(j['cipher'])
r = RealNumber(j['key'].replace("[REDACTED]","0"))

# build recovery matrix
p = 2**(n+4)
b1 = [1,0,int(2*p*r)]
b2 = [0,1,-p]

M = matrix([b1,b2])

from sage.modules.free_module_integer import IntegerLattice
# Directly taken from rbtree's LLL repository
# From https://oddcoder.com/LOL-34c3/, https://hackmd.io/@hakatashi/B1OM7HFVI
def Babai_CVP(mat, target):
    M = IntegerLattice(mat, lll_reduce=True).reduced_basis
    G = M.gram_schmidt()[0]
    diff = target
    for i in reversed(range(G.nrows())):
        diff -=  M[i] * ((diff * G[i]) / (G[i] * G[i])).round()
    return target - diff

# solve closest vector problem to recover full N
target = vector([0,0, int(-p*r**2)])
(k,m,_) = Babai_CVP(M, target)
recovered_N = m+k**2

# check that this N is the correct N used to decrypt cipher
# Note that this solve script doesn't work on _all_ outputs generated from the generation script, likely some tweaking needed on the lattice parameters
recovered_key = long_to_bytes(recovered_N)
if len(recovered_key) == 32:
	a = AES.new(recovered_key, mode=AES.MODE_ECB)
	if b'cybears' in a.decrypt(cipher):
		print("FLAG FOUND!")
		exit(int(0))
else:
	print("unlikely correct result")
	exit(int(-1))

exit(int(-1))

