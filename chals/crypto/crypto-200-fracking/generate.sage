import Crypto.Cipher.AES as AES
from Crypto.Util.number import long_to_bytes, bytes_to_long
from binascii import *
from cybears import flag
import json

n=256
N = 4
count = 0
while(is_squarefree(N) == False or len(long_to_bytes(N)) != 32 ):
	#print(f"count: ({count})")
	count+=1
	N = random_prime(2**(n))
	
a = AES.new(long_to_bytes(N), mode=AES.MODE_ECB)
cipher = a.encrypt(flag)

j = {"cipher": hexlify(cipher).decode(), "key": "[REDACTED].{}".format(N.sqrt(prec=(n+n//2)).frac().str()[2:]) }

print(json.dumps(j))


