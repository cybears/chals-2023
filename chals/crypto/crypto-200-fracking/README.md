# Fracking

* _author_: Cybears:cipher
* _title_: Fracking
* _points_: 200
* _tags_:  crypto

## Intro
* Crypto challenge to recover a value given the _fractional_ part of the square-root of the value

## Attachments
* `output.json`: Generated ciphertext and hint
* `generate.sage`: script to generate cipher and hint

## Notes (build)
* `docker build --network=host -t frac_solver -f Dockerfile.solve .`
* `docker run -it --rm -v $(pwd):/tmp -w /tmp  frac_solver sage solve.sage`

