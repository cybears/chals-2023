#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stddef.h>
#include <sys/prctl.h>
#include <sys/syscall.h>

#include "filter.h"

// These macros are defined at compile time.
// WIN_NAME - Name of the win function.
// BUF_LEN - Length of the buffer to overflow.

void librarian();


__attribute__((naked))
void reg()
{
    asm(
        "pop %rdi\n"
        "ret\n"
    );
}

__attribute__((naked))
void ret()
{
    asm(
        "ret\n"
    );
}


// Use a puts thunk here to avoid providing any library pointers.
int _puts(const char *s)
{
    return puts(s);
}

// We need our own exit because the regular one runs afoul of seccomp.
void __exit(int status)
{
    syscall(__NR_exit_group, status);
}

int main (int argc, char *argv[])
{
    // Create a large stack variable to allow room for the ROP chain.
    char *_dummy;
    _dummy = alloca(4 * 1024 * 1024); // 4MB

    if(0 != setvbuf(stdin, NULL, _IONBF, 0))
    {
        perror("setvbuf");
        exit(-99);
    }
    if(0 != setvbuf(stdout, NULL, _IONBF, 0))
    {
        perror("setvbuf");
        exit(-99);
    }

    // Need to have at least one puts/printf call before the prctl to allow glibc to initialise stuff.
    puts("");

    int ret = filter_syscalls();
    if(0 != ret)
    {
        fprintf(stderr, "Seccomp Error: %d", ret);
        exit(-99);
    }

    librarian();
    __exit(0);
    return 0;
}

void librarian()
{
    char buffer[BUF_LEN];

    puts("Hello. I am the Librarian.\n");
    puts("Let me tell you about our services.");
    // Can't actually use the real 'main()' here because that would
    // run functions that are now forbidden by seccomp.
    printf("   The 'main' desk is at: %p\n", &librarian);
    printf("   If you wish to register for an RDI card please see the staff at: %p\n", &reg);
    printf("   You will find 'puts' in our .text section at: %p\n", &_puts);
    printf("   If you need to return anything you may do so at: %p\n", &ret);
    printf("   Patrons may use the buffer for research. It is only %d bytes. Please do not overflow it.\n", BUF_LEN);
    printf("   The library will be closing soon. The exit is at: %p\n", &__exit);
    puts("\n");
    puts("I am currently looking for this tome: " WIN_NAME);
    puts("If you find it please call its location to notify the staff.\n\n");

    puts("What would you like to research today?\n");
    read(STDIN_FILENO, buffer, BUF_LEN + 64);
}
