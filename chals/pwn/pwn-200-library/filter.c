#include <stddef.h>
#include <seccomp.h>

int filter_syscalls()
{
	int rc;
	scmp_filter_ctx ctx = NULL;

	ctx = seccomp_init(SCMP_ACT_KILL_PROCESS);
	if (ctx == NULL)
		return -1;

    // Native is already added by default.
    //rc = seccomp_arch_add(ctx, SCMP_ARCH_NATIVE);
	//if (rc != 0)
	//	goto out;

	rc = seccomp_rule_add_exact(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0);
	if (rc != 0)
		goto out;

	rc = seccomp_rule_add_exact(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 0);
	if (rc != 0)
		goto out;

	rc = seccomp_rule_add_exact(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);
	if (rc != 0)
		goto out;

	rc = seccomp_rule_add_exact(ctx, SCMP_ACT_ALLOW, SCMP_SYS(rt_sigreturn), 0);
	if (rc != 0)
		goto out;

	rc = seccomp_load(ctx);
	if (rc != 0)
		goto out;

out:
	seccomp_release(ctx);
	return rc;
}