# A Strange Game

* _author_: humphrey_c_bear
* _title_: Wandering Through the Libraries
* _points_: 200
* _tags_: pwn

## Challenge Text
* See MANIFEST.yml

## Handouts
* None

## Notes

### Build the server container
* `docker build -t library -f ./Dockerfile.runner .`

### Run the server
* `docker run -it --rm -p 2323:2323 library`

### Build the test container
* `docker build -t library-test -f Dockerfile.test .`

### Run the test against the server
* `docker run --network=host -it --rm library-test python3 solve.py HOST=127.0.0.1 PORT=2323`
