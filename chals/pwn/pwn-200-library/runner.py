import os
import sys
import subprocess
import random
import string

lib_size = 16       # Number of functions in the library.
iterations = 10     # Number of times the user has to succeed.
timeout = 60        # Time given to the user to exploit the program.


def random_str(length):
    return ''.join(random.choices(string.ascii_letters, k=length))


# Adjust make to run from a temp dir if required.
make_args = ['make']
if len(sys.argv) == 2:
    base_dir = sys.argv[1]
    make_args += ['-f', f'{base_dir}/Makefile', f'VPATH={base_dir}']


# Generate some "books"
fn_list = [random_str(8) for _ in range(lib_size)]
secret_list = [random_str(16) for _ in range(lib_size)]

secret_dict = dict(zip(fn_list, secret_list))

# We precompile the functions to save time later.
# This could also be done per iteration to get unique functions each time if we wanted.
for fn_name, secret in secret_dict.items():
    subprocess.check_output(make_args+[f'fn_{fn_name}.o', f'SECRET=\'"{secret}"\''])

print("""While looking for a particular book in your local library you round a corner 
to find yourself among an unfamiliar set of bookshelves. You have wandered into L-Space!

As you travel through the different libraries you encounter a series of friendly
Librarians. They all seem to be too preoccupied with finding missing books to provide
you with directions home. Maybe if you help them one might have the time to help you
in return. 
"""
)


for _ in range(iterations):
    # Shuffle the library.
    # The order that the files are linked changes where they are placed in the final file.
    random.shuffle(fn_list)
    objs = ' '.join(f'fn_{func}.o' for func in fn_list)

    try:
        os.unlink('library.so')
    except FileNotFoundError:
        pass

    subprocess.check_output(
        make_args+['library.so', f'OBJS={objs}'],
        stderr=subprocess.STDOUT
    )

    try:
        os.unlink('main')
    except FileNotFoundError:
        pass

    # Choose a random winner for the main binary.
    win_fn = random.choice(fn_list)
    secret = secret_dict[win_fn]
    buf_len = random.randint(4, 16) * 8

    # Compile the main binary.
    subprocess.check_output(
        make_args+['main', f'WIN_NAME=\'"{win_fn}"\'', f'BUF_LEN={buf_len}'],
        stderr=subprocess.STDOUT
    )

    # Create the FIFO that we will use to read the success string.
    try:
        os.unlink('./success')
    except FileNotFoundError:
        pass

    os.mkfifo('./success')

    # Opening the FIFO will block so we have to start the process in parallel.
    main_proc = subprocess.Popen(
        ['./main'],
        env={'LD_PRELOAD': './library.so'}
    )

    success = open('./success', 'rb')

    ret_code = None
    try:
        ret_code = main_proc.wait(timeout)
    except subprocess.TimeoutExpired:
        # User took too long.
        print('''"I'm very sorry but the library is closing now."
The Librarian ushers you through a door and you find yourself in an unfamiliar world.
Perhaps you can come back tomorrow.''')
        sys.exit()

    success_data = success.read()

    if len(success_data) == 0:
        # Error during start
        if ret_code == (-99 & 0xff):
            print("""The librarian puts an "Out of Order" sign on the main desk.
"Unfortunately our system seems to have suffered a failure."
"Perhaps you could report it to the helpdesk for me?"

~~~ If you see this message before providing any input then something is ~~~  
~~~ wrong with the challenge. Please report it to the Cybears helpdesk.  ~~~""")

        # Exit without sending data.
        elif ret_code == 0:
            print("""You exit the library to find yourself in a strange land. Turning around you find
the library has closed. Perhaps you can come back tomorrow.""")

        # Crash or something else.
        else:
            print("""As you are searching you bump into one of the shelves and it comes crashing down.

"I'm sorry but if you cannot behave properly you will have to leave."
The Librarian ushers you to outside and you find yourself in a strange world.
Turning around you find the library has closed. Perhaps you can come back tomorrow.""")

    elif success_data == secret.encode():

        if ret_code == 0:
            print(""""Thank you very much." The Librarian smiles and directs you past a nearby set of shelves.
As you wander the shelves start to change and you find yourself in another library.""")
            # Success! This is the only path that should continue the loop.
            continue

        else:
            # Success but it crashed?
            print("""You find the book that the Librarian is looking for but it seems to be propping up
a bookshelf. You try to gently pry it out but the shelf comes crashing down.

"I'm sorry but if you cannot behave properly you will have to leave."
The Librarian ushers you to outside and you find yourself in a strange world.
When you find the book tomorrow you resolve to exit quietly after you are done.""")

    else:
        # Wrong function.
        print("""You see what looks like the lost book and shout for the Librarian but as you 
get closer you realise it is not the correct title.

"I'm sorry but you are making too much noise. You will have to leave."
The Librarian ushers you to the exit and you find yourself outside in a strange world.""")

    # Failure of any kind exits.
    sys.exit()

# Success
print("""Finally you recognise where you are. You run to the exit to find yourself back at home.""")
with open("flag.txt", "r") as flag:
    print(flag.read())
