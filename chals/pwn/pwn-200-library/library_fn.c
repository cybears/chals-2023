#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// These macros are defined at compile time.
// SECRET - Secret that is used to indicate success.
// FN_NAME - Name of the win function.

extern int out_pipe;

void FN_NAME()
{
    write(out_pipe, SECRET, sizeof(SECRET)-1);
}

