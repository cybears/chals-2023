#!/bin/bash

BASE_DIR=$(pwd)

# Create a temporary directory and build everything there.
TMP_DIR=$(mktemp -d)
cp flag.txt "${TMP_DIR}"

cd "${TMP_DIR}" || exit

PYTHONUNBUFFERED=True python3 "${BASE_DIR}"/runner.py "${BASE_DIR}"

cd /
rm -rf "${TMP_DIR}"
