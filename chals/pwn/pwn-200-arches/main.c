#define _GNU_SOURCE
#include <fcntl.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

// These macros are defined at compile time.
// WIN_TEXT - Win text to be sent over the pipe.
// BUF_LEN - Length of the buffer to overflow.

static int pipe_fd;
static char secret[] = SECRET;

#define CHECK_ERR(x, msg) {if((x) != 0){perror(msg); _exit(-99);}}
#define CHECK_NEG(x, msg) {if((x) < 0){perror(msg); _exit(-99);}}

void whisper(char * input)
{
    char * value = input;

    write(pipe_fd, value, strlen(value));
    exit(0);
}

void vuln()
{
    char buffer[BUF_LEN];
    read(STDIN_FILENO, buffer, BUF_LEN + 64);
}


// Needs to be a nested call to allow for architectures that have an LR.
void call_vuln()
{
    puts("I have inscribed a secret message on this arch. Do you think you can find it?");
    puts("You should WHISPER the SECRET to me so eavesdroppers won't hear.");
    vuln();
}

void init()
{
    CHECK_ERR(setvbuf(stdin, NULL, _IONBF, 0), "setvbuf")
    CHECK_ERR(setvbuf(stdout, NULL, _IONBF, 0), "setvbuf")

    pipe_fd = open("./success", O_WRONLY);
    CHECK_NEG(pipe_fd, "open");

    CHECK_ERR(chroot("./chroot"), "chroot");
    CHECK_ERR(chdir("/"), "chdir");

    CHECK_ERR(setresgid(1000, 1000, 1000), "setresgid");
    CHECK_ERR(setresuid(1000, 1000, 1000), "setresuid");
}

int main (int argc, char *argv[])
{
    int stack_var;

    init();

    printf("The architect points to a one side of the arch. \"Look at the beautiful stack over here: %p\"\n", &stack_var);

    call_vuln();
    return 0;
}
