import os
import sys
import subprocess
import random
import string
import base64
import resource
import shutil

timeout = 30     # Time given to the user to exploit the program.

# Progression from familiar to unfamiliar
arch_list = ["i686", "aarch64", "riscv64"]

# How unfamiliar do we want to make it?
#arch_list += ["mips64el", "sparc64", "s360x"]

def random_str(length):
    return ''.join(random.choices(string.ascii_letters, k=length))



# Adjust make to run from a temp dir if required.
make_args = ['make']
if len(sys.argv) == 2:
    base_dir = sys.argv[1]
    make_args += ['-f', f'{base_dir}/Makefile', f'VPATH={base_dir}']

print("Hello. I am the architect. I designed these wonderful arches. Would you like me to give you a tour?")
response = input("[y/N]")

if not response.lower().startswith("y"):
    sys.exit()

for arch in arch_list:
    try:
        os.unlink('main')
    except FileNotFoundError:
        pass

    try:
        shutil.rmtree('./chroot')
    except FileNotFoundError:
        pass
    os.mkdir('./chroot')

    secret = random_str(16)
    buf_len = random.randint(4, 16) * 8

    # Compile the main binary.
    # spr = subprocess.Popen(
    subprocess.check_output(
        make_args+[f"CC={arch}-linux-gnu-gcc", 'main', f'SECRET=\'"{secret}"\'', f'BUF_LEN={buf_len}'],
        stderr=subprocess.STDOUT
    )
    # spr.wait()

    print(f"This arch is in the style of {arch}.")
    print(f"Let me show you the Base64 blueprint. Careful, it is quite large.")
    with open('main', 'rb') as main_file:
        print(base64.b64encode(main_file.read()).decode())

    # Create the FIFO that we will use to read the success string.
    try:
        os.unlink('./success')
    except FileNotFoundError:
        pass

    os.mkfifo('./success')

    qemu_arch = arch
    if arch == 'i686':
        qemu_arch = 'i386'

    # Opening the FIFO will block so we have to start the process in parallel.
    main_proc = subprocess.Popen([f'qemu-{qemu_arch}-static', './main'], env={"QEMU_LD_PREFIX":f"/usr/{arch}-linux-gnu/"})

    # Prevent creating new processes or opening new files.
    resource.prlimit(main_proc.pid, resource.RLIMIT_NPROC, (0, 0))
    #resource.prlimit(main_proc.pid, resource.RLIMIT_NOFILE, (0, 0))

    success = open('./success', 'rb')

    ret_code = None
    try:
        ret_code = main_proc.wait(timeout)
    except subprocess.TimeoutExpired:
        # User took too long.
        print('''As you inspect the arch you realise you have taken too long and the Architect has wandered off and
is busy talking to someone else. You will have to come back another time.''')
        sys.exit()

    success_data = success.read()
    if success_data.strip() == secret.encode():
        print("""You spot the inscription and excitedly point it out.
"Fantastic! I knew you could do it." The Architect smiles and hurries onwards.""")
        continue

    if len(success_data) == 0:
        # Error during start
        if ret_code == (-99 & 0xff):
            print("""The Architect gets a strange look. "Hmm, something has damaged this arch."
"Perhaps you could find a contractor to fix it?"

~~~ If you see this message before providing any input then something is ~~~  
~~~ wrong with the challenge. Please report it to the Cybears helpdesk.  ~~~""")

        # Exit without sending data.
        elif ret_code == 0:
            print("""You look but you cannot find the secret message. The architect looks disappointed.
"I don't feel like continuing right now. Why don't you come back another time.\"""")

        else:
            # Crash
            print("""You are a bit too aggressive in searching for the inscription and the arch collapses.
You quickly leave while the architect is not looking. Maybe you can try again another time.""")
    else:
        print("""You confidently whisper to the architect. The architect looks disappointed.
    \"That is not the secret.\"""")
    # Failure of any kind exits.
    sys.exit()

# Success
print("""As you pass the last arch the Architect turns to you. "I rarely meet anyone as passionate about arches as I am."
Please have this flag as a memento of your visit.""")
with open("flag.txt", "r") as flag:
    print(flag.read())
