#include "Item.h"
#include <io.h>
#include <fcntl.h>
#include <windows.h>
#include <map>

using std::cin;
using std::cout;
using std::string;

static std::map<string, std::unique_ptr<DataItem>> global_map;
char *date_string = (char *)"date /T";

static void clear_line()
{
  string temp;
  std::getline(cin, temp);
  cin.clear();
}

char get_char()
{
  char input;
  while (!(cin.get(input)))
  {
    clear_line();
  }
  clear_line();
  return input;
}

static string get_key()
{
  string key;
  cout << "key name: ";
  cin >> key;
  clear_line();
  return key;
}

static void get_item(void)
{
  auto key = get_key();
  auto search = global_map.find(key);
  if (search != global_map.end())
  {
    cout << search->first << " : ";
    search->second->print();
    cout << endl;
  }
}

static void set_item(void)
{
  auto key = get_key();
  auto search = global_map.find(key);
  if (search != global_map.end())
  {
    search->second->set();
  }
  else
  {
    global_map[key] = std::make_unique<DataItem>();
    global_map[key]->set();
    if (!global_map[key]->valid())
    {
      global_map.erase(key);
    }
  }
}

static void delete_item(void)
{
  auto key = get_key();
  if (global_map.erase(key))
  {
    cout << "deleted" << endl;
  }
  else
  {
    cout << "key not found" << endl;
  }
}

static void clone_item(void)
{
  cout << "clone ";
  auto cloneKey = get_key();
  cout << "new ";
  auto newKey = get_key();
  auto search = global_map.find(cloneKey);
  if (search != global_map.end())
  {
    global_map[newKey] = std::make_unique<DataItem>(*search->second);
  }
  else
  {
    cout << "key not found" << endl;
  }
}

void help()
{
  cout << "g\tGet a Value" << endl;
  cout << "s\tSet a Value" << endl;
  cout << "d\tDelete a Value" << endl;
  cout << "c\tClone a Value" << endl;
  cout << "q\tQuit" << endl;
}

static void menu()
{
  char ch = '\0';
  do
  {
    cout << "Menu > ";
    ch = get_char();
    switch (ch)
    {
    case 'g':
      get_item();
      break;
    case 's':
      set_item();
      break;
    case 'd':
      delete_item();
      break;
    case 'c':
      clone_item();
      break;
    case 'h':
      help();
      break;
    default:
      cout << ch;
      break;
    }
  } while (ch != 'q');
  cout << endl;
}
void __declspec(noinline)  welcome_message()
{
    cout << "Welcome to Memcrashd, a production ready remote key value store."
        << endl;
    cout << "The current time is " << system(date_string) << endl;
}

int main()
{

  _setmode(_fileno(stdout), _O_BINARY);
  setvbuf(stdout, NULL, _IONBF, 0);
  std::cin.exceptions(std::istream::failbit | std::istream::badbit | std::istream::eofbit);
  std::cout.exceptions(std::ostream::failbit | std::ostream::badbit | std::istream::eofbit);

  date_string = (char*)malloc(10);
  strcpy(date_string, "date /T");
  // std::ios::sync_with_stdio(true);
  // setvbuf(stdin, 0LL, 2LL, 0LL);
  // setvbuf(stdout, 0LL, 2LL, 0LL);
  welcome_message();
  help();
  menu();

  return 0;
}
