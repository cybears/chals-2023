#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#define FLAG_LEN 0x200

const char *adjectives[5] = {
    "Mighty",
    "Amazing",
    "Courageous",
    "Invincible",
    "Legendary"
};
const char *valid_animal[5] = {
    "snail",
    "dragon",
    "tiger",
    "leopard",
    "fly",
};

#define COLOR_LEN 0x20
#define ANIMAL_LEN 0x80
__attribute__((noinline))
int generate(void) {

    char color[COLOR_LEN]; // = {0};
    char animal[ANIMAL_LEN]; // = {0};
    char *readString;
    bool valid = false;

    printf("To get started I need your favourite animal: ");
    readString = fgets(animal, ANIMAL_LEN, stdin);
    if(readString != NULL) {
        animal[strcspn(animal, "\n")] = 0;
        for(int i = 0; i< sizeof(valid_animal) / sizeof(valid_animal[0]); i++) {
            if(0 == strcmp(animal, valid_animal[i])) {
                valid = true;
                break;
            }
        }
    }
    if(valid) {
        printf("\nAnd the color of shirt you are wearing now: "); 
        readString = fgets(color, ANIMAL_LEN, stdin);

        if(readString != NULL) {
            color[strcspn(color, "\n")] = 0;
            printf("Asking our AI");
            for(int i=0; i< 4; i++) {
                //sleep(1);
                printf("."); 
            }
            printf("\nYour super hero name is: The %s %s %s\n", adjectives[rand() % 5], color, animal); 
        }
    } else {
        printf("\nIm not sure your ready to be a super hero.");
    }
    return 0;
}
int main(void)
{
    time_t t;
    setvbuf(stdin, 0LL, 2LL, 0LL);
    setvbuf(stdout, 0LL, 2LL, 0LL);
    srand((unsigned) time(&t));


    printf("Welcome to our Super Hero name generator.\n"); 
    return generate();
}


void readFlag() {
    char buffer[128] = {0};
    FILE * flag_file = fopen("flag.txt", "r");
    if (flag_file)
    {
        fread(buffer, 1, sizeof(buffer), flag_file);
        fclose(flag_file);
        printf("%s\n", buffer);
    }
    else
    {
        printf("Good start, you have got execution locally. Now try it against the competition server\n");
    }
}
