// gcc -fsanitize=address -O2 -o temple-of-aswan temple-of-aswan.c
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>


struct auth_block_t {
	char buf[8];
	uint32_t cookie;
};

#define MAGIC_VALUE 0x424344

/*
 * Get an unsigned long from stdin and store it in *number.
 *
 * Returns NULL if an error occurred (EOF encountered or invalid input)
 *
 * Returns number on success
 */
unsigned long *get_number(unsigned long *number)
{
#define INPUT_SIZE 255
    char line[INPUT_SIZE] = {0};
    char *endptr = NULL;
    unsigned long ret = 0;

    if (fgets(line, INPUT_SIZE, stdin) == NULL) {
        return NULL;
    }

    /* Remove the \n from the input so that strtoul error
     * checking is accurate
     */

    line[strcspn(line, "\n")] = '\0';

    errno = 0;

    ret = strtoul(line, &endptr, 10);

    if (errno == 0 && (*endptr == '\0' && line[0] != '\0')) {
        *number = ret;
        return number;
    }

    return NULL;
}

void read_flag() {
    char buffer[128] = {0};
    FILE * flag_file = fopen("flag.txt", "r");
    if (flag_file)
    {
        printf("Flag: \n");
        fread(buffer, 1, sizeof(buffer), flag_file);
        fclose(flag_file);
        printf("%s\n", buffer);
    }
    else
    {
        printf("Good start, you have re-directed control flow locally. Now try it against the competition server\n");
    }
}

void vuln() {
	struct auth_block_t auth = { .buf = "", .cookie = 0 };
  size_t gift_size;  // = 0;

	puts("Offer a gift to enter:");
  puts("\nEnter size in bytes of the gift: ");

  if (get_number(&gift_size) == NULL) {
    printf("Invalid input (gift_size = %ld)\n", gift_size);
    // return;
  }

  puts("\nEnter gift as ASCII characters: ");

  if (fgets(auth.buf, gift_size, stdin) == NULL) {
    printf("Invalid input\n");
    return;
  }

	// printf("auth.cookie = 0x%x\n", auth.cookie);

	// strncpy(auth.buf, "AAAAAAAABBB", gift_size);

	printf("auth.buf    = %s\n",   auth.buf);
	printf("auth.cookie = 0x%x\n", auth.cookie);

	if (auth.cookie == MAGIC_VALUE) {
		read_flag();
	} else {
    printf("\nCookie incorrect!! The Temple of ASwAN door's remain closed to you!!!!\n");
  }
}

int main(void) {

  setbuf(stdin, NULL);
  setbuf(stdout, NULL);
  setbuf(stderr, NULL);

	puts("==============================\n");
	puts("Welcome to the Temple of ASwAN\n");
	puts("==============================\n");
	puts("134cf24 (HEAD -> main) After having trouble with nefarious visitors lately, enable additional protections in prod.");
	puts("936c790 add: cookie field to auth struct, this should keep them out");
	puts("fd49801 fix: still can't get the buf field in auth struct right. Maybe this fixes it?");
	puts("\n\n");

	vuln();

	return 0;
}
