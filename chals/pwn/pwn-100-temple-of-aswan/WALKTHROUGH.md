# pwn-100-temple-of-aswan

This challenge is a beginner challenge that takes a classic buffer overflow but complicates exploitation by the presence of libasan (ASan) in the runtime environment.

## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

## Solution

As a stack canary and Address Sanitizer (ASan) protect the saved return address, an alternative exploitation strategy needs to be identified:
 * There are a number of memory corruption bugs that ASan does not detect.
    - Overflowing of adjacent buffers within a C structure is the bug class that is relevant here.
 * Challenge sets up a pretty common length + arbitrary buffer that can be submitted to the challenge over stdin.
 * To help the CTFer identify ASan's effect quickly, the default value for the `gift_size` is uninitialized.
    - This means an initial run of the challenge executable whilst pressing 'Enter' repeatedly will print out the random, large `gift_size` by default --- hopefully indicating what type of values might trigger an uncontrolled overflow.
 * Instead of overflowing the saved return address, a partial overflow of the `uint32_t cookie` next in the struct will avoid triggering the ASan checks.
 * Reverse engineering of the comparison within vulnerable function should indicate that the constant value used to check against is ASCII printable.
 * Careful construction of the 12 character ASCII buffer and length to overflow into the `uint32_t cookie`, but not into the adjacent ASan stack redzone will pass the check and then the executable will print out the flag.

</details>
