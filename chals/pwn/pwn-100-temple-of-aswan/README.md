# Temple of ASwAN

* _author_: Cybears:c5e2f8f21143
* _title_: Temple of ASwAN
* _points_: 100
* _tags_: pwn

## Challenge Text
* See MANIFEST.yml

## Handouts
* `temple-of-aswan`: compiled executable

## Notes - Build
requires:
* libasan

* Run `make && ./solve.py --local` (with flag.txt in same directory)

## Notes - Docker

### Build the binary in the build container
* `docker run -v $(pwd):/build -it --rm --workdir /build registry.gitlab.com/cybears/base-images/builder--build-tools make`

### Build the server container
* `docker build -t temple-of-aswan .`

### Run the server
* `docker run -it --rm -p 2323:2323 --privileged temple-of-aswan`


## References
* Bugs ASan doesn't detect (https://nandynarwhals.org/bugs-asan-doesnt-detect/)
* Harder ASan-enabled CTFs (https://faraz.faith/2020-11-23-dragonctf-bitmapmanager/ and https://github.com/agadient/CTF/tree/master/0ctf_2019/baby_aegis)
