# pwn-300-secureshell

This challenge turns a 1-byte NULL overwrite into a cryptographic type-confusion bug that allows you to forge GCM tags with an unknown encryption and authentication key.

## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

## Solution


### pwn
* The initial vulnerability is in the `update` function, it uses a strcpy and doesn't account for the appending of the null-byte at the end of a string that is `MAX_COMMAND_LENGTH` long. The command struct that holds the encrypted and authenticated commands looks like: 

```
typedef struct {
        unsigned int cipherlen;
        byte nonce[ENC_GCM_NONCE_LEN];
        byte cipher[MAX_ENCRYPTED_COMMAND_LEN];
        byte alg_id;
        byte tag[ENC_GCM_TAG_LEN];
} encrypted_command;

```

The null-byte overwrites the algorithm ID, making it 0, putting it into CTR mode. This provides a decryption oracle, as now the server will return the decrypted message, even if the tag is incorrect. 

### crypto
Three things are needed to forge a GCM cipher text and tag with an unknown key:

1. `ECB(K,0)` and `ECB(K, IV<<32|1)` are needed to create the GHASH key for GCM authentication
2. `CTR(K, cmd, IV<<32|2)`

Note that the nonce in both GCM and CTR mode is 96-bits (12-bytes) and it is shifted left before the counter is OR'd into the bottom. Also, for GCM, the counter starts at 2. 

#### ECB(K,0)
To recover this, we use the CTR mode decryption oracle and call it with IV=0, cipher=0

This returns `CTR-DEC(K,cipher=0, IV=0) = ECB(K,0) XOR 0 = ECB(K,0)`

#### ECB(K, IV<<32|1)
To reover this, we use the CTR mode decryption oracle and call it with IV=IV, cipher = 0 and send two blocks

This returns `CTR-DEC(K, cipher=0||0, IV=IV) = ECB(K,IV<<32|0) XOR 0 || ECB(K,IV<<32|1) XOR 0`

We grab the second block of this

#### CTR(K,cmd,IV<<32|2)
To recover this, we use the CTR mode decryption oracle and call it with IV=IV, cipher = cmd and send three blocks `A || A || cmd`

This returns `CTR-DEC(K, cipher = A || A || cmd, IV=IV) = ECB(K, IV<<32|0) XOR A || ECB(K, IV<<32|1) XOR A || ECB (K, IV<<32|2) XOR cmd`

The third block (`ECB(K, IV<<32|2) XOR cmd`) is equivalent to `CTR-ENC(K, cmd, IV)`

We reconstruct the auth-tag of this cipher and then submit the IV, cipher and forged tag to the server to run!

Full script in `solve.py`

## References
* This CTF challenge is inspired by the AWS: In-band key negotiation issue in the AWS S3 Crypto SDK for golang
   * https://github.com/google/security-research/security/advisories/GHSA-7f33-f4f5-xwgw

</details>


