# SecureShell

* _author_: Cybears:cipher
* _title_: Secureshell
* _points_: 300
* _tags_:  pwn,crypto

## Attachments
* `handout/secureshell`: Server binary

## Notes - Build
* run and solve locally:
* `socat -d TCP-LISTEN:3141,reuseaddr,fork EXEC:"secureshell"`

* run and test with docker
* `docker build -t ss -f Dockerfile.nsjail .`
* `docker run -it --rm -p 2323:2323 --name ss_test ss `

* Then 
* `nc localhost 2323` to connect

## References
* Pure python GCM implementation modified from https://github.com/bozhu/AES-GCM-Python
