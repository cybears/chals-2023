#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#include "libsecureshell.h"

unsigned int is_hex_string(unsigned char * s)
{
    return s[strspn((const char *)s, "0123456789abcdefABCDEF")] == 0;
}

unsigned int is_odd_length(unsigned char *s)
{
    return (strlen((const char *)s) & 1) == 0;
}

unsigned int is_multiple_of_blocklen(unsigned char *s)
{
    return (strlen((const char *)s) % AES_BLOCK_LEN_HEX) == 0;
}

void convert_hex_to_bytes(unsigned char *s, unsigned char *output)
{
    unsigned int i=0, len;
    unsigned int temp, ret;

    len = strlen((const char *)s);
    //printf("DEBUG: strlen(s) was: %d\n", len);

    for(i=0; i<len/2; i++)
    {
        //ret = sscanf((const char *)s[2*i], "%02x", &temp);
        ret = sscanf((const char *)s+2*i, "%02x", &temp);
        //printf("DEBUG: sscanf returned: %d\n", ret);
        //printf("[%d] - %c%c = %d\n", i, s[2*i], s[2*i+1], temp);
        if (1!=ret)
        {
            printf("ERROR in hex conversion. Exiting...\n");
            return;
        }
        output[i] = (unsigned char) (temp &0xff);
    }

    output[i] = 0; //NULL terminate string

    return;
}


int encrypt_gcm(byte *plain, unsigned int plainlen, byte *key, byte *nonce, byte *cipher, byte *tag)
{
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	int cipherlen = 0, tmplen = 0;

	EVP_EncryptInit_ex(ctx, EVP_aes_128_gcm(), NULL, key, nonce);
    	if (!EVP_EncryptUpdate(ctx, cipher, &cipherlen, plain, plainlen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_ENC_GCM_UPDATE;
    	}
    	/*
     	* Buffer passed to EVP_EncryptFinal() must be after data just
     	* encrypted to avoid overwriting it.
     	*/
    	if (!EVP_EncryptFinal_ex(ctx, cipher + cipherlen, &tmplen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_ENC_GCM_FINAL;
   	 }
    	cipherlen += tmplen;

	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_GET_TAG, ENC_GCM_TAG_LEN, tag);

    	EVP_CIPHER_CTX_free(ctx);


	return SUCCESS;
}

int decrypt_gcm(byte *cipher, unsigned int cipherlen, byte *key, byte *nonce, byte *decrypted, byte *tag)
{
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	int  tmplen = 0, decryptedlen = 0;


	EVP_DecryptInit_ex(ctx, EVP_aes_128_gcm(), NULL, key, nonce);
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_AEAD_SET_TAG, ENC_GCM_TAG_LEN, tag);
    	if (!EVP_DecryptUpdate(ctx, decrypted, &decryptedlen, cipher, cipherlen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_DEC_GCM_UPDATE;
    	}
    	/*
     	* Buffer passed to EVP_EncryptFinal() must be after data just
     	* encrypted to avoid overwriting it.
     	*/
    	if (!EVP_DecryptFinal_ex(ctx, decrypted + decryptedlen, &tmplen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_DEC_GCM_FINAL;
   	 }
    	decryptedlen += tmplen;

    	EVP_CIPHER_CTX_free(ctx);

	return SUCCESS;
}

int encrypt_ctr(byte *plain, unsigned int plainlen, byte *key, byte *nonce, byte *cipher)
{
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	int cipherlen = 0, tmplen = 0;
	int ret = 0;
	byte ctr_nonce[ENC_CTR_NONCE_LEN] ={};

	//The nonce from GCM is 12-bytes. Pad this out to 16-bytes
	memcpy(ctr_nonce, nonce, 12);

	ret = EVP_EncryptInit_ex(ctx, EVP_aes_128_ctr(), NULL, key, ctr_nonce);
	printf("DEBUG: enc ctr init ret: %d\n", ret);
	
    	if (!EVP_EncryptUpdate(ctx, cipher, &cipherlen, plain, plainlen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_ENC_CTR_UPDATE;
    	}
    	/*
     	* Buffer passed to EVP_EncryptFinal() must be after data just
     	* encrypted to avoid overwriting it.
     	*/
    	if (!EVP_EncryptFinal_ex(ctx, cipher + cipherlen, &tmplen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_ENC_CTR_FINAL;
   	 }
    	cipherlen += tmplen;
    	EVP_CIPHER_CTX_free(ctx);


	return SUCCESS;
}

int decrypt_ctr(byte *cipher, unsigned int cipherlen, byte *key, byte *nonce, byte *decrypted)
{
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	int tmplen = 0, decryptedlen = 0;

	byte ctr_nonce[ENC_CTR_NONCE_LEN] ={};

	//The nonce from GCM is 12-bytes. Pad this out to 16-bytes
	memcpy(ctr_nonce, nonce, 12);

	EVP_DecryptInit_ex(ctx, EVP_aes_128_ctr(), NULL, key, ctr_nonce);
    	if (!EVP_DecryptUpdate(ctx, decrypted, &decryptedlen, cipher, cipherlen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_DEC_CTR_UPDATE;
    	}
    	/*
     	* Buffer passed to EVP_EncryptFinal() must be after data just
     	* encrypted to avoid overwriting it.
     	*/
    	if (!EVP_DecryptFinal_ex(ctx, decrypted + decryptedlen, &tmplen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_DEC_CTR_FINAL;
   	 }
    	decryptedlen += tmplen;

    	EVP_CIPHER_CTX_free(ctx);

	return SUCCESS;
}

int encrypt_ecb(byte *plain, unsigned int plainlen, byte *key, byte *cipher)
{
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	int cipherlen = 0, tmplen = 0;

	EVP_EncryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, NULL);
	EVP_CIPHER_CTX_set_padding(ctx, 0); //disable padding - raw ECB    
	if (!EVP_EncryptUpdate(ctx, cipher, &cipherlen, plain, plainlen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_ENC_ECB_UPDATE;
    	}
    	/*
     	* Buffer passed to EVP_EncryptFinal() must be after data just
     	* encrypted to avoid overwriting it.
     	*/
    	if (!EVP_EncryptFinal_ex(ctx, cipher + cipherlen, &tmplen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_ENC_ECB_FINAL;
   	 }
    	cipherlen += tmplen;

    	EVP_CIPHER_CTX_free(ctx);

	return SUCCESS;
}

int decrypt_ecb(byte *cipher, unsigned int cipherlen, byte *key, byte *decrypted)
{
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
	int  i, tmplen = 0, decryptedlen = 0;


	EVP_DecryptInit_ex(ctx, EVP_aes_128_ecb(), NULL, key, NULL);
	EVP_CIPHER_CTX_set_padding(ctx, 0); //disable padding - raw ECB    
    	if (!EVP_DecryptUpdate(ctx, decrypted, &decryptedlen, cipher, cipherlen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_DEC_ECB_UPDATE;
    	}
    	/*
     	* Buffer passed to EVP_EncryptFinal() must be after data just
     	* encrypted to avoid overwriting it.
     	*/
    	if (!EVP_DecryptFinal_ex(ctx, decrypted + decryptedlen, &tmplen)) {
		/* Error */
		EVP_CIPHER_CTX_free(ctx);
		return ERROR_DEC_ECB_FINAL;
   	 }
    	decryptedlen += tmplen;

	printf("DEBUG: decrypted [%d]: ", decryptedlen);
	for (i=0;i<decryptedlen; i++)
	{
		printf("%02x", decrypted[i]);
	} printf("\n");
	

    	EVP_CIPHER_CTX_free(ctx);


	return SUCCESS;
}


