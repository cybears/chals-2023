from pwn import * 
from binascii import *
from Crypto.Util.number import long_to_bytes, bytes_to_long
import argparse

def update_slot(s, slot, nonce_hex, cipher_hex, tag_hex, debug=False):
    s.sendline("3")
    s.sendline(str(slot))
    r = s.readuntil("(hex):") #nonce (12 hex)
    #print(r)

    if(len(nonce_hex) != 12*2) or (len(tag_hex) != 16*2):
        log.warn("warning: nonce or tag length not correct")
    if(len(cipher_hex) > 100*2):
        log.warn("warning: cipher length too long")

    s.sendline(nonce_hex)
    s.readuntil("(hex):") #cipher
    s.sendline(cipher_hex)
    s.readuntil("(hex):") #tag (16 hex)
    s.sendline(tag_hex)
    r = s.readuntil("choice:")
    if debug==True:
        print(r)
    return

def list_cmd(s, slot, expected_len, debug=False):
    s.sendline("1") #list
    r = s.readuntil("choice:")
    
    if (debug == True):
        print(r)

    header = 'COMMAND [{}]: '.format(slot).encode() 

    #cmd_line = r.split(b'\n')[1]
    if header not in r:
        log.failure("ERROR: something went wrong, can't find command")
        return False, b""

    #cmd = cmd_line.split(b":")[1].strip()

    start = r.find(header) + len(header)

    if expected_len == None:
        end = r.find(b'\n', start)
        return True, r[start:end]

    else:
        return True, r[start:start+expected_len]

    #if(len(cmd) != expected_len):
        #this could happen if there is a ":" in the cipher, or a "\n", or a few other weird cases. Should be rare, just rerun
    #    log.failure("ERROR: something went wrong, cmd length wrong")
    #    log.failure("ERROR: got {}, len {}".format(hexlify(cmd), len(cmd)))
    #    return False, b""

    #return True, cmd

def run_command(s, slot, debug=False):
    s.sendline("2")
    s.sendline(str(slot))
    r = s.readuntil("choice:")
    if (debug ==True):
        print(r)
    
    #output1 = r.split(b'\n')[2]
    if(b'RUNNING COMMAND' in r):
        #log.info(output1)
        #output2 = r.split(b'\n')[3]
        #log.info(output2)
        return r
    else:
        log.failure("ERROR: in running command")
        return b""

if __name__ == "__main__":

    success = False
    retries = 0
    max_retries = 10

    while(success == False):

        log.info("############################")
        log.info("Attempt #{}".format(retries))

        if retries > max_retries: 
            log.failure("Maximum retries attempted - exitting")
            exit(-1)

        DEBUG = False

        parser = argparse.ArgumentParser()
        parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge", required=False)
        parser.add_argument('-l', '--liveness', help="If remote - just check the server is responding", required=False)
        args = parser.parse_args()

        if args.liveness != None:
            if args.remote == None:
                log.failure("-r/--remote required for liveness check")
                exit(-1)
            host = args.remote.split(":")[0]
            port = int(args.remote.split(":")[1])
            p = remote(host, port)
            try:
                r = p.readuntil("choice:")
            except Exception as e:
                log.failure("ERROR: liveness check - could not connect to host {}".format(e))
                exit(-1)
            log.success("SUCCESS: Connected to server, exitting")
            p.close()
            exit(0)

        if args.remote != None:
            host = args.remote.split(":")[0]
            port = int(args.remote.split(":")[1])
            p = remote(host, port)
        else:
            p = process("./secureshell")

        r = p.readuntil("choice:")
        #print(r)

        ## 0. Send overwrite to change GCM to CTR (slot 0)
        update_slot(p,0, "BB"*12, "CC"*100, "DD"*16, debug=DEBUG)

        ## 1. Send 0 to recover ECB(K,0)
        # send all 0's as nonce, and all 0's as cipher
        # As CTR, this will return E(K,0) XOR '\x00'*16
        log.info("Step 1: Send 0 to recover ECB(K,0)")

        update_slot(p,0, "00"*12, "00"*16, "DD"*16, debug=DEBUG)
        ret, ECB_K_0 = list_cmd(p,0,16,debug=DEBUG)

        if ret != True: 
            log.failure("ERROR: didn't receive expected ECK_K_0")
            #exit(-1)
            p.close()
            retries += 1
            continue

        ## 2. Send IV to recover ECB(K, IV<<32|1)
        # send nonce (12-bytes) and cipher as all 0's (32)
        # As CTR this will return E(K, IV<<32|0) XOR '\x00'*16 ||  E(K, IV<<32|1) XOR '\x00'*16 
        log.info("Step 2: Send IV to recover ECB(K,IV<<32|1)")

        update_slot(p,0, "BB"*12, "00"*32, "DD"*16, debug=DEBUG)
        ret, ECB_K_IV_full = list_cmd(p,0,32,debug=DEBUG)

        if ret != True: 
            log.failure("ERROR: didn't receive expected ECB_K_IV")
            #exit(-1)
            p.close()
            retries += 1
            continue

        ECB_K_IV = ECB_K_IV_full[16:]

        ## 3. Send cmd 'cat flag.txt' to recover CTR(K, cmd, IV=2)
        # send nonce (12-bytes) and cipher as all 0's (32) + cmd
        # As CTR this will return E(K, IV<<32|0) XOR '\x00'*16 ||  E(K, IV<<32|1) XOR '\x00'*16 || E(K, IV<<32|2) XOR cmd
        log.info("Step 3: Send cmd to recover CTR(K,cmd, IV=2)")

        cmd = 'cat flag.txt'
        cmd_hex = hexlify(cmd.encode())
        update_slot(p,0, "BB"*12, "61"*16*2 + cmd_hex.decode(), "DD"*16, debug=DEBUG)
        ret, CTR_cmd_full = list_cmd(p,0,32+len(cmd),debug=DEBUG)

        if ret != True: 
            log.failure("ERROR: didn't receive expected CTR_cmd")
            #exit(-1)
            p.close()
            retries += 1
            continue

        CTR_cmd = CTR_cmd_full[32:]

        ## 4. create forged GCM cmd
        log.info("Step 4: Create forged GCM command")
        import gcm #customised pygcm
        g = gcm.AES_GCM(0)
        g.change_key(0, new_auth_key=bytes_to_long(ECB_K_0))
        forged_tag = g.ghash(b'', CTR_cmd)
        forged_tag ^= bytes_to_long(ECB_K_IV)
        forged_tag_hex = hexlify(long_to_bytes(forged_tag)).decode()
        CTR_cmd_hex = hexlify(CTR_cmd).decode()

        ## 5. update slot 1 with forged command (can't use 0 as the alg_id is stuck at CTR)
        log.info("Step 5: Update slot with forged command")
        update_slot(p,1, "BB"*12, CTR_cmd_hex, forged_tag_hex, debug=DEBUG)

        ## run command in slot 1
        log.info("Step 6: Run command")
        output = run_command(p, 1)

        if (b'cybears' in output):
            log.success("FLAG FOUND!!")
            success = True
            p.close()
            exit(0)
        else:
            log.failure("ERROR: Run command failed [{}]".format(output))
            p.close()
            retries += 1
            continue

        #print("DEBUG")
        #print("key = unhexlify({})".format(key))
        #print("ECB_K_0 = unhexlify({})".format(hexlify(ECB_K_0)))
        #print("ECB_K_IV = unhexlify({})".format(hexlify(ECB_K_IV_full)))
        #print("CTR_cmd = unhexlify({})".format(hexlify(CTR_cmd_full)))


