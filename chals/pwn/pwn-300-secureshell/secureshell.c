#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#include "./libsecureshell.h"

//change length with NUM_COMMANDS
const char * commands[] = {
        "/usr/bin/ls",
        "/usr/bin/id",
        "groups",
        "sha1sum secureshell"
};

void print_menu()
{
    printf("1. list commands\n");
    printf("2. run command\n");
    printf("3. update command\n");
    printf("q. quit\n");
    printf("\nEnter your choice: \n");

    return;
}

void decrypt_and_print_commands(encrypted_command c[NUM_COMMANDS], byte *key)
{
	int i,j, ret;
	byte decrypted[MAX_ENCRYPTED_COMMAND_LEN+1] = {0};
	for (i=0;i<NUM_COMMANDS;i++)
	{
		switch (c[i].alg_id)
		{
			case ALGID_ECB:
				if ((c[i].cipherlen % AES_BLOCK_LEN) != 0)
				{
					printf("COMMAND [%d]: Incorrect block length for ECB\n", i);
					break;
				}
				ret = decrypt_ecb(c[i].cipher, c[i].cipherlen, key, decrypted);
				// printf("DEBUG: ECB[%d] ret = %d\n", i, ret);
				if (SUCCESS != ret)
				{
					printf("COMMAND [%d]: Decryption failed\n", i);
					break;
				}
				printf("COMMAND [%d]: ",i);
				for(j=0;j<c[i].cipherlen; j++)
				{
					printf("%c", decrypted[j]);
				} 
#ifdef DEBUG
				printf(" : alg_id = [%d]", c[i].alg_id);
#endif
				printf("\n");
				break;

			case ALGID_CTR:
				ret = decrypt_ctr(c[i].cipher, c[i].cipherlen, key, c[i].nonce, decrypted);
				// printf("DEBUG: GCM[%d] ret = %d\n", i, ret);
				if (SUCCESS != ret)
				{
					printf("COMMAND [%d]: Decryption failed\n", i);
					break;
				}
				printf("COMMAND [%d]: ",i);
				for(j=0;j<c[i].cipherlen; j++)
				{
					printf("%c", decrypted[j]);
				} 
#ifdef DEBUG
				printf(" : alg_id = [%d]", c[i].alg_id);
#endif
				printf("\n");
				break;


			case ALGID_GCM:
				ret = decrypt_gcm(c[i].cipher, c[i].cipherlen, key, c[i].nonce, decrypted, c[i].tag);
				// printf("DEBUG: GCM[%d] ret = %d\n", i, ret);
				if (SUCCESS != ret)
				{
					printf("COMMAND [%d]: Validation failed\n", i);
					break;
				}
				printf("COMMAND [%d]: ",i);
				for(j=0;j<c[i].cipherlen; j++)
				{
					printf("%c", decrypted[j]);
				} 
#ifdef DEBUG
				printf(" : alg_id = [%d]", c[i].alg_id);
#endif
				
				printf("\n");

				break;
		}
	}
	return;

}

//This should only enter if alg_id == ALGID_GCM
int decrypt_and_run(encrypted_command c, byte *key)
{
	int j, ret;
	byte decrypted[MAX_ENCRYPTED_COMMAND_LEN+1] = {0};
	ret = decrypt_gcm(c.cipher, c.cipherlen, key, c.nonce, decrypted, c.tag);

	if (SUCCESS != ret)
	{
		printf("RUNNING COMMAND: Validation failed\n");
		return ret;
	}

	printf("RUNNING COMMAND : ");
	for(j=0;j<c.cipherlen; j++)
	{
		printf("%c", decrypted[j]);
	} 
	printf("\n");

	system( (const char *) decrypted);
	printf("FINISHED RUNNING\n");

	return ret;

}

int update_command(encrypted_command *c)
{
	
	char * nonce_hex_str = NULL;
	char * cipher_hex_str = NULL;
	char * tag_hex_str = NULL;

	byte tmp_nonce[ENC_GCM_NONCE_LEN +1] ={0};
	byte tmp_cipher[MAX_ENCRYPTED_COMMAND_LEN +1]={0};
	byte tmp_tag[ENC_GCM_TAG_LEN +1]={0};

	int nonce_hex_str_len, cipher_hex_str_len, tag_hex_str_len;

	//update nonce
	printf("Enter nonce (hex): ");
        fflush(stdout);
	scanf("%ms", &nonce_hex_str);
	nonce_hex_str_len = strlen(nonce_hex_str);
	if (	(nonce_hex_str_len != ENC_GCM_NONCE_LEN*2) ||
		!is_hex_string(nonce_hex_str) ||
            	!is_odd_length(nonce_hex_str) )
	{
		free(nonce_hex_str);
		printf("Error in update nonce - invalid input\n");
		return ERROR_UPDATE_NONCE;
	}

	//update cipher
	printf("Enter cipher (hex): ");
        fflush(stdout);
	scanf("%ms", &cipher_hex_str);
	cipher_hex_str_len = strlen(cipher_hex_str);
#ifdef DEBUG
	printf("DEBUG: cipher length (unhex) is: %d \n", cipher_hex_str_len/2);
#endif
	if (	cipher_hex_str_len > MAX_ENCRYPTED_COMMAND_LEN*2 ||
		!is_hex_string(cipher_hex_str) ||
            	!is_odd_length(cipher_hex_str) )
	{
		free(nonce_hex_str);
		free(cipher_hex_str);
		printf("Error in update cipher - invalid input\n");
		return ERROR_UPDATE_CIPHER;
	}
	
	//update tag
	printf("Enter tag (hex): ");
        fflush(stdout);
	scanf("%ms", &tag_hex_str);
	tag_hex_str_len = strlen(tag_hex_str);
	if (	(tag_hex_str_len != ENC_GCM_TAG_LEN*2) ||
		!is_hex_string(tag_hex_str) ||
            	!is_odd_length(tag_hex_str) )
	{
		free(nonce_hex_str);
		free(cipher_hex_str);
		free(tag_hex_str);
		printf("Error in update tag - invalid input\n");
		return ERROR_UPDATE_TAG;
	}

	//All hex inputs, lengths OK
	convert_hex_to_bytes(nonce_hex_str, tmp_nonce);
	convert_hex_to_bytes(cipher_hex_str, tmp_cipher);
	convert_hex_to_bytes(tag_hex_str, tmp_tag);
#ifdef DEBUG
	printf("updating with alg_id: %d\n", c->alg_id);
	int i;

	printf("DEBUG: ENC = ");
	for (i=0; i<MAX_ENCRYPTED_COMMAND_LEN+1; i++)
	{
		printf("%02x", tmp_cipher[i]);
	} printf("\n");
#endif

	memcpy(c->nonce, tmp_nonce, ENC_GCM_NONCE_LEN);
	memset(c->cipher, 0, MAX_ENCRYPTED_COMMAND_LEN);
	strcpy(c->cipher, tmp_cipher);
	c->cipherlen = cipher_hex_str_len/2;
	memcpy(c->tag, tmp_tag, ENC_GCM_TAG_LEN);
	
	free(nonce_hex_str);
	free(cipher_hex_str);
	free(tag_hex_str);

	return SUCCESS;

}


#ifdef DEBUG
void debug_print(encrypted_command c[NUM_COMMANDS])
{
	int i,j;

	for(i=0; i<NUM_COMMANDS; i++)
	{
		printf("nonce: ");
		for(j=0;j<ENC_GCM_NONCE_LEN;j++)
		{
			printf("%02x", c[i].nonce[j]);
		}printf("\n");
		
		printf("cipher: ");
		for(j=0;j<MAX_ENCRYPTED_COMMAND_LEN;j++)
		{
			printf("%02x", c[i].cipher[j]);
		}printf("\n");

		printf("tag: ");
		for(j=0;j<ENC_GCM_TAG_LEN;j++)
		{
			printf("%02x", c[i].tag[j]);
		}printf("\n");

		printf("alg_id: %d\n", c[i].alg_id );
		printf("cipherlen: %d\n", c[i].cipherlen );

	}

	return;
}
#endif

/***********************************************************
 *
 *  Start of main function
 *
 * *********************************************************/

int main(int argc, char *argv[])
{

	int ret, i ;
	byte key[AES_KEY_LEN] = {0};
	char *c = NULL, s;
	char *index_str = NULL;
	unsigned long int index = -1;

	//Generate random key and iv for session
	ret = RAND_bytes(key,AES_KEY_LEN);
	if (1!=ret)
	{
		printf("Error generating random bytes (key)\n");
		return ERROR_RAND;
	}

	//initialise the encrypted/validated commands
	encrypted_command enc_cmds[NUM_COMMANDS]={0};
	byte nonce[ENC_GCM_NONCE_LEN];
	byte cipher[MAX_ENCRYPTED_COMMAND_LEN];
	byte tag[ENC_GCM_TAG_LEN];
	int len = 0;

	for (i=0; i<NUM_COMMANDS; i++)
	{
		ret = RAND_bytes(nonce,ENC_GCM_NONCE_LEN);
		if (1!=ret)
		{
			printf("Error generating random bytes (nonce)\n");
			return ERROR_RAND;
		}

		len = strlen(commands[i]);
		ret = encrypt_gcm(
			 	commands[i], 
				len, 
				key, 
				nonce, 
				cipher,
				tag);

		if (SUCCESS != ret)
		{
			printf("Error encrypting commands: %d\n", ret);
			return ret;
		}
		
		memcpy(enc_cmds[i].nonce, nonce, ENC_GCM_NONCE_LEN);
		memcpy(enc_cmds[i].cipher, cipher, len);
		memcpy(enc_cmds[i].tag, tag, ENC_GCM_TAG_LEN);
		enc_cmds[i].alg_id = ALGID_GCM & 0xff;
		enc_cmds[i].cipherlen = len;
	}

	while(TRUE)
	{
		#ifdef DEBUG
		printf("//////// DEBUG ////////\n");
		printf("key: ");
		for(i=0; i<AES_KEY_LEN; i++)
		{
			printf("%02x", key[i]);
		} printf("\n");

		//corruption
		//enc_cmds[1].tag[0] = 0;
		//enc_cmds[2].alg_id = ALGID_CTR;

		debug_print(enc_cmds);			
		printf("//////// DEBUG ////////\n\n");
		#endif

		print_menu();
                fflush(stdout);
                scanf("%ms", &c);
                s = c[0];
		free(c); c = NULL;
		
		switch(s)
		{
			case '1':
				decrypt_and_print_commands(enc_cmds, key);
				break;

			case '2':
				printf("Which command would you like to run? \n");
				fflush(stdout);
				scanf("%ms", &index_str);

				errno = 0;
				index = strtoul(index_str, NULL, 10);
				free(index_str); index_str = NULL;

				if ( 0!= errno)
				{
					perror("strtoul");
					printf("invalid command index\n");
					break;
				}
				if ((index < 0) || (index >= NUM_COMMANDS))
				{
					printf("invalid command index\n");
					break;
		
				}

				if (enc_cmds[index].alg_id != ALGID_GCM)
				{
					printf("invalid alg_id, can't validate\n");
					break;
				}

				decrypt_and_run(enc_cmds[index], key);

				break;

			case '3':
				printf("Which command would you like to update? \n");
				fflush(stdout);
				scanf("%ms", &index_str);

				errno = 0;
				index = strtoul(index_str, NULL, 10);
				free(index_str); index_str = NULL;
				
				if ( 0!= errno)
				{
					perror("strtoul");
					printf("invalid command index\n");
					break;
				}
				if ((index < 0) || (index >= NUM_COMMANDS))
				{
					printf("invalid command index\n");
					break;
		
				}
				
				ret = update_command(&(enc_cmds[index]));
				if (SUCCESS != ret)
				{
					printf("Error updating command\n");
					break;
				}

				break;

			case 'q':
				printf("Option q\n");
				return SUCCESS;

		} //end switch
		
	} //end while


    	return SUCCESS;
}
