typedef unsigned char byte;


#define TRUE 1
#define FALSE 0
#define SUCCESS 0

#define ENC_GCM_TAG_LEN 16
#define ENC_GCM_NONCE_LEN 12
#define ENC_CTR_NONCE_LEN 16
#define AES_KEY_LEN 16
#define AES_BLOCK_LEN_HEX 16*2
#define AES_BLOCK_LEN 16
#define MAX_ENCRYPTED_COMMAND_LEN 100
#define NUM_COMMANDS 4

#define ALGID_CTR 0
#define ALGID_GCM 1
#define ALGID_ECB 2

#define ERROR_ENC_GCM_UPDATE -3
#define ERROR_ENC_GCM_FINAL -4
#define ERROR_DEC_GCM_UPDATE -5
#define ERROR_DEC_GCM_FINAL -6
#define ERROR_ENC_CTR_UPDATE -7
#define ERROR_ENC_CTR_FINAL -8
#define ERROR_DEC_CTR_UPDATE -9
#define ERROR_DEC_CTR_FINAL -10
#define ERROR_RAND -11
#define ERROR_UPDATE_NONCE -12
#define ERROR_UPDATE_CIPHER -13
#define ERROR_UPDATE_TAG -14
#define ERROR_ENC_ECB_UPDATE -15
#define ERROR_ENC_ECB_FINAL -16
#define ERROR_DEC_ECB_UPDATE -17
#define ERROR_DEC_ECB_FINAL -18


typedef struct {
	unsigned int cipherlen;
        byte nonce[ENC_GCM_NONCE_LEN];
        byte cipher[MAX_ENCRYPTED_COMMAND_LEN];
        byte alg_id;
        byte tag[ENC_GCM_TAG_LEN];
} encrypted_command;



unsigned int is_hex_string(unsigned char * s);

unsigned int is_odd_length(unsigned char *s);

unsigned int is_multiple_of_blocklen(unsigned char *s);

void convert_hex_to_bytes(unsigned char *s, unsigned char *output);

int encrypt_gcm(byte *plain, unsigned int plainlen, byte *key, byte *nonce, byte *cipher, byte *tag);

int decrypt_gcm(byte *cipher, unsigned int cipherlen, byte *key, byte *nonce, byte *decrypted, byte *tag);

int encrypt_ctr(byte *plain, unsigned int plainlen, byte *key, byte *nonce, byte *cipher);

int decrypt_ctr(byte *cipher, unsigned int cipherlen, byte *key, byte *nonce, byte *decrypted);

int encrypt_ecb(byte *plain, unsigned int plainlen, byte *key, byte *cipher);

int decrypt_ecb(byte *cipher, unsigned int cipherlen, byte *key, byte *decrypted);

