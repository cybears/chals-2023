#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sendfile.h>

#define MAX_SIZE 15

#define xy_move(x, y) (((y) << 4) | (x))
#define move_y(move) (((move) >> 4) & 0xf)
#define move_x(move) ((move) & 0xf)
#define brd(game, x, y) ((game)->board[(x) + (game->size * (y))])

// NOTE - Hard mode requires a libc with a one-gadget at an address that does not end in 0xe0-0xff
// Some manipulation of the final registers may also be necessary. R12, R15 and RSI were all null but RDX was not.
// #define HARD_MODE

typedef struct
{
    uint8_t size;
    uint8_t board[MAX_SIZE * MAX_SIZE];
    uint8_t moves[MAX_SIZE * MAX_SIZE];
    uint8_t move;
} game_data;

void print_moves(game_data * game)
{
    printf("\n|");
#ifdef HARD_MODE
    for(uint8_t i = 0; i <= game->move; i++)
#else
    for(uint8_t i = 0; i < game->move; i++)
#endif
    {
        uint8_t x = move_x(game->moves[i]);
        uint8_t y = move_y(game->moves[i]);

        printf("%c%d|", 'A'+y, x+1);
    }
    printf("\n");
}

int check_win(game_data* game)
{
    uint8_t winner;
    uint8_t x;
    uint8_t y;

    x = move_x(game->moves[game->move - 1]);
    y = move_y(game->moves[game->move - 1]);

    // Check row
    winner = 3;
    for (int n = 0; n < game->size; n++)
    {
        winner &= brd(game, n, y);
    }
    if (winner) return winner;

    // Check column
    winner = 3;
    for (int n = 0; n < game->size; n++)
    {
        winner &= brd(game, x, n);
    }
    if (winner) return winner;

    if (x == y)
    {
        // Check diagonal
        winner = 3;
        for (int n = 0; n < game->size; n++)
        {
            winner &= brd(game, n, n);
        }
        if (winner) return winner;
    }

    return 0;
}

int get_move(game_data * game, uint8_t player)
{
    char y_move;
    uint8_t x = 0;
    uint8_t y = 0;

    while (1)
    {
        printf("\nPlayer %d\n", player);
        printf("Enter your move: ");
        if(EOF == scanf(" %c", &y_move)) exit(-1);

        y_move = (char)toupper(y_move);

        if(y_move == '*')
        {
            print_moves(game);
            continue;
        }

#ifdef HARD_MODE
        if(y_move == '!')
        {
            return -1;
        }
#endif

        if(EOF == scanf("%hhu", &x)) exit(-1);
        // Convert letter to integer
        y = y_move - 'A';

        // Adjust for 0-index
        x = x - 1;

        if (x > game->size || y > game->size)
        {
            printf("\nIllegal move!\n");
            continue;
        }

        if (brd(game, x, y))
        {
            printf("\nThat space is already occupied!\n");
            continue;
        }

        // Update board
        brd(game, x, y) = player;

        // Update moves list
        game->moves[game->move] = xy_move(x, y);
        game->move += 1;

        return 0;
    }
}

void print_border(uint8_t size)
{
    printf("  ");
    for (int x = 0; x < size; x++)
    {
        printf("---");
    }
    printf("-\n");
}

void print_board(game_data * game)
{
    // Print column indices
    printf("  ");
    for (int x = 1; x <= game->size; x++)
    {
        printf(" %2d", x);
    }
    printf("\n");

    // Top Border
    print_border(game->size);

    for (int y = 0; y < game->size; y++) {
        // Print row index
        printf("%c |", 'A' + y);
        for (int x = 0; x < game->size; x++) {
            char disp;

            switch(brd(game, x, y))
            {
                case 1:
                    disp = 'X';
                    break;

                case 2:
                    disp = 'O';
                    break;

                default:
                    disp = ' ';
                    break;
            }

            printf(" %c|", disp);
        }
        printf("\n");
        print_border(game->size);
    }
}

# ifdef HARD_MODE
__attribute__((__always_inline__))
#endif
void play_game()
{
    int player, winner;
    game_data game;

    memset(&game, 0, sizeof(game));

    printf("GREETINGS PROFESSOR\nSHALL WE PLAY A GAME?\n\n");
    printf("Board Size (3-15): ");

    if(EOF == scanf(" %hhu", &game.size)) exit(-1);

    printf("\n"
           "Enter a move as a coordinate pair (e.g. 'A1')\n"
           " *) Previous Moves\n"
#ifdef HARD_MODE
           " !) Quit\n"
#endif
           "\n"
   );


    if ((game.size > (MAX_SIZE + 1)) || (game.size < 3))
    {
        printf("Invalid board size!\n");
        return;
    }

    print_board(&game);

    winner = 0;
    player = 0;

    while ((winner == 0) && (game.move < (game.size * game.size))){
        if(get_move(&game, player+1) < 0) return;
        print_board(&game);
        player ^= 1;

        winner = check_win(&game);
    }

    if (winner)
    {
        printf("Player %d wins!\n", winner);
    }
    else
    {
        printf("It was a Draw!\n");
    }
}


int main(int argc, char *argv[])
{
    alarm(120);

    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);

    play_game();

    return 0;
}

#ifndef HARD_MODE
// NOTE - The address of win in the binary cannot end with 0xE0-0xFF.
void win()
{
    int fd;

    fd = open("flag.txt", O_RDONLY);
    sendfile(STDOUT_FILENO, fd, 0, 1024);
    close(fd);

    printf("HOW ABOUT A NICE GAME OF CHESS?\n");

    exit(0);
}
#endif
