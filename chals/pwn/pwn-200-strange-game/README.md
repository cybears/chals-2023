# A Strange Game

* _author_: humphrey_c_bear
* _title_: A Strage Game
* _points_: 200
* _tags_: pwn

## Challenge Text
* See MANIFEST.yml

## Handouts
* `strange_game`: compiled executable

## Notes

### Build the binary in the build container
* `docker run -v $(pwd):/build -it --rm --workdir /build registry.gitlab.com/cybears/base-images/builder--build-tools make`

### Build the server container
* `docker build -t strange-game -f Dockerfile.nsjail .`

### Run the server
* `docker run -it --rm -p 2323:2323 --privileged strange-game`


### Build the test container
* `docker build -t strange-game-test -f Dockerfile.test .`

### Run the test agaisnt the server
* `docker run --network=host -it --rm strange-game-test python3 solve.py HOST=127.0.0.1 PORT=2323`
