#!/usr/bin/env bash
timeout 420 cpulimit -f -c 1 -l 30 -- qemu-system-i386 \
		-nographic \
		-m 4M \
    -drive file=/disk.img,index=0,if=floppy,format=raw,file.locking=off \
    -snapshot \
    -monitor /dev/null