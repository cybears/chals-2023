#!/bin/bash
gdb --nh -ix gdb_init_real_mode.txt \
        -ex 'add-symbol-file ./build/program/main.bin 0x4000' \
        -ex 'target remote localhost:1234' \
        -ex 'break *0x4000' \
        -ex 'break *0x750' \
        -ex 'continue'
