#!/bin/bash
# Execute from the project root directory:
# ./src/scripts/build_local.sh

# Build the builder
docker build -t bfbl-builder:latest -f ./Dockerfile.build .

# Build the images
docker run -ti -v $(pwd):/home bfbl-builder:latest make

# Build the containing image
docker build -t bfbl:latest -f ./Dockerfile.qemu .

docker build -t bfbl-test:latest -f ./Dockerfile.test .
