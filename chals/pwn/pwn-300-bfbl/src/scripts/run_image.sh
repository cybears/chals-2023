#!/bin/bash
qemu-system-i386 \
		-nographic \
		-m 4M \
		-drive file=./disk.img,index=0,if=floppy,format=raw,file.locking=off \
		-snapshot
		# -monitor /dev/null