#include "config.h"
#include "term.h"
#include "include/stdint.h"

unsigned char terminal_row = 0;
unsigned char terminal_col = 0;

void set_video_mode(mode)
        unsigned short mode;
{
    // Setting video mode to 0 clears the screen
#asm
    mov ah, #0x0
    mov bx, sp
    mov al, [bx + 0x2] //; (arg1) mode
    int 0x10
#endasm
}

void terminal_check_wrap_around() {
    // Go to the next row if we're past the end of one
    if (terminal_col >= VGA_WIDTH) {
        terminal_row += 1;
        terminal_col = 0;
    }
    // Wrap around to the top
    if (terminal_row >= VGA_HEIGHT) {
        terminal_col = 0;
        terminal_row = 0;
    }
}

void disable_cursor(){
#asm
    mov ah, #0x1
    mov ch, #0x2d
    mov cl, #0x0e
//    mov cx, 0x2d0e
    int 0x10
#endasm
}

void enable_cursor(){
#asm
    mov ah, #0x1
    mov ch, #0xd
    mov cl, #0x0e
//    mov cx, 0x2d0e
    int 0x10
#endasm
}


void init_term(){
    set_video_mode(VIDEO_MODE);
    disable_cursor();
}

void set_cursor(col, row)
    unsigned char col;
    unsigned char row;
{
#asm
    mov ah, #0x02
    mov bx, sp
    mov dh, [bx + 4]
    mov dl, [bx + 2]
    xor bx, bx
    int 0x10
#endasm
}

void putchar(c)
        char c;
{
#asm
    mov ah, #0x0e
    mov bx, sp
    mov al, [bx + 2]
    xor bx, bx
    int 0x10
#endasm
}

void term_putchar(col, row, c)
        unsigned char col;
        unsigned char row;
        char c;
{
    set_cursor(col, row);
    putchar(c);
}

void terminal_backspace() {
    if (terminal_row == 0 && terminal_col == 0) {
        return;
    }

    if (terminal_col == 0) {
        terminal_row -= 1;
        terminal_col = VGA_WIDTH;
        term_putchar(terminal_col, terminal_row, ' ');
    }

    terminal_col -= 1;
    term_putchar(terminal_col, terminal_row, ' ');
}

void terminal_newline() {
    // Zero the remainder of the row and progress the column
    int i = terminal_col;
    for (i = terminal_col; i < VGA_WIDTH; i++) {
        term_putchar(i, terminal_row, ' ');
    }
    terminal_row += 1;
    terminal_col = 0;
}

void term_write(c)
        char c;
{
    if (c == 0x08 || c == 0x7F) {
        terminal_backspace();
        return;
    }

    if (c == '\n' || c == '\r') {
        terminal_newline();
        terminal_check_wrap_around();
        return;
    }

    term_putchar(terminal_col, terminal_row, c);
    terminal_col += 1;
    terminal_check_wrap_around();
}

void term_clear() {
    int y = 0;
    int x = 0;
    terminal_row = 0;
    terminal_col = 0;

    // Clear the screen
    for (y = 0; y < VGA_HEIGHT; y++) {
        for (x = 0; x < VGA_WIDTH; x++) {
            term_putchar(x, y, ' ');
        }
    }
}

void print(str)
        char* str;
{
    while (*str != '\0') {
        term_write(*str);
        str++;
    }
}

void printn(str, len)
        char* str;
        size_t len;
{
    int i = 0;
    while (i < len) {
        term_write(*str);
        str++;
        i++;
    }
}

char get_key(){
#asm
    mov ah, #0x0
    int 0x16
#endasm
}

typedef enum {
    PRINTF_CHARACTER,
    PRINTF_GET_TOKEN,
    PRINTF_PRINT_UINT16,
    PRINTF_PRINT_HEX,
    PRINTF_PRINT_STR
} printfState;

// printf() feature support:
// - %u (unsigned int)
// - %d (signed int)
// - %x (unsigned int as hex)
// - %s (null-terminated string)
// Taken from https://gist.github.com/ImpulseAdventure/374192f340dd7d432c40b7a5602b5a01
// with additions for hex printing / minor tweaks
void printf(ptr_format_string, a1, a2, a3, a4, a5, a6)
    char *ptr_format_string;
    void *a1;
    void *a2;
    void *a3;
    void *a4;
    void *a5;
    void *a6;
{
    void *ptr_curr_arg = NULL;
    int curr_arg = 1;
    char *ptr_str = NULL;
    char hex_val = 0;
    char out_val = 0;
    unsigned max_divisor = 10000;
    unsigned num_remainder = 0;
    int num_start = 0;
    int num_negative = 0;
    int num_int = 0;
    int i = 0;
    unsigned int num_val_digit = 0;
    unsigned int divisor = 1;
    unsigned int format_index = 0;
    char char_format = 0;
    char char_out = 0;
    int hex_str_index = 0;
    char hex_str[MAX_HEX_STRING_LEN];
    printfState formatter_state;

    formatter_state = PRINTF_CHARACTER;
    char_format = ptr_format_string[format_index];

    while (char_format != 0) {

        if (formatter_state == PRINTF_CHARACTER){
            if (char_format == '%') {
                formatter_state = PRINTF_GET_TOKEN;

                // get the arg for this token
                // this is a hack for K&R pre-ANSI seemingly not
                // supporting vargs which is kinda annoying!
                switch (curr_arg){
                    case 1:
                        ptr_curr_arg = a1;
                        break;
                    case 2:
                        ptr_curr_arg = a2;
                        break;
                    case 3:
                        ptr_curr_arg = a3;
                        break;
                    case 4:
                        ptr_curr_arg = a4;
                        break;
                    case 5:
                        ptr_curr_arg = a5;
                        break;
                    case 6:
                        ptr_curr_arg = a6;
                        break;
                    default:
                        ptr_curr_arg = NULL;
                        break;
                }
                curr_arg++;

            } else {
                if (char_format == '\n'){
                    term_write('\r');
                }
                term_write(char_format);
            }
            format_index++;

        } else if (formatter_state == PRINTF_GET_TOKEN) {

            if (char_format == 'd') {
                // SIGNED INTEGER SETUP
                formatter_state = PRINTF_PRINT_UINT16;

                num_int = (int) ptr_curr_arg;
                if (num_int < 0) {
                    num_negative = true;
                    num_remainder = -num_int;
                } else {
                    num_negative = false;
                    num_remainder = num_int;
                }
                num_start = false;
                divisor = max_divisor;

            } else if (char_format == 'u') {

                // UNSIGNED INTEGER SETUP
                formatter_state = PRINTF_PRINT_UINT16;

                num_remainder = (unsigned int) ptr_curr_arg;
                num_negative = false;
                num_start = false;
                divisor = max_divisor;

            } else if (char_format == 'x') {
                // HEX SETUP
                formatter_state = PRINTF_PRINT_HEX;

                num_remainder = ptr_curr_arg;
                num_negative = false;
                num_start = false;
                divisor = 16;

            } else if (char_format == 's') {
                // STRING SETUP
                formatter_state = PRINTF_PRINT_STR;
                ptr_str = (char *) ptr_curr_arg;
            } else {
                // ERROR
                formatter_state = PRINTF_CHARACTER;

            }

            format_index++; // Advance formatter

        } else if (formatter_state == PRINTF_PRINT_STR) {
            print(ptr_str);
            formatter_state = PRINTF_CHARACTER;
        } else if (formatter_state == PRINTF_PRINT_UINT16) {

            // Handle the negation flag if required
            if (num_negative) {
                char_out = '-';
                term_write(char_out);
                num_negative = false;  // Clear the negation flag
            }

            // We remain in this state until we have consumed all of the digits
            // in the original number (starting with the most significant).
            // Each time we process a digit, the parser doesn't advance its input.
            if (num_remainder < divisor) {
                if (num_start) {
                    char_out = '0';
                    term_write(char_out);
                } else {
                    // We haven't started outputting a number yet
                    // Check for special case of zero
                    if (num_remainder == 0) {
                        char_out = '0';
                        term_write(char_out);
                        // Now fall through to done state
                        divisor = 1;
                    }
                }
            } else {
                num_start = true;
                num_val_digit = unsigned_div(num_remainder, divisor);
                char_out = num_val_digit + '0';
                num_remainder = num_remainder - (divisor * num_val_digit);
                term_write(char_out);
            }

            // Detect end of digit decode (ie. 1's)
            if (divisor == 1) {
                // Done
                formatter_state = PRINTF_CHARACTER;
            } else {
                // Shift the divisor by an order of magnitude
                divisor = unsigned_div(divisor, 10);
            }
            // Don't advance format string index

        } else if (formatter_state == PRINTF_PRINT_HEX) {
            // Get the hexadecimal value
            hex_val = mod(num_remainder, divisor);
            if (hex_val < 10) {
                hex_val = hex_val + 48;
            } else {
                hex_val = hex_val + 55;
            }

            // Store the string to print in reverse order (max of 8 bytes)
            if (hex_str_index < MAX_HEX_STRING_LEN){
                hex_str[hex_str_index] = hex_val;
                hex_str_index += 1;
            }
            num_remainder = unsigned_div(num_remainder, divisor);

            // Print the resulting string
            if (num_remainder <= 0) {

                // Make sure hex is a "byte" print 00-FF
                if (hex_str_index % 2 != 0){
                    hex_str_index++;
                }

                for (i = hex_str_index - 1; i >= 0; i--){
                    out_val = hex_str[i] == 0 ? '0' : hex_str[i];
                    term_write(out_val);
                    hex_str[i] = 0;
                }
                hex_str_index = 0;
                // We've converted the value to hex, print it out then continue
                formatter_state = PRINTF_CHARACTER;
            }
        }

        // Get the next char if the state is to advance the next character
        if (formatter_state == PRINTF_CHARACTER || formatter_state == PRINTF_GET_TOKEN){
            char_format = ptr_format_string[format_index];
        }
    }

}

int getsn(input_buff, length)
        char* input_buff;
        int length;
{
    int i = 0;
    while (i < length){
        char c = get_key();

        // End of input
        if (c == '\r' || c == '\n'){
            break;
        }

        // Handle backspaces
        if (c == 0x08 || c == 0x7F) {
            // i must be greater than zero
            if (i > 0){
                term_write(c);
                i--;
                input_buff[i] = 0x0;
            }
            continue;
        }

        // Get normal chars
        term_write(c);
        input_buff[i] = c;
        i++;
    }
    input_buff[i] = '\0';
    return i;
}