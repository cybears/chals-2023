#ifndef BOOTABLE_RESUME_STRINGS_H
#define BOOTABLE_RESUME_STRINGS_H

#define VGA_WIDTH 80
#define VGA_HEIGHT 20
#define MAX_HEX_STRING_LEN 4

void set_video_mode(mode);
void putchar(c);
void print(str);
void printn(str, len);
void printf(ptr_format_string, a1, a2, a3, a4, a5, a6);
int getsn(input_buff, length);
void disable_cursor();
void enable_cursor();
void set_cursor(row, col);
void term_write(c);
void term_clear();

#endif
