#include "main.h"
#include "config.h"
#include "lang.h"
#include "term/term.h"
#include "time/time.h"
#include "math/rand.h"
#include "memory/memory.h"
#include "strings/strings.h"
#include "include/stdint.h"
#include "brainfuck/interpreter.h"

// Loading message things
//#define LOAD_MSG_COUNT 4
#define HELP_MSG_COUNT 12
#define PROGRAM_BUFFER_SIZE 512

#asm
    // Reset all the registers to a known state for the program
    cli
    mov ax, #0x0
    mov ds, ax
    mov es, ax
    mov ax, #0x0
    mov ss, ax
    mov sp, #0x4000
    mov bp, sp
    sti

    jmp 0x0000:_main  // Reset the instruction segment
#endasm


char* program_buffer[PROGRAM_BUFFER_SIZE];

char* loading_messages[LOAD_MSG_COUNT] = {
        MSG_LOADING_1,
        MSG_LOADING_2,
        MSG_LOADING_3,
        MSG_LOADING_4,
        MSG_LOADING_5
};

char* help_messages[HELP_MSG_COUNT] = {
        MSG_HELP_1,
        MSG_HELP_2,
        MSG_HELP_3,
        MSG_HELP_4,
        MSG_HELP_5,
        MSG_HELP_6,
        MSG_HELP_7,
        MSG_HELP_8,
        MSG_HELP_9,
        MSG_HELP_10,
        MSG_HELP_11,
        MSG_HELP_12,
};

// This will be pointed to by IVT 0x0
// and will get called when there is a divide by zero
// error.
void div_zero_interrupt(){
    print(MSG_DIV_ZERO_ERROR);
#asm
    // Do the function prologue as usual
    // but with an iret. Ideally this is a pure
    // assembly routine to get rid of the compiled
    // prologue but whatever...
    pop si
    pop di
    pop bp
    iret
#endasm
}

void write_int_to_address(offset, value)
        unsigned int offset;
        void* value;
{
    unsigned int* addresses = 0;
    addresses[offset] = (unsigned int*)value;
}

void setup_interrupts(){
    write_int_to_address(0, (void*)div_zero_interrupt);
    write_int_to_address(1, 0);
}

void intro_messages(){
    int i;
    print(MSG_WELCOME_VERSION);
    sleep(500);
    for (i = 0; i < LOAD_MSG_COUNT; i++){
        print(loading_messages[i]);
        sleep(200);
    }
    print(MSG_WELCOME_FIX);
}

void print_help_messages(){
    int i;
    print(MSG_HELP_BANNER);
    for (i = 0; i < HELP_MSG_COUNT; i++){
        print(help_messages[i]);
        sleep(100);
    }
    print("\n");
}

void main(){
    int test_int = 0;
    int input_len = 0;
    int res = 0;
    char c = 0;

    init_rand();
    setup_interrupts();
    init_interpreter();

    // Init the terminal
    init_term();

    // Print the loading messages
    intro_messages();

    while (1) {
        print("\nINPUT > ");
        input_len = getsn(&program_buffer, PROGRAM_BUFFER_SIZE - 1);

        res = strncmp(program_buffer, "?", 1);
        if (res == 0){
            term_clear();
            print_help_messages();
            continue;
        }

        res = strncmp(program_buffer, "!", 1);
        if (res == 0){
            term_clear();
            print_machine_state();
            continue;
        }

        // Execute the program
        term_clear();
        exec_brainfuck(&program_buffer, input_len);

        // Zero the program input buffer
        memset(&program_buffer, 0, 4096);
    };
}