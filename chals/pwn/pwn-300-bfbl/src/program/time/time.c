//
// Created by luser on 12/10/22.
//

#include "time.h"

void sleep(ms)
        unsigned int ms;
{
#asm
    xor dx, dx         // clear out DX
    xor cx, cx         // clear out CX
    mov bx, sp
    mov ax, [bx + 2]   // get the milliseconds into ax
    mov bx, #0x3e8     // multiply by 1000
    mul bx
    mov cx, dx         // high byte to CX
    mov dx, ax         // low byte to AX
    mov ah, #0x86      // call sleep (int 0x15 / ah 0x86)
    int 0x15
#endasm
}
