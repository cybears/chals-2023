//
// Created by luser on 14/06/23.
//

#ifndef BFBL_LANG_H
#define BFBL_LANG_H

#define PROGRAM_LANG 0

#if PROGRAM_LANG == 0
    // Welcome message
    #define MSG_WELCOME_VERSION "== BtFk v0.0 ==\n\n"
    #define MSG_WELCOME_FIX "* Warning: Very early POC!\n\n"
    // Load messages
    #define MSG_LOADING_1 "* Welcome to BtFk: Keepin' it Real...\n"
    #define MSG_LOADING_2 "* An interactive BF interpreter\n"
    #define MSG_LOADING_3 "* Slightly expanded instruction set!\n"
    #define MSG_LOADING_4 "* TODO: Fix input problems?\n"
    #define MSG_LOADING_5 "* TODO: Confirm Ralph Brown's Interrupt List\n"
    // Divide by zero
    #define MSG_DIV_ZERO_ERROR "Cannot divide by zero!\n\n"
    // Help messages
    #define MSG_HELP_BANNER "\nHelp [HELP]\n\n"
    #define MSG_HELP_1 "> : increment cursor\n"
    #define MSG_HELP_2 "< : decrement cursor\n"
    #define MSG_HELP_3 "+ : increment cell at cursor\n"
    #define MSG_HELP_4 "- : decrement cell at cursor\n"
    #define MSG_HELP_5 ". : output cell value\n"
    #define MSG_HELP_6 ", : input cell value\n"
    #define MSG_HELP_7 "* : multiply current cell with next cell, store in current\n"
    #define MSG_HELP_8 "/ : divide current cell with next cell, store in current\n"
    #define MSG_HELP_9 "= : add current cell with next cell, store in current\n"
    #define MSG_HELP_10 "_ : subtract current cell with next cell, store in current\n"
    #define MSG_HELP_11 "? : HELP\n"
    #define MSG_HELP_12 "! : STATE\n"
    // Interpreter messages
    #define MSG_INTERPRETER_STATE "\nInterpreter state\n\n"
    #define MSG_INTERPRETER_UNBALANCED_BRACKETS "\nUnbalanced brackets. Exiting!\n"
    #define MSG_INTERPRETER_EXEC "\nExecuting BF script:\n"
    #define MSG_INTERPRETER_INPUT "Input: "
    #define MSG_INTERPRETER_EXEC_FINISHED "\nDone!\n"
#else
    // Welcome message
    #define MSG_WELCOME_VERSION "== BootFk 发布版本 v0.1rc ==\n\n"
    #define MSG_WELCOME_FIX "* 修复：除以零内存\n\n"
    // Load messages
    #define MSG_LOADING_1 "* 欢迎来到 BootFk\n"
    #define MSG_LOADING_2 "* 交互式 8086 脑他妈的解释器\n"
    #define MSG_LOADING_3 "* 部分实施的II型扩展BF埃索朗\n"
    #define MSG_LOADING_4 "* 我需要修复中断 鬼神\n"
    #define MSG_LOADING_5 "* 鬼神\n"
    // Divide by zero
    #define MSG_DIV_ZERO_ERROR "不一半零!\n\n"
    // Help messages
    #define MSG_HELP_BANNER "\n帮助 [HELP]\n\n"
    #define MSG_HELP_1 "> : 递增指针\n"
    #define MSG_HELP_2 "< : 递减指针\n"
    #define MSG_HELP_3 "+ : 递增指针处的字节\n"
    #define MSG_HELP_4 "- : 递减指针处的字节\n"
    #define MSG_HELP_5 ". : 输出\n"
    #define MSG_HELP_6 ", : 输入\n"
    #define MSG_HELP_7 "* : 将字节指针与上一个单元格相乘。存储在当前单元格中\n"
    #define MSG_HELP_8 "/ : 将字节指针与上一个单元格分开。存储在当前单元格中\n"
    #define MSG_HELP_9 "= : 添加带有上一个单元格的字节指针。存储在当前单元格中\n"
    #define MSG_HELP_10 "_ : 用上一个单元格减去字节指针。存储在当前单元格中\n"
    #define MSG_HELP_11 "? : HELP\n"
    #define MSG_HELP_12 "! : STATE\n"
    // Interpreter messages
    #define MSG_INTERPRETER_STATE "\n电脑 状况\n\n"
    #define MSG_INTERPRETER_UNBALANCED_BRACKETS "\n检测到不平衡的括号。 退出！\n"
    #define MSG_INTERPRETER_EXEC "\n他妈的与大脑...\n"
    #define MSG_INTERPRETER_INPUT "输入: "
    #define MSG_INTERPRETER_EXEC_FINISHED "\n完毕!\n"
#endif

#endif //BFBL_LANG_H
