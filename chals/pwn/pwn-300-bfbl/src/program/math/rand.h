#ifndef BOOTABLE_RESUME_RAND_H
#define BOOTABLE_RESUME_RAND_H

void init_rand();
unsigned int rand();
unsigned int rand_range(low, high);
#endif
