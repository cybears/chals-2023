#ifndef BOOTABLE_RESUME_MATH_H
#define BOOTABLE_RESUME_MATH_H

unsigned int mod(a, b);
unsigned int unsigned_div(a, b);
unsigned char unsigned_char_div(a, b);

#endif
