#include "math.h"

unsigned int mod(a, b)
        unsigned int a;
        unsigned int b;
{
    // a % b
#asm
    xor ax, ax
    mov bx, sp
    mov ax, [bx + 2] ; a
    mov bx, [bx + 4] ; b
    xor dx, dx
    div bx
    mov ax, dx
#endasm
}

unsigned int unsigned_div(a, b)
    unsigned int a;
    unsigned int b;
{
#asm
    mov bx, sp
    xor ax, ax
    mov ax, [bx + 2] ; a
    mov bx, [bx + 4] ; b
    xor dx, dx
    div bx
#endasm
}

unsigned char unsigned_char_div(a, b)
        unsigned char a;
        unsigned char b;
{
#asm
    mov bx, sp
    xor ax, ax
    mov al, [bx + 2] ; a
    mov bl, [bx + 4] ; b
    mov bh, #0x0 ; null high
    xor dx, dx
    div bl
#endasm
}