#include "rand.h"
#include "math.h"

unsigned int seed = 0;

void init_rand(){
#asm
    mov ah, #0x0
    int 0x1A
    mov _seed, dx
#endasm
}

unsigned int _rand(){
    seed = seed * 25171;
    seed = seed + 13849;
    return seed;
}

unsigned int rand(){
    /** PRNG "good enough" compromise to stop the odd / even
     * behaviour of _rand() otherwise you will never
     * see a odd -> odd or even -> even sequence */
    unsigned int a = _rand();
    unsigned int b = _rand();
    unsigned int threshold = _rand();
    if (threshold < 0x7fff){
        return a;
    }
    return b;
}

unsigned int rand_range(low, high)
    unsigned int low;
    unsigned int high;
{
    // low + (rand() % (high - low));
    unsigned int difference = high - low;   // [bp - 0x8]
    unsigned int rand_val = rand();         // [bp - 0xA]
    unsigned int result = mod(rand_val, difference);
    result = result + low;
//#asm
//    nop
//    nop
//    nop
//    nop
//    nop
//    mov bx, sp
//    mov ax, [bp - 0x6]
////    mov dx, #0x1
////    add ax, #0x1
//    nop
//    nop
//    nop
//    nop
//    nop
////    mov ax, [bx + 4] ; high
////    mov bx, [bx + 2] ; low
////    sub ax, bx
////    mov dx, ax
////
////    call _rand
////    div ax, dx
////    mov ax, dx
////
////    add ax, bx
////    mov _result, ax
//#endasm
////    return low + (rand() % (high - low));
    return result;
}