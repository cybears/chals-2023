//
// Created by luser on 3/04/23.
//

#ifndef BFBL_INTERPRETER_H
#define BFBL_INTERPRETER_H

/** Brainfuck VM settings **/
#define MAX_CELL_ADDRESS 0x3500
#define MIN_CELL_ADDRESS 0x500
#define START_CELL_ADDRESS 0x750
#define INTERPRETER_INIT_BYTE_VAL 0x00

void exec_brainfuck(input_buff, length);
void print_machine_state();
void init_interpreter();

#endif //BFBL_INTERPRETER_H
