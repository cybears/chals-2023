//
// Created by luser on 3/04/23.
//

#include "interpreter.h"
#include "../lang.h"
#include "term/term.h"
#include "math/math.h"

// VM state details
unsigned char* cells = 0;

unsigned int min_cursor_position = MIN_CELL_ADDRESS;
unsigned int max_cursor_position = MAX_CELL_ADDRESS;
unsigned int cursor_position = START_CELL_ADDRESS;

void init_interpreter(){
    // Set the memory range for the tape
    memset(
            (char*)MIN_CELL_ADDRESS,
            INTERPRETER_INIT_BYTE_VAL,
            MAX_CELL_ADDRESS - MIN_CELL_ADDRESS
    );
}

int get_next_cell_position(){
    if (cursor_position == max_cursor_position){
        return 0;
    } else if (cursor_position == 1) {
        return min_cursor_position;
    } else {
        return cursor_position + 1;
    }
}

void increment_cursor(){
    if (cursor_position == max_cursor_position){
        cursor_position = 0;
    } else if (cursor_position == 1) {
        cursor_position = min_cursor_position;
    } else {
        cursor_position += 1;
    }
}

void decrement_cursor(){
    if (cursor_position == min_cursor_position){
        cursor_position = 1;
    } else if (cursor_position == 0) {
        cursor_position = max_cursor_position;
    } else {
        cursor_position -= 1;
    }
}

void print_machine_state(){
    // Print some stuff about the machine
    // including the tape values near the cursor
    // don't bother doing any thresholding on the
    // boundaries and just print whatever.
    int i;
    int offset;

    print(MSG_INTERPRETER_STATE);
    printf("Cursor: %d\n", cursor_position);
    if (cursor_position < 2){
        i = 0;
        offset = 6;
    } else {
        i = cursor_position - 2;
        offset = cursor_position + 3;
    }

    print("Tape:");
    for (; i < offset; i++){
        printf(" %d", cells[i]);
    }
    print("\n");

}

void exec_brainfuck(input_buff, length)
        char* input_buff;
        int length;
{
    int count_open_bracket = 0;
    int count_close_bracket = 0;
    char tmp_result = 0;
    char c = 0;
    int i = 0;
    int j = 0; // Temporary index for interpreter

    // Check that [ and ] are balanced
    for (i = 0; i < length; i++){
        c = input_buff[i];
        switch (c){
            case '[':
                count_open_bracket += 1;
                break;
            case ']':
                count_close_bracket += 1;
                if ((count_open_bracket - count_close_bracket) < 0){
                    print(MSG_INTERPRETER_UNBALANCED_BRACKETS);
                    return;
                }
                break;
            default:
                break;
        }
    }
    if (count_open_bracket != count_close_bracket){
        print(MSG_INTERPRETER_UNBALANCED_BRACKETS);
        return;
    }

    print(MSG_INTERPRETER_EXEC);
    printn(input_buff, length);
    print("\n");
    i = 0; // Reset i for interpreter
    while (i < length){
        c = input_buff[i];
        if (c == '\0'){
            break;
        }

        switch (c) {
            case '>':  // SHIFT TO CELL RIGHT
                increment_cursor();
                i++;
                break;
            case '<':  // SHIFT TO CELL LEFT
                decrement_cursor();
                i++;
                break;
            case '+':  // INCREMENT CELL
                cells[cursor_position] += 1;
                i++;
                break;
            case '-':  // DECREMENT CELL
                cells[cursor_position] -= 1;
                i++;
                break;
            case '.':  // PRINT OUTPUT
                term_write(cells[cursor_position]);
                i++;
                break;
            case ',':
                print(MSG_INTERPRETER_INPUT);
                tmp_result = get_key();
                term_write(tmp_result);
                print("\n");
                cells[cursor_position] = tmp_result;
                i++;
                break;
            case '[':  // JUMP TO THE NEXT INSTRUCTION AFTER ] IF CELL == 0

                // Continue on if cell is non zero
                if (cells[cursor_position] != 0){
                    i++;
                    break;
                }

                // Otherwise iterate forward to the next ]
                j = i;
                while (input_buff[j] != ']' && j < length){
                    j++;
                }
                i = j + 1; // Instruction _after_ ]

                break;
            case ']':  // JUMP TO PREV [ IF CELL != 0

                // Continue on if cell is zero
                if (cells[cursor_position] == 0){
                    i++;
                    break;
                }

                // Otherwise iterate backwards to the previous [
                j = i;
                while (input_buff[j] != '[' && j > 0){
                    j--;
                }
                i = j;

                break;
            case '*':  // MULTIPLY
                tmp_result = cells[cursor_position]
                        * cells[get_next_cell_position()];
                cells[cursor_position] = tmp_result;
                i++;
                break;
            case '/':  // DIVIDE
                tmp_result = unsigned_char_div(
                        cells[cursor_position],
                        cells[get_next_cell_position()]
                );
                cells[cursor_position] = tmp_result;
                i++;
                break;
            case '=':  // ADD
                tmp_result = cells[cursor_position]
                        + cells[get_next_cell_position()];
                cells[cursor_position] = tmp_result;
                i++;
                break;
            case '_':  // SUBTRACT
                tmp_result = cells[cursor_position]
                        - cells[get_next_cell_position()];
                cells[cursor_position] = tmp_result;
                i++;
                break;
            default:
                i++;
                break;
        }
    }
    print("\nDone!\n");
}