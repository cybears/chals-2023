#ifndef BOOTABLE_RESUME_STDINT_H
#define BOOTABLE_RESUME_STDINT_H

#define NULL 0
#define true 1
#define false 0

typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef signed int __int16_t;
typedef unsigned int __uint16_t;
typedef __uint8_t uint8_t;
typedef __uint16_t uint16_t;
typedef __uint16_t size_t;

#endif
