#include "include/stdint.h"
#include "memory.h"

void memcpy(dst, src, size)
    void* dst;
    void* src;
    size_t size;
{
    int i;
    for (i = 0; i < size; i++){
        ((char*) dst)[i] = ((char*) src)[i];
    }
}


void memset(ptr, value, size)
        char* ptr;
        char value;
        int size;
{
    int i;
    for (i = 0; i < size; i++){
        ptr[i] = value;
    }
    return;
}