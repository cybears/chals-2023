#ifndef BOOTABLE_RESUME_MEM_H
#define BOOTABLE_RESUME_MEM_H

void memcpy(dst, src, size);
void memset(ptr, value, size);

#endif
