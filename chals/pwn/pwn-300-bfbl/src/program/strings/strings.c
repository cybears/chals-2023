//
// Created by luser on 3/04/23.
//

#include "strings.h"

int strncmp(buff_a, buff_b, size)
    char* buff_a;
    char* buff_b;
    unsigned int size;
{
    int count = 0;
    while (*buff_a != '\0' && *buff_b != '\0' && count < size){
        if (*buff_a < *buff_b){
            return -1;
        }
        if (*buff_a > *buff_b){
            return 1;
        }
        buff_a++;
        buff_b++;
        count++;
    }
    return 0;
}
