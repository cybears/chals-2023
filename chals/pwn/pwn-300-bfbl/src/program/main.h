#ifndef BOOTABLE_RESUME_BOOT_H
#define BOOTABLE_RESUME_BOOT_H

// Loading message things
#define LOAD_MSG_COUNT 5

// User input defines
#define INPUT_BUFF_SIZE 64

// Command loop state machine values
#define STATE_GET_PASSWORD     0
#define STATE_CHECK_PASSWORD   1
#define STATE_DECRYPT_CONTENT  2
#define STATE_LOAD_CONTENT     3
#define STATE_DISPLAY_CONTENT  4

#endif
