// The bootstrap will relocate itself to a lower memory address
// then it will jump to the relocated area and continue loading
// the program for execution
#include "config.h"

//__asm__("STACK_LOCATION equ #0x7c00");

#asm
    // setup registers
    cli
    mov ax, #0x00
    mov ds, ax
    mov es, ax
    mov ax, #0x00
    mov ss, ax
    mov sp, SP_INIT
    mov bp, sp
    sti

    // relocate bootloader
    cld
    mov cx, #0x100  // 256 words
    mov si, BOOTSTRAP_SRC_ADDR // this location
    mov di, BOOTSTRAP_DST_ADDR // target location
    rep
    movsw

    // Execute the relocated bootloader
    jmp 0x0000:_main
#endasm

void putchar(c)
    char c;
{
#asm
    mov ah, #0x0e
    mov bx, sp
    mov al, [bx + 2]
    xor bx, bx
    int 0x10
#endasm
}

void print(str)
    char* str;
{
    while (*str != '\0') {
        if (*str == '\n'){
            putchar('\r');
        }
        putchar(*str);
        str++;
    }
}

int load_sectors(
        sectors_to_read,
        cylinder_low_8bits,
        cylinder_sector_num,
        head_number,
        buf_out)
    int sectors_to_read;
    int cylinder_low_8bits;
    int cylinder_sector_num;
    int head_number;
    char* buf_out;
{
#asm
        //; Ready the rest of the read
        mov ah, #0x02 //; Read sector command
        mov bx, sp
        mov al, [bx + 0x2] //; (arg1) number of sectors to read
        mov ch, [bx + 0x4] //; (arg2) cylinder low eight bits
        mov cl, [bx + 0x6] //; (arg3) sector number 1-63
        mov dh, [bx + 0x8] //; (arg4) head number
        mov bx, [bx + 0xA] //; (arg5) data buffer to write to
        int 0x13
        //; Set the error out
        mov ax, #0xFFFF
        jc ._load_sectors_error
        mov ax, #0x0
    ._load_sectors_error:
#endasm
}

void main(){

#asm
    jmp print_load
load_str:
    .asciz "Bootstrapping...\n"
print_load:
    push #load_str
    call _print
    add sp, #0x2
#endasm

    unsigned int test = load_sectors(
            SECTORS_TO_LOAD,     // number of sectors to read
            0,     // cylinder low eight bits
            START_SECTOR,     // sector number 1-63
            0,     // head number
            LOAD_ADDRESS // data buffer to write to
    );

    // Failed to load sectors. Print error and loop
    if (test == -1){
    #asm
        jmp print_load_fail
    fail_str:
        .asciz "Loading Failed!\n"
    print_load_fail:
        push #fail_str
        call _print
        add sp, #0x2
    #endasm
        while (1){};
    }
#asm
    jmp program_start
start_str:
    .asciz "Starting program...\n"
program_start:
    push #start_str
    call _print
    add sp, #0x2

    jmp 0x0000:LOAD_ADDRESS  // long jump to 0x7e00
#endasm

}

#asm
    ORG 510
    .word 0xAA55
#endasm