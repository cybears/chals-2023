#ifndef BOOTABLE_RESUME_BOOTSTRAP_H
#define BOOTABLE_RESUME_BOOTSTRAP_H

/** ASM literal register value helpers LABEL(0x0000) -> #0x0000 */
#define HASH #
#define F(X) X
#define LABEL(A) F(HASH)A

/** Relocation settings */
#define BOOTSTRAP_SRC_ADDR LABEL(0x7c00)
#define BOOTSTRAP_DST_ADDR LABEL(0x0500)

/** Register settings */
#define SP_INIT LABEL(0x4000)

/** Program load settings */
#define LOAD_ADDRESS 0x4000
#define SECTORS_TO_LOAD 24
#define START_SECTOR 2

#endif
