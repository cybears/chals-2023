# BfBL

* _author_: Cybears:SquareBear
* _title_: BfBL
* _points_: 300
* _tags_:  pwn

## Attachments
* `output/handout/handout.tar`: disk.img + run.sh script

## Notes - Build

### Local from handout

Unpack and run:
```
tar xf handout.tar
socat -d TCP-LISTEN:3141,reuseaddr,fork EXEC:"timeout 300 ./run.sh"
```
Connect:
```
socat -d -,icanon=0,echo=0 TCP:localhost:3141
```

### Local from docker

Build the containers:
```
./src/scripts/build_local.sh
```
Then run the container:
```
docker run -p 2323:2323 --rm --name testme -ti bfbl
```
Connect:
```
socat -d -,icanon=0,echo=0 TCP:localhost:2323
```

### Docker with Docker solver

Build the containers:
```
./src/scripts/build_local.sh
```
Then run the challenge container:
```
docker run -p 2323:2323 --rm --name tester -ti bfbl
```
Then run the solver container:
```
docker run --rm --name solver -ti bfbl-test python3 solve.py -r 172.17.0.2:2323
```
You may have to check the appropriate target IP address by (after having run the challenge container)
running docker network inspect bridge and identifying the IPv4address for the container named
"tester".
