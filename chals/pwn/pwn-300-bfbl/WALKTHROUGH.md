# pwn-300-bfbl

In this challenge you're presented with a bootable janky brainfuck 
interpreter that runs on bare metal. 

## Steps - Official Walkthrough


<details>
<summary>Spoiler warning</summary>

## Solution

Brainfuck is a turing complete language where you operate on cells on a tape. Broadly speaking the
tape should be infinite, but in this case we have a limited resource.

The `!` command shows you the location on the tape the cursor is at, and the values of the immediate
cell and the two cells on either side of it.

### The trigger bug

The main bug is that the cursor does not wrap around the tape correctly, but instead jumps to 0 and 1
first.

Because the interpreter is running in 8086 real-mode the cursor pointer on the tape is actually pointing
to a position in physical memory (https://wiki.osdev.org/Memory_Map_(x86)). The very bottom of the physical address range in real mode is reserved
for BIOS interrupt vectors. Specifically: the bottom 2-bytes point to the interrupt handler for a div0 
error. Bytes 3 and 4 are the address segment values as real mode has segment:offset, but fortunately we
don't have to deal with this, as one segment can range from 0x0000 - 0xFFFF and all our stuff lives in that
first segment.

There is no protection, no mitigations, no weird stuff in real-mode and you can do whatever you want.

If you overwrite these bottom two bytes with an address of your choosing, when you divide by 0, that will
trigger an interrupt to then jump to wherever that vector points to.

### Shellcode payload

If you look at the disk image provided, you'll notice that the flag is embedded in the last 512-byte
sector on the disk. This isn't loaded into memory anywhere, so we need to work out a way to load it.

It is pretty simple. There is an interrupt for that (http://www.ctyme.com/intr/rb-0607.htm):

The following assembly will read that sector into address 0x500 (the start of the BF tape)
```asm
  mov ah, 0x02  ; Read sector command
  mov al, 0x1   ; number of sectors to read
  mov ch, 0x0   ; cylinder low eight bits
  mov cl, 0x24  ; sector number 1-63
  mov dh, 0x0   ; head number
  mov dl, 0x0   ; disk number (0 = floppy, 80h = disk)
  mov bx, 0x500 ; data buffer to write to
  int 0x13
```

You can then combine this with a simple call to a print function that is inside the system code. 
If you load the first 512 bytes of the image into ghidra and set the language to real-mode you'll eventually
follow the program (dd out each bit) to find it eventually has a loaded program running at 0x4000.

Load that with a load offset of 0x4000 and you'll fairly easily find the address of the print function (0x43df)
as it decompiles relatively smoothly.

```asm
  mov  ax, 0x500 ; pointer to loaded buffer
  push ax
  call 0x43df    ; address of print(char* string)
```

This will the print the value of the flag.

Throw in an `iret` and it should be good (enough!)

If you compile the assembly together with nasm `nasm -f bin shellcode.asm -o shellcode.bin` 
you'll be able to get the bytes out and then create your shellcode.

### Writing the payload

This is the fiddly bit.

The interpreter is garbage. There is a way of inputting characters but it only seems to
accept ASCII range 0-127 correctly. I solved it by using the multiply option if the 
value is greater than 127 like so:

- input the char code for N/2 into the cell
- move right
- input 0x2 into the cell
- move left
- multiply
- add any remainder (+)
- shift right
- clear the 2
- shift left

There are some bad characters like 0xa, 0x7f etc. to account for as well if you payload
has them.

Once you've written the payload and overwritten the IVT0 then you just need to trigger it with 
a `/` brainfuck command where the cell to the right is 0 for a div0 exception interrupt.

Hopefully you'll now have a flag printed!

## References
* This challenge was inspired by building my own dodgy bootloader!
* I used Bruce's C Compiler to save me from most of the assembly I'd previously written.
* Ralph Brown's interrupt list is the place to go for knowing BIOS interrupts (http://ctyme.com/rbrown.htm)
* OSDev wiki is also an excellent resource 
  * https://wiki.osdev.org/Real_Mode
  * https://wiki.osdev.org/Memory_Map_(x86)

</details>


