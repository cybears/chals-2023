; Compile with:
;   nasm -f bin shellcode.asm -o shellcode.bin
; Check with:
; objdump -b binary -m i8086 -D shellcode.bin
BITS 16   ; set the operation mode to 16-bits
push ax
push bx
push cx
push dx

mov ah, 0x02 ; Read sector command
mov al, 0x1 ;                number of sectors to read
mov ch, 0x0 ;                cylinder low eight bits
mov cl, 0x24 ;               sector number 1-63
mov dh, 0x0 ;                head number
mov dl, 0x0 ;                disk number
mov bx, 0x500 ;              data buffer to write to
int 0x13

; print the string
mov ax, 0x500
push ax
call 0x43df
pop ax

pop dx
pop cx
pop bx
pop ax
iret