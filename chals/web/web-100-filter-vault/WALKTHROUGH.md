# web-100-filter-vault

This beginner web challenge starts when a player clicks the 'Clear Filters' button on the CTF challenge page, while having the console open.

The following output appears: 

```sh
-= SECRET VAULT UNLOCK ATTEMPTED!  Result: [         ] =-
```

By playing with the filters, and including/excluding certain categories, the vault unlocks one correct 'bit' at a time, from left to right:

```sh
-= SECRET VAULT UNLOCK ATTEMPTED!  Result: [*        ] =- 
-= SECRET VAULT UNLOCK ATTEMPTED!  Result: [**       ] =-
```

When all 9 challenge filters are correctly set:

```sh
-= SECRET VAULT UNLOCK ATTEMPTED!  Result: [*********] =-
-= UNLOCKED! Flag data sent to localStorage for access =-
```

And in localStorage we can see the entry:

```json
{
    ...
    "secret_flag": "cybears{i_Am_n0t_5uR3_Th1S_!s_wEb}"
    ...
}
```

I, too, am not sure that was a web challenge :D

Nb. Flag is stored repeating-key-xor encrypted; key is the 9bits of correct include/exclude filter status.
