# Filter Vault

* _author_: Cybears:Heef
* _title_: Filter Vault
* _points_: 100
* _tags_:  web, beginner

## Attachments
* None. Content is within the CTF challenge page filter controls.

## Notes
* This beginner web challenge is just aimed at showing newbies two things:
  - that there's a web console in their browser (and that it can react to page stuff); and
  - there is something called localStorage that might contain state for a webpage
* Challenge is also solvable by finding/reversing the javascript but that's much harder

