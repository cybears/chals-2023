from flask import Flask, render_template, request
from selenium import webdriver
from selenium.webdriver.chrome.options import (
    Options,
)
from os import environ
import urllib

app = Flask(__name__)

chrome_options = Options()
chrome_options.add_argument("--headless")  # Run Chrome in headless mode
chrome_options.add_argument("--disable-gpu")  # Disable GPU acceleration
chrome_options.add_argument("--no-sandbox")  # Bypass OS security model
chrome_options.add_argument("--disable-dev-shm-usage")  # Disable /dev/shm usage


@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")

def validate_query_aws(q):
    qq = urllib.parse.unquote(q)
    check = ["2852039166","a9.fe.a9.fe","0251.376.251.376", "10101001111111101010100111111110", "169.254.169.254", "dynamic", "meta-data", "a9fe:a9fe", "iam"]

    for c in check: 
        if qq.find(c) != -1:
            return False

    return True

@app.route("/surf", methods=["GET"])
def surf():
    q = request.args.get("q")
    
    if not (q.startswith("http") or q.startswith("https") or q.startswith("view-source:http")):
        return (
            render_template(
                "error.html",
                err_msg="Unsupported URI",
            ),
            500,
            ) 

    # This is the AWS metadata server
    if validate_query_aws(q) == False: 
       return (
            render_template(
                "error.html",
                err_msg="Error: attacking AWS infra is out of scope for this challenge. Please stop!",
            ),
            500,
            )


    if q is None:
        return "q is None"

    driver = webdriver.Chrome(options=chrome_options)

    try:
        driver.get(q)
        img = driver.get_screenshot_as_base64()
    except Exception:
        return (
            render_template(
                "error.html",
                err_msg="Something went wrong catching the wave.",
            ),
            500,
        )
    else:
        return render_template("surf.html", b64img=img)


@app.route("/admin", methods=["GET", "POST"])
def admin():
    if request.remote_addr != "127.0.0.1":
        return (
            render_template("error.html", err_msg="You aren't allowed here."),
            403,
        )

    if request.method == "GET":
        return render_template("admin.html")
    else:
        username = request.form.get("username")
        password = request.form.get("password")

        if username == "bumblebear" and password == "iheartpandroids":
            return environ.get("FLAG")
        else:
            return (
                render_template(
                    "error.html", err_msg="Incorrect username/password."
                ),
                401,
            )


@app.errorhandler(404)
def err404(e):
    return (
        render_template(
            "error.html", err_msg="I couldn't find what you're looking for."
        ),
        404,
    )


if __name__ == "__main__":
    app.run(debug=True)
