from flask import Flask, render_template

import argparse


app = Flask(__name__)
username = ""
password = ""


@app.route("/", methods=["GET"])
def csrf():
    return render_template("csrf.html", username=username, password=password)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Surf\'s up CSRF server.')
    parser.add_argument('--username', '-u', type=str, required=True, help='Admin username')
    parser.add_argument('--password', '-p', type=str, required=True, help='Admin password')

    args = parser.parse_args()
    username = args.username
    password = args.password

    app.run(host='0.0.0.0', debug=True)
