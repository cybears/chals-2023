# Surfs Up!

* _author_: bad5ect0r
* _title_: Surfs Up!
* _points_: 200
* _tags_:  web

## Attachments
* (none)

## Build and test (Docker)
(Note: all builds are from base directory)

* Build and run challenge web server
 * `docker build -t surfsup .`
 * `docker run -it --rm surfsup`

* Build and run CSRF server as part of solution
 * `docker build -t surfsupcsrf -f Dockerfile.csrf .` 
 * `docker run -it --rm surfsupcsrf`

* Build and run solver
  * `docker build -t surfsupsolver -f Dockerfile.solver .`
  * Spin up both challenge web server and CSRF server - note IP addresses of both. 
  * `docker run -it --rm surfsupsolver python3 solve.py -l http://172.17.0.3:5000 -r http://172.17.0.2:5000`

## Build and test (standalone)
1. Start the CSRF server
        ```
        python3 app.py -u admin -p admin
        ```
2. In another terminal, run the solve script
        ```
        python3 solve.py -l https://blah.ngrok.com -r http://target.local:5000
        ```
