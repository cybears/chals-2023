# Surf's Up Solution

The target is a web application that allows the user to provide any URL. The web application will use a headless Chrome browser to visit the provided URL and attempts to take a screenshot of the page. The screenshot is then presented back to the user.

This is a challenge that walks the player through CSRF and SSRF vulnerability exploitation. 

## Steps

<details>
<summary>Spoiler warning</summary>

1. Attempt to access the /admin page.
	1. ![cannot_access_admin_directly](images/cannot_access_admin_directly.png)
2. Attempt to access the same page through the screenshot feature of the web application. We use 127.0.0.1 here because we want the web application to access it as localhost - which is trusted. Here we have successfully exploited a Server-Side Request Forgery (SSRF) vulnerability.
	1. ![admin_ssrf1](images/admin_ssrf1.png)
	2. ![admin_ssrf2](images/admin_ssrf2.png)
3. To figure out the username and password, we need to find out more about the user agent being used by the web application to visit these sites and render the pages. We can do this using a netcat server and ngrok to temporarily forward the netcat listener to the Internet.
	1. ![user_agent](images/user_agent.png)
4. Since it's just a headless Chrome browser, we can use the trick of prepending `view-source:` to the URL to view the login page's HTML source code via the screenshot utility.
	1. ![view_source_admin](images/view_source_admin.png)
	2. ![view_source_admin2](images/view_source_admin2.png)
		1. The password is exposed in a comment.
		2. The names of the username field is just username, it's safe to assume the password field is just password.
5. Now we can exploit a login CSRF on the headless chrome browser to have it log in as the admin user. For this, we create a basic HTML document containing a self-submitting web form.
```html
<!DOCTYPE html>
<html>
<body onload="document.createElement('form').submit.call(document.getElementById('myForm'))">
	<form id="myForm" name="myForm" action="http://127.0.0.1:5000/admin" method="POST">
		<input type=hidden name="username" id="username" value="admin"/>
		<input type=hidden name="password" id="password" value="admin"/>
		<input type=hidden name="submit" id="submit" value="Continue"/>
	</form>
</body>
</html>
```

6. Using a Python web server and ngrok, we can host this document to the Internet and have the target application access it. This would cause the headless Chrome browser in use to submit the login form using the correct credentials and get us past the login page.
	1. ![pwned](images/pwned.png)

</details>

