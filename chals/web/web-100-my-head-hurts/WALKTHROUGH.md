# web-100-my-head-hurts

This is a beginner web challenge designed to teach players about HTTP headers and more unusual error codes.

This challenge was inspired by this web post: [http://www.wisec.it/sectou.php?id=4698ebdc59d15](http://www.wisec.it/sectou.php?id=4698ebdc59d15)

When viewing in a web browser, you will get the default flag.html page served. If the Accept header contains text/html or */* you will get the flag.html page. If you change this to anythign else, the webserver won't know which file to serve based on the MultiViews config in the Apache config and return a HTTP 406 error.

When returning a 406 error, Apache will list off all the files and their content type, which is to be specified in the Accept: client header.

In order to get to stage one a user will:

```sh
$ curl -vvv 'http://targetip/flag'
*   Trying 127.0.0.1:80...
* Connected to localhost (127.0.0.1) port 80 (#0)
> GET /flag HTTP/1.1
> Host: localhost
> User-Agent: curl/8.0.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< Date: Thu, 21 Sep 2023 13:18:29 GMT
< Server: Apache/2.4.57 (Fedora Linux) OpenSSL/3.0.9
< Content-Location: flag.html
< Vary: negotiate
< TCN: choice
< Last-Modified: Wed, 20 Sep 2023 13:34:26 GMT
< ETag: "7e-605ca6f293d80;605de4d5b1c2a
< Accept-Ranges: bytes
< Content-Length: 126
< Content-Type: text/html; charset=UTF-8
< Content-Language: en
< 
<html>
	<head><title>is this a flag?</title></head>
	<body><center><img width="50%" src="home.gif" /></center></body>
</html>
* Connection #0 to host localhost left intact

$ curl -vvv 'http://localhost/flag' -H "Accept: wat"
*   Trying 127.0.0.1:80...
* Connected to localhost (127.0.0.1) port 80 (#0)
> GET /flag HTTP/1.1
> Host: localhost
> User-Agent: curl/8.0.1
> Accept: wat
> 
< HTTP/1.1 406 Not Acceptable
< Date: Thu, 21 Sep 2023 13:22:49 GMT
< Server: Apache/2.4.57 (Fedora Linux) OpenSSL/3.0.9
< Vary: negotiate,accept-language
< TCN: list, list
< Alternates: {"flag.html" 1 {type text/html}}, {"flag.xyz" 1 {type application/x-flag}}
< Content-Length: 407
< Content-Type: text/html; charset=iso-8859-1
< Content-Language: en
< 
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>406 Not Acceptable</title>
</head><body>
<h1>Not Acceptable</h1>
<p>An appropriate representation of the requested resource could not be found on this server.</p>
Available variants:
<ul>
<li><a href="flag.html">flag.html</a> , type text/html</li>
<li><a href="flag.xyz">flag.xyz</a> , type application/x-flag</li>
</ul>
</body></html>
* Connection #0 to host localhost left intact
```

The user may alternatively run Nikto, a common vulnerability scanning tool:

```sh
$ nikto -host targetip
- ***** RFIURL is not defined in nikto.conf--no RFI tests will run *****
- Nikto v2.1.6
---------------------------------------------------------------------------
+ Target IP:          127.0.0.1
+ Target Hostname:    localhost
+ Target Port:        80
+ Start Time:         2023-09-21 23:19:55 (GMT10)
---------------------------------------------------------------------------
+ Server: Apache/2.4.57 (Fedora Linux) OpenSSL/3.0.9
+ Server leaks inodes via ETags, header found with file /, fields: 0x5 0x6058e7305cf3d 
+ The anti-clickjacking X-Frame-Options header is not present.
+ The X-XSS-Protection header is not defined. This header can hint to the user agent to protect against some forms of XSS
+ The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type
+ Uncommon header 'tcn' found, with contents: list, list
+ Apache mod_negotiation is enabled with MultiViews, which allows attackers to easily brute force file names. See http://www.wisec.it/sectou.php?id=4698ebdc59d15. The following alternatives for 'index' were found: flag.html, flag.xyz
+ Allowed HTTP Methods: GET, POST, OPTIONS, HEAD, TRACE 
+ OSVDB-877: HTTP TRACE method is active, suggesting the host is vulnerable to XST
+ OSVDB-3092: /cgi-bin/test.cgi: This might be interesting...
+ OSVDB-3233: /index.html.en: Apache default foreign language file found. All default files should be removed from the web server as they may give an attacker additional system information.
+ OSVDB-3233: /index.html.fr: Apache default foreign language file found. All default files should be removed from the web server as they may give an attacker additional system information.
+ OSVDB-3268: /icons/: Directory indexing found.
+ OSVDB-3268: /icons/: Directory indexing found.
+ OSVDB-3233: /icons/README: Apache default file found.
+ 5734 requests: 0 error(s) and 14 item(s) reported on remote host
+ End Time:           2023-09-21 23:20:04 (GMT10) (9 seconds)
---------------------------------------------------------------------------
+ 1 host(s) tested
```

Noting the HTTP 406 error returns a file 'flag.xyz' with a content type of application/x-flag, the user re-requests /flag.xyz with a content type of 'application/x-flag'. Doing so produces:

```sh
# curl -vvv 'http://localhost/flag.xyz' -H "Accept: application/x-flag"
*   Trying 127.0.0.1:80...
* Connected to localhost (127.0.0.1) port 80 (#0)
> GET /flag.xyz HTTP/1.1
> Host: localhost
> User-Agent: curl/8.0.1
> Accept: application/x-flag
> 
< HTTP/1.1 200 OK
< Date: Thu, 21 Sep 2023 13:31:34 GMT
< Server: Apache/2.4.57 (Fedora Linux) OpenSSL/3.0.9
< Transfer-Encoding: chunked
< Content-Type: text/html; charset=UTF-8
< Content-Language: en
< 
                                   
<html>
	<head><title>drapeau!</title></head>
	<body><center><img src="not-a-flag.png"></center></body>
</html>
* Connection #0 to host localhost left intact
```

There is a small intuitive leap here to get to the final flag. Noticing the french language in the title and in the embedded image, and that a HTTP requests usually includes an Accept-Language: client header, the user should try including one. This header is also managed by Apache's mod_negotiation.

```sh
$ curl -vvv 'http://localhost/flag.xyz' -H "Accept: application/x-flag" -H "Accept-Language: en"
*   Trying 127.0.0.1:80...
* Connected to localhost (127.0.0.1) port 80 (#0)
> GET /flag.xyz HTTP/1.1
> Host: localhost
> User-Agent: curl/8.0.1
> Accept: application/x-flag
> Accept-Language: en
> 
< HTTP/1.1 200 OK
< Date: Thu, 21 Sep 2023 13:34:17 GMT
< Server: Apache/2.4.57 (Fedora Linux) OpenSSL/3.0.9
< Transfer-Encoding: chunked
< Content-Type: text/html; charset=UTF-8
< Content-Language: en
< 
                                   
<html>
	<head><title>drapeau!</title></head>
	<body><center><img src="not-a-flag.png"></center></body>
</html>
* Connection #0 to host localhost left intact
```

English doesn't change the result, but the clues were in french. So changing the Accept-Language: client header to fr we get the flag.

```sh
$curl -vvv 'http://localhost/flag.xyz' -H "Accept: application/x-flag" -H "Accept-Language: fr"
*   Trying 127.0.0.1:80...
* Connected to localhost (127.0.0.1) port 80 (#0)
> GET /flag.xyz HTTP/1.1
> Host: localhost
> User-Agent: curl/8.0.1
> Accept: application/x-flag
> Accept-Language: fr
> 
< HTTP/1.1 200 OK
< Date: Thu, 21 Sep 2023 13:35:15 GMT
< Server: Apache/2.4.57 (Fedora Linux) OpenSSL/3.0.9
< Transfer-Encoding: chunked
< Content-Type: text/html; charset=UTF-8
< Content-Language: en
< 
<html>
<body>
cybears{ap4ch3_c0nf1g5_hur7_m3}
</body>
</html>
<!-- Padding only
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░░░░░▒▒▒▒▒▒▒▒▒░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░▒▓▒▒▒░░░░░░░▓▓▒▓▓██████████████████▓▓▓▓░░░░░░░░▒▒▓▒░░░░░░░░░░░░░░
░░░░░░░░░░░░░▒▓░░░▒▒▒▒▒▒▒▒░▒██████▓▓▓▓▓▓▓▓▓▓▓▓▓▓█████▒░▒▒▒▒▒▒▒▒░░▒▓▒░░░░░░░░░░░░
░░░░░░░░░░░░▒▓░▒▒▒▒▒▒▒▒▒▒▒▒░▓█████▓▒░░░░░░░░░▒▓█████▓░▒▒▒▒▒▒▒▒▒▒▒▒░▓▒░░░░░░░░░░░
░░░░░░░░░░░░█░░▒▒▒▒▒▒▒▒░░▒▒▒░▒▒▓█████▓▒░░░▒▓█████▓▒▒░▒▒▒░░▒▒▒▒▒▒▒▒▒▒█░░░░░░░░░░░
░░░░░░░░░░░░█▒░▒▒▒▒▒▒▒▒▒▒░░░▒▒▒░░▒▓█████▓█████▓▒▒░▒▒▒░░░▒▒▒▒▒▒▒▒▒▒▒▒█░░░░░░░░░░░
░░░░░░░░░░░░▒▓░▒▒▒▒▒▒▒▒▒░▒▒▒░░░▒▒▒░░▒▓█████▓▒░░▒▒▒░░░▒▒▒░▒▒▒▒▒▒▒▒▒░▓▒░░░░░░░░░░░
░░░░░░░░░░░░░▒▓▒░▒▒▒▒▒▒▒▒░░░▒▒▒▒▒▒▒░▒▒▒▒▓▒▒▒▒░▒▒▒░▒▒▒░░░▒▒▒▒▒▒▒▒░▒▓▒░░░░░░░░░░░░
░░░░░░░░░░░░░░░▒▓▓▒▒▒░░▒▒▒▒▒░░░▒▒▒▒▒░███▓▓██▒░▒▒▒▒░░░▒▒▒▒▒░░▒▒▒▓▓▒░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░▒▓▒░░░░▒▒▒▒▒░░░▒▒░▓██████░▒▒▒░░▒▒▒▒▒░░░░▒▓▒░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░▓▒░▒▒░░░░░░▒▒▒▒▒░▓█████▓░▒▒▒▒▒░░░░░░▒▒░▓▓░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░▒▓░▒▒▒▒░░░░░░░░░░░▒▒▒▒▒░░░░░░░░░░░▒▒▒▒░▓▒░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░▒▓░▒▒▒▒▒▒░░░░░░░▒░░░░░░▒▒░░░░░░░░▒▒▒▒▒░▓▒░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░█░░▒▒▒▒▒▒▒░▒▒▒▒▒▒░░░░░▒▒▒▒▒▒░░▒▒▒▒▒▒▒░█░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░█░░▒▒▒▒▒▒▒░▒▒▒▒▒▒▒▒░▒▒▒▒▒▒▒▒░░▒▒▒▒▒▒▒░█░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░▓▒░▒▒▒▒▒▒▒░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒▒▒▒▒▒░▒▓░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░▓▒░▒▒▒▒▒▒▒░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒▒▒▒▒▒░▒▓░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░▓▓░░▒▒▒▒▒▒░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒▒▒▒▒░░▓▓░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░▒▓▒░░▒▒▒▒░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒▒▒░░▒▓▒░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░▓▒░░░▒░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░▒░░▓▓░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▓▒░░▒▒▒▒░░▒▒▒▒▒▒░▒▒▒▒░░▒▓▒░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒░▒▒▒▒░███████▒░▒▒▒░▒▒░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒░▓████████▒▒▒░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
-->
* Connection #0 to host localhost left intact
```

Note: I had to emulate some of HTTPD's behaviour with a simple php script in order to prevent the HTTP 406 error from returning the file containing the flag. Had I not done this the file containing the flag would have been in the Nikto output and could be directly requested.



