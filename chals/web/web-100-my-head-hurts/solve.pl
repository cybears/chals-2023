#!/usr/bin/perl

$HOST = shift || "localhost";
$PORT = shift || "2323";
$success = 1;

#print "Host: " . $HOST . ":" . $PORT . "\n";

#Correct page displayed for default query
$res = `curl -vvv "http://$HOST:$PORT/flag" 2>/dev/null`;
if ($res =~ /home\.gif/) {
	#print "/flag: ✅\n";
} else {
	print "/flag: ❌\n";
	$success = 0;
}

$res = `curl -vvv "http://$HOST:$PORT/flag.xyz" 2>/dev/null`;
if ($res =~ /home\.gif/) {
        #print "/flag.xyz: ✅\n";
} else {
        print "/flag.xyz: ❌\n";
	$success = 0;
}

#Correct first step of challenge. Find the 406.
$res = `curl -vvv "http://$HOST:$PORT/flag" -H "Accept: asdf/asdf" 2>/dev/null`;
if ($res =~ /406 Not Acceptable/) {
        #print "/flag invalid Accept: ✅\n";
} else {
        print "/flag invalid Accept: ❌\n";
	$success = 0;
}

$res = `curl -vvv "http://$HOST:$PORT/flag.xyz" -H "Accept: asdf/asdf" 2>/dev/null`;
if ($res =~ /406 Not Acceptable/) {
        #print "/flag.xyz invalid Accept: ✅\n";
} else {
        print "/flag.xyz invalid Accept: ❌\n";
	$success = 0;
}

#Correct Accept, Missing Accept-Language: fr, show en version.
$res = `curl -vvv "http://$HOST:$PORT/flag" -H "Accept: application/x-flag" 2>/dev/null`;
if ($res =~ /drapeau/) {
        #print "/flag x-flag: ✅\n";
} else {
        print "/flag x-flag: ❌\n";
	$success = 0;
}

$res = `curl -vvv "http://$HOST:$PORT/flag.xyz" -H "Accept: application/x-flag" 2>/dev/null`;
if ($res =~ /drapeau/) {
        #print "/flag.xyz x-flag: ✅\n";
} else {
        print "/flag.xyz x-flag: ❌\n";
	$success = 0;
}

#Missing Accept: application/x-flag, correct Language, show home page.
$res = `curl -vvv "http://$HOST:$PORT/flag" -H "Accept-Language: fr" 2>/dev/null`;
if ($res =~ /home\.gif/) {
        #print "/flag Accept-Language fr: ✅\n";
} else {
        print "/flag Accept-Language fr: ❌\n";
	$success = 0;
}

$res = `curl -vvv "http://$HOST:$PORT/flag.xyz" -H "Accept-Language: fr" 2>/dev/null`;
if ($res =~ /home\.gif/) {
        #print "/flag.xyz Accept-Language fr: ✅\n";
} else {
        print "/flag.xyz Accept-Language fr: ❌\n";
	$success = 0;
}

#Correct Language and Correct type
$res = `curl -vvv "http://$HOST:$PORT/flag" -H "Accept: application/x-flag" -H "Accept-Language: fr" 2>/dev/null`;
if ($res =~ /ap4ch3_c0nf1g5_hur7_m3/) {
        #print "/flag Correct Answer: ✅\n";
} else {
        print "/flag Correct Answer: ❌\n";
	$success = 0;
}

$res = `curl -vvv "http://$HOST:$PORT/flag.xyz" -H "Accept: application/x-flag" -H "Accept-Language: fr" 2>/dev/null`;
if ($res =~ /ap4ch3_c0nf1g5_hur7_m3/) {
        #print "/flag.xyz Correct Answer: ✅\n";
} else {
        print "/flag.xyz Correct Answer: ❌\n";
	$success = 0;
}

if (!$success) {
	exit 1;
} else {
	print `curl -vvv "http://$HOST:$PORT/flag.xyz" -H "Accept: application/x-flag" -H "Accept-Language: fr" 2>/dev/null | grep cybears`;
	exit 0;
}

