# My Head Hurts

* _author_: Cybears:Mr Bearenstain
* _title_: My Head Hurts
* _points_: 100
* _tags_:  web, beginner

## Attachments
* None. 

## Notes
* Apache HTTPD config challenge. Inspired by http://www.wisec.it/sectou.php?id=4698ebdc59d15

### Build the server container
* `docker build -t my-head-hurts .`

### Run the server
* `docker run -it --rm -p 2323:80 --name my-head-hurts-test my-head-hurts`

### Build the solver container
* `docker build -t my-head-hurts-solver -f Dockerfile.healthcheck .`

### Run the solver
* `docker run --network=host -it --rm my-head-hurts-solver ./solve.pl <serverIP> <serverPort>`

