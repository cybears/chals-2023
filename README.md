# Cybears CTF 2023

This repository contains the CTF challenges developed by the Cybears CTF team
for our 2023 event.

## Repository Layout

As usual, the repository is laid out by challenge category with each challenge
having its own sub-directory containing all assets required to build and test
it. We use GitLab CI to execute the challenge build steps, and make use of core
base container images from the cybears/base-images repository where necessary.

## Adding a Challenge

Take a look at existing challenges in this repository, or ones from the
precursor repositories like cybears/fall-of-cybeartron or cybears/chals-test
for inspiration. Generally, you'll want some or all of the following:

 - some server-side code (e.g. pwn or web challenges)
 - some handout-oriented code (e.g. rev challenges)
 - a build script
 - an automated solver script
 - a `Dockerfile` to build a server container
 - a `Dockerfile.healthcheck` to automate health checks (usually uses solver)
 - a `gitlab-ci.yml` to automate the build and testing in CI

A new challenge can be wired into the GitLab CI pipeline by adding a new child
pipeline job to the `chals/gitlab-ci.yml` file.
